#
# what uses this Makefile?
NAME=atscap
VERSION=1.1
DISTRO=$(NAME)-$(VERSION)
#
# where do you want to install the software?
PREFIX=/usr/local
BIN=$(PREFIX)/bin
MAN1=$(PREFIX)/share/man/man1
MAN5=$(PREFIX)/share/man/man5
DOC=$(PREFIX)/share/doc/$(NAME)
#
# specify directory for for captures, program guides and images
# -E energy save option will use $DTV/ram, don't forget to mount as tmpfs
DTV=/dtv
#
# what is the compiler?
CC=gcc
# what are the default compile flags?
CFLAGS=-Wall -lm
# Extra debug information. You may want to comment this if it runs OK.
CFLAGS += -g -rdynamic -DUSE_GNU_BACKTRACE -DUSE_GNU_BACKTRACE_SCRIPT
# librt (clock_gettime et al)
CFLAGS += -DUSE_LIBRT -lrt
# 
# debug options for CFLAGS. -g should be left on until 1.1 final.
# -g -pg
# -g to track segfaults with gdb
# -pg to generate profiling information
# -g -rdynamic -DUSE_GNU_BACKTRACE to enable backtrace dump data for addr2line
# -g -rdynamic -DUSE_GNU_BACKTRACE_SCRIPT for backtrace .sh calling addr2line
# Backtrace .bt data dump or .sh script will be stored in $DTV directory.
# Use btfd.sh </dtv/infile.bt if USE_GNU_BACKTRACE_SCRIPT not defined.
#
# Audit gcc flags, may generate a few warnings that would alarm the users.
# You can enable these if you are attempting to port to another platform.
#CFLAGS += -Wmissing-declarations -Wmissing-prototypes -Wstrict-prototypes -Wpointer-arith -Wshadow -Wbad-function-cast
# -Wcast-qual
#
# NOTE: -Wcast_qual may generate errors with #define WHO __FUNCTION__ due
# to inconsistency in __FUNCTION__ as char or unsigned char in various GCCs
#
# gcc man page says to use this for multi-threaded, but it breaks asnprintf?
#CFLAGS += -fstack-check
#
# default flags that should work for most cards
DVBFLAGS= -DUSE_DVB
# Comment USE_DYNAMIC for static FIFO, EIT, ETT, frames sequences, 21M.
# USE_DYNAMIC is the new default because it uses about 5M when idle.
DVBFLAGS += -DUSE_DYNAMIC
# Comment this if you don't want it to go to sleep because of driver bugs.
DVBFLAGS += -DUSE_POWERDOWN

# Console colors, comment this for 28kbit modem low bandwidth ssh remote
VFLAGS= -DUSE_ECMA48

# Multicast support, VERY experimental. Be CAREFUL. Know the dangers.
# You don't want to be sending out data your outbound link can't handle.
#UFLAGS= -DUSE_MCAST

# USE_WWW enables HTTP experimental support, May be insecure. Use a firewall.
# Change WWW_DVBS to the number of DVB API supported ATSC cards in this box
# USE_PNG enables display of signal strength as graph in HTTP inteface.
WFLAGS= -DUSE_WWW 
WFLAGS += -DUSE_PNG -lpng
WFLAGS += -DUSE_CSS_SCROLLBARS
# vert scrollbar works but causes EPG render to be VERY CPU intensive/slow

# libmpeg2 and imlib2 flags for xtscut, remote X should not -DUSE_X_EXPOSE
# imlib1 is deprecated with xtscut 1.3.0, but can still be compiled,
MFLAGS1 = -lmpeg2 -lmpeg2convert -DUSE_IMLIB1 -DUSE_EXPOSE
MFLAGS2 = -lmpeg2 -lmpeg2convert -DUSE_IMLIB2 -DUSE_EXPOSE
MFLAGS3 = -lmpeg2 -lmpeg2convert -DUSE_LIBVO -lXv

# Both Imlibs have package configuration scripts. Use them if possible.
# If you don't have these scripts you need to install the -devel packages.
MFLAGS1 += `imlib-config --libs`
MFLAGS2 += `imlib2-config --libs`
# If you still get "imlib-config: command not found" error, try these instead:
#MFLAGS1 += -lImlib
#MFLAGS2 += -lImlib2

# Xtscut and Xsig need these for bare minimum functionality
XFLAGS= -L/usr/X11R6/lib -lX11 

# Comment out one or more of these leaving only the tools you want.
PROGS=atscap atscut xtscut xtscut1 xtscut3 xsig
# atscap atscap-debug atscap-ssh atscap-udp xtscut1 xtscut2 xtscut3 xsig
# rxsock rzloop

DOCS=atscap.1.gz atscut.1.gz xtscut.1.gz atscap_conf.5.gz atscap.FEATURES.gz atscap.README.gz atscap.CHANGELOG.gz atscap.NEWS.gz atscap.MEMORY.gz

# Top level that should use make all targets listed in PROGS
# make install is automatic to save keyboard wear
all: $(PROGS) $(DOCS)
	@echo '+++ compiled $(PROGS)'
	@echo ' '
	@echo '*** If no errors, only warnings, do this:'
	@echo '$      make install'
	@echo ' '
	@echo '+++ For more make options, do this:'
	@echo '$      make help'
	@echo ' '
	@echo '+++ Upgrading from older atscap version? Do this:'
	@echo '$      make eclean'
	@echo ' '
#	make eclean
#	make install

help:
	@echo
	@echo 'The following make options are available:'
	@echo 'make [all]              makes the following programs: '
	@echo '	  '$(PROGS)
	@echo ' '
	@echo 'make atscap             make Standard ATSC DVB API capture tool'
	@echo 'make atscap-debug       make debug ATSC DVB API capture tool'
	@echo 'make atscap-udp         make Multicast UDP capture tool'
	@echo 'make atscap-ssh         make SSH no-color low-band capture tool'
	@echo ' '
	@echo 'make atscut             make the ATSC Transport Stream utility'
	@echo ' '
	@echo 'make xtscut             make default TS cutter (imlib2)'
	@echo 'make xtscut1            make TS cutter, imlib1 based (slow)'
	@echo 'make xtscut2            make TS cutter, imlib2 based (medium)'
	@echo 'make xtscut3            make TS cutter, Xvideo based (fast)'
	@echo ' '
	@echo 'make xsig               make a signal strength graphing tool'
	@echo ' '
	@echo 'make rxsock             make UDP socket receiver test'
	@echo 'make rzloop             make command exit rc nz test'
	@echo ' '
	@echo 'make eclean             removes guide data from $(DTV)/pg/'
	@echo 'make clean              removes compiled versions in this dir'
	@echo 'make install            install goes to '$(PREFIX)
	@echo 'make docs               Help! I need the man pages, NOW!'
	@echo ' '

%.gz:
	@gzip -c $* > $*.gz
	@touch -m -c -r $* $*.gz

docs: $(DOCS)
	@echo +++ copying man pages to $(MAN1)/
	@mkdir -p $(MAN1)
	@cp -pf atscap.1.gz $(MAN1)
	@cp -pf atscut.1.gz $(MAN1)
	@cp -pf xtscut.1.gz $(MAN1)
	@echo +++ copying man pages to $(MAN5)/
	@mkdir -p $(MAN5)
	@cp -pf atscap_conf.5.gz $(MAN5)
	@echo +++ copying documentation to $(DOC)/
	@mkdir -p $(DOC)
	@cp -pf atscap.FEATURES.gz  $(DOC)
	@cp -pf atscap.README.gz    $(DOC)
	@cp -pf atscap.CHANGELOG.gz $(DOC)
	@cp -pf atscap.NEWS.gz $(DOC)
	@cp -pf atscap.MEMORY.gz    $(DOC)
	@echo $(DISTRO) >$(DOC)/$(DISTRO)

#	strip $(PROGS)
install: $(PROGS) $(DOCS)
	@echo make install
	@echo +++
	@echo +++ copying binary files to $(BIN)/
	@cp -pf $(PROGS) $(BIN)
	@cp -pf btfd.sh $(BIN)
	@echo +++ copying shell scripts to $(BIN)/
	@cp -pi mpeg.sh  $(BIN)
	@cp -pi xtc.sh   $(BIN)
	@echo +++ copying man pages to $(MAN1)/
	@mkdir -p $(MAN1)
	@cp -pf atscap.1.gz $(MAN1)
	@cp -pf atscut.1.gz $(MAN1)
	@cp -pf xtscut.1.gz $(MAN1)
	@echo +++ copying man pages to $(MAN5)/
	@mkdir -p $(MAN5)
	@cp -pf atscap_conf.5.gz $(MAN5)
	@echo +++ copying documentation to $(DOC)/
	@mkdir -p $(DOC)
	@cp -pf atscap.FEATURES.gz  $(DOC)
	@cp -pf atscap.README.gz    $(DOC)
	@cp -pf atscap.CHANGELOG.gz $(DOC)
	@cp -pf atscap.NEWS.gz $(DOC)
	@cp -pf atscap.MEMORY.gz    $(DOC)
	@echo $(DISTRO) >$(DOC)/$(DISTRO)
	@echo +++ copying image files to $(DTV)/pg/img
	@mkdir -p $(DTV)/pg/img
	@cp -pf img/* $(DTV)/pg/img/
	@echo +++ creating cut directory
	@mkdir -p $(DTV)/cut
	@echo +++ creating /etc/$(NAME) and /var/run/$(NAME) directories
	@mkdir -p /etc/$(NAME)
	@mkdir -p /var/run/$(NAME)
	@echo $(DISTRO) is now installed
	@echo +++

rxsock: rxsock.c
	@echo rxsock is a program to test receiving stream via UDP.
	$(CC) $(CFLAGS) -o $@ $^

# small regression tester that checks for exit non zero
rzloop: rzloop.c
	@echo rzloop will loop on a command until it returns non-zero.
	$(CC) -Wall -o $@ $^

# dvb only version
# compile, thread and DVB flags
atscap: atscap.c
	@echo
	@echo Building a colorful object:
	$(CC) $(CFLAGS) $(DVBFLAGS) $(VFLAGS) $(UFLAGS) $(WFLAGS) -lpthread -o $@ $^
	@echo +++ You have a capture tool. Setup is atscap -S.
	@echo +++
	@echo +++ 'Configuration has moved to /etc/atscap/atscap.?.conf'
	@echo +++ You should do the following to remove old guide data:
	@echo +++'          make eclean'
	@echo +++ See also make help ... some things are not usual defaults.
	@echo +++ You may also set some defaults at the top of atscap.c.
	@echo +++ You should do make install now.
	@echo +++

# from Peter Knaggs wiki, a large binary about 3x > strip atscap version
atscap-debug: atscap.c
	$(CC) -g -static -rdynamic \
	-DUSE_GNU_BACKTRACE -DUSE_GNU_BACKTRACE_SCRIPT -DUSER_TEST \
	-DUSE_LIBRT -DUSE_DVB -DUSE_POWERDOWN -DUSE_DYNAMIC \
	-DUSE_ECMA48 -DUSE_WWW -DUSE_PNG -DUSE_CSS_SCROLLBARS \
	-o atscap-debug atscap.c -lpthread -lrt -lm -lpng -lz

# compile, thread, DVB and multicast flags, no colors without VFLAGS
atscap-udp: atscap.c
	@echo
	@echo Building a noisy UDP colorful object:
	$(CC) $(CFLAGS) $(DVBFLAGS) $(VFLAGS) $(UFLAGS) -lpthread -o $@ $^
	@echo +++ You have a capture tool. Setup is atscap -S.

# compile, thread and DVB flags minus colors
atscap-ssh: atscap.c
	@echo
	@echo Building a drab object:
	$(CC) $(CFLAGS) $(DVBFLAGS) -lpthread -o $@ $^
	@echo +++ You have a capture tool. Setup is atscap -S.

# compile flags, need librt to time certain events
atscut: atscut.c common.h
	@echo
	@echo Building a shiney object:
	$(CC) $(CFLAGS) -o $@ atscut.c
	@echo +++ You have a transport stream tool. Try atscut -h.

# X11 and DVB flags
xsig: xsig.c
	@echo
	@echo Building a kaleidescope:
	$(CC) $(CFLAGS) $(DVBFLAGS) $(XFLAGS) -o $@ $^
	@echo +++ You have a graphical signal tool. Try xsig -h.

# X11 flags, mpeg2dec, with Imlib2, for xtscut 1.3.0 and later
xtscut: xtscut.c common.h
	@echo
	@echo Building a slice-n-dice-o-matic:
	@echo +++ NOTE: This is the new standard preview render method.
	@echo +++"  "xtscut was tested with mpeg2dec-0.4.1 and imlib2-1.4.0.
	@echo +++"  "You should install these if you get related compile errors.
	@echo +++"  "The compiler flags for using libmpeg2 and libImlib2 are:
	@echo +++"    "$(MFLAGS2)
	$(CC) $(CFLAGS) $(XFLAGS) $(MFLAGS2) -o xtscut xtscut.c
	@echo +++ You have a visual transport stream cut tool. Try xtscut -h.

# X11 flags, mpeg2dec, old Imlib1 for backwards compatibility/benchmark tests
xtscut1: xtscut.c common.h
	@echo
	@echo Building a slice-n-dice-o-matic:
	@echo +++ NOTE: This is the old standard preview render method.
	@echo +++"  "xtscut was tested with mpeg2dec-0.4.1 and imlib-1.9.14.
	@echo +++"  "You should install these if you get related compile errors.
	@echo +++"  "The compiler flags for using libmpeg2 and libImlib are:
	@echo +++"    "$(MFLAGS1)
	$(CC) $(CFLAGS) $(XFLAGS) $(MFLAGS1) -o xtscut1 xtscut.c
	@echo +++ You have a visual transport stream cut tool. Try xtscut -h.

# X11 flags, mpeg2dec, with Imlib2, for xtscut 1.3.0 and later
xtscut2: xtscut.c common.h
	@echo
	@echo Building a slice-n-dice-o-matic:
	@echo +++ NOTE: This is the new standard preview render method.
	@echo +++"  "xtscut was tested with mpeg2dec-0.4.1 and imlib2-1.4.0.
	@echo +++"  "You should install these if you get related compile errors.
	@echo +++"  "The compiler flags for using libmpeg2 and libImlib2 are:
	@echo +++"    "$(MFLAGS2)
	$(CC) $(CFLAGS) $(XFLAGS) $(MFLAGS2) -o xtscut2 xtscut.c
	@echo +++ You have a visual transport stream cut tool. Try xtscut -h.

# X11 flags, mpeg2dec, with XShm/XVideo rendering for xtscut 1.3.2 and later
xtscut3: xtscut.c common.h xtc_libvo.c
	@echo
	@echo Building a slice-n-dice-o-matic:
	@echo +++ NOTE: Xvideo preview render may not work with older systems.
	@echo +++"  "xtscut Xvideo was tested with mpeg2dec-0.4.1
	@echo +++"  "You should install this if you get related compile errors.
	@echo +++"  "The compiler flags for using libmpeg2 and Xvideo are:
	@echo +++"    "$(MFLAGS3)
	$(CC) $(CFLAGS) $(XFLAGS) $(MFLAGS3) -o xtscut3 xtscut.c
	@echo +++ You have a visual transport stream cut tool. Try xtscut -h.

eclean:
	@echo '*** make eclean'
	@echo Deleting old EPG data in /dtv/pg and /dtv/ram/pg
	rm -f $(DTV)/pg/*.vc
	rm -f $(DTV)/pg/*.epg
	rm -f $(DTV)/ram/pg/*.vc
	rm -f $(DTV)/ram/pg/*.epg

# dont know why it's phony
.PHONY clean:
	@echo Carnival
	@rm -f $(PROGS) $(DOCS)
