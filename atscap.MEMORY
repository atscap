WARNING:

	This may not work on memory-poor systems. More testing is needed.

	It is recommended that you have at least 64MB RAM per DVB device,
    not only for memory use of atscap but also for write caching to storage.

	The maximum working set atscap requires for a 4 DVB-device system
    is over 80MB. A single DVB-device can get by with around 20MB of maximum
    memory usage, plus what is needed for hard drive cache to write files.
    This is in addition to the kernel and various OS libraries, of course.

	It's possible to build a system that boots to atscap instead of
    login/bash, as long as all the modules are loaded and ready to run,
    and all the libraries can be found (libc, libpng, librt, libatsc[?]).
    A slow (400Mhz) CPU with 256M RAM should handle 4 DVB-devices nicely.

	It's not much fun booting to single-user from my own testing of the
    environment. It would be better to have PAM and a login for root to do
    similar. This way a capture machine is not a useless piece of HDTV junk
    and you can actually login to it and do other things with it.
    This will help eventual goal of tiny system CD/USB boot.


Here are the latest memory usage stats on my machine.


This is when idle:

 Static memory:
        EPG + helpers        3,145,824 (8192 events x 376)
       ATSC + helpers          345,856 (4 tables + vc 83616 + mg* 245760)
       MPEG + helpers          222,128 (3 tables + pa 74432 + pids 106496)
       Text + helpers          429,672
       Chans & Timers          143,360
        Search & Spam           39,936
        Total static:        4,326,776 bytes
 Dynamic memory:
             http tts           24,972
       Total dynamic:           24,972 bytes
Total memory:                4,351,748 bytes


This is when capturing Letterman (static is same as above):

 Dynamic memory:
             http tts           24,972
          fifo buffer        9,699,296
             EIT00-7F          527,360
             ETT00-7F          527,360
             frames[]        2,592,000
          sequences[]          172,800
       Total dynamic:       13,543,788 bytes
Total memory:               17,870,564 bytes


    Old pchdtvr 1.0 was over 24M and older atscap versions were over 28M
of static memory usage. The static idle usage is much better now, with
the dynamic usage providing a little bit of memory help when not busy.
This is only something that users with 512M or less might notice.

    Memory usage during cap is reduced with the smaller EPG, and some
creative bit shearing on the frames[] and sequences[] internal storage.

Memory usage -- Mode

 4,351,748	IDLE

 6,619,068      CAP INFO MANUAL

 9,699,260      CAP INFO AUTOMATIC 

15,105,764      CAP MANUAL FULL CAP

17,870,564      CAP VC TIMER and CAP VC MANUAL

    Idle uses minimum memory because of no FIFO, EIT/ETT or frame/sequences.
Manual EPG info uses small FIFO and no frames/sequences or copy of EPG data.
Automatic EPG info uses same as manual with copy of EPG for multi-channel.
Full cap does not allocate frames/sequences but needs EIT/ETT plus big FIFO.
VC cap uses EIT/ETT for EPG, frame and sequence for xtscut, plus big FIFO.

    HTTP usage adds some dynamic allocations but they are too fast to see.
It is important when designing small systems to keep these in mind because
some of them are large enough to push you over a 32M total allocation.

    You need a 9M FIFO or else you can't edit and cap to the same volume at
the same time, but EPG info cap seems happy enough with the smaller FIFO and
editing. If you always edit to a different volume, you can probably reduce the
normal capture FIFO to 4M or so. Or simply edit files only when it's idle. :)

    2.5MB is the minimum recommended FIFOZ but even with that you're going to
have problems if using the volume for any intensive compiles or copies. XFS to
a real-time partition might help but it's a lot of bother for so little return.


WHY is FIFO% always non-zero even with a small fast RAM-only EPG info cap?

    Tupari reported a dvico nano DVB driver post-AOS bug that requires it
to wait a bit before reading the DVB demux output. This is why the FIFO
always starts out with non-zero FIFO max%. In case you were curious, it also
limits how small you can make FIFOZ for EPG info cap. Keep in mind that the
size of the DVB kernel buffer may make a FIFOZ of less than that size useless.
