#define NAME "atscut"
#define AUTHOR "inkling"
#define EMAIL "inkling@users.sourceforge.net"
#define WEBPAGE "http://atscap.sourceforge.net"
#define COPYRIGHT "Copyright (C) 2004-2008"
#define LICENSE "GNU General Public License Version 2"
#define LASTEDIT "20080101"
#define VERSION "1.1.7"

#warning README is first 180 lines of this file
/*
 * atscut.c (c) Copyright 2004-2008 by inkling@users.sourceforge.net
 *  ATSC Transport Stream dumper/cutter utility.
 *   The Sledge-O-Matic for ATSC (apologies to Gallagher)
 *
 * atscut is free software; you may only redistribute it and/or modify
 * it under the terms of the GNU General Public License, Version 2 or later,
 * as published by the Free Software Foundation.
 *
 * atscut is distributed to you in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License Version 2
 * along with this program; if not, write the Free Software Foundation, Inc.,
 * at 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


/*
			   ATSCut.c version 1.1.7
			      January 1, 2008
		    ATSC Transport Stream Container Utility
	  Copyright (c) 2004-2008 by inkling@users.sourceforge.net



Goals:
	Show all the details in a Transport Stream sent over ATSC broadcast.
	Cut a single VC capture using a simple X visual editor interface.
	Extract PIDs by number or grouping.
*/

/*
 * Corporations or individuals who wish to use any part of this program with
 *    their product(s), but do not wish to be bound by the GPL, must contact
 *    the author to arrange suitable licensing for your product(s).
 */
 
/*
 * Compile with (when xtscut is included):
 *
 *   gcc -Wall -O3 -lrt -lImlib -lmpeg2 -lmpeg2convert -lX11 \
 *	(optional) -L/usr/lib/X11R6 or -L/usr/X11R6/lib      \
 *	-o atscut atscut.c
 *
 * Dependencies:
 *	librt			real time clock for timing file i/o
 *	libImlib		ImageMagick package handles X images
 *				  tested with version 1.9.14
 *	libmpeg2		mpeg2dec package
 *	libmpeg2convert		mpeg2dec package
 *				  tested with version 0.4.0b
 *	libX11			Xlib generic interface
 *

*/

/* HELP!
	atscut -h
*/

/* TODO:
    Provide extract function for copy/paste of PDF A/65 Huffman:
	It's possible the Huffman tables may change some day and may
	need a way to regenerate the table from the new specification.

BUG:
    KHCW has the TUBE on program 4, but PAT has 4,3 order instead of
    3,4 order. atscap doesn't cap it, and atscut crashes on the stream
    PAT/VCT abstraction is supposed to handle this gracefully.
*/
	
/* CREDITS:
	The CRC32 table and routine are from ffmpeg.
	The Huffman decode tables are from the A/65b spec, reformatted.
	The MPEG2 display code is modified from mpeg2dec sample2.c
*/

/*

MODIFICATION HISTORY:
    // initial version, not much more than head/tail/cut with different cli
    0.1		Mar-01-2004, first cutter, used integers, wasn't very precise.
			      Was actually more irritating than useful.

    // finally had a chance to get back to working on this again
    // sync and cuts added, tweaked to work during capture
    0.2		Jan-12-2005, works with floats to find cut point
    0.2.1	Jan-15-2005, new try at find sync code added
    0.2.2	Jan-20-2005, multiple cuts from command line
    0.2.3	Jan-21-2005, xine time adjusts, works for full cap only
    0.3		Feb-08-2005, duty cycle allows concurrent pchdtvr cap, -d95
    0.3.1	Feb-10-2005, tweaking sync code to find the right trick
    0.4		Feb-16-2005, more command line options -p -t -r
    0.4.1	Feb-17-2005, fixed sync, skip offset bytes, works! simple! doh!

    // ATSC dump for some tables
    0.5		Feb-18-2005, initial ATSC traffic director
    0.5.1	Feb-19-2005, added STT
    0.5.2	Feb-20-2005, added CRC32
    0.5.3	Feb-24-2005, added VCT single packet parse
    0.5.4	Feb-28-2005, added MGT single packet parse
    0.5.5	Mar-01-2005, added EIT multi-packet parse
    0.5.6	Mar-02-2005, added ETT multi-packet parse
    0.5.7	Mar-03-2005, added MGT multi-packet parse
    0.5.8	Mar-04-2005, added guide dump at end, working code to pchdtvr
    0.5.9	Apr-12-2005, added multiple -k options to extract PIDs as PES

    // MPEG2 dump for some tables
    0.6		Apr-20-2005, added mpeg2 PAT CAT PMT and few descriptors
    0.6.1	Apr-30-2005, added more descriptors for PMT, -v macros
    0.6.2	May-03-2005, added test mpeg2 video, build video payload
    0.6.3	May-04-2005, added mpeg2 video start code dump
    0.6.4	May-05-2005, added test seq end cut function, fixed -d default
    0.6.5	May-08-2005, added more mpeg2 video header dump
    0.6.6	May-13-2005, changed EIT reserved bits not set? cni chk only
    0.6.7	May-14-2005, all packet counters in pkt struct. better stats.
    0.6.8	May-17-2005, changed mss display to show mode too
    0.6.9	May-20-2005, added VCT multi-packet parse, KPXB has BIG VCT!

    // final table parse and other finishing touches
    0.7.0	May-24-2005, added parse EIT descriptors
    0.7.1	May-31-2005, added -u option, changed -r -s -t
    0.7.2	Jun-02-2005, added -e option, re-arrange parse mpeg, bugfixes
    0.7.3	Jun-08-2005, fixed PAT and PMT to always keep if -k mgt
    0.7.4	Jun-12-2005, added Huffman decode tables and decoder
    0.7.5	Jun-14-2005, ignores mss mode for KPXB broken Huffman :/
    0.7.6	Jun-16-2005, GOP parsed
    0.7.7	Jun-17-2005, -y option, i frame rate calc
    0.7.8	Jun-18-2005, -a -m options for detail control, bad ATSC CRC
    0.7.9	Jun-21-2005, forgot r += rdl after test eit ca mss

    // touch-ups to the paint
    0.8.0	Jul-13-2005, gcc 2.96 says *t++ wrong in huffman; -s -t tested
    0.8.1	Aug-03-2005, -z PID rip video ES. but loses the PCR data
    0.8.2	Aug-04-2005, add video PES header parse PTS DTS; fix sync align

    // trying to make an auto-cutter to cut at every black frame
    0.9.0	Nov-08-2005, sequence header scan with -i
    0.9.1	Nov-17-2005, sync removed, block size 188 for better cuts
    0.9.2	Nov-20-2005, -i cuts at sequence before low B frame
    0.9.3	Nov-25-2005, more stringent PMT PID lookup for audio video
    0.9.4	Dec-01-2005, -i It:Pt:Bt threshold cuts via low IPB frame
    0.9.5	Dec-04-2005, -i load/save frames+sequences speeds up next -i
    0.9.6	Dec-05-2005, changes exported to pchdtvr to match .tsf .tss
    0.9.7	Dec-06-2005, -w for -i no cut write, but does gen .tsf .tss

    // gave up on auto-cutter for new xtscut idea. xtscut is much easier.
    0.9.8	Jan-10-2006, imported xtscut
		Jan-18-2006, backed out xtscut, not ready yet
		Jan-19-2006, FILE_?MODE FILE_PERMS for norm
		Feb-21-2006, posted to nop.org, hope it isn't broken
		Feb-25-2006, add buffered i/o from xtscut
    		Mar-06-2006, removed obsoleted cut methods & scale options;
		Mar-07-2006, tested -kX -kmgt to make sure unbroken;
		Mar-14-2006, cc error breakdown by PID
		Mar-15-2006, tupari reported WPIX huffman crash, added sanity
		Apr-28-2006, -o output name override requested by users
		May-02-2006, -e extracts specific program,audio from full cap

    0.9.9	Jun-13-2006 -r to replace program association table from file

		testing and bug shakedown to 1.0.0

    1.0.0	Jul-01-2006, performance changes (these hold down -m8 output):
			    -s outputs single tsx file for xtscut,
			    -s does not process video packets after PSI1,
			    -q does not process video packets at all?
			    -s is about 4x faster now, more if fully cached
		Jul-06-2006, cleaned up MGT display a little bit
		Jul-07-2006, added more STT info to see if dst bits work.

    1.0.1	Sep-04-2006, reduce gcc pre-processor million item frames list.
			     added calloc and free code.
		Sep-05-2006, replaced frame_idx index wrap with limit check.
			     two hour wrap was breaking .tsx for large caps

    1.0.2	Sep-17-2006, -q reduces output to table version changes only
			     NOTE: This causes problems with missing data
			          because rest of packet processing is skipped
    1.0.3	Sep-18-2006, test pmt can handle 2 packet pmt now
    1.0.4	Sep-19-2006, test pmt using pmt[] instead of pmt.
    1.0.5	Sep-20-2006, test pmt did not get section length correct
    1.0.6	Sep-30-2006, test eit did not show MPEG program number
    1.0.7	Nov-06-2006, changes for -g: sort and dup removal
    1.0.8	Dec-12-2006, show bytecount on I frame slice checklist.
			     mpeg payload 128K, fits in CPU cache, faster
    1.0.9	Jan-12-2007, -q fixups for system() call from xtscut
    1.1.0	Feb-02-2007, mpeg video payload size is a little bit larger

    1.1.1	Jul-02-2007, -a8 -g -q holds down output if no ETMID match
    1.1.2	Aug-17-2007, -n -N -o handling for nulls and output
    1.1.3	Aug-24-2007, test eit incomplete desc parse; [_rr]_mss bugs
    1.1.4	Nov-20-2007, disabled -y -z due to lack of use
    1.1.5	Nov-27-2007, added USE_LIBRT define for clock gettime
    1.1.6	Dec-05-2007, fix -y ES extract for mpeg2dec and xtscut test
    1.1.7	Dec-13-2007, use common.h header for code sharing
*/

/*
    TODO:

	Put xtscut in, with -X option to use it. Could make it conditional
	compile so atscut can be used to generate .tsf + .tss without Xlib.

	Fix -m8 Quantizer Table parse. It's off by a bit or two.
*/

/////////////////////////////// definitions ////////////////////////////////

#define USE_ES_EXTRACT
#define WHO (char *) __FUNCTION__

// #warning using _GNU_SOURCE _FILE_OFFSET_BITS 64
#define GNULINUX
#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE

#define FILE_RMODE O_RDONLY
#define FILE_WMODE O_CREAT | O_TRUNC | O_RDWR
/* umask 022 makes this 0644 */
#define FILE_PERMS 0666

#define RD_MODE O_RDONLY
// | O_DIRECT broken?

#include <features.h>

#ifdef USE_PLATFORM_H
#ifndef PLATFORM_NAME
#include <platform.h>
#endif
#endif

#ifndef PLATFORM_NAME
#define PLATFORM_NAME "Linux"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <time.h>


/* 16 VC * 128 EIT * 8 EVENTS PER EIT, 16384 events possible in 8 VCs */
/* use 16 vct's for cable. inefficient memory use is ok */
#define VCZ 16
#define PEZ 8
#define EIZ 128
#define PGZ (VCZ * EIZ * PEZ)

// name limit
#define PNZ 512
// desc limit
#define PDZ 512

// transport stream packet size
#define TSPZ 188
// 21 packets is almost 4k
#define TSBUFZ (21 * TSPZ)

// sync byte value (not always unique in the stream)
#define SYNC 0x47
// sequence header start code (should be unique in the stream)
#define SEQ 0x000001B3

/* -v bit 4 (16 value) is used to turn on display of START NO packets */
#define dprintf8 if (0 != (arg_verbose & 128)) fprintf
#define dprintf7 if (0 != (arg_verbose &  64)) fprintf
#define dprintf6 if (0 != (arg_verbose &  32)) fprintf
#define dprintf5 if (0 != (arg_verbose &  16)) fprintf
#define dprintf4 if (0 != (arg_verbose &   8)) fprintf
#define dprintf3 if (0 != (arg_verbose &   4)) fprintf
#define dprintf2 if (0 != (arg_verbose &   2)) fprintf
#define dprintf1 if (0 != (arg_verbose &   1)) fprintf

// atsc detail control bits
#define aprintf8 if (0 != (arg_adump & 128)) fprintf
#define aprintf7 if (0 != (arg_adump &  64)) fprintf
#define aprintf6 if (0 != (arg_adump &  32)) fprintf
#define aprintf5 if (0 != (arg_adump &  16)) fprintf
#define aprintf4 if (0 != (arg_adump &   8)) fprintf
#define aprintf3 if (0 != (arg_adump &   4)) fprintf
#define aprintf2 if (0 != (arg_adump &   2)) fprintf
#define aprintf1 if (0 != (arg_adump &   1)) fprintf
#define aprintf0 if (0 !=  arg_adump       ) fprintf

// mpeg detail control bits
#define mprintf9 if (0 != (arg_mdump & 256)) fprintf
#define mprintf8 if (0 != (arg_mdump & 128)) fprintf
#define mprintf7 if (0 != (arg_mdump &  64)) fprintf
#define mprintf6 if (0 != (arg_mdump &  32)) fprintf
#define mprintf5 if (0 != (arg_mdump &  16)) fprintf
#define mprintf4 if (0 != (arg_mdump &   8)) fprintf
#define mprintf3 if (0 != (arg_mdump &   4)) fprintf
#define mprintf2 if (0 != (arg_mdump &   2)) fprintf
#define mprintf1 if (0 != (arg_mdump &   1)) fprintf
#define mprintf0 if (0 !=  arg_mdump       ) fprintf

// how many unix seconds between jan 1, 1970 and jan 6, 1980
#define ATSC_UNIX_TIME 315986400

// some slight amount of cursor control for progress indicator via pcr
// pcr is a bad choice for progress indicator, especially for full stream
#define CR "\015"
#define LF "\012"
#define CEL "\033[K"

/////////////////////////////// globals ///////////////////////////////////
static unsigned char *usage =
"\nUsage:\n"
"   %s [options] file.ts\n"
"\n"
"	file.ts		ATSC/ATSC+MPEG2/MPEG2-only Transport Stream file\n"
"\n"
"Options:\n"
"\n"
" Packet extraction control (PIDs are given in hexadecimal):\n"
"\n"
"	-p path		Output path. Default is .\n"
"\n"
"	-o name		Use this output basename instead of input basename.\n"
"\n"
"	-e integer	Extract single Program from full stream capture.\n"
"			 Program numbers are 1-65534, not 0 or 65535.\n"
"\n"
"	-s		Generate frame/sequence files for xtscut/atscut -X.\n"
"			 This requires single VC capture/extract to work.\n"
"			 Use pchdtvr -m option to generate during single VC\n"
"			 capture, to prevent having to do it here.\n"
"\n"
"	-k PID		Extract the hexadecimal PID from the stream.\n"
"			 You may use multiple -k PID to get more than one.\n"
"			 \042MGT\042 for PID keeps only ATSC PSIP PIDs.\n"
"			 MGT extract requires full stream capture.\n"
"\n"
"	-d integer	Duty cycle %% e.g. -d95. Default 100, no sleeping.\n"
"			 Lower CPU usage prevents pchdtvr capture dropout.\n"
"\n"

"	-y PID		Extract specified Video PID as ES to basename-pid.es.\n"
"\n"

"	-n		NULLs: keeps the bitrate same as source file\n"
"\n"
"\n	-r file.ts	replace Progam Association Table 188 byte file.ts\n"
"			   single program stream only\n"
"\n"
"\n	-f		forge PAT + PMT to one video + one audio, see -e\n"
"\n"
"	-h 		Help. You're looking at it.\n"
"\n"
" Packet dump detail control. Default is to give summary counts at end.\n"
"  NOTE: You can specify these multiple times to avoid the addition.\n"
"	xxx is don't care, no code or ideas for the bit usage yet.\n"
"\n"
"	-v integer	Verbose bits, global enables: see -m -a\n"
"				1:PKT  2:ATSC 4:MPEG   8: SYN\n"
"			      16:PSI0 32:xxx 64:xxx  128:xxxx\n"
"\n"
"	-m integer	MPEG detail level bits:\n"
"				 1:PAT  2:PMT  4:AUD   8:VID\n"
"				16:SMB 32:PAY 64:PCR 128:IPB\n"
"\n"
"	-a integer	ATSC detail level bits (only for full streams):\n"
"				 1:MGT  2:VCT  4:EIT   8:ETT\n"
"				16:RRT 32:PAY 64:STT 128:xxx\n" /* 128:CVCT */
"\n"
"	-g		dump ATSC PSIP Event Program Guide, if any.\n"
"			 Guide dump requires full stream capture.\n"
"\n"
"	-q		quiet, redux stdout to only version changes + PES\n"
"\n"
"\n";

/* args for above help */
static int arg_verbose = 0; /* verbose and payload assembly detail control */
static int arg_mdump = 0; /* mpeg detail control */
static int arg_adump = 0; /* atsc detail control */
static int arg_guide = 0; /* not zero is dump program guide dump at end */
static int arg_redux = 0; /* limits table dump to version changes */
static char arg_path[256] = "/dtv/cut/"; /* -p default output path */
static char arg_name[256]; /* -o output name override */
static char arg_newpat[256]; /* -r replace PAT with first 188 bytes this file */
static int arg_seqwr = 0;  /* nz if generating frame and sequence files */
static int arg_pids = 0;   /* nz if -k used at least once */
static int arg_mgtpids = 0; /* nz if -k mgt used to build PID list from MGT */
static int arg_duty = 100; /* 100 is default, no sleep, else 1-99. */
static int arg_espid = 0;  /* extract one PID as ES */
static int arg_nulls = 0;  /* -n inserts nulls to replace -e removed packets */
static int arg_epn = 0;	   /* -e sets program number to extract */
static int arg_ean = 0;	   /* -e3,1 sets this to 1 */
static int arg_forge = 0;  /* forge new PAT + PMT, only for -e option */
static int arg_crc = 0;	   /* generate list of crc32s for each packet */

static unsigned long long pcr = 0;	/* current pcr */
static unsigned long long ppcr = 0;	/* previous pcr */

static unsigned long long first_pcr = 0; /* MPEG clock reference */
static unsigned long long last_pcr = 0;
static unsigned int first_stt = 0;	 /* ATSC system time */
static unsigned int last_stt = 0;

#define FILE_MAX 512
static unsigned char in_name[ FILE_MAX ];
static unsigned char es_name[ FILE_MAX ];
static unsigned char base_name[ FILE_MAX ];
static unsigned char out_name[ FILE_MAX ];
static unsigned char out_path[ FILE_MAX ] = ".";

long long in_size = 0;			/* input file size */
static int in_file = 0;			/* normal TS input file */
static int out_file;			/* normal TS output file */
static int es_file = 0;			/* -y ES output file */

static unsigned char  ts_buf[ TSPZ ];
static unsigned char  newpat[ TSPZ ];

static char ipb[256]; /* text display of last frames seen */
static unsigned char ipbx = 0; /* index to above */

static unsigned int keep_pids[0x2000];	/* lots of pids to keep, -k options */
					/* -m and -e options use as well */
/* counts from the stream */
static unsigned char  last_cc[0x2000];	/* last continuity counter, by PID */
static unsigned char   pid_cc[0x2000];	/* error flags, by PID */
static unsigned char   pid_vn[0x2000];	/* version numbers, by PID */
static unsigned char     psit[0x2000];	/* payload start table types, by PID */
static unsigned int      pids[0x2000];	/* packet counts, by PID */
static unsigned int    cc_err[0x2000];  /* cc error by pid */

static int vct_ncs;			/* number of channels in vct */
static int pat_ncs;			/* number of channels in pat */

/* is the current packet to be kept? 0 is no. *using keep_pids[] now* */
/* static unsigned int keep_pkt = 0; */

static struct {
	unsigned int tt;
	unsigned int ttpid;
	unsigned int ttvn;
	unsigned int nb;
	unsigned int ttdl;
} mg[0x2000];

static unsigned int mgt_tt_pidx = 0; /* increments with new list entries */
static unsigned int mgt_eit = 0;

/* static unsigned int last_pid = -1; */

/* static int tsprate = TSPRATE; / / default, integer arg_rate */

static int sumrate = 0;
static int iframe = 0;
static int iframen = 0;

/* align is forcing unsigned char store as 32 bits, might as well use int
   1.8m output for 1 hour 60fps.
*/
struct frame_s {
    int pct;		/* picture type I P or B (1 2 or 3) */
    int vpn;		/* video pkt# for picture: 4 hours is < 200 million */
};

/* 24hrs at 60fps */
#define FRAME_MAX 5184000
//static struct frame_s frames[ FRAME_MAX ];
static struct frame_s *frames;
/* increments every I P or B frame header */
static int frame_idx;

/* pick the larger of 1/15th or 1/30th total frame count */
#define SEQUENCE_MAX (FRAME_MAX / 15)
//static long long sequences[ SEQUENCE_MAX ];
static long long *sequences;
/* increments every sequence header */
static int sequence_idx = 0;

static long long bytes_in = 0;
static long long bytes_out = 0;
static long long bytes_total = 0;
static long long ppsbytes_in = 0;  /* previous payload start bytes in */
static long long psbytes_in = 0; /* payload start bytes in */

static unsigned int duty_cycle_sleep = 0;
static unsigned int duty_cycle_packet = 0;

static long long file_size;

struct timespec cap_start, cap_stop, cap_et;


/* ATSC packet counters in an easy to clear structure */
static
struct {
    int keep;	/* want keep to be at zero so optimizer will see no index */

/* NOTE: packet count should be supplemented by table count for Table entries */
/* remember that Table counts are packet counts not table counts */
    int count;	// total count */
    int pcount; // previous count from last I frame */
    int errors; // total of all errors found */

    // transport errors
    int errsyn;	// packet error
    int errtei;	// transport error indicator
    int errscr;	// any scramble bits set as another error indicator
    int errcce;	// countinuity counter error
    
    int atsc;	// ATSC PID packet count count
    int atscce; // ATSC countinuity counter errors
    int null;	// NULL PID packet count

    int psi;	// payload start indicator for current packet
    int atsctid;	// atsc payload table id 
    int mpegtid;	// mpeg payload table id

    // ATSC table types
    int stt;	// System Time Table packet count
    int mgt;	// Master Guide Table packet count
    int tvct;	// Terrestrial Virtual Channel Table packet count
    int cvct;	// Cable Virtual Channel Table packet count
    int eit;	// Event Information Table packet count
    int ett;	// Extended Text Table packet count
    int rrt;	// Region Rating Table packet count
    int dcct;	// Directed Channel Change Table packet count
    int dccsct;	// Directed Channel Change Selection Code Table packet count

    // CRC error counts for each ATSC table type
    int crcstt;
    int crcmgt;
    int crctvct;
    int crccvct;
    int crceit;
    int crcett;
    int crcrrt;
    int crcdcct;
    int crcdccsct;
    
    int mpeg2;	// number of mpeg2 packets, pat cat pmt aud vid
    int mpegce; // number of continuity counter errors for mpeg
    int pcrdi;  // number of pcr discontinuity errors (pcr in video usually)
    int pes;	// aud + vid count
    int vid;	// number of video packets
    int aud;	// number of audio packets

    // MPEG table types
    int pat;	// Program Association Table packet count
    int cat;	// Conditional Access Table packet count
    int pmt;	// Program Map Table packet count

    // CRC error counts for each MPEG table type
    int crcpat;
    int crccat;
    int crcpmt;

    // MPEG headers
    int seqend; // count sequence end codes (commercial breaks?)
    int iframe;
} pkt;

/* program association has list of program numbers and pmt pids */
static
struct {
    unsigned int pn;
    unsigned int pmtpid;
} pa[VCZ];


/* generic payload structure */
struct payload_s {
    unsigned int sl;
    unsigned int vn;
    unsigned int payoff;
    unsigned int crc;
    unsigned int payok;
    char payload[4096];
};


/* generic payload structures */
static struct payload_s rrt;		// 4k
static struct payload_s mgt;		// 4k
static struct payload_s vct;		// 4k
static struct payload_s pat;		// 4k
static struct payload_s pmt[ VCZ ];	// 4k * 16
static struct payload_s eit[ EIZ ];	// 512k
static struct payload_s ett[ EIZ ];	// 512k


/* not seen yet */
/* static struct payload_s cat;		// 4k */

/* see ATSC A/65b Table 6.1 System Time Table */
/* aside from the system time, it has dst status that may work */
struct stt_s {
    unsigned short sl;
    unsigned char pv;
    unsigned long st;
    unsigned char guo;
    unsigned short ds;
    unsigned char dss;
    unsigned char dsd;
    unsigned char dsh;
    unsigned int crc;
};

static struct stt_s stt;

struct vid_s {
    unsigned int payoff;		// video payload offset
    unsigned int payok;			// video payload o
    unsigned char payload[300 << 10];	// video payload buffer 300k
};

static struct vid_s vid;

#ifdef USE_A52_PAYLOAD
/* each payload should be 8 packets, with padding, for 40ms of audio.
   This might be per channel, so multiply by 8 to handle 7.1 audio.
    Gives around 300kbit/s, which is a little bit lower than 448kbit/s.
*/
struct aud_s {
    unsigned int payoff;		// section payload offset
    unsigned int payok;			// section ok
    unsigned char payload[64 * 188];	// section payload buffer
};

static struct aud_s aud;

#endif


struct vc_s {
    unsigned char name[16];	// 7 unicode 16 bit words for vc name, lsb
    unsigned short major;	// 10 bit major channel number for vc
    unsigned short minor;	// 10 bit minor channel number for vc
    unsigned char mode;		// modulation mode: see A/65b table 6.5
    unsigned int freq;		// frequency, obsolete, 0 after jan 1 2010
    unsigned short ctsid;	// TSID to match against PAT TSID
    unsigned int pn;		// program number for PAT and PMT
    unsigned char etm;		// extended text message location, 0 if none

/* next few get ignored, but stored */
    unsigned char actrl;	// 1 bit access is not controlled if 0
    unsigned char hide;		// 1 bit surf skip if not 0
    unsigned char path;		// CVCT path select, rsvd in TVCT
    unsigned char oob;		// CVCT out of band, rsvd in TVCT
    unsigned char hideg;	// if hidden set, hide the guide too

/* the rest of these may or may not be useful */
    unsigned char svct;		// service type
    unsigned short src;		// source id, for EPG ETMID lookup
    unsigned short dlen;	// descriptor length, descriptor is skipped

/* additional descriptors have not been seen */
    unsigned short adl;		// additional descriptor length
};

/* 8 vc's is more than terrestrial uses, but less than cable uses */
static struct vc_s vc[ VCZ ];

struct pgm_s {
    unsigned int pn;			/* MPEG program number */
    unsigned int st;			// 32 bit start time
    unsigned int ls;			// 20 bit length seconds
    unsigned int etmid;			// 16 bit src, 14 bit event, 2 bit '10'
    unsigned char name[PNZ];		// bytes of program title
    unsigned char desc[PDZ];		// ETT description
};

struct pgm_s pgm[ PGZ ]; // whopping usage
struct pgm_s pgm1;

/* need sort indices, int faster than short */
static unsigned int pg[ PGZ ];	// unsorted index of existing
static unsigned int pg1[ PGZ ]; // copy of above, then sorted by ETMID
static unsigned int pgm_idx;	// inc by new EIT with new ETMID, may be stale
static unsigned int pg_idx;	// first level of freshening the stale
static unsigned int pg1_idx;	// sorted level of fresh stale tv

/*
  RRT delivers same information that is in this list of strings
    but this list doesn't need huffman decode to use.
    If the list ever changes, this needs to be fixed, too.
    This is V-Chip, in case you were wondering.
*/
static unsigned char *ratings[] = {
"Audience", "", "None", "TV-G", "TV-PG", "TV-14", "TV-MA","","","",
"Dialogue", "", "D","","","","","","","",
"Language", "", "L","","","","","","","",
"Sex", "", "S","","","","","","","",
"Violence", "", "V","","","","","","","",
"Children", "", "TV-Y","TV-Y7","","","","","","",
"Fantasy Violence", "", "FV","","","","","","","",
"MPAA", "", "N/A","G", "PG", "PG-13","R","NC-17","X","NR",
};

//static time_t stt_t = 0;
static struct tm stt_tm;
static unsigned char stt_text[32];
//static unsigned int stt_crc;
static unsigned int utc_offset;

static unsigned int mgt_crc;
static unsigned int vct_crc;
static unsigned int rrt_crc;

static unsigned char mgt_vn = 0xFF;
static unsigned char vct_vn = 0xFF;
static unsigned char eit_vn = 0xFF;
static unsigned char ett_vn = 0xFF;
static unsigned char rrt_vn = 0xFF;
static unsigned char eitvn[ VCZ * EIZ ];
static unsigned char ettvn[ VCZ * EIZ ];

/* last pat/pmt with good crc */
static unsigned char old_pat[188];
static unsigned char old_pmt[188];

/* so far 5 PMT PIDs seen for, at most, 5 Terrestrial VCs */
static int vid_pids[ 8 ];	/* PMT supplied Video ES PIDs */
static int vid_pidx = 0;
static int aud_pids[ 32 ];	/* PMT supplied Audio ES PIDs */
static int aud_pidx = 0;


//static unsigned short vidpid;
//static unsigned short audpid;


/*********************************************************************** CRC32
 from ffmpeg
*/
static const unsigned int crc_lut[256] = {
0x00000000, 0x04c11db7, 0x09823b6e, 0x0d4326d9, 0x130476dc, 0x17c56b6b,
0x1a864db2, 0x1e475005, 0x2608edb8, 0x22c9f00f, 0x2f8ad6d6, 0x2b4bcb61,
0x350c9b64, 0x31cd86d3, 0x3c8ea00a, 0x384fbdbd, 0x4c11db70, 0x48d0c6c7,
0x4593e01e, 0x4152fda9, 0x5f15adac, 0x5bd4b01b, 0x569796c2, 0x52568b75,
0x6a1936c8, 0x6ed82b7f, 0x639b0da6, 0x675a1011, 0x791d4014, 0x7ddc5da3,
0x709f7b7a, 0x745e66cd, 0x9823b6e0, 0x9ce2ab57, 0x91a18d8e, 0x95609039,
0x8b27c03c, 0x8fe6dd8b, 0x82a5fb52, 0x8664e6e5, 0xbe2b5b58, 0xbaea46ef,
0xb7a96036, 0xb3687d81, 0xad2f2d84, 0xa9ee3033, 0xa4ad16ea, 0xa06c0b5d,
0xd4326d90, 0xd0f37027, 0xddb056fe, 0xd9714b49, 0xc7361b4c, 0xc3f706fb,
0xceb42022, 0xca753d95, 0xf23a8028, 0xf6fb9d9f, 0xfbb8bb46, 0xff79a6f1,
0xe13ef6f4, 0xe5ffeb43, 0xe8bccd9a, 0xec7dd02d, 0x34867077, 0x30476dc0,
0x3d044b19, 0x39c556ae, 0x278206ab, 0x23431b1c, 0x2e003dc5, 0x2ac12072,
0x128e9dcf, 0x164f8078, 0x1b0ca6a1, 0x1fcdbb16, 0x018aeb13, 0x054bf6a4,
0x0808d07d, 0x0cc9cdca, 0x7897ab07, 0x7c56b6b0, 0x71159069, 0x75d48dde,
0x6b93dddb, 0x6f52c06c, 0x6211e6b5, 0x66d0fb02, 0x5e9f46bf, 0x5a5e5b08,
0x571d7dd1, 0x53dc6066, 0x4d9b3063, 0x495a2dd4, 0x44190b0d, 0x40d816ba,
0xaca5c697, 0xa864db20, 0xa527fdf9, 0xa1e6e04e, 0xbfa1b04b, 0xbb60adfc,
0xb6238b25, 0xb2e29692, 0x8aad2b2f, 0x8e6c3698, 0x832f1041, 0x87ee0df6,
0x99a95df3, 0x9d684044, 0x902b669d, 0x94ea7b2a, 0xe0b41de7, 0xe4750050,
0xe9362689, 0xedf73b3e, 0xf3b06b3b, 0xf771768c, 0xfa325055, 0xfef34de2,
0xc6bcf05f, 0xc27dede8, 0xcf3ecb31, 0xcbffd686, 0xd5b88683, 0xd1799b34,
0xdc3abded, 0xd8fba05a, 0x690ce0ee, 0x6dcdfd59, 0x608edb80, 0x644fc637,
0x7a089632, 0x7ec98b85, 0x738aad5c, 0x774bb0eb, 0x4f040d56, 0x4bc510e1,
0x46863638, 0x42472b8f, 0x5c007b8a, 0x58c1663d, 0x558240e4, 0x51435d53,
0x251d3b9e, 0x21dc2629, 0x2c9f00f0, 0x285e1d47, 0x36194d42, 0x32d850f5,
0x3f9b762c, 0x3b5a6b9b, 0x0315d626, 0x07d4cb91, 0x0a97ed48, 0x0e56f0ff,
0x1011a0fa, 0x14d0bd4d, 0x19939b94, 0x1d528623, 0xf12f560e, 0xf5ee4bb9,
0xf8ad6d60, 0xfc6c70d7, 0xe22b20d2, 0xe6ea3d65, 0xeba91bbc, 0xef68060b,
0xd727bbb6, 0xd3e6a601, 0xdea580d8, 0xda649d6f, 0xc423cd6a, 0xc0e2d0dd,
0xcda1f604, 0xc960ebb3, 0xbd3e8d7e, 0xb9ff90c9, 0xb4bcb610, 0xb07daba7,
0xae3afba2, 0xaafbe615, 0xa7b8c0cc, 0xa379dd7b, 0x9b3660c6, 0x9ff77d71,
0x92b45ba8, 0x9675461f, 0x8832161a, 0x8cf30bad, 0x81b02d74, 0x857130c3,
0x5d8a9099, 0x594b8d2e, 0x5408abf7, 0x50c9b640, 0x4e8ee645, 0x4a4ffbf2,
0x470cdd2b, 0x43cdc09c, 0x7b827d21, 0x7f436096, 0x7200464f, 0x76c15bf8,
0x68860bfd, 0x6c47164a, 0x61043093, 0x65c52d24, 0x119b4be9, 0x155a565e,
0x18197087, 0x1cd86d30, 0x029f3d35, 0x065e2082, 0x0b1d065b, 0x0fdc1bec,
0x3793a651, 0x3352bbe6, 0x3e119d3f, 0x3ad08088, 0x2497d08d, 0x2056cd3a,
0x2d15ebe3, 0x29d4f654, 0xc5a92679, 0xc1683bce, 0xcc2b1d17, 0xc8ea00a0,
0xd6ad50a5, 0xd26c4d12, 0xdf2f6bcb, 0xdbee767c, 0xe3a1cbc1, 0xe760d676,
0xea23f0af, 0xeee2ed18, 0xf0a5bd1d, 0xf464a0aa, 0xf9278673, 0xfde69bc4,
0x89b8fd09, 0x8d79e0be, 0x803ac667, 0x84fbdbd0, 0x9abc8bd5, 0x9e7d9662,
0x933eb0bb, 0x97ffad0c, 0xafb010b1, 0xab710d06, 0xa6322bdf, 0xa2f33668,
0xbcb4666d, 0xb8757bda, 0xb5365d03, 0xb1f740b4
};

/***************************************************************** ATSC HUFFMAN
 A/65b Table C5 Huffman Title Decode Tree (c) 1997 General Instruments Corp.
 The following, according to ATSC specs, is royalty free for ATSC use.

 not exact table, but a faster, more usable version of it,
 since exact table is big endian but x86 isn't.

 byte offsets of character i tree root

 Non-compliant stations crash this so need limits
*/
static unsigned int huffman1bo[128] = { 
0x0000, 0x003A, 0x003C, 0x003E, 0x0040, 0x0042, 0x0044, 0x0046, 
0x0048, 0x004A, 0x004C, 0x004E, 0x0050, 0x0052, 0x0054, 0x0056, 
0x0058, 0x005A, 0x005C, 0x005E, 0x0060, 0x0062, 0x0064, 0x0066, 
0x0068, 0x006A, 0x006C, 0x006E, 0x0070, 0x0072, 0x0074, 0x0076, 
0x0078, 0x00CE, 0x00D2, 0x00D4, 0x00D6, 0x00D8, 0x00DA, 0x00DC, 
0x00E6, 0x00E8, 0x00EA, 0x00F0, 0x00F2, 0x00F4, 0x0106, 0x0112, 
0x0114, 0x011C, 0x0128, 0x0130, 0x0134, 0x0136, 0x0138, 0x013A, 
0x013C, 0x013E, 0x0146, 0x0148, 0x014A, 0x014C, 0x014E, 0x0150, 
0x0152, 0x0154, 0x017E, 0x0192, 0x01AC, 0x01BA, 0x01D2, 0x01E4, 
0x01FA, 0x0206, 0x021E, 0x0226, 0x0232, 0x023E, 0x0252, 0x0264, 
0x027A, 0x0294, 0x0298, 0x02A4, 0x02C8, 0x02DE, 0x02E6, 0x02F4, 
0x0304, 0x0306, 0x030C, 0x0310, 0x0312, 0x0314, 0x0316, 0x0318, 
0x031A, 0x031C, 0x0352, 0x036A, 0x038E, 0x03AE, 0x03EE, 0x0406, 
0x0428, 0x0444, 0x0472, 0x0476, 0x0490, 0x04BE, 0x04D6, 0x050A, 
0x0544, 0x0564, 0x0566, 0x059A, 0x05D0, 0x05FC, 0x0622, 0x062C, 
0x0646, 0x0654, 0x067C, 0x068A, 0x068C, 0x068E, 0x0690, 0x0692
};

#define TITLE_COZ 1683
/* character i order-1 trees */
static unsigned char huffman1co[1684] = { 
0x1B,0x1C,0xB4,0xA4,0xB2,0xB7,0xDA,0x01,0xD1,0x02,0x03,0x9B,0x04,0xD5,0xD9,0x05,
0xCB,0xD6,0x06,0xCF,0x07,0x08,0xCA,0x09,0xC9,0xC5,0xC6,0x0A,0xD2,0xC4,0xC7,0xCC,
0xD0,0xC8,0xD7,0xCE,0x0B,0xC1,0x0C,0xC2,0xCD,0xC3,0x0D,0x0E,0x0F,0x10,0xD3,0x11,
0xD4,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,
0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,
0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,
0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,
0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x29,0x2A,0xD8,0xE5,0xB9,0x01,0xA7,0xB1,
0xEC,0xD1,0x02,0xAD,0xB2,0xDA,0xE3,0xB3,0x03,0xE4,0xE6,0x04,0x9B,0xE2,0x05,0x06,
0x07,0x08,0x09,0xD5,0x0A,0xD6,0x0B,0xD9,0x0C,0xA6,0xE9,0xCB,0xC5,0xCF,0x0D,0x0E,
0xCA,0xC9,0x0F,0xC7,0x10,0x11,0xE1,0x12,0x13,0xC6,0xD2,0xC8,0xCE,0xC1,0xC4,0xD0,
0xCC,0x14,0x15,0xEF,0xC2,0xD7,0x16,0xCD,0x17,0xF4,0xD4,0x18,0x19,0x1A,0xC3,0xD3,
0x1B,0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x01,0x80,
0xA0,0x9B,0x9B,0x9B,0x9B,0x9B,0xB1,0x9B,0x9B,0x9B,0x9B,0xA0,0x04,0xF3,0xE4,0xB9,
0x01,0xF4,0xA0,0x9B,0x02,0x03,0x9B,0x9B,0x9B,0x9B,0x01,0x02,0x9B,0xC1,0xC8,0xD3,
0x9B,0x9B,0x9B,0xA0,0x07,0x08,0xB1,0xD2,0xD3,0xD4,0xD5,0xAD,0xCD,0xC1,0x01,0x02,
0x03,0xA0,0x04,0x9B,0x05,0x06,0xA0,0x05,0xC9,0xD7,0xD3,0x01,0x02,0x9B,0xAE,0x80,
0x03,0x04,0x9B,0x9B,0x02,0x03,0xAD,0x9B,0x01,0x80,0xA0,0xB0,0x04,0x05,0x80,0x9B,
0xB1,0xB2,0xA0,0xB0,0xB9,0x01,0x02,0x03,0x02,0x03,0xB1,0xBA,0x01,0xB0,0x9B,0x80,
0x80,0x01,0xB0,0x9B,0x9B,0xB8,0x9B,0x9B,0x9B,0x9B,0x9B,0xB0,0x9B,0xA0,0x02,0x03,
0xB1,0xB3,0xB9,0xB0,0x01,0x9B,0x9B,0xA0,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,
0x9B,0x80,0x9B,0x9B,0x13,0x14,0xAA,0xAD,0xAE,0xF6,0xE7,0xF4,0xE2,0xE9,0x01,0x02,
0xC2,0xF0,0x9B,0xF3,0xE3,0xE6,0xF7,0x03,0xF5,0x04,0x05,0x06,0xF2,0x07,0x08,0x09,
0x0A,0x0B,0x0C,0xE4,0xA0,0x0D,0xEC,0xEE,0x0E,0xED,0x0F,0x10,0x11,0x12,0x08,0x09,
0xC1,0xD3,0x9B,0x01,0xC3,0x02,0xE9,0xEC,0x03,0xF2,0xF5,0x04,0xEF,0xE1,0x05,0xE5,
0x06,0x07,0x0B,0x0C,0xC1,0xF9,0x01,0xC2,0xCF,0xE5,0xF5,0x9B,0xE9,0x02,0xA0,0x03,
0x04,0x05,0xF2,0x06,0xEC,0x07,0xE1,0x08,0x09,0xE8,0x0A,0xEF,0x05,0x06,0xF9,0x9B,
0x01,0xF5,0x02,0xF2,0xE9,0xE5,0xEF,0x03,0xE1,0x04,0x0A,0x0B,0xF1,0xF5,0xF3,0x01,
0xED,0xF9,0xC3,0x02,0xEC,0xEE,0xE4,0xF8,0x03,0x9B,0xF6,0x04,0x05,0xE1,0x06,0x07,
0x08,0x09,0x07,0x08,0xA0,0x9B,0xCC,0x01,0xE5,0x02,0xEC,0xF5,0xEF,0x03,0xE9,0xF2,
0x04,0x05,0xE1,0x06,0x09,0x0A,0xAE,0xEC,0xF9,0xC1,0xE8,0x01,0x9B,0x02,0x03,0x04,
0xE1,0xF5,0xE9,0x05,0xE5,0x06,0xF2,0xEF,0x07,0x08,0xEF,0x05,0x80,0x9B,0xF5,0x01,
0x02,0xE9,0xE1,0x03,0xE5,0x04,0xEE,0x0B,0xBA,0xD4,0xAE,0xF2,0xE3,0x01,0xA0,0x02,
0x80,0x9B,0xED,0x03,0xC9,0xF3,0xF4,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x02,0x03,
0x9B,0xF5,0x01,0xE1,0xEF,0xE5,0x05,0xE9,0xE1,0xEF,0xF5,0xEE,0x9B,0xE5,0x01,0x02,
0x03,0x04,0x04,0x05,0xA0,0x9B,0x01,0xF5,0x02,0xE5,0xEF,0x03,0xE1,0xE9,0x08,0x09,
0xAA,0xD4,0x01,0x9B,0xE3,0x02,0xF2,0x03,0xE5,0x04,0xF5,0xF9,0xE9,0x05,0xEF,0x06,
0x07,0xE1,0xE5,0x08,0xCE,0xA0,0xC6,0xF5,0x01,0x02,0x9B,0xC2,0x03,0xE1,0x04,0xEF,
0x05,0xE9,0x06,0x07,0x09,0x0A,0xE4,0xF3,0xE6,0xF6,0xF7,0xF0,0xF2,0x01,0xEC,0x02,
0x03,0xA0,0x9B,0x04,0x05,0xF5,0x06,0x07,0xEE,0x08,0x0B,0x0C,0xA0,0xF3,0xF9,0xAE,
0xD2,0xC7,0x01,0x9B,0x02,0xF5,0x03,0x04,0x05,0xE9,0xEC,0x06,0xE5,0x07,0xEF,0x08,
0xE1,0x09,0xF2,0x0A,0x01,0xF5,0x9B,0xD6,0x04,0x05,0xE8,0x9B,0x01,0xF5,0x02,0xE1,
0xE9,0xEF,0x03,0xE5,0x10,0x11,0xAA,0xEC,0xF1,0xAE,0xA0,0xF7,0xED,0xEE,0x01,0x02,
0x9B,0xEB,0x03,0x04,0x05,0x06,0xE3,0x07,0xEF,0x08,0xE9,0xF5,0x09,0xE1,0xE5,0xF0,
0xE8,0x0A,0x0B,0x0C,0x0D,0xF4,0x0E,0x0F,0xE8,0x0A,0xAD,0xCE,0x9B,0x01,0xD6,0x02,
0xF5,0xF7,0x03,0x04,0xE1,0xE5,0xE9,0x05,0xF2,0x06,0xEF,0x07,0x08,0x09,0xEE,0x03,
0xEC,0xAE,0x01,0x9B,0x02,0xF0,0x06,0xE9,0xA0,0xC3,0xEF,0x9B,0xE5,0x01,0x80,0x02,
0x03,0xE1,0x04,0x05,0x06,0x07,0xC6,0xD7,0x01,0x9B,0xF2,0x02,0x03,0xE8,0xE5,0xE1,
0x04,0xE9,0xEF,0x05,0x9B,0x9B,0x02,0xEF,0xE1,0x9B,0x01,0xE5,0x01,0xEF,0x9B,0xE1,
0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x19,0x1A,0x9B,0xBA,
0xE5,0xEA,0xF8,0x01,0x02,0xE6,0xA7,0x03,0xFA,0xE8,0x04,0xF7,0x05,0xF5,0xE2,0x06,
0xEB,0x07,0xF0,0x08,0x80,0xF6,0xE7,0x09,0xE4,0x0A,0xA0,0xE9,0x0B,0xE3,0xF9,0x0C,
0x0D,0xED,0x0E,0x0F,0xF3,0x10,0x11,0xEC,0x12,0xF4,0xF2,0x13,0xEE,0x14,0x15,0x16,
0x17,0x18,0x0A,0x0B,0xF3,0x9B,0xF5,0xE2,0x01,0x80,0xA0,0x02,0xE5,0xF2,0xE9,0x03,
0xEC,0x04,0xF9,0x05,0xEF,0x06,0xE1,0x07,0x08,0x09,0x10,0x11,0xC3,0xCC,0xC7,0x9B,
0xE3,0x01,0x80,0xEC,0xF9,0x02,0xF3,0x03,0xF5,0x04,0x05,0xF2,0x06,0xE9,0xA0,0x07,
0x08,0xEF,0xF4,0x09,0x0A,0xE1,0x0B,0xE8,0xEB,0xE5,0x0C,0x0D,0x0E,0x0F,0x0E,0x0F,
0xAE,0xF5,0xF7,0x01,0xEC,0x02,0xE4,0xE7,0xF2,0x03,0x9B,0xEF,0x04,0xF6,0x05,0x06,
0xF9,0xF3,0x07,0xE9,0xE1,0x08,0x09,0x80,0x0A,0x0B,0xE5,0x0C,0x0D,0xA0,0x1E,0x1F,
0x9B,0xA1,0xAD,0xE8,0xEA,0xF1,0xF5,0xFA,0x01,0x02,0x03,0x04,0xBA,0xF8,0xA7,0xE2,
0xE9,0x05,0x06,0x07,0xE6,0xED,0xE7,0xEB,0x08,0x09,0xF6,0xF0,0x0A,0xEF,0x0B,0xE3,
0x0C,0x0D,0x0E,0xF9,0x0F,0xE4,0xEC,0x10,0xE5,0x11,0xF4,0xF7,0x12,0x13,0xE1,0x14,
0x15,0x16,0xEE,0xF3,0x17,0x80,0x18,0x19,0xF2,0x1A,0x1B,0xA0,0x1C,0x1D,0xA0,0x0B,
0xF5,0x9B,0x01,0xEC,0xF3,0xF2,0x80,0xE1,0x02,0x03,0xF4,0xE9,0xEF,0xE6,0x04,0x05,
0x06,0x07,0xE5,0x08,0x09,0x0A,0x0F,0x10,0xBA,0xF9,0xA7,0xF4,0x9B,0x01,0xE7,0xEC,
0x02,0xEE,0x03,0xEF,0xF5,0x04,0xF2,0x05,0x06,0xE9,0x07,0xF3,0xE1,0x08,0x09,0x0A,
0x0B,0xE5,0x80,0x0C,0xE8,0xA0,0x0D,0x0E,0xE5,0x0D,0xE2,0xF5,0xF7,0x9B,0xEC,0x01,
0xF9,0xEE,0x02,0x03,0x04,0xF2,0x05,0x80,0x06,0xA0,0xE1,0xEF,0x07,0xF4,0xE9,0x08,
0x09,0x0A,0x0B,0x0C,0x15,0x16,0xA1,0xF8,0xE9,0xEB,0x01,0x80,0x9B,0xFA,0xE2,0x02,
0x03,0x04,0xA0,0xF0,0x05,0x06,0x07,0xE1,0x08,0xE6,0xF2,0xED,0xF6,0x09,0xE4,0x0A,
0xEF,0xF4,0xEC,0xF3,0xE7,0xE5,0x0B,0xE3,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,
0xEE,0x14,0xEF,0x01,0x9B,0xE1,0x0B,0x0C,0xD4,0xEF,0xE6,0xEC,0xF7,0xE1,0x01,0xBA,
0x02,0x9B,0xF9,0x03,0x04,0x05,0xF3,0x06,0x07,0x08,0xE9,0xA0,0x09,0x80,0xE5,0x0A,
0x15,0x16,0xA7,0xBA,0xE3,0xF7,0xF2,0xAD,0xE2,0x01,0x02,0x9B,0xE6,0x03,0xED,0xF6,
0x04,0xEB,0x05,0xF4,0x06,0x07,0x08,0xF3,0x09,0xF5,0x0A,0xEF,0x0B,0x0C,0x80,0xF9,
0xE1,0x0D,0xE4,0xE9,0xA0,0x0E,0x0F,0xEC,0xE5,0x10,0x11,0x12,0x13,0x14,0x0A,0x0B,
0xF9,0x9B,0xF5,0xF3,0x01,0x02,0xE2,0xED,0x80,0x03,0xF0,0xEF,0x04,0xA0,0x05,0xE9,
0x06,0xE1,0x07,0x08,0x09,0xE5,0x18,0x19,0xE2,0xEA,0xF2,0xE8,0xEC,0xED,0xFA,0x9B,
0x01,0xF5,0x02,0x03,0xF6,0x04,0xBA,0xE6,0x05,0x06,0xEB,0xEF,0x07,0xA7,0xF9,0x08,
0x09,0x0A,0x0B,0xE3,0x0C,0xEE,0xE1,0x0D,0xF3,0x0E,0xE9,0x0F,0x10,0xF4,0x80,0xE4,
0xE5,0x11,0x12,0xE7,0xA0,0x13,0x14,0x15,0x16,0x17,0x1B,0x1C,0xAE,0xFA,0xBF,0x01,
0xA7,0x9B,0x02,0xE9,0xF8,0xF9,0x03,0xE5,0xE8,0x04,0xE1,0xEB,0x05,0xE2,0x06,0x07,
0xE3,0x08,0xE7,0xF4,0x09,0x80,0xF6,0xF0,0x0A,0xE4,0x0B,0xF3,0xF7,0x0C,0x0D,0xEF,
0xEC,0xA0,0x0E,0x0F,0xED,0xE6,0x10,0xF5,0x11,0x12,0x13,0x14,0x15,0xF2,0x16,0xEE,
0x17,0x18,0x19,0x1A,0x0E,0x0F,0xED,0xA7,0x9B,0xE4,0x01,0xF9,0xF3,0xF2,0xF4,0x02,
0xE8,0x03,0xEC,0xF0,0x04,0xE1,0xE9,0x05,0x06,0x80,0xA0,0x07,0x08,0x09,0x0A,0xE5,
0xEF,0x0B,0x0C,0x0D,0x9B,0xF5,0x18,0x19,0xBA,0xAC,0xF6,0x9B,0xF0,0xE2,0x01,0xE6,
0x02,0xA7,0xAE,0xE7,0x03,0xE3,0xF5,0x04,0xED,0x05,0x06,0x07,0xEB,0x08,0x09,0xEE,
0xF2,0x0A,0xE4,0x0B,0xF9,0xEC,0x0C,0x0D,0xF4,0x80,0x0E,0xEF,0xF3,0xA0,0xE1,0x0F,
0xE9,0x10,0x11,0xE5,0x12,0x13,0x14,0x15,0x16,0x17,0x19,0x1A,0xA7,0xAC,0xBF,0xC3,
0xC8,0xE4,0xE6,0xED,0xF2,0xAE,0xEC,0xEE,0xF9,0x01,0x02,0x03,0x04,0xBA,0x05,0x9B,
0xF5,0x06,0x07,0x08,0x09,0xEB,0xF0,0x0A,0x0B,0x0C,0xE1,0xE3,0x0D,0xE8,0x0E,0x0F,
0xEF,0x10,0x11,0xF3,0x12,0xE9,0x13,0xE5,0x14,0x15,0xF4,0x16,0x17,0xA0,0x18,0x80,
0x14,0x15,0xBA,0xBF,0xE4,0xF7,0x9B,0xA7,0x01,0xEE,0x02,0x03,0x04,0xE3,0xE2,0xED,
0x05,0xF9,0x06,0xF4,0x07,0xEC,0x08,0xF5,0xF2,0x09,0xE1,0xF3,0x0A,0xEF,0x0B,0x0C,
0x0D,0xE9,0x80,0xE5,0x0E,0xA0,0x0F,0xE8,0x10,0x11,0x12,0x13,0x11,0x12,0xEB,0xFA,
0x80,0xE6,0x9B,0x01,0xA0,0x02,0x03,0xE9,0xE1,0x04,0xE4,0xF0,0xED,0xE2,0xE3,0xE7,
0xEC,0x05,0xE5,0x06,0x07,0x08,0x09,0xF4,0x0A,0x0B,0x0C,0xF3,0xEE,0x0D,0x0E,0xF2,
0x0F,0x10,0x04,0xE5,0xF3,0xEF,0x9B,0x01,0xE1,0x02,0x03,0xE9,0x0B,0x0C,0xA7,0xE2,
0xEC,0xE3,0xF2,0x01,0x9B,0x02,0x03,0x04,0xE9,0xEF,0xEE,0xE5,0xE1,0x80,0x05,0xA0,
0x06,0x07,0x08,0x09,0xF3,0x0A,0x05,0x06,0x9B,0xA0,0xE1,0xE5,0xE9,0x01,0x80,0xF0,
0x02,0xF4,0x03,0x04,0xA0,0x13,0xE3,0xAD,0xE4,0xE9,0xEE,0xEF,0xF0,0xF4,0xF6,0xA1,
0xE1,0xED,0x01,0xE2,0x02,0x03,0x04,0xA7,0x05,0x06,0xF7,0x07,0x9B,0xEC,0x08,0xE5,
0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0xF3,0x0F,0x10,0x11,0x80,0x12,0x05,0x06,0xE5,0xFA,
0xA0,0xF9,0x9B,0x01,0x80,0xE9,0x02,0xE1,0x03,0x04,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,
0x9B,0x9B,0x9B,0x9B
};
/*
 A/65b Table C7 Huffman Description Decode Tree (c) General Instruments Corp.
 The following, according to ATSC specs, is royalty free for ATSC use.

 not exact table, but a faster, more usable version of it,
 since exact table is big endian but x86 isn't.

 byte offsets of character i tree root
*/
static unsigned int huffman2bo[128] = { 
0x0000, 0x002C, 0x002E, 0x0030, 0x0032, 0x0034, 0x0036, 0x0038, 
0x003A, 0x003C, 0x003E, 0x0040, 0x0042, 0x0044, 0x0046, 0x0048, 
0x004A, 0x004C, 0x004E, 0x0050, 0x0052, 0x0054, 0x0056, 0x0058, 
0x005A, 0x005C, 0x005E, 0x0060, 0x0062, 0x0064, 0x0066, 0x0068, 
0x006A, 0x00DE, 0x00E0, 0x00EA, 0x00EC, 0x00EE, 0x00F0, 0x00F2, 
0x00F8, 0x00FA, 0x00FC, 0x00FE, 0x0100, 0x0104, 0x0116, 0x0120, 
0x0122, 0x012C, 0x0132, 0x0138, 0x013C, 0x0140, 0x0144, 0x0146, 
0x014A, 0x014C, 0x0154, 0x0156, 0x0158, 0x015A, 0x015C, 0x015E, 
0x0160, 0x0162, 0x0176, 0x0184, 0x0194, 0x01A2, 0x01B2, 0x01BA, 
0x01C8, 0x01D2, 0x01DE, 0x01EA, 0x01F2, 0x01FC, 0x0208, 0x0210, 
0x021A, 0x0228, 0x022A, 0x0234, 0x024A, 0x025A, 0x025E, 0x0264, 
0x026E, 0x0270, 0x0272, 0x0274, 0x0276, 0x0278, 0x027A, 0x027C, 
0x027E, 0x0280, 0x02B4, 0x02CE, 0x02F0, 0x031A, 0x0358, 0x036E, 
0x038E, 0x03AC, 0x03D8, 0x03E0, 0x03F4, 0x0424, 0x0440, 0x0476, 
0x04AE, 0x04CE, 0x04D0, 0x0506, 0x0534, 0x0560, 0x0586, 0x0592, 
0x05AA, 0x05B8, 0x05DC, 0x05EC, 0x05EE, 0x05F0, 0x05F2, 0x05F4, 
};

#define DESCR_COZ 1525
/* character i order-1 trees */
static unsigned char huffman2co[1526] = { 
0x14,0x15,0x9B,0xD6,0xC9,0xCF,0xD7,0xC7,0x01,0xA2,0xCE,0xCB,0x02,0x03,0xC5,0xCC,
0xC6,0xC8,0x04,0xC4,0x05,0xC2,0x06,0xC3,0xD2,0x07,0xD3,0x08,0xCA,0xD4,0x09,0xCD,
0xD0,0x0A,0xC1,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x9B,0x9B,0x9B,0x9B,
0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,
0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,
0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,
0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x38,0x39,0xAD,0xAF,0xB7,0xDA,
0xA8,0xB3,0xB5,0x01,0x02,0x9B,0xB4,0xF1,0xA2,0xD5,0xD6,0xD9,0x03,0x04,0x05,0xCF,
0x06,0xC9,0xF9,0xEA,0xEB,0xF5,0xF6,0x07,0x08,0x09,0xB2,0xC5,0xC6,0xB1,0x0A,0xEE,
0xCB,0x0B,0xD4,0x0C,0xC4,0xC8,0xD2,0x0D,0x0E,0x0F,0xC7,0xCA,0xCE,0xD0,0xD7,0x10,
0xC2,0x11,0xCC,0xEC,0xE5,0xE7,0x12,0xCD,0x13,0x14,0xC3,0x15,0x16,0x17,0xED,0x18,
0x19,0xF2,0x1A,0xD3,0x1B,0x1C,0xE4,0x1D,0xC1,0xE3,0x1E,0xE9,0xF0,0xE2,0xF7,0x1F,
0xF3,0xE6,0x20,0x21,0x22,0xE8,0xEF,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2A,0xF4,
0x2B,0x2C,0x2D,0x2E,0x2F,0xE1,0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x9B,0x9B,
0x03,0x04,0x80,0xAE,0xC8,0xD4,0x01,0x02,0x9B,0xA0,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,
0x9B,0x9B,0x02,0xF3,0xA0,0xF4,0x9B,0x01,0x9B,0x9B,0xAC,0x9B,0x9B,0x9B,0x9B,0x9B,
0x01,0xA0,0x9B,0xA2,0x07,0x08,0xE2,0xE4,0xE5,0xE6,0xA0,0xF2,0xE1,0x01,0x02,0xF3,
0xE3,0x03,0x04,0x05,0x9B,0x06,0x04,0x80,0xCA,0xD3,0xA2,0x01,0x9B,0x02,0x03,0xA0,
0x9B,0xA0,0x03,0x04,0x9B,0xB7,0xF4,0xA0,0xB0,0xF3,0x01,0x02,0xB9,0x02,0xB8,0x9B,
0xA0,0x01,0xAE,0x02,0xB6,0x9B,0x01,0xA0,0xA0,0x01,0x9B,0xB0,0xAE,0x01,0x9B,0xA0,
0xAE,0x01,0xA0,0x9B,0x9B,0x9B,0x9B,0x01,0xAC,0xAE,0x9B,0x9B,0x02,0x03,0x9B,0xA0,
0xB5,0xB6,0xB8,0x01,0x9B,0xA0,0x9B,0xA0,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0xA0,
0x9B,0x9B,0x08,0x09,0xE6,0xF5,0xF3,0xF4,0x9B,0xE4,0x01,0xED,0x02,0x03,0x04,0xF2,
0x05,0x06,0xEC,0xEE,0x07,0xA0,0x05,0x06,0x9B,0xEC,0xF5,0x01,0x02,0xE1,0xEF,0xE5,
0xE9,0xF2,0x03,0x04,0x06,0x07,0x9B,0xE9,0xF9,0xF2,0xF5,0x01,0x02,0x03,0xEC,0xEF,
0xE1,0x04,0xE8,0x05,0x05,0x06,0xF9,0xF2,0xF5,0x9B,0xE5,0xEF,0x01,0x02,0xE9,0xE1,
0x03,0x04,0x06,0x07,0xE1,0xE9,0xEE,0xF6,0xE4,0xEC,0xF3,0x01,0x02,0xF2,0x03,0x04,
0x9B,0x05,0x02,0x03,0xE5,0xEC,0x9B,0xEF,0x01,0xF2,0x05,0x06,0xF5,0xEF,0x9B,0xEC,
0xE9,0x01,0xE1,0xF2,0x02,0xE5,0x03,0x04,0x03,0x04,0x9B,0xE5,0xE9,0xF5,0xE1,0x01,
0xEF,0x02,0x04,0x05,0xA0,0xC9,0xF3,0x9B,0xAE,0xF2,0x01,0x02,0x03,0xEE,0xEF,0x05,
0x9B,0xAE,0xE9,0xE5,0x01,0xF5,0x02,0xE1,0x03,0x04,0xE5,0x03,0xE1,0xE9,0xF2,0x9B,
0x01,0x02,0x03,0x04,0x9B,0xE9,0xF5,0x01,0xE5,0x02,0xEF,0xE1,0xE1,0x05,0x9B,0xE3,
0xEF,0x01,0xF5,0xE5,0x02,0x03,0xE9,0x04,0xE5,0x03,0x9B,0xE9,0x01,0xE1,0xEF,0x02,
0x03,0x04,0xA7,0xEE,0xEC,0xF2,0xF3,0x01,0x9B,0x02,0xE1,0x06,0x9B,0xE8,0xE9,0x01,
0xF2,0xEC,0x02,0xEF,0x03,0xE5,0x04,0x05,0x9B,0x9B,0x03,0x04,0x9B,0xAE,0x01,0xE9,
0x02,0xE1,0xE5,0xEF,0x09,0x0A,0xF6,0xF9,0x01,0xAE,0xE3,0xE9,0xF5,0x9B,0xE5,0xEF,
0x02,0x03,0xE1,0x04,0xE8,0x05,0x06,0xF4,0x07,0x08,0xE8,0x07,0xE5,0xF7,0xD6,0xE1,
0x9B,0xE9,0xF2,0x01,0x02,0x03,0x04,0xEF,0x05,0x06,0xAE,0x01,0x9B,0xEE,0xE9,0x02,
0xE5,0x9B,0xA0,0x01,0x03,0x04,0x9B,0xE8,0xE5,0xE1,0xEF,0x01,0xE9,0x02,0x9B,0x9B,
0x9B,0xEF,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,
0x18,0x19,0xE8,0xEF,0xF8,0x9B,0xA7,0xF7,0xFA,0x01,0x02,0x03,0x04,0xE5,0xAE,0x05,
0xE6,0xE2,0x06,0xF6,0xEB,0xF5,0xE9,0x07,0xF0,0xF9,0xE7,0x08,0x09,0xE4,0x0A,0xE3,
0x0B,0xED,0x0C,0xF3,0x0D,0x0E,0x0F,0xEC,0x10,0xF4,0x11,0x12,0xF2,0xA0,0x13,0x14,
0x15,0xEE,0x16,0x17,0x0B,0x0C,0xE4,0xF3,0x9B,0xAE,0xE2,0x01,0x02,0x03,0xEC,0xA0,
0x04,0xE9,0xF2,0xF5,0x05,0xF9,0xE1,0x06,0xEF,0x07,0xE5,0x08,0x09,0x0A,0x0F,0x10,
0xF1,0xAE,0xC4,0xF9,0xAC,0x01,0xE3,0x02,0x9B,0xF2,0x03,0x04,0xA0,0xEC,0xF5,0x05,
0x06,0xE9,0x07,0xEB,0x08,0xF4,0x09,0xE5,0x0A,0xEF,0xE1,0xE8,0x0B,0x0C,0x0D,0x0E,
0x13,0x14,0xA7,0xBB,0xE6,0xED,0xF7,0xE7,0xF6,0x01,0x02,0x9B,0xEE,0x03,0x04,0xEC,
0x05,0xF5,0x06,0xAC,0xE4,0xF9,0xF2,0x07,0x08,0x09,0xAE,0x0A,0xEF,0x0B,0xE1,0xF3,
0x0C,0xE9,0x0D,0x0E,0x0F,0x10,0xE5,0x11,0x12,0xA0,0x1D,0x1E,0xA9,0xE8,0xF5,0x9B,
0x01,0xAD,0xBB,0xEB,0xFA,0x02,0xA7,0xE6,0xE2,0xE7,0x03,0x04,0x05,0x06,0xE9,0xF8,
0x07,0xAC,0xEF,0xF0,0x08,0xED,0xF6,0xF9,0x09,0xF7,0x0A,0x0B,0xAE,0x0C,0xE3,0x0D,
0xE5,0xF4,0x0E,0x0F,0xE4,0x10,0xEC,0x11,0xE1,0x12,0x13,0x14,0x15,0x16,0xEE,0xF3,
0x17,0x18,0xF2,0xA0,0x19,0x1A,0x1B,0x1C,0x09,0x0A,0xAE,0x9B,0xEC,0x01,0xF5,0x02,
0xF4,0xE6,0x03,0xE1,0xE5,0xE9,0x04,0xF2,0xEF,0x05,0x06,0x07,0xA0,0x08,0x0E,0x0F,
0xAD,0xE7,0x9B,0xA7,0xF9,0x01,0xEC,0x02,0xAC,0xF2,0x03,0xAE,0xF3,0xF5,0x04,0x05,
0xEF,0x06,0x07,0xE9,0xE1,0x08,0x09,0xE8,0x0A,0x0B,0xE5,0x0C,0xA0,0x0D,0x0D,0x0E,
0xA7,0xAC,0xF3,0xAD,0x01,0x02,0x9B,0xF9,0xF5,0xAE,0x03,0xEE,0x04,0xF2,0x05,0x06,
0xF4,0x07,0x08,0x09,0xEF,0xE1,0xA0,0x0A,0xE9,0x0B,0x0C,0xE5,0x14,0x15,0xAC,0xE2,
0xF8,0x9B,0xAE,0xFA,0x01,0xEB,0x02,0xA0,0x03,0x04,0xF0,0x05,0x06,0xE6,0xF6,0x07,
0xE4,0xED,0xE7,0x08,0xE1,0xEF,0xF2,0x09,0x0A,0x0B,0xEC,0x0C,0xE5,0xE3,0x0D,0xF4,
0x0E,0xF3,0x0F,0x10,0x11,0xEE,0x12,0x13,0x03,0xEF,0x9B,0xE1,0xE5,0xF5,0x01,0x02,
0x08,0x09,0xEC,0xF9,0xA7,0xEE,0x01,0xAC,0x9B,0xAE,0x02,0x03,0x04,0xF3,0x05,0xE9,
0x06,0xA0,0x07,0xE5,0x16,0x17,0xA7,0xAD,0xEE,0xE3,0xEB,0xF2,0x9B,0xE2,0x01,0x02,
0xF5,0x03,0xF4,0xAC,0x04,0x05,0xE6,0xED,0xF6,0x06,0xAE,0xF0,0x07,0x08,0xF3,0x09,
0x0A,0xE4,0x0B,0x0C,0xF9,0x0D,0xEF,0x0E,0xE1,0x0F,0x10,0xE9,0xEC,0x11,0xA0,0xE5,
0x12,0x13,0x14,0x15,0x0C,0x0D,0xA7,0xBB,0x9B,0x01,0xF9,0xAE,0xE2,0x02,0xED,0xF3,
0x03,0xF5,0xEF,0xF0,0x04,0x05,0xE9,0x06,0x07,0x08,0x09,0xA0,0xE1,0xE5,0x0A,0x0B,
0x19,0x1A,0xAD,0xBB,0xE2,0xEA,0xED,0xF2,0xFA,0xE6,0xEC,0x01,0x02,0x03,0x9B,0xF5,
0x04,0xA7,0xF6,0xF9,0x05,0x06,0xEB,0xEF,0x07,0x08,0x09,0x0A,0xAC,0x0B,0x0C,0xE3,
0xAE,0x0D,0xEE,0xE9,0x0E,0xE1,0x0F,0xF3,0x10,0x11,0xF4,0x12,0xE7,0xE5,0x13,0x14,
0xE4,0x15,0x16,0x17,0xA0,0x18,0x1A,0x1B,0xC2,0x9B,0xAD,0xAC,0xF8,0x01,0xAE,0x02,
0x03,0xE5,0xE7,0xE8,0xF9,0xE9,0xEB,0x04,0xE3,0xE1,0x05,0xF6,0x06,0xE4,0x07,0xE2,
0xF0,0x08,0x09,0xF3,0xF4,0xF7,0xEF,0x0A,0x0B,0x0C,0x0D,0xEC,0x0E,0x0F,0x10,0xF5,
0xED,0x11,0xE6,0xA0,0x12,0xF2,0x13,0x14,0x15,0xEE,0x16,0x17,0x18,0x19,0x0E,0x0F,
0xAD,0xED,0xF9,0x9B,0xAE,0x01,0xF3,0x02,0x03,0xF5,0xF4,0xF0,0x04,0xEF,0x05,0xE9,
0x06,0xE8,0xA0,0xE1,0xEC,0x07,0xF2,0x08,0xE5,0x09,0x0A,0x0B,0x0C,0x0D,0x9B,0xF5,
0x19,0x1A,0xA9,0xBB,0xF6,0xE6,0x01,0x9B,0xAD,0xE2,0xF0,0x02,0xA7,0x03,0x04,0x05,
0xF5,0xE3,0xAC,0xE7,0xF2,0x06,0xEB,0x07,0xEC,0xED,0xEE,0xF9,0x08,0xAE,0x09,0x0A,
0xE4,0x0B,0x0C,0xF4,0x0D,0xF3,0x0E,0x0F,0x10,0xE1,0xEF,0x11,0xE9,0x12,0x13,0xE5,
0x14,0xA0,0x15,0x16,0x17,0x18,0xA0,0x16,0xA2,0xA7,0xE2,0xEB,0xED,0xEE,0x9B,0xF7,
0x01,0x02,0x03,0xBB,0xF9,0xF0,0x04,0x05,0xEC,0x06,0x07,0x08,0xF5,0xE1,0x09,0xAC,
0xE3,0x0A,0xE8,0x0B,0xE9,0x0C,0xEF,0xF3,0xAE,0x0D,0x0E,0xE5,0x0F,0x10,0x11,0xF4,
0x12,0x13,0x14,0x15,0x14,0x15,0xBB,0xE2,0xAD,0xED,0x01,0x9B,0xA7,0xE3,0xAC,0xEC,
0xEE,0x02,0xF7,0x03,0x04,0xF9,0x05,0x06,0x07,0x08,0xF4,0xAE,0xF5,0x09,0x0A,0xF2,
0xE1,0xF3,0x0B,0x0C,0x0D,0xE9,0x0E,0x0F,0xEF,0xE5,0x10,0xA0,0xE8,0x11,0x12,0x13,
0x11,0x12,0xEF,0xF6,0x9B,0xEB,0xF9,0x01,0xA0,0xE2,0x02,0xE1,0x03,0xED,0x04,0xE3,
0xE9,0x05,0xE4,0xE5,0xE7,0x06,0xEC,0xF0,0x07,0x08,0x09,0x0A,0x0B,0xF3,0x0C,0xF4,
0xEE,0x0D,0xF2,0x0E,0x0F,0x10,0x05,0xE5,0xF3,0xF9,0x9B,0x01,0xEF,0x02,0x03,0xE1,
0x04,0xE9,0x0A,0x0B,0xAE,0x9B,0xEC,0xED,0x01,0x02,0xF3,0xEE,0xF2,0x03,0xE5,0x04,
0xE8,0xA0,0xE1,0x05,0xEF,0x06,0x07,0x08,0xE9,0x09,0x05,0x06,0xA0,0xAC,0xAD,0xF4,
0xE9,0x01,0x02,0xE1,0xE5,0x03,0x9B,0x04,0x11,0xA0,0xBF,0xE1,0xE2,0xE6,0xED,0xE4,
0xE9,0xF7,0xA7,0x01,0x02,0xBB,0x03,0x04,0xEC,0x05,0x9B,0xEE,0x06,0xEF,0x07,0xAC,
0xE5,0xF3,0x08,0x09,0x0A,0xAE,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x06,0x07,0xA0,0xAE,
0xE1,0xE5,0xEC,0xFA,0x9B,0xEF,0xE9,0x01,0x02,0x03,0x04,0x05,0x9B,0x9B,0x9B,0x9B,
0x9B,0x9B,0x9B,0x9B,0x9B,0x9B,
};
/************************************************************** ATSC HUFFMAN */

/***************************** globals end **********************************/


#include "common.h"

#ifdef USE_BUFFER
#include "buffer.h"
static buf_t *in_buf;
static buf_t *es_buf;
static buf_t *out_buf;
#endif

/********************************************************************** CRC32
* from ffmpeg
*/
static
unsigned int
calc_crc32( unsigned char *data, unsigned int len )
{
    unsigned int i;
    unsigned int crc = 0xffffffff;
    
    if (len > 4096) return ~0;

    for (i=0; i<len; i++)
        crc = (crc << 8) ^ crc_lut[ 0xFF & ((crc >> 24) ^ *data++) ];
    
    return crc;
}
/******************************************************************** CRC32 */


/************************************************************** HUFFMAN DECODE
* huffman title and description text decode.
* d destination needs 512 bytes in case source is 255 bytes
*/
static
void
huffman_decode( unsigned char *d, unsigned char *s, int dlen, int slen, int comp)
{
    unsigned char p, c, o, r;
    unsigned int *bo;
    unsigned char *co;
    unsigned int to, zo, z;
    unsigned int i, j, k;
    unsigned char b;
    unsigned char *t;

    t = d;
//    dprintf2( stdout, "\n        Huffman L%02X C%02X\n", slen, comp );

    // sanity check
    if ((NULL == d) || (NULL == s))  { fprintf( stdout, "bad null\n"); return; }
//    if ((slen < 1)  || (slen > 255)) { fprintf( stdout, "bad slen\n"); return; }
//    if ((dlen < 1)  || (dlen > 255)) { fprintf( stdout, "bad dlen\n"); return; }
    if ((comp < 1)  || (comp > 2))
	{ fprintf( stdout, "bad compression mode %d\n", comp); return; }    

    p = c = 0; // first char assumed to be NUL or term char

    bo = huffman1bo;		// byte offset of char p tree root
    co = huffman1co;		// char p order-1 tree
    z = TITLE_COZ;		// table limit
    
    if (comp == 2) {
	bo = huffman2bo;
	co = huffman2co;
	z = DESCR_COZ;
    }

    // should be pointing to correct tables now
    o = 0;
    for (i=0; i < (slen<<3); i++)
    {
//	if (0 == (i%8)) dprintf2( stdout, "\ns[%d] = %02X\n", i>>3, s[i>>3] );
	// get tree offset for char p from order-1 tree byte offset table
	if (p > 127) { /* fault detection, don't stray outside table */
	    fprintf( stdout, "BAD Huffman tree offset %02X\n", p);
	    return;
	}
	to = bo[ p ];
	// direction in tree from bit in compressed string
	// bit sets o to left(0) or right(1) for next branch or leaf
	b = s[ i >> 3 ] & (1 << (~i & 7));
	if (b != 0) b = 1; // force comparison result in b0
	// minimum of two linked-list lookups for shortest first order entry
	// such as common following letters, but most will be multiple lookups

	zo = to + (o<<1) + b;
	if (zo > z) { /* fault detection, don't stray outside table */
	    fprintf( stdout, "BAD Huffman branch offset %04X\n", zo);	    
	    return;
	}
	// first entry has tree lookup to first order left/right choice tree
	o = co[ zo ]; // tree root offset is anchor for branches
	r = co[ zo ]; // o has branch, r has leaf or branch
	
#ifdef DEMOTH
	if (0 == (128 & o)) {
	    fprintf(stdout,	"BRANCH s[%02X].%d %d pchr %02X toff %03X "
			"%s "
			"delta %d+%02X<<1 = [%02X]\n",
			i >> 3, ~i & 7, b, p, to,
			(b==0)?"LEFT ":"RIGHT",
			b, o, r );
	}
#endif

	if (0 != (128 & o) )
	{
	    c = 0x7F & o;

#ifdef DEMOTH
	    if ( c != 27)
		fprintf( stdout,"LEAF   s[%02X].%d %d pchr %02X toff %03X "
			    "chr %02X [%c] ",
			    i >> 3, ~i & 7, b, p, to, c, (c<32)?' ':c);
#endif
	    if ( c == 27 )
	    {
		// handle Escape to 8 bit mode
		i++; // point to msb of bytes
		j = i & 7;
		k = 8 - j;
		// get current byte
		c = s[ i >> 3 ];
		// shift needed?
		if (0 != j) {
		    c <<= j;
		    b = s[ (i >> 3) + 1];
		    b >>= k;
		    c |= b;
		}
		i += 7; // skip past lsb of byte

#ifdef DEMOTH
		fprintf( stdout, "ESCAPE s[%02X].%d     j %d     k %d    chr %02X [%c] ",
			i>>3, ~i & 7, j, k, c, c );
#endif

	    }

	    p = c; // c leaf becomes new index for order-1 tree root offset
	    o = 0; // clear offset to order-1 tree
	    *t = c;
	    dlen--;
	    if (dlen < 1) {
		*d = 0;
		break;
	    } // out of space gets nul term

	    t++; // else move to next char 

	    // nul term stops loop
	    if ( c == 0 ) {
#ifdef DEMOTH
		fprintf( stdout, "TERM\n");
#endif
		break;
	    }
#ifdef DEMOTH
	    fprintf( stdout, "\n");
#endif
	}
    }
#ifdef DEMOTH
    fprintf( stdout, "Huffman done: [%s]\n", d );
#endif
}

/*
// atsc picture user data embedded into MPEG video stream
// has cc data and bar data only according to a53c_amend1
// bar data has letter/pillar box settings, but haven't seen bar data yet.
// cc data is seen in at least some, if not most, streams.
*/
static
void
test_ap_user_data( unsigned char *p )
{
    if ((3 != p[4]) && (6 != p[4])) {
	mprintf4( stdout, "ATSC user data type %02X unknown\n", p[4]);
	return; // other types ?
    }

    mprintf4( stdout, "ATSC User Data ID [%c%c%c%c] Type %02X: ",
		p[0], p[1], p[2], p[3], p[4]);

    /* cc data does exist in some streams. */
    /* looks like 24bits in a list terminated by 0x0000FF */
    if (3 == p[4]) { 
	unsigned int i, b;
	mprintf4( stdout, "Closed Caption\n     ");
	p += 5;
	for (i = 0; i<63; ) { /* 63 bytes, 21 triplets */
	    b = (p[i]<<16) | (p[i+1]<<8) | p[i+2];

	    /* start code? */
	    if (1 == b) break;
	    mprintf4( stdout, "%06X ", b );
	    i += 3;
	    if (0 == (i%8)) mprintf4( stdout, "\n     " );
	}
	mprintf4( stdout, "\n" );
    }

    // letter/pillar box settings is in bar data if exists
    if (6 == p[4]) {
	mprintf4( stdout, "Bar Data\n" );
    }
}


static
void
test_mpeg2_group_of_pictures( unsigned char *r )
{
    unsigned char df, h, m, s, p, cg, bl;

    df = h  = m = s = p = cg = bl = 0;

    /* did atscap vc cap subvert timecode into sequence start counter? */
    if ( 0xFF == r[0] ) { 
	int gopx = 0;
	/* yes */
	gopx = 0xFFFF & ((r[1]<<8) | r[2]);
	mprintf4( stdout, "GOPX: %5d\n", gopx);
	return;
    }    

    /* no, full cap or different origin than atscap/pchdtvr */
    if (r[1] & 8) {
	df = r[0] >> 7;				// 1 bit  drop flag
	h = 0x1F & (r[0] >> 2);			// 5 bits hours 
	m = 0x3F & ((r[0] << 4) | (r[1] >> 4));	// 6 bits minutes
	s = 0x3F & ((r[1] << 3) | (r[2] >> 5));	// 6 bits seconds
	p = 0x3F & ((r[2] << 1) | (r[3] >> 7));

	cg = 1 & (r[3]>>6);
	bl = 1 & (r[3]>>5);
    }
    mprintf4( stdout, " TC HMSP: %02d:%02d:%02d:%02d\n",
		h,m,s,p);
    mprintf4( stdout, "    Drop flag %d Closed GOP %d Broken Link %d\n",
	    df, cg, bl);
}

/* returns picture type index */
static
int
test_mpeg2_picture( unsigned char *r )
{
    unsigned short tr, vbvd;
    unsigned char pct;			/* picture type */
    unsigned char *pt;
    unsigned int br;

    br = 0;
    pt = "Reserved"; // picture text
    tr = (r[0]<<2) | (r[1]>>6); // 10 bits temporal reference
    pct = 7 & (r[1]>>3); // picture type
    vbvd = ((7 & r[1])<<13) | (r[2]<<5) | (r[3]>>3); // vbv delta
    if (0 == pct) pt = "Forbidden";

    /* is Intra frame ? */
    if (1 == pct) {

	memset( ipb, 0, sizeof(ipb));
	ipbx = 0;
	ipb[ipbx++] = 'I';

	pt = "I";
	// check delta to compute bit rate
	// 2 I-frames per second, 8 bits in 188 byte packets
	br = 3008 * (pkt.count - pkt.pcount); // 2 * 8 * 188 * pkt diff
	sumrate += br;
	iframe++;
	iframen++;

	// reset to avoid overflow
	if (sumrate > 1500000000) {
	    sumrate = sumrate / iframe;
	    iframe = 1;
	}
	// set for next delta
	pkt.pcount = pkt.count;
    }
    if (2 == pct) {
	ipb[ipbx++] = 'P';
	pt = "P";
    }
    if (3 == pct) {
	ipb[ipbx++] = 'B';
	pt = "B";
    }

    if (4 == pct) {
	pt = "D?"; // mpeg1 only
    }

    mprintf4( stdout, " %s  Temporal %2u VBVd %5u\n", pt, tr, vbvd);

    if (1 == pct) {
	unsigned char brt[32];
	lltoasc( brt, br ); // dont forget to free()
	mprintf4( stdout, "    I Frame # %5d Rate is %s bits/second, avg %d\n",
		iframen, brt, sumrate/iframe);
    }

    return pct;
}

/* tally up the slices */
static
void
test_mpeg2_slice( unsigned char *r )
{
	/* does nothing for now */
	return;
}

/* PES Packetized Elementary Stream info, where are found PTS and DTS.
   Presentation Time Stamp is when data is to be displayed.
   Deccoder Time Stamp is when data is to be ready for display.
   These are related to PCR in the AFC */
static
void
test_mpeg2_video_pes( unsigned char *p )
{
    unsigned char	*r, pscf, ppri, daif, crif, oocf,
			ptsf, escf, esrf, dtmf, acif, pcrf, pexf,
			peshl, d, h, m, s;
    unsigned short	ppl; // spec needs to make this 32 bits and use it
    
    unsigned long long pts;
    unsigned long long dts;
    unsigned int rts;

    ppl = (p[4]<<8) | p[5]; // always 0?

    r = p + 6;		// skip header ID and always 0 len, already shown
    pscf = 3 & (*r>>4);	// r[6] b5:4
    ppri = 1 & (*r>>3);
    daif = 1 & (*r>>2);
    crif = 1 & (*r>>1);
    oocf = 1 & *r;

    mprintf4( stdout, "    PES plen %04X, dlen %02X, flags %04X:\n     ",
		ppl, r[2], (r[0]<<8) | r[1]);

    if ( 0 != pscf ) mprintf4( stdout, "Scramble%d, ", pscf);
    if ( 0 != ppri ) mprintf4( stdout, "Priority, ");
    if ( 0 != daif ) mprintf4( stdout, "Align, "   );
    if ( 0 != crif ) mprintf4( stdout, "(c), "     );
    if ( 0 != oocf ) mprintf4( stdout, "Original, ");
//    mprintf4( stdout, "\n");

    r++;
    ptsf = 3 & (*r>>6);	// r[7] b7:6
    escf = 1 & (*r>>5);
    esrf = 1 & (*r>>4);
    dtmf = 1 & (*r>>3);
    acif = 1 & (*r>>2);
    pcrf = 1 & (*r>>1);
    pexf = 1 & *r;

    if (2 == ptsf) mprintf4( stdout, "PTS "    );
    if (3 == ptsf) mprintf4( stdout, "PTS DTS ");

    if (0 != escf) mprintf4( stdout, "ESCR, "  );
    if (0 != esrf) mprintf4( stdout, "ESRate, ");
    if (0 != dtmf) mprintf4( stdout, "DSM, "   );
    if (0 != acif) mprintf4( stdout, "(c)+, "  );
    if (0 != pcrf) mprintf4( stdout, "CRC, "   );
    if (0 != pexf) mprintf4( stdout, "EXT, "   );
    mprintf4( stdout, "\n");

    r++;
    peshl = *r;

    r++;

    // pts only?
    if (2 == ptsf) {
	// check pts marker bits
	if ( (0x21 == (0xF1 & r[0])) && (1 & r[2]) && (1 & r[4]) ) {
	    pts = 0;
	    pts |= (7 & (*r >> 1)) << 30;	// 32..30
	    r++;
	    pts |= *r << 22;			// 29..22
	    r++;
	    pts |= (*r>>1) << 15;		// 21..15
	    r++;
	    pts |= *r << 7;			// 14..7
	    r++;
	    pts |= *r>>1;			// 6..0
	    r++;
	    pts &= 0x1FFFFFFFFLL;

	    rts = pts / 90000;
	    s = rts % 60;
	    m = ( rts / 60 ) % 60;
	    h = ( rts / 3600) % 24;
	    d = ( rts / 86400);
	    mprintf4( stdout, "      PTS %01d:%02u:%02u:%02u.%llu\n",
			 d, h, m, s, pts % 90000 );
	    
	}
    }

    // pts and dts?
    if (3 == ptsf) {
	// check pts marker bits
	if ( (0x31 == (0xF1 & r[0])) && (1 & r[2]) && (1 & r[4]) ) {
	    pts = 0;
	    pts |= (7 & (*r >> 1)) << 30;	// 32..30
	    r++;
	    pts |= *r << 22;			// 29..22
	    r++;
	    pts |= (*r>>1) << 15;		// 21..15
	    r++;
	    pts |= *r << 7;			// 14..7
	    r++;
	    pts |= *r>>1;			// 6..0
	    r++;
	    pts &= 0x1FFFFFFFFLL;

	    rts = pts / 90000;
	    s = rts % 60;
	    m = ( rts / 60 ) % 60;
	    h = ( rts / 3600) % 24;
	    d = ( rts / 86400);
	    mprintf4( stdout, "      PTS %01d:%02u:%02u:%02u.%llu",
			 d, h, m, s, pts % 90000 );

	    // check dts marker bits
	    if ( (0x11 == (0xF1 & r[0])) && (1 & r[2]) && (1 & r[4]) ) {
		dts = 0;
		dts |= (7 & (*r >> 1)) << 30;	// 32..30
		r++;
		dts |= *r << 22;		// 29..22
		r++;
		dts |= (*r>>1) << 15;		// 21..15
		r++;
		dts |= *r << 7;			// 14..7
		r++;
		dts |= *r>>1;			// 6..0
		r++;
		dts &= 0x1FFFFFFFFLL;

		rts = dts / 90000;
		s = rts % 60;
		m = ( rts / 60 ) % 60;
		h = ( rts / 3600) % 24;
		d = ( rts / 86400);
		mprintf4( stdout, "  DTS %01d:%02u:%02u:%02u.%llu",
			 d, h, m, s, dts % 90000 );

	    }

	    mprintf4( stdout, "\n");
	}
    }
    mprintf4( stdout, "\n");
}

/* tests up to vid.payoff only, p is payload */
static
void
test_mpeg2_video_seq( unsigned char *p, int len )
{
    unsigned int b, i;
    b = 0xFFFFFFFF;

//    for (i = 0; i < vid.payoff; i++ )
    for (i=0; i < len; i++) /* only first packet, after AFC to save cycles */
    {
	unsigned char pct;
	b = (b<<8) | p[i];
	switch (b) {

	    /* Sequence Start code means this payload has:
		[optional GOP] and I-Frame 
	    */
	    case 0x1B3:
//		fprintf( stdout, "Sequence header @ %llX\n", bytes_in-188);

		/* what about pkt.vid * 188 ? not as fast... */
		sequences[ sequence_idx++ ] = bytes_in - 188;

/* NOTE, if running out of sequences, probably running out of frames, too */
		if (sequence_idx >= SEQUENCE_MAX) {
		    fprintf( stderr,
			    "\n FATAL: sequence_idx exceeds SEQUENCE_MAX.\n"
			    "Recompile with larger FRAME_MAX.\n");
		    exit(1);
		}

/* in case quantizer table(s) (+256 bytes possible) pork up first packet,
   bump frame counter for Intra frame type and consider it counted,
   because we don't want to spend a lot of cycles looking for it.
*/
		frames[ frame_idx ].pct = 1;
		frames[ frame_idx ].vpn = pkt.vid;
		frame_idx++;
		if (frame_idx >= FRAME_MAX) {
		    fprintf( stderr,
			    "\n FATAL: frame_idx exceeds FRAME_MAX.\n"
			    "Recompile with larger FRAME_MAX.\n");
		    exit(1);
		}
		break;

/* some stations send a few quantizer tables in the sequence header.
   have seen where this test will fail on good Intra frame because it's not
   in 1st packet, or only has a few bytes of start code in 1st packet.
*/
	    /* Picture Start code means this is I P or B frame */
	    case 0x100:
		pct = 7 & (p[i+2] >> 3);

/* Intra frame treated above in case too far to find in first packet.
   P and B frames won't have quantizer table(s) porking up the first packet,
   so ok to check for them.
 */
		if (1 == pct)
		    return;
/* save cycles, Intra frame already counted */

		frames[ frame_idx ].pct = pct;
		frames[ frame_idx ].vpn = pkt.vid;
		frame_idx++;
		if (frame_idx >= FRAME_MAX) {
		    fprintf( stderr,
			    "\n FATAL: frame_idx exceeds FRAME_MAX.\n"
			    "Recompile with larger FRAME_MAX.\n");
		    exit(1);
		}

/* save cycles by not looking past picture start */
		return;


		break;

	    default:
		break;
	}
    }
}

/**************************************************** MPEG2 ELEMENTARY VIDEO */
/* this is full parse/display of video es, truncated by arg_mdump bits */
/* NOTE: vid.payload needs to be large enough to handle the largest I-frames,
         but small enough to fit in CPU cache for performance reasons. */
static
void
test_mpeg2_video_es ( void )
{
    int i, j, k;
    unsigned int b;
    unsigned char *frt, *art, *xidt, *p, c;
    unsigned short hz, vz, vbvz;
    unsigned int brv;
    unsigned char ari, frc, cpf, iqm, niqm, xid, qmf;
    unsigned long long iqr, niqr;

    unsigned char sn, sm, sp; /* slice number, slice max Vsize/16 */
    unsigned char sc[176]; /* slice checklist up to slice 0xB0 */

    char *ptt[8] = { "Forbidden","I","P","B","D?","","","" };
    int pti = 0;

    sn = sm = sp = 0;
    memset( sc, 0, sizeof(sc));
/*    unsigned long long ciqr, cniqr; / / quantizer matrix row value */

    b = 0xFFFFFFFF; /* zeros could give false start indication */
    p = vid.payload;
    
    frt = "*unparsed*";

    k = vid.payoff;

/* speed up header search, but needs to be 256 for quantizer table max,
   plus anticipated size of other headers, maybe 384 is enough.
   otherwise, don't call this at all to speed it up even more.
 */
    for (i = 0; i < k; i++) {
	c = p[i];
	b = (b << 8) |  p[i];
	/* valid start code? */
        if (0x00000100 == (0xFFFFFF00 & b)) {

//	    mprintf4( stdout, "   STC %09llX: %08X ", (ppsbytes_in-184) + (i-3), b);
	    
	    /* Sequence Header */
	    if (0xB3 == c) {
		i++;
		mprintf4( stdout,
			 "Sequence Start # %5d [below has last IPB]\n",
			    sequence_idx-1 );

		if (0 == (0x20 & p[i+6])) {
		    mprintf4( stdout, "Marker bit not set, skipping\n");
		    continue;
		}

		ipb[64] = 0;
		if (0 != ipb[0])
		    mprintf8( stdout, "SEQ # %5d [%-64s]\n",
			sequence_idx-2, ipb );

		/* mprintf4( stdout, "\n"); */

		hz = 0xFFF & ( (p[i+0] << 4) | (p[i+1] >> 4) ); // 3 nybs
		vz = 0xFFF & ( (p[i+1] << 8) | p[i+2] ); // 3 nybs

		sm = vz / 16; /* slice max is vsize / 16 */

		ari = p[i+3] >> 4; // hi nyb
		art = "Reserved";
		if (0 == ari) art = "Forbidden";
		if (1 == ari) art = "Square";
		if (2 == ari) art = "4:3";
		if (3 == ari) art = "16:9";
		if (4 == ari) art = "2.21:1";
		

		frc = 15 & p[i+3]; // low nyb
		frt = "Reserved";
		if (0 == frc) frt = "Forbidden";
		if (1 == frc) frt = "23.976 FPS";
		if (2 == frc) frt = "24 FPS";
		if (3 == frc) frt = "25 FPS";
		if (4 == frc) frt = "29.97 FPS";
		if (5 == frc) frt = "30 FPS";
		if (6 == frc) frt = "50 FPS";
		if (7 == frc) frt = "59.94 FPS";
		if (8 == frc) frt = "60 FPS";
		
		// sanity limit silly shifter tricks for 18 bit number
		brv = 0x3FFFF & ( (p[i+4]<<10) | (p[i+5]<<2) | (p[i+6]>>6) );
		// more oddball offsets
		vbvz = 0x3FF & ( (p[i+6]<< 5) | ( p[i+7]>> 3) );
		// quantizer matrices load offset 1 bit left
		// spec looks to allow intra and/or non intra load
		// so last bit of intra for non-intra flag makes it even
		cpf = 1 & (p[i+7] >> 2); // constrained parameters flag

		i += 7; // skip to byte with first bit of quantizer table
		mprintf4( stdout,
			"    Horizontal %u Vertical %u Aspect %s Frame Rate %s\n"
			"    Bit Rate %u VBVz %u CPF %u\n",
			hz, vz, art, frt, brv * 400, vbvz * 2048, cpf );

		iqm = 1 & (p[i] >> 1); // intra quantizer matrix present
		// may have bit shifts wrong for both of these
		if (1 == iqm) {
		    mprintf4( stdout, "    Intra Quantizer Matrix\n");
		    // don't care about the values particularly
		    // but could show what a pain in the ass it is
		    for (j=0; j < 8; j++) {
			iqr  = (long long)(1 & p[i])<< 63; // 63
			iqr |= (long long)p[i+1]<<55; // 62 61 60 59 . 58 57 56 55
			iqr |= (long long)p[i+2]<<47; // 54 53 52 51 . 50 49 48 47
			iqr |= (long long)p[i+3]<<39; // 46 45 44 43 . 42 41 40 39
			iqr |= (long long)p[i+4]<<31; // 38 37 36 35 . 34 33 32 31

			iqr |= (long long)p[i+5]<<23; // 30 29 28 27 . 26 25 24 23
			iqr |= (long long)p[i+6]<<15; // 22 21 20 19 . 18 19 16 15
			iqr |= (long long)p[i+7]<<7;  // 14 13 12 11 . 10  9  8  7
			iqr |= (long long)p[i+8]>>1;  //  6  5  4  3    2  1  0

			mprintf4( stdout, "      %016llX\n", iqr);
			i += 8;
		    }		    
		}		    

		niqm = 1 & p[i];
		if (1 == niqm) {
		    mprintf4( stdout, "     Non-intra Quantizer Matrix\n");
		    // don't care about the values particularly
		    // this one is easier because the bits line up
		    for (j=0; j < 8; j++) {
			niqr  = (long long)(1 & p[i]) << 56;	// 63-56
			niqr |= (long long)p[i+1] << 48;   	// 55-48
			niqr |= (long long)p[i+2] << 40;	// 47-40
			niqr |= (long long)p[i+3] << 32;	// 39-32
			niqr |= (long long)p[i+4] << 24;	// 31-24
			niqr |= (long long)p[i+5] << 16;	// 23-16
			niqr |= (long long)p[i+6] << 8;		// 15-8
			niqr |= (long long)p[i+7];		// 7-0
			mprintf4( stdout, "       %016llX\n", niqr);
			i += 8;
		    }		    
		}; // p was adjusted above
		
		if (0 == (iqm | niqm)) {
		    mprintf4( stdout, "     ERROR: No Quantizer Matrix\n");
		}
		// it's a bit convoluted after this, see mscan.c
		continue; // find next start code
	    }

	    // Extension Start Code for Sequence or Picture extensions
	    if (0xB5 == c)
	    {
		xidt = "Reserved";
		xid = p[i+1]>>4; // 1-A see 13818-2 table 6-2
		mprintf4( stdout, "Extension ID (%1X): ", xid);

		if (1 == xid)
		{
		    unsigned char prid, lvid, vbze;	  // 3,4,8 bits
		    unsigned char cf, hze, vze, frn, frd; // 2 bits
		    unsigned short bre;			  // 12 bits
		    unsigned char lod, ps;		  // 1 bit
		    unsigned char *cft, *prit, *lvit;	  // chroma profile lvl
		    i++;
		    xidt = "Sequence";
		    mprintf4( stdout, "%s\n", xidt);
		    if (1 & p[i+3]) {
			prid = 0x7 & p[i];
			prit = "Reserved";
			if (1 == prid) prit = "High";
			if (2 == prid) prit = "Spatially Scalable";
			if (3 == prid) prit = "SNR Scalable";
			if (4 == prid) prit = "Main";
			if (5 == prid) prit = "Simple";

			lvid = p[i+1] >> 4;
			lvit = "Reserved";
			if ( 4 == lvid) lvit = "High";
			if ( 6 == lvid) lvit = "High 1440";
			if ( 8 == lvid) lvit = "Main";
			if (10 == lvid) lvit = "Low";
			
			ps = 1 & (p[i+1] >> 3); // 1 bit progressive sequence

			cft = "Reserved";
			cf = 3 & (p[i+1] >> 1); // 2 bits chroma format
			if (1 == cf) cft = "4:2:0";
			if (2 == cf) cft = "4:2:2";
			if (3 == cf) cft = "4:4:4";
			
			hze = ((1 & p[i+1]) << 1) | (p[i+2] >> 7);
			vze = 3 & (p[i+2] >> 5);
			bre = 0xFFF & ( (p[i+2] << 7) | (p[i+3] >> 1) );
			vbze = p[i+4];
			lod = p[i+5] >> 7;
			frn = 3 & p[i+5] >> 5;
			frd = 31 & p[i+5];
			mprintf4( stdout, "    Profile %s, Level %s, Progressive %0X, Chroma %s\n",
				prit, lvit, ps, cft);
			mprintf4( stdout, "    HX %0X, VX %0X, BRX %03X, VBVZX %02X Low delay %0X FRXn %0X, FRXd %02X\n",
				hze, vze, bre, vbze, lod, frn, frd);
			i += 5;
		    }
		    continue;
		}

		if (2 == xid)
		{
		    unsigned char vf, cd, cp, tc, mc;
		    unsigned short dhz, dvz;
		    unsigned char *vft;
		    xidt = "Sequence Display";
		    i++;

		    cp = tc = mc = 0;

		    vf = 7 & (p[i]>>1);
		    vft = "Reserved";
		    if (0 == vf) vft = "Component";
		    if (1 == vf) vft = "PAL";
		    if (2 == vf) vft = "NTSC";
		    if (3 == vf) vft = "SECAM";
		    if (4 == vf) vft = "MAC";
		    if (5 == vf) vft = "Unspecified";
		    mprintf4( stdout, "%s\n", xidt);
		    mprintf4( stdout, "    Video Format %s ", vft );
		    		    
		    cd = 1 & p[i];
		    i++;

		    if (1 == cd) {
			cp = p[i++];
			tc = p[i++];
			mc = p[i++];
		    }
		    if (2 == (2 & p[i+1])) {
			dhz = (p[i]<<6) | (p[i+1]>>2);
			dvz = ((1 & p[i+1]) << 13) | (p[i+2]<<5) | (31 & p[i+3]>>3);
			mprintf4( stdout, "Horizontal %u Vertical %u\n", dhz, dvz);
		    }

		    if (1 == cd) {
			mprintf4( stdout, "     Color Primaries %02X, Transfer Chars %02X, Matrix Co-effs %02X\n", cp, tc, mc);
		    } else {
			mprintf4( stdout, "     Colors, Transfer, and Matrix Coefficients follow ITU-R BT.709\n");
		    }

		    i += 4;
		    continue;
		}

		if (3 == xid) {
		    i++;
		    xidt = "None";
		    qmf = 0x0F & p[i];
		    if ( 0 != (8 & qmf)) xidt = "Intra";
		    if ( 0 != (4 & qmf)) xidt = "Non-Intra";
		    if ( 0 != (2 & qmf)) xidt = "Chroma Intra";
		    if ( 0 != (1 & qmf)) xidt = "Chroma Non-Intra";

		    mprintf4( stdout, "Quantizer Matrix Type %d:\n", qmf);

		    if (qmf == 0) continue;

		    mprintf4( stdout, "     %s:\n", xidt);
		    for (j=0; j<8; j++) {
			iqr  = (long long)(7 & p[i])<< 60; 	// 63 62 61
			iqr |= (long long)p[i+1]<<52;	//          60 . 59 58 57 56 . 55 54 53
			iqr |= (long long)p[i+2]<<44;	//          52 . 51 50 49 48 . 47 46 45
			iqr |= (long long)p[i+3]<<36;	//          44 . 43 42 41 40 . 39 38 37
			iqr |= (long long)p[i+4]<<28;	//	    36 . 35 34 33 32 . 31 30 29

			iqr |= (long long)p[i+5]<<20;	//          28 . 27 26 25 24 . 23 22 21
			iqr |= (long long)p[i+6]<<12;	//          20 . 19 18 19 16 . 15 14 13
			iqr |= (long long)p[i+7]<<4;	//          12 . 11 10  9  8 .  7  6  5   
			iqr |= (long long)p[i+8]>>3;	//           4 .  3  2  1  0    
			mprintf4( stdout, "       %016llX\n", iqr);
			i += 9;
		    }
		    continue;
		}

		if (4 == xid) {
		    xidt = "Copyright";
		}

		if (5 == xid) {
		    xidt = "Sequence Scalable";
		}

		if (7 == xid) {
		    xidt = "Picture Display";
		}

		if (8 == xid) {
		    unsigned char fh, fv, bh, bv, idcp, ps, tff, fpfd;
		    unsigned char cmv, qst, ivf, as, rff, c420, pf, cdf;
		    unsigned char *pst;
		    xidt = "Picture Coding";
		    mprintf4( stdout, "%s\n", xidt);
		    i++;
		    fh  = 0x0F & p[i];
		    fv  = p[i+1]>>4;
		    bh  = 0x0F & p[i+1];
		    bv  = p[i+2]>>4;

		    idcp = 3 & p[i+2]>>2;
		    
		    ps   = 3 & p[i+2];
		    pst = "Reserved";
		    if (1 == ps) pst = "Top Field";
		    if (2 == ps) pst = "Bottom Field";
		    if (3 == ps) pst = "Frame";
		    
		    mprintf4( stdout,
			"    Forward Horizontal %2d Forward Vertical %2d\n"
			"    Backward Horizontal %2d Backward Vertical %2d\n"
			"    Intra DC Precision %d bits, Picture Structure %s\n",
				fh, fv, bh, bv, idcp+8, pst);

		    tff  = 1 & p[i+3]>>7;
		    fpfd = 1 & p[i+3]>>6;
		    cmv  = 1 & p[i+3]>>5;
		    qst  = 1 & p[i+3]>>4;
		    ivf  = 1 & p[i+3]>>3;
		    as   = 1 & p[i+3]>>2;
		    rff  = 1 & p[i+3]>>1;
		    c420 = 1 & p[i+3];

		    pf   = 1 & p[i+4]>>7;
		    cdf  = 1 & p[i+4]>>6;

		    mprintf4( stdout,
			"    Top Field First %d Frame Predict DCT %d Concealment Vectors %d\n"
			"    Quantizer scale %d Intra-vlc format %d Alternate Scan %d\n"
			"    Repeat First Field %d Chroma420 %d Progressive %d Composite %d\n",
				tff, fpfd, cmv, qst, ivf, as, rff, c420, pf, cdf
			);


		    // don't see these
		    if (cdf != 0) {
			// v_axis:1 field_seq:3 sub_carrier:1 burst_amplitude:7 sub_carrier_phase:8
		    }
		    i += 5;
		    continue;
		}		    		    

		if (9 == xid) xidt = "Picture Spatial Scalable";
		if (10== xid) xidt = "Picture Temporal Scalable";
		mprintf4( stdout, "%s: NOT PARSED\n", xidt);
		i++;
		continue; // find next start code
	    } // end of sequence extensions


	    /* Group Of Pictures */
	    if (0xB8 == c) {
		mprintf4( stdout, "Group Of Pictures: ");
		test_mpeg2_group_of_pictures( p + i + 1 );
		continue; // find next start code
	    }

	    /* picture start? */
	    if (0 == c) {
		mprintf4( stdout, "Picture ");
		pti = test_mpeg2_picture( p + i + 1 );
		memset( sc, 0, sizeof(sc) ); /* clear slice checklist */
		sp = 0;
		continue;
	    }

	    /* ATSC User Data, Closed Caption or Bar data, only seeing CC */
	    if (0xB2 == c) { 
    		test_ap_user_data( p + i + 1 );
		i += 6;
		continue; // find next start code, dont parse two above
	    }

	    /* MPEG video starts with E0 to E7 or EF? */
	    if (0xE0 == (0xF0 & c)) {
		mprintf4( stdout,
			 "MPEG Video PES Header, stream ID %02X\n", p[i]);
		test_mpeg2_video_pes( p );
		i++;
		continue;
	    }

	    // slice macroblocks? only if arg_mdump & 4 
	    if ( (c > 0) && (c < 0xB0) ) {
		sn = c; /* slice number */

		if (0 != (16 &arg_mdump)) {
		    mprintf5( stdout, "   STC %09llX: %08X ",
				(ppsbytes_in-184) + (i-3), b);
		    mprintf5( stdout,
			    "Slice %02X Macroblock Pixel Rows %u - %u\n",
			    sn, (sn - 1) << 4, 15 + ((sn - 1) << 4) );
		} else {
		    if (1 == sn) mprintf4( stdout,
			 "   Slice Macroblocks follow\n");
		}

		/* doesn't do much, not doing video decoder here */
		test_mpeg2_slice( p + i + 1 );

/* TESTME: how do you know the slices are over if last few missing? */
/* 	   also, there could be multiple slices per macroblock row */

		if ((sn != sp+1) && (sn != sp)){
		    /* 	slice sequence error */	
		    mprintf5( stdout, "     Slice not in sequence\n");
		}
		sp = sn;

		if (0 != sm) { /* seq sets sm, pic clears sc[] */
		    sc[sn] = sn;
		    if (sn == sm) { /* last slice found, are any missing? */
		
			sc[0] = 0; /* reset tally */
			for (j = 1; j <= sm; j++) {
			    if (0 != sc[j]) sc[0]++;
			}
			/* print list of missing slices, if any missing */
			if (sc[0] != sm) {
			    mprintf5( stdout, "Missing slices: ");
			    for (j = 1; j <= sm; j++)
				if (0 == sc[j])
				    mprintf5( stdout, "%u ", j );
			    mprintf5( stdout, "\n");
			} else {
			    char *t = "";
			    if (vid.payoff > (128 << 20)) t = "*";
			    mprintf5( stdout, "Slice checklist OK %s-Frame "
					 "payload size %d %s\n",
					 ptt[pti], vid.payoff, t );
			}
		    }
		}
		continue;
	    }

	    /* Sequence End. Have seen it trigger on network to local bug,
		but it's not reliable enough to use for cutting
	     */
	    if (0xB7 == c) {
		pkt.seqend++;
		mprintf4( stdout, "Sequence End #%d\n", pkt.seqend);
		continue; // find next start code
	    }

	    mprintf4( stdout, "   STC %09llX: %08X NOT PARSED\n",
			ppsbytes_in + (i-3), b);
	}
    }
    mprintf4( stdout, "\n");
}

/////////////////////////////////////////////////////////////////// MPEG2
// MPEG2 payload related, usefulness yet to be determined.
// afc 2 is afc only
// afc 3 is afc, then payload afterwards
// should precede payload on any packet with afc 3
// ATSC packets should not see these, as there is no need
// all of this is Too Much Information For This Application
static
void
test_mpeg2_adaptation_field( unsigned char *p )
{
    unsigned char *r;
    unsigned char afl, di, rai, espi, sp, tpd;
    unsigned char afx = 0;
    unsigned long long npcr, opcr, pcrb, opcrb, pwr, dtsnau;
    unsigned long long pcrd, px, py;
//    unsigned long long pcrt;
    unsigned short pcrx, opcrx;
    char sc; // splice countdown is signed
    unsigned char tpdl;
    unsigned char afb, afxl, ltw, pwrf, ssf, ltwv, ltwo, st;

    unsigned char pd, ph, pm, ps; // pcr d h m s
    unsigned int pr, pw; // pcr remainder, pcr wall time
    
    pcrb = opcrb = pwr = dtsnau = pcrx = opcrx = 0;

    ph = pm = ps = pr = pw = 0;

    r = p + 4; // adaptation field starts immediately after continuity counter

    afl = *r;
    r++; // adaptation field length is always first byte after cc
    afb = *r;

    if (afl == 0) return; // nothing to do                

    di   = 0x80 & afb; // discontinuity if set
    rai  = 0x40 & afb; // random access if set
    espi = 0x20 & afb; // elementary stream priority if set

    npcr = 0x10 & afb; // has new program clock reference
    opcr = 0x08 & afb; // has original program clock reference

    sp   = 0x04 & afb; // has splicing point
    tpd  = 0x02 & afb; // has transport private data
    afx  = 0x01 & afb; // has adaptation field extension (decoder time stamps)


    // besides pcr, main reason for parsing down this far
    // random access indicator may be smoothest cut-in/out point
    mprintf4( stdout, "\n MPEG Adaptation Field (%02X) FLAGS:", afb);
    if (0 == afb)  mprintf4( stdout, " NONE");
    if (0 != di)   mprintf4( stdout, " DI");
    if (0 != rai)  mprintf4( stdout, " RAI");
    if (0 != espi) mprintf4( stdout, " ESPI");
    if (0 != npcr) mprintf4( stdout, " PCR");
    if (0 != opcr) mprintf4( stdout, " OPCR");
    if (0 != sp)   mprintf4( stdout, " SP");
    if (0 != tpd)  mprintf4( stdout, " TPD");
    if (0 != afx)  mprintf4( stdout, " AFX");

    mprintf4( stdout, "\n");

    // parsing some but not keeping anything but pcr and opcr
    // r steps forward with each section done, in spec order
    r++;

    // these should be done in the right order
    // program clock reference, main mpeg system clock
    if (0 != npcr) {
	unsigned int rb;
	rb = 0x7E & r[4];
	// bad reserved should abort?
	if ( (0 == rb) || (0x7E == rb) ) // reserved can be 1 or 0? very odd
	{
	    // 33 bit PCR bits
	    pcrb  = (*r++)<<25; // bits 32 . 31 30 29 28 . 27 26 25
	    pcrb |= (*r++)<<17; // bits 24 . 23 22 21 20 . 19 18 17
	    pcrb |= (*r++)<<9;  // bits 16 . 15 14 13 12 . 11 10  9
	    pcrb |= (*r++)<<1;  // bits  8 .  7  6  5  4    3  2  1 
	    pcrb |= 1 & (*r>>7); // bit  0
	    pcrb &= 0x1FFFFFFFFULL;


	    // 9 bit PCR extension
	    // 300 * (90kc pcrb % 90000) + pcrx
	    // to make pcrx / 27,000,000
	    pcrx  = (1 & *r++) << 8; // bit 9
	    pcrx |= 0xFF & *r++;
	    // limit pcrx to 0-299
	    pcrx %= 300;
	    
	    /* make 42 bit PCR as a 27 mhz clock value (300 * 90kc) */
	    pcr = (300 * pcrb) + pcrx;
	    pcrd = pcr - ppcr;
	    
	    px = pcr % 300;
	    py = pcr / 300;		// pcr extension
	    pr = py % 90000;		// save 90kc clock
	    pw = py / 90000;		// strip 90kc clock to total seconds
    	    ps = pw % 60;		// limit to 0-59 seconds
	    pm = (pw / 60) % 60;	// limit to 0-59 minutes
	    ph = (pw / 3600) % 24;      // limit to 24 hours (should be 26?)
	    pd = (pw / 86400);
	    // normal pcr increment behaviour is default
	    ppcr = pcr;

	    if (0 == first_pcr) first_pcr = pcr;
	    last_pcr = pcr;


	    // print if mpeg video info asked for
	    mprintf7( stdout,
		 "  %cPCR %01dd:%02dh:%02dm:%02ds %05uc %3llux diff %10lld\n",
		 (pcrd < 27000000) ? ' ':'*', pd, ph, pm, ps, pr, px, pcrd
		);

	} else {
	    mprintf7( stdout, "  PCR reserved bits not set %02X\n", rb);
	}
    }

    // original program clock reference, from previous multiplex or source?
    if (0 != opcr) {
	unsigned int rb;
	rb = 0x7E & r[4];
	if ( (0 == rb) || (0x7E == rb) ) // reserved can be 1 or 0?
	{ // bad reserved should abort?
	    // 33 bit Original PCR
	    opcrb  = (*r++)<<25; // bits 32 . 31 30 29 28 . 27 26 25
	    opcrb |= (*r++)<<17; // bits 24 . 23 22 21 20 . 19 18 17
	    opcrb |= (*r++)<<9;  // bits 16 . 15 14 13 12 . 11 10  9
	    opcrb |= (*r++)<<1;  // bits  8 .  7  6  5  4    3  2  1 
	    opcrb |= 1 & (*r>>7); // bit  0
	    opcrb &= 0x1FFFFFFFFULL;
	    // 9 bit Original PCR extension
	    opcrx  = (1 & *r++) << 8; // bit 9
	    opcrx |= 0xFF & *r++;

	    pr = opcrb % 90000;
	    pw = opcrb / 90000;
    	    ps = pw % 60;
	    pm = (pw / 60) % 60;
	    ph = (pw / 3600) % 24;
	    pd = (pw / 86400);


	    mprintf7( stdout, "  OPCR %01d:%02d:%02d:%02d.%05d ext %3d\n",
			pd, ph, pm, ps, pr, opcrx);

	} else {
	    mprintf7( stdout, "  OPCR Reserved bits not set %02X\n", rb);
	}
    }

    // splicing point
    if (0 != sp) {

	sc = *r++; // 8 bit signed
    }

    // transport private data, skips and does not keep private data bytes
    if (0 != tpd) {
	int i, pdb;
	tpdl = *r++;
	for (i = 0; i < tpdl; i++) pdb = *r++;
    }

    // adaptation field extension
    if (0 != afx) {
	afxl = *r++;
	if ( 0x1F == (0x1F & *r) ) {		// reserved
	    ltw  = 1 & (*r >> 7);		// ltw flag
	    pwrf = 1 & (*r >> 6);		// piecewise rate flag
	    ssf  = 1 & (*r >> 5);		// seamless splice flag
	    r++;

	    if (0 != ltw) {
		ltwv  = 1 & (*r >> 7);		// 1 bit ltw valid
		ltwo  = (0x7F & *r++) << 8;	// 15 bit ltw offset
		ltwo |= 0xFF & *r++;
	    }
	    if (0 != pwrf) {
		if (0xC0 == (0xC0 & *r)) {	// reserved
		    pwr  = 0x3F & *r++;		// 22 bit piecewise rate
		    pwr |= (*r++) << 16;
		    pwr |= (*r++) << 8;
		    pwr |= *r++;

		}
	    }

	    // seamless splice, Decoder Time Stamp next access unit
	    if (0 != ssf) {
		if  (  (1 == (1 & r[0])) // marker bits
		    || (1 == (1 & r[2]))
		    || (1 == (1 & r[4]))
		    )
		{
		    // splice type
		    st = 0xF & (*r>>4); // blech spec mess

		    // dts next access unit
		    // 32 31 30
		    dtsnau  = (7 & (*r++ >> 1 )) << 30;

		    // 29 28 27 26 . 25 24 23 22
		    dtsnau |= *r++ << 22;

		    // 21 20 19 18 . 17 16 15 14
		    dtsnau |= (0x7F & (*r++ >> 1)) << 14;

		    // 13 12 11 10 .  9  8  7  6
		    dtsnau |= *r++ << 6;

		    //  5  4  3  2 .  1  0
		    dtsnau |= 0x3F & (*r++ >> 1);

		    pr = dtsnau % 90000;
		    pw = dtsnau / 90000;
		    ps = pw % 60;
		    pm = (pw / 60) % 60;
		    ph = (pw / 3600) % 24;
		    pd = (pw / 86400);

		    mprintf4( stdout, "\n  DTS NAU %01d:%02d:%02d:%02d.%05d",
			pd, ph, pm, ps, pr );

		}
	    }	
	}
    }
    if ( 0 != afb) mprintf4( stdout, "\n");
}

#if 0
/* for atscap, not here. atscut needs the various printfs for info output */

/* generic build payload: p is packet, s is payload structure, n is name.
   this only works for payloads with section lengths, not mpeg or a52.
   also, to be generic, all payloads are allocated 4096 bytes.
   returns 0 if done, 1 if not done
*/
static
int
build_payload(char *p, struct payload_s *s, char *n)
{
    char *r;
    char psi;
    
    r = p + 5;
    psi = 1 & ( p[1] >> 6);

/* payload start indicator yes */
    if (0 != psi) {
/* save header parts that are not part of payload */
	s->sl = ((0xF & r[1])<<8) | r[2];
	s->vn = 0x1F & ( r[5] >> 1);
	s->payoff = 183;
	memcpy( s->payload, r, s->payoff );
	if ( s->payoff > s->sl+3 ) {
	    return 0;
	}
	return 1;
    }	
/* payload start indicator no */
    r = p+4;
    if (0 == psi) {
	// first time, or last payload was good?
	if (0 == s->payoff) { // last valid payload clears s->payoff
	    return 1;    // drop payload, it is out of order
	}
	if (s->payoff+184 > 4095) {
	    return 1; // drop payload, it will over-run buffer
	}
	
	memcpy( &s->payload[s->payoff], r, 184);
	s->payoff += 184;

	if ( s->payoff > s->sl+3 ) {
	    return 0; // payload is done
	}
	return 1;
    }

    return 1; // payload not handled
}
#endif

#if 0
////////////////////////////////////////////////////////////////// MPEG2 PAT
// payload assembly for PAT, not needed, all ATSC PATs are 188 bytes or less
static
unsigned int
build_pat_payload( unsigned char *p )
{
    unsigned char *r;
    unsigned char psi;
    //, vn;
    

    r = p + 5;
    psi = 1 & ( p[1] >> 6);


    // handle payload start packet by copying start to PAT payload buffer
    if (psi != 0)
    {
	if ( (0x02 == r[0]) 			// table id PAT
	  && (0xB0 == (0xF0 & r[1]) )		// syntax,private,reserved bits
	   )
	{
	    pat.vn = 0x1F & ( r[5] >> 1);
	    pat.sl = ((0xF & r[1])<<8) | r[2];

	    pat.payoff = 183;
	    aprintf6( stdout,
		" START PAT Version %02X SLen %03X Payoff %03X\n",
		pat.vn, pat.sl, pat.payoff);

	    memset( pat.payload, 0, sizeof(pat.payload) );
	    memcpy( pat.payload, r, pat.payoff );

	    if ( pat.payoff > pat.sl+3 ) {
		aprintf6( stdout, "   DONE PAT\n");
		return 0;
	    }
	}
	return 1;
    }

    r = p+4;

    // additional non start payloads append to PAT payload buffer
    if ( psi == 0 ) {
	memcpy( &pat.payload[pat.payoff], r, 184);
	pat.payoff += 184;
	aprintf6( stdout, 
		"  COPY PAT Version %02X SLen %03X Payoff %03X\n",
		    pat.vn, pat.sl, pat.payoff);

	if ( pat.payoff > pat.sl+3 ) {
	    aprintf6( stdout, "   DONE PAT\n");
	    return 0;
	}
	return 1;
    }

    aprintf6( stdout, "PAT not parsed\n");
    return 1;
}
#endif

/* from atscap cross pollinated to atscut */
/****************************************************************** MPEG2 PAT
 * rebuild PAT to single Program Map Table entry. called by test pat.
 * vc is the loop index for the program lookup from test pat.
 * p is pointer to the current PAT, new_pat is built here, and is
 * copied over current PAT at p.
 * arg_epn is the user supplied program number
 */
static
void
build_new_pat ( unsigned char *p, int vcn )
{
    unsigned int new_crc;
    unsigned short sl = 13;
    unsigned char new_pat[188];
    
    if (arg_epn < 1) return;

    memset( new_pat, 0, 188 );
/* copy section control header portion */
    memcpy( &new_pat[0], p, 13 );

/* copy section data portion for a specific VC */
    memcpy( &new_pat[13], p + 13 + (vcn*4), 4);

/* debug */
{
	unsigned short pgn, pmtpid;
	pgn = new_pat[13] << 8;			/* 16 bit program number */
	pgn |= new_pat[14];
	pmtpid = (0x1F & new_pat[15]) << 8;	/* 13 bit PID number */
	pmtpid |= new_pat[16];
//	fprintf( stdout, "%s %04X new PAT sl %03X p %04X m %04X\n",
//		__FUNCTION__, 0, sl, pgm, pmtpid);

}

/* set new pat section length, is overcorrect: 0xf0 is enough */
/* is over-correct because sl will always be well under 256 bytes */
    new_pat[6] = (0xF0 & new_pat[6]) | (0x0F & (sl>>8));
/* 1 offset byte from payload start, 8 byte section header, 4 byte data */
    new_pat[7] = 0xFF & sl;


/* compute new pat crc */
    new_crc = calc_crc32( &new_pat[5], sl-1 );

/* store crc at end of new pat */
    new_pat[ sl+4 ] = 0xFF & (new_crc>>24);
    new_pat[ sl+5 ] = 0xFF & (new_crc>>16);
    new_pat[ sl+6 ] = 0xFF & (new_crc>>8);
    new_pat[ sl+7 ] = 0xFF & new_crc;

#if 0
/* only needed for debugging, remove from production for one less crc32 */
    {
	unsigned int chk_crc;
	chk_crc = calc_crc32( &new_pat[5], sl+3);
	if (0 != chk_crc)
	    fprintf( stdout, "%s chk_crc %08X", __FUNCTION__, chk_crc);
    }
#endif

    memcpy( p, new_pat, 188 ); /* copy it back on top of old one */
}

/**************************************************************** MPEG2 PMT
 * rebuild PMT to single Program entry. called by test pmt
 * p is pointer to the current PMT, new_pmt is built here, and is
 * copied over current PMT at p.
 * arg_epn is the user supplied program number
 * arg_epa is the user supplied audio number
 */
static
void
build_new_pmt ( unsigned char *p, unsigned short vpid, unsigned short apid )
{
    unsigned char *r, *d;
    unsigned short c, sl, osl, pid;
    unsigned int new_crc;
    unsigned char new_pmt[188];

    if (arg_epn < 1) return;

    d = new_pmt;
    memset( d, 0, 188 );
    memcpy( d, p, 17); /* 4 byte header, 13 byte control data */
    d += 17; /* new pmt */
    sl = 13; /* does not count header */
    r = p + 17; /* old pmt */

/* pcr pid is copied. should have been set correctly elsewhere */

    pid = ( (0x1F & p[1]) << 8) | p[2]; /* pid for this pmt packet */
    osl = ( (0x0F & p[6]) << 8) | p[7]; /* old section len for comparison */

/* 4 byte TS header, 4 bytes to sl, then sl bytes data and 4 byte crc */
    if (sl > 179) return;

    c = ( ( 0x0F & p[15]) << 8) | p[16]; /* program info len 12 bottom bits */

/* ATSC further restricts this to 10 bits, i think */
//    c &= 0x3FF;

/* only copy and increment if program info len non zero */
    if (0 != c) {
	int i;
	unsigned char *s;
	i = c;
	s = r;
	for ( i = 0; i < c; ) {
	    if (0xAA == *s) *s = 0x80; /* stuff the RC */
	    i += s[1]+2;
	    s += s[1]+2; /* step thru descriptors */
	}	
	memcpy( d, r, c );
	r += c;	/* next place in old pmt */
	sl += c; /* section len in new pmt */
    }

//    fprintf( stdout, "%s %04X PMT pilen %03X newsl %03X\n",
//	    __FUNCTION__, pid, c, sl );

/* ok to add zero, not ok to memcpy size 0 */
    d += c; /* next place in new pmt */
    
    while (r < (p + (osl-1)) ) {
	unsigned int e;

/* reserved bits set ? */
	if ( (0xE0 == (0xE0 & r[1])) && (0xF0 == (0xF0 & r[3])) ) {
	    e = ((0x1F & r[1]) << 8 ) | r[2]; /* es pid bottom 13 bits */
	    c = ((0x0F & r[3]) << 8 ) | r[4]; /* es info len 12 bottom bits */
/* ATSC limits this to 10 bits I think */
//	    c &= 0x3FF;

/* preamble 5 bytes, copy this much at least, es info len may be zero */
	    c += 5;

/* if es pid matches program number and audio number, copy entry to new pmt */
	    if ( (e == vpid) || (e == apid) ) {

/* FIXME: needs a strict boundary check. the while above is not enough */
		memcpy( d, r, c );

		d += c; /* next place in new pmt */
		sl += c; /* section len in new pmt */

//		fprintf( stdout, "%s %04X PMT pid %04X esi %03X newsl %03X\n",
//			__FUNCTION__, pid, e, c, sl);
	    }
	    r += c; /* next place in old pmt */

	} else {
	    /* no reserved bits stops loop */
	    fprintf( stdout, "%s %04X no reserved bits\n",
		    __FUNCTION__, pid);
	    break;
	}
    }    

/* set new pmt section length */
    new_pmt[6] = (0xF0 & new_pmt[6]) | (0x0F & (sl>>8));
    new_pmt[7] = 0xFF & sl;

/* compute new pmt crc */
    new_crc = calc_crc32( &new_pmt[5], sl-1 );

/* store crc at end of new pmt */
    new_pmt[ sl+4 ] = 0xFF & (new_crc>>24);
    new_pmt[ sl+5 ] = 0xFF & (new_crc>>16);
    new_pmt[ sl+6 ] = 0xFF & (new_crc>>8);
    new_pmt[ sl+7 ] = 0xFF & new_crc;

//    fprintf( stdout, "%s %04X PMT oldsl %03X newsl %03X\n",
//	    __FUNCTION__, pid, osl, sl);

#if 0
/* only needed for debugging, remove from production for one less crc32 */
    {
	unsigned int chk_crc;
	chk_crc = calc_crc32( &new_pmt[5], sl+3);
	if (0 != chk_crc)
	    fprintf( stdout, "%s %04X chk_crc %08X\n",
		    __FUNCTION__, pid, chk_crc);
    }
#endif

    memcpy( p, new_pmt, 188 ); /* copy it back on top of old one */
}



/////////////////////////////////////////////////////////////////// MPEG2 PAT
// so far, all these show no PSI = 0 for PID matching PAT or PMT
// so processing in place as single packet. maybe a bad shortcut later?
// there's not a lot interesting until/unless have to modify it.
static
void
test_pat ( unsigned char *p )
{
    unsigned char *r;
    unsigned short sl;
    unsigned char vn, cni, sn, lsn;
    unsigned short tsid, pgn, netpid, pmtpid, pid;
    unsigned int pat_crc, pat_crc0;
    int vcn;
//  int i;

    pat.payok = ~0;
    pid = 0;

    if (arg_epn > 0) keep_pids[0] = ~0; /* always keep pat */

    // point to table id
    r = p+5;

    // check reserved and report any errors
    if  ( 0x30 != (0x30 & r[1]))
	mprintf1( stdout, " PAT reserved bits not set r[1] != 30 is %02X\n",
		0x30 & r[1]);

    if ( 0xC0 != (0xC0 & r[5]) )
	mprintf1( stdout, " PAT reserved bits not set r[5] != C0 is %02X\n",
		0xC0 & r[5]);
 
    sl = ((0xF & r[1])<<8) | r[2];	// 12 bit section length
    pat.sl = sl;

//    pat_crc = calc_crc32( r, sl+3 );	// only want crc to validate it

/* compute CRC32 */
    pat_crc0 = calc_crc32( r, sl-1 );

/* received CRC32 */
    pat_crc  = r[ sl - 1 ];
    pat_crc <<= 8;
    pat_crc |= r[ sl ];
    pat_crc <<= 8;
    pat_crc |= r[ sl + 1 ];
    pat_crc <<= 8;
    pat_crc |= r[ sl + 2 ];

    if (pat_crc != pat_crc0) {
	pkt.crcpat++;
	mprintf1( stdout,
		    "PAT CRC ERROR PID %04X CALC %08X RCVD %08X\n",
		    0, pat_crc0, pat_crc);
#if 0
	for (i = 0; i < 188; i++) {
	    if (old_pat[i] != p[i])
		mprintf1( stdout,
			    "p[%d] %02X != %02X\n", i, old_pat[i], p[i] );
	}
#endif
	return;
    } else {
	if (sl < 180)
	    memcpy( old_pat, p, 188);
    }

    pkt.pat++;

// too short for cable? no is ok for the 16 programs seen in cable
    memcpy( pat.payload, p, 188 );
    pat.payoff = 188;
    pat.payok = 0;

    if (arg_mgtpids != 0) keep_pids[0] = ~0; // -k mgt keeps PAT

    tsid = (r[3]<<8) | r[4];
    vn = 0x1F & (r[5]>>1);
    pat.vn = vn;

/* OUTPUT REDUX: */
    if ( (0 != arg_redux) && (vn == pid_vn[pid]) ) return;
    pid_vn[pid] = vn;
    
    if (pat_crc == pat_crc0) mprintf1( stdout, "PAT CRC %08X OK", pat_crc);

    cni = 1 & r[5];
    sn = r[6];
    lsn = r[7];
    mprintf1( stdout, "\n MPEG PAT PID 0000: SLen %03X TSID %04X VN %02X " 
		    "CNI %01X SN %02X LSN %02X\n",
	    sl, tsid, vn, cni, sn, lsn );
    r += 8;

    vcn = 0;

// start the program loop, not sure when to stop. p+5+sl?
    for ( ; r < p+4+sl;  ) {
	if ( 0xC0 != (0xC0 & r[2]) ) break; // stop if no reserved bits too
	pgn = ( r[0]<<8 ) | r[1];
	if (pgn == 0) {
	    netpid = 0x1FFF & ( (r[2]<<8) | r[3] );
	    mprintf1( stdout, "    Program %04X Network PID %04X\n",
		     pgn, netpid);
	    // don't keep network PID's, they should be obsoleted by now
	} else {
	    pmtpid = 0x1FFF & ( (r[2]<<8) | r[3] );
	    mprintf1( stdout, "    Program %04X PMT PID %04X\n",
		     pgn, pmtpid );

	    pa[vcn].pn = pgn;
	    pa[vcn].pmtpid = pmtpid;

	    if (arg_epn == pgn) { /* -e keeps PAT if program matches */
		keep_pids[0] = ~0; /* program is in PAT */

/* -e rebuilds PAT as a single program entry */
		if (0 != arg_forge)
		    build_new_pat( p, vcn );
	    }
	} 
	vcn++;
	r += 4; // skip to next program number and PID
    }
    pat_ncs = vcn;
    mprintf1( stdout, "\n");
}

#if 1
//////////////////////////////////////////////////////////////////// MPEG PMT
// payload assembly for PMT, this one is needed for KQED-HD PMT
static
unsigned int
build_pmt_payload( unsigned char *p, int pan )
{
    struct payload_s *pmp;
    unsigned char *r;
    unsigned char psi;

    pmp = &pmt[ pan ];

    r = p+5;

    psi = 1 & ( p[1] >> 6);

// handle payload start packet by copying start to PMT payload buffer
    if (0 != psi)
    {
	if ( (0x02 == r[0]) 			// table id PMT
//		&& (0xB0 == (0xF0 & r[1]))	// syntax,private,rsvd bits
	    )
	{
	    pmp->vn = 0x1F & ( r[5] >> 1);
	    pmp->sl = ((0xF & r[1])<<8) | r[2];

	    pmp->payoff = 183;
	    mprintf6( stdout,
		" START PMT Version %02X SLen %03X Payoff %03X\n",
		pmp->vn, pmp->sl, pmp->payoff);

	    memset( pmp->payload, 0, sizeof(pmp->payload) );
	    memcpy( pmp->payload, r, pmp->payoff );

/* if it fit within first packet, get out with 0 code */
	    if ( pmp->payoff > pmp->sl+3 ) {
		mprintf6( stdout, "   DONE PMT\n");
		return 0;
	    }
	}
/* otherwise needs more packets */
	return 1;
    }

    r = p+4;

    // additional non start payloads append to PMT payload buffer
    if ( psi == 0 ) {
	if (0 == pmp->payoff) return 1;
	memcpy( &pmp->payload[pmp->payoff], r, 184);
	pmp->payoff += 184;
	aprintf6( stdout, 
		"  COPY PMT Version %02X SLen %03X Payoff %03X\n",
		    pmp->vn, pmp->sl, pmp->payoff);

	if ( pmp->payoff > pmp->sl+3 ) {
	    aprintf6( stdout, "   DONE PMT\n");
	    return 0;
	}
	return 1;
    }

    aprintf6( stdout, "PMT not parsed\n");
    return 1;
}
#endif


//////////////////////////////////////////////////////////////////// MPEG PMT
// A/65b Table 6.38 Multiple String Structure
// does do huffman decomp
static
void
test_pmt_mss( unsigned char *r )
{
    unsigned int nstr, nseg, comp, mode, nchr;
    unsigned int i,j,k;
    unsigned char lang[4];
    unsigned char s[256];
    unsigned char d[512];
//    unsigned char *t;

    memset( s, 0, sizeof(s));
    nstr = r[0];
    for (i = 0; i < nstr; i++) {
	memcpy(lang, &r[1], 3);
	lang[3] = 0;
	r += 4; // 3?
	nseg = *r++;
	for (j = 0; j < nseg; j++) {
	    comp = *r++;
	    mode = *r++;
	    nchr = *r++;
//	    mprintf2( stdout, "LEN %02X: ", nchr);
	    for (k = 0; k < nchr; k++) {
		s[k] = *r++;
//		mprintf2( stdout, "%02X ", s[k]);
	    }
//	    mprintf2( stdout, "\n");

	    // show non-compressed
	    if ( (0 == comp) && (0 == mode) ) {
		mprintf2( stdout, "[%s] [%-s] ", lang, s);
	    }
	    // show huffman compressed
	    if  (
//		 (mode == 0xFF) && // spec requires, KPXB ignores
		 (comp > 0) &&
		 (comp < 3)
		)
	    { 
		    huffman_decode( d, s, sizeof(d), nchr, comp);
		    mprintf2( stdout, "[%s] [%-s] ", lang, d);
	    }
	    // show as unknown compress
	    if (comp > 2)
		mprintf2( stdout, "[%s] [L%02X C%02X M%02X]",
			lang, nchr, comp, mode);

	}
    }
    mprintf2( stdout, "\n");
}


//////////////////////////////////////////////////////////////////// MPEG PMT
static
void
test_pmt_descriptor( unsigned char *r ) {
    unsigned char *t;
    unsigned char n, j;
    unsigned short i, k;
    t = "FUBAR";
    n = r[0];
    k = 0xFF & r[1];

//    r += 2; // bytes start here
//    mprintf2( stdout, "desc %s tag# %u len %u\n", t, n, l);    
/////////////////////////////////////////////////////////////// MPEG specific
    switch ( n ) {

	case 2:
	    t = "Video Stream";
	    {
		unsigned char mfr, frc, m1o, cpf, spf;
		unsigned char *frct, *mfrt;
		mprintf2( stdout, "     Tag %02X DLen %02X %s: ", n, k, t);
		mfr = 1 & (r[2]>>7);
		frc = 15 & (r[2]>>3);
		m1o = 1 & (r[2]>>2);
		cpf =  1 & (r[2]>>1);
		spf =  1 & r[2];

		frct = "Reserved";
		if (0 == mfr) {
		    mfrt = "Single";
		    if (0 == frc) frct = "Forbidden";
		    if (1 == frc) frct = "23.976";
		    if (2 == frc) frct = "24";
		    if (3 == frc) frct = "25";
		    if (4 == frc) frct = "29.97";
		    if (5 == frc) frct = "30";
		    if (6 == frc) frct = "50";
		    if (7 == frc) frct = "59.94";
		    if (8 == frc) frct = "60";
		} else {
		    mfrt = "Multiple";
		    if (0 == frc) frct = "Forbidden";
		    if (1 == frc) frct = "23.976";
		    if (2 == frc) frct = "24/23.976";
		    if (3 == frc) frct = "25";
		    if (4 == frc) frct = "29.97/23.976";
		    if (5 == frc) frct = "30/23.976/24/29.97";
		    if (6 == frc) frct = "50 25";
		    if (7 == frc) frct = "59.94/23.976/29.97";
		    if (8 == frc) frct = "60/23.976/24/29.97/30/59.94";
		}

		mprintf2( stdout, " %s Frame Rate %s\n", mfrt, frct);
		mprintf2( stdout, "      MPEG1 Only %u Constrained %u Still Picture %u\n",
				m1o, cpf, spf);

		// not likely for terrestrial ATSC but who knows what's on cable
		if (0 != m1o) {
		    // check reserved bits
		    if (0x1F == (0x1F & r[4])) {
			unsigned char pli, cf, fre;
			pli = r[3];
			cf  = 3 & (r[4]>>6);
			fre = 1 & (r[4]>>5);
			mprintf2( stdout,"    MPEG1 Profile and Level %02X Chroma Format %01X Frate Rate Ext %1X",
				    pli, cf, fre);
		    }
		}
//		return;
	    }
	    break;

 	case 3:
	    t = "Audio Stream";
	    break;

	case 4:
	    t = "Hierarchy";
	    break;

	case 5:
	    t = "Registration";
	    mprintf2( stdout, "     Tag %02X DLen %02X %s: ", n, k, t);
	    mprintf2( stdout, "Format ID [%c%c%c%c]\n",
			    r[2],r[3],r[4],r[5]);
	    if (k != 4)
		mprintf2( stdout, "    add'l id info exists but not parsed\n");
//	    return;
	    break;

	case 6:
	    t = "Stream Alignment";
	    {
		unsigned char *s;
		unsigned char at;
		s = "Reserved";
		at = r[2];
		if (1 == at) s = "SLI, PIC, GOP or SEQ";
		if (2 == at) s = "PIC, GOP or SEQ";
		if (3 == at) s = "GOP or SEQ";
		if (4 == at) s = "SEQ";
		mprintf2( stdout, "     Tag %02X DLen %02X %s: %s\n",
			    n, k, t, s);
//		return;
	    }	 
	    break;

	case 7:
	    t = "Target Background Grid";
	    break; // write me if needed

	case 8:
	    t = "Video Window";
	    break; // write me if needed

	case 9:
	    t = "Conditional Access";
	    if ( 0xE0 == (0xE0 & r[4] ) ) { // reserved bits check
		unsigned short casid, capid;
		r += 2;
		mprintf2( stdout, "     Tag %02X DLen %02X %s: ", n, k, t);
		casid = (r[0]<<8) | r[1];
		capid = 0x1FFF & ( (r[2]<<8) | r[3] );
		mprintf2( stdout, " CA system ID %04X CA PID %04X\n", casid, capid);
		r += 4; // skip to private data
		k -= 4;

		if (k > 0)
		{
		    mprintf2( stdout,   "      CA descriptor private data: "
				    "      ");
		    for ( i = 0; i < k; i++ ) {
			mprintf2( stdout, "%02X ", r[i]);
		    }
		    mprintf2( stdout, "\n");
		}
//		return;
	    }
	    break;

	case 10:
	    t = "ISO 639 Language(s)";
	    {
		// no reserved bits to check so balls it out
		unsigned char lang[4];
		unsigned char at; // audio type
		unsigned char *s;
		r += 2;
		mprintf2( stdout, "     Tag %02X DLen %02X %s: ", n, k, t);
		// spec mentions can be multiple audios, whee!
		for (i = 0; i < (k - 1); ) { // manually bump i
		    memcpy( lang, r, 3 );
		    lang[3] = 0;
		    r += 3;
		    i += 3;
		    mprintf2( stdout, "[%s] ", lang);
		}		
//		mprintf2( stdout, "\n");
		
		// audio type
		at = r[0];
		s = "ATSC Normal";
		if (1 == at) s = "Clean Effects";
		if (2 == at) s = "Hearing Impaired";
		if (3 == at) s = "Visual Impaired Commentary";
		if (4 <= at) s = "Reserved";
		mprintf2( stdout, " Audio Type %02X %s\n", at, s);
//		return;
	    }
	    break;

	case 11:
	    // reserved bits must be set to qualify for rest
	    if ( (0x40 == (0x40 & r[2]))
	      && (0x1F == (0x1F & r[3])) ) {
		unsigned char ecri, cai, cae;
		t = "System Clock";
		ecri = 1 & (r[2]>>7);	// external clock reference indicator
		cai = 0x3F & r[2];	// clock accuracy integer
		cae = 7 & (r[3]>>5);	// clock accuracy exponent
		mprintf2( stdout, "     Tag %02X DLen %02X %s: ", n, k, t);
		mprintf2( stdout, "External Clock %u ", ecri);
		mprintf2( stdout, "Accuracy is %u^%u\n",
			cai, cae);
//		return;
	    }
	    break;

	case 12:
	    t = "Multiplex Buffer Utilization";
	    {
		unsigned char bv;
		unsigned short ltwl, ltwu;

		ltwl = ltwu = 0;
		
		bv = 1 & (r[2] >> 7);	// bound valid flag

		// valid and reserved set?
		if ( (1 == bv) && (0x80 == (0x80 & r[4])) ) {
		    ltwl = 0x7FFF & ( (r[2]<<8) | r[3] );
		    ltwu = 0x7FFF & ( (r[4]<<8) | r[5] );
		}
		mprintf2( stdout, "     Tag %02X DLen %02X %s: ", n, k, t);
		mprintf2( stdout, "Valid %u\n", bv);

		// don't bother if not valid
		if (1 == bv) {
		    mprintf2( stdout, "      Legal Time Window Lower Bound %u Upper Bound %u\n",
			    ltwl, ltwu);
		}
//		return;
	    }
	    break;

	case 13:
	    t = "Copyright"; // lucky 13?
	    break;

	case 14:
	    t = "Maximum Bitrate";
	    if ( 0xC0 == (0xC0 & r[2]) ) {
		unsigned int mbr;
		unsigned char m[32];
		mbr = 0x3FFFFF & ( ( r[2] << 8 ) | r[3] );
		mprintf2( stdout, "     Tag %02X DLen %02X %s: ", n, k, t);
		lltoasc( m, (long long) mbr * 400 );
		mprintf2( stdout, "%s bits/second\n", m);
//		return;
	    }		
	    break;

	case 15:
	    t = "Private Data Indicator";
	    break;

	// smoothing buffer, not in 13818-1 draft 1994 jun 10, but in nov 13
	case 0x10:
	    t = "Smoothing Buffer";
	    {
		unsigned int sblr, sbz;
		r+=2;
		if ( (0xC0 == (0xC0 & r[0]))
		 &&  (0xC0 == (0xC0 & r[3])) ) {
		    mprintf2( stdout, "     Tag %02X DLen %02X %s: ", n, k, t);
//		    mprintf2( stdout, " %02X %02X %02X %02X %02X %02X\n", r[0], r[1], r[2], r[3], r[4], r[5]);
		    
		    sblr = 0x3FFFFF & ( (r[0]<<16) | (r[1]<<8) | r[2] );
		    sbz  = 0x3FFFFF & ( (r[3]<<16) | (r[4]<<8) | r[5] );
		    mprintf2( stdout, "Leak Rate %u bits/s Size %u\n",
			sblr*400, sbz);
//		    return;
		}
	    }
	    break;

//////////////////////////////////////////////////////////////// ATSC specific
	case 0x52:
	    t = "SCTE 35 Cue ID";
	    break;

	case 0x80:
	    t = "Stuffing";
	    mprintf2( stdout, "     Tag %02X DLen %02X %s: ", r[0], k, t);
	    for (i=0; i < k; i++) mprintf2( stdout, "%02X ", r[ 2 + i ] );
	    mprintf2( stdout, "\n");
//	    return;
	    break;

	case 0x81:
	    t = "A/52 Audio";
	    {
//		unsigned char lc, lc2, mid, asvcf, tl, tc, tb; //??
		unsigned char src, bsid, brc, sm, bsm, nc, fs;
		unsigned char *srt, *brt, *brt1, *smt, *nct, *lft, *bsmt;
		r += 2;
		src = 0x07 & (r[0]>>5);
		srt = "Reserved";
		if (0 == src) srt = "48 kHz";
		if (1 == src) srt = "44.1 kHz";
		if (2 == src) srt = "32 kHz";
		// must be the can't decide section nyuk
		if (4 == src) srt = "48 or 44.1 kHz";
		if (5 == src) srt = "48 or 32 kHz";
		if (6 == src) srt = "44.1 or 32 kHz";
		if (7 == src) srt = "48, 44.1 or 32 kHz";

		bsid= 0x1F & r[0];
		brc = 0x3F & (r[1]>>2); // bit rate code
		sm  = 0x03 & r[1]; // surround mode, docs also name it dsumod?
		bsm = 0x07 & (r[2]>>5); // bsmod
		nc  = 0x0F & (r[2]>>4); // number of channels
		fs  = 0x01 & r[2];

		brt1 = "Exact";
		if (0x20 == (0x20 & brc)) brt1 = "Maximum";
		brc &= 0x1F; // limit it now brt1 has been extracted

		brt = "Reserved";
		if ( 0 == brc) brt = " 32 kbits/second";
		if ( 1 == brc) brt = " 40 kbits/second";
		if ( 2 == brc) brt = " 48 kbits/second";
		if ( 3 == brc) brt = " 56 kbits/second";
		if ( 4 == brc) brt = " 64 kbits/second";
		if ( 5 == brc) brt = " 80 kbits/second";
		if ( 6 == brc) brt = " 96 kbits/second";
		if ( 7 == brc) brt = "112 kbits/second";
		if ( 8 == brc) brt = "128 kbits/second";
		if ( 9 == brc) brt = "160 kbits/second";
		if (10 == brc) brt = "192 kbits/second";
		if (11 == brc) brt = "224 kbits/second";
		if (12 == brc) brt = "256 kbits/second";
		if (13 == brc) brt = "320 kbits/second";
		if (14 == brc) brt = "384 kbits/second";
		if (15 == brc) brt = "448 kbits/second";
		if (16 == brc) brt = "512 kbits/second";
		if (17 == brc) brt = "576 kbits/second";
		if (18 == brc) brt = "640 kbits/second"; // like to hear that!
		

		smt = "Reserved";
		if (0 == sm) smt = "Not Indicated";
		if (1 == sm) smt = "NOT Dolby";
		if (2 == sm) smt = "Dolby";
		
		nct = "Reserved";
		lft = "";
		if ( 8 == (8 & nc)) lft = " + LFE";
		if ( 0 == nc) nct = "1+1";
		if ( 1 == nc) nct = "1/0";
		if ( 2 == nc) nct = "2/0";
		if ( 3 == nc) nct = "3/0";
		if ( 4 == nc) nct = "2/1";
		if ( 5 == nc) nct = "3/1";
		if ( 6 == nc) nct = "2/2";
		if ( 7 == nc) nct = "3/2";
		// lfe is extra channel but not in the text count
		if ( 8 == nc) nct = "1";
		if ( 9 == nc) nct = "<=2";
		if (10 == nc) nct = "<=3";
		if (11 == nc) nct = "<=4";
		if (12 == nc) nct = "<=5";
		if (13 == nc) nct = "<=6";

		bsmt = "Reserved";
		if ( 0 == bsm) bsmt = "Main Audio: Complete Main";
		if ( 1 == bsm) bsmt = "Main Audio: Music and Effects";
		if ( 2 == bsm) bsmt = "Associated: Visually Impaired";
		if ( 3 == bsm) bsmt = "Associated: Hearing Impaired";
		if ( 4 == bsm) bsmt = "Associated: Dialogue";
		if ( 5 == bsm) bsmt = "Associated: Commentary";
		if ( 6 == bsm) bsmt = "Associated: Emergency";
		if ( 7 == bsm) bsmt = "Associated: Voice Over";

		mprintf2( stdout, "     Tag %02X DLen %02X %s: Sample Rate %s, bsid %02X\n",
			    n, k, t, srt, bsid);
		mprintf2( stdout, "      %s Bit Rate %s (%02X) Surround %s (%02X)\n",
				    brt1, brt, brc, smt, sm);
		mprintf2( stdout, "      Service %s, Channels %s %s Full Service %u\n",
			    bsmt, nct, lft, fs);
//		return;
	    }
	    break;

	case 0x86:
	    t = "Caption Service";
	    if ( 0xC0 == (0xC0 & r[2]) ) { // reserved bits check
		unsigned char nos, cct, l21f, csn, er, war;
		unsigned char lang[4];

		mprintf2(stdout,"     Tag %02X DLen %02X %s: ",n, k, t);
		nos = 0x1F & r[2];
		r += 3; // skip parsed
		mprintf2( stdout, "Number of Services %u\n", nos);
		for (i = 0; i < nos; i++) {
		    if ( 0x40 == (0x40 & r[3]) ) {
			unsigned char *cctt;
			cctt = "ATVCC";
			memcpy( lang, r, 3 );
			lang[3] = 0; // 3 char lang code per ISO 639.2/B
			r += 3; // skip lang
			cct = 1 & (r[0]>>7);
			if (0 == cct) {
			    cctt = "NTSC CC";
			// don't care about EIA/CEA-608-B. that's NTSC
			    if ( 0x3E == (0x3E & r[0]) ) { // reserved
				l21f = 1 & r[0];
				mprintf2( stdout, "      [%s] Type %s Line21 %s Field ",
				    lang, cctt, (l21f == 0) ? "Even" : " Odd" );
			    }				
			} else {
			    // might be EIA-708-A ATVCC spec, not testing this
			    csn = 0x3F & r[0];
			    mprintf2( stdout,  "      [%s] Type %s Service Number %02X ",
				    lang, cctt, csn);
			}
			if ( (0x3F == (0x3F & r[1])) && (0xFF == r[2]) ) {
			    er = 1 & (r[1]>>7);
			    war = 1 & (r[1]>>6);
			    mprintf2( stdout, "Easy Reader %u Wide Aspect Ratio %u",
					er, war); 
			    r += 3;
			}			    
		    }
		    mprintf2( stdout, "\n");
		}
//		return;
	    }
	    break;

	case 0x87:
	    t = "Content Advisory";
	    if ( 0xC0 == (0xC0 & r[2]) ) {
		unsigned char rrc, rr, rd, rdj, rv, rdl;

		mprintf2(stdout,"     Tag %02X DLen %02X %s: ",n, k, t);
		rrc = 0x3F & r[2];
		mprintf2( stdout, " Rating Region Count %u\n", rrc);
		r += 3; // skip to loop vals
		for (i = 0; i < rrc; i++) {
		    rr = r[0]; // rating region
		    rd = r[1]; // rated dimensions

		    // one dimension will try to print on one line
		    mprintf2( stdout,
			    "      Region %u Dimensions %u:%s",
			    rr, rd, (rd > 1) ? "\n":"");
			    
		    r += 2;
		    for (j = 0; j < rd; j++) {
			if ( 0xF0 == (0xF0 & r[1]) ) {
			    rdj = r[0]; // rating dimension j
			    rv = 0xF & r[1]; // rating value
			    mprintf2( stdout, "       Dimension j %u Value %u Abbrev %s\n",
				    rdj, rv, ratings[ (10 * rdj) + rv ] );
			    r += 2;
			}
		    }
		    rdl = r[0]; // rating descriptor length
		    r++;
		    if (rdl != 0) {
			mprintf2( stdout, "      Description: ");
			test_pmt_mss( r );
		    }
		}
//		return;
	    }
	    break;

	case 0xA3:
	    t = "Component Name";
	    r += 2;
	    mprintf2( stdout, "     Tag %02X DLen %02X %s:  ", n, k, t);
	    test_pmt_mss( r );
//	    return;
	    break;

	case 0xAA:
	    t = "Redistribution Control";
	    mprintf2( stdout, "     Tag %02X DLen %02X %s: ", r[0], k, t);
	    for (i=0; i < k; i++) mprintf2( stdout, "%02X ", r[ 2 + i ] );
	    mprintf2( stdout, "\n");
	    break;
//	    return;
/***************************************************************************/

	// handle 0-1, 16-63, 64-255
	default:
	    // these have nothing to parse
	    t = "ignored";
	    if (n < 2) t = "Reserved";
	    if ((n >= 16) && (n <= 63)) t = "ISO/IEC 13818-1 Reserved";
	    if (n >= 64) t = "User Private";
	    mprintf2( stdout, "    *Tag %02X DLen %02X %s: ", n, k, t);
	    for (i = 0; i < k+2; i++) mprintf2( stdout, "%02X ", r[i] );
	    mprintf2( stdout, "\n");
	    break;
    }
}
/****************************************************************** MPEG PMT */



/***************************************************************** MPEG2 CAT */
static
void
test_cat_descriptor( unsigned char *r )
{
}

/***************************************************************** MPEG2 CAT */
static
void
test_cat( unsigned char *p )
{
    unsigned char *r;
    unsigned short sl;
    unsigned char vn, cni, sn, lsn;
    unsigned int cat_crc = 0;

    pkt.cat++;

    // point to table id
    r = p+5;

    // check reserved first, before crc to save time
    if  (  ( 0x30 != (0x30 & r[1]) )
	|| ( 0xFF != r[3] ) // reserved must be set
	|| ( 0xFF != r[4] ) // reserved
	|| ( 0xC0 != (0xC0 & r[5]) )
	)
    {
	mprintf1( stdout, " CAT reserved bits not set\n");
	return; // ignore if all reserved bits not set
    }

    sl = ((0xF & r[1])<<8) | r[2];	// 12 bit section length
    cat_crc = calc_crc32( r, sl+3 );	// only want crc to validate it
    if (cat_crc != 0) {
	pkt.crccat++;
	mprintf1( stdout, " CAT has CRC error\n");
	return;
    }
    pkt.cat++;
    vn = 0x1F & (r[5]>>1);
    cni = 1 & r[5];
    sn = r[6];
    lsn = r[7];

    mprintf1( stdout, " PID 0001 Conditional Access: SLen %03X VN %02X CNI %01X SN %02X LSN %02X\n",
	    sl, vn, cni, sn, lsn );

    r += 8;

    if ( r < (p+sl+4) ) {
	test_cat_descriptor( r );
    } else {
	mprintf1( stdout, "    Descriptor(s) 0\n");
    }

    mprintf1( stdout, "\n");
}

/***************************************************************** MPEG2 PID */
/* if apid list has room, add Audio PID to list */
static
void
add_aud_pid( short pid )
{
    int i;
    /* needs #define AUD_PID_MAX */
    if (aud_pidx >= 32) return;
    
    for (i=0; i < aud_pidx; i++)
	if ( pid == aud_pids[ i ] )
	    return;

//    fprintf( stdout, "added aud_pid[%d] %04X\n", aud_pidx, pid);
    aud_pids[ aud_pidx ] = pid;
    aud_pidx++;
}

/* return zero if pid if on audio list */
static
int
test_aud_pid( short pid )
{
    int i;
    for (i=0; i < aud_pidx; i++) {
//	fprintf( stdout, "testing audio pid %04X aud_pids[%d] %04X\n", pid, i, aud_pids[i]);
    	if ( pid == aud_pids[ i ] ) {
//	    fprintf( stdout, "audio pid %04X found\n", pid);
	    return 0;
	}
    }

//    fprintf( stdout, "audio pid %04X not found\n", pid);
    return ~0;
}

/* if vpid list has room, add Video PID to list */
static
void
add_vid_pid( short pid )
{
    int i;
    /* needs #define VID_PID_MAX */
    if (vid_pidx >= 8) return;
    
    for (i=0; i < vid_pidx; i++)
	if ( pid == vid_pids[ i ] )
	    return;

//    fprintf( stdout, "added vid_pids[%d] %04X\n", vid_pidx, pid);
    vid_pids[ vid_pidx ] = pid;
    vid_pidx++;
}

/* return zero if pid is on video list */
static
int
test_vid_pid( short pid )
{
    int i;
    for (i=0; i < vid_pidx; i++) {
//	fprintf( stdout, "testing video pid %04X vid_pids[%d] %04X\n", pid, i, vid_pids[i]);
	if ( pid == vid_pids[ i ] ) {
//	    fprintf( stdout, "video pid %04X found\n", pid);
	    return 0;
	}
    }
//    fprintf( stdout, "video pid %04X not found\n", pid);
    return ~0;
}


/**************************************************************** MPEG2 PMT */
static
void
test_pmt ( unsigned char *p, int pan )
{
    struct payload_s *pmp;
    unsigned char *r, *b, t;
    unsigned short sl, pgn, pcrpid, pilen, espid, esilen, pid, i;
    unsigned short vpid, acn, apid, apids[8];
    unsigned char vn, cni, sn, lsn, st;
    unsigned int pmt_crc, pmt_crc0;

    pkt.pmt++;

    pmp = &pmt[ pan ];
    
    pid = 0x1FFF & ((p[1]<<8)|p[2]);

    if (0 != arg_mgtpids) keep_pids[pid] = ~0; /* -k mgt keeps all PMT */

/* -e keeps PMT if program number matches */
    if (0 < arg_epn) {

/* TESTME: hold down until PAT seen */
//	if (0 == pkt.pat) return;

	if (pa[pan].pn == arg_epn) {
		keep_pids[ pid ] = ~0;
	}
    }

    pmp->payok = build_pmt_payload( p, pan );
    if (0 != pmp->payok) return; /* payload is not done yet */

/* point to table id */
    r = pmp->payload;

    /* check reserved first, before crc to save time */
    if (2 != r[0]) return; /* table id is always 2 */

    if ( 0xB0 != (0xF0 & r[1]) ) { /* check for syntax and zero bit after */
	t = 0xF0 & r[1];
	if (0 == (0x80 & t))
	    mprintf2( stdout, "PMT r[1] syntax bit 7 not set: %02X\n", t);
	if (0 == (0x30 & t))
	    mprintf9( stdout, "PMT r[1] reserved bits 4 & 5 are: %02X\n",
			0x30 & t);
    }

    if (   ( 0xC0 != (0xC0 & r[5]) )
	|| ( 0xE0 != (0xE0 & r[8]) )
	|| ( 0xF0 != (0xF0 & r[10]) )
	)
    {
	mprintf2( stdout, "PMT reserved bits not set: ");
	mprintf2( stdout, "%02X %02X %02X %02X\n",
		0xF0 & r[1], 0xC0 & r[5], 0xE0 & r[8], 0xF0 & r[10] );
//	return; /* ignore if all reserved bits not set */
    }

    sl = ((0xF & r[1])<<8) | r[2];

/* compute CRC32 */
    pmt_crc0 = calc_crc32( r, sl-1 );

/* received CRC32 */
    pmt_crc  = r[ sl - 1 ];
    pmt_crc <<= 8;
    pmt_crc |= r[ sl ];
    pmt_crc <<= 8;
    pmt_crc |= r[ sl + 1 ];
    pmt_crc <<= 8;
    pmt_crc |= r[ sl + 2 ];

    if (pmt_crc != pmt_crc0) {
	pkt.crcpmt++;
	mprintf2( stdout,
		    "PMT CRC ERROR PID %04X CALC %08X RCVD %08X\n",
		    pid, pmt_crc0, pmt_crc);
#if 0
	for (i = 0; i < 188; i++) {
	    if (old_pmt[i] != p[i])
		mprintf2( stdout,
			    "p[%d] %02X != %02X\n", i, old_pmt[i], p[i] );
	}
#endif
	return;
    } else {
	if (sl < 180)
	    memcpy( old_pmt, p, 188);
    }


    pgn = (r[3] << 8) | r[4];
    vn = 0x1F & (r[5] >> 1);

/* -q OUTPUT REDUX: FIXME: should? fall thru or else -f doesn't work */
    if ((0 != arg_redux) && (vn == pid_vn[pid])) return;
    pid_vn[pid] = vn; /* multi-VC issue */

/* crc displays after version check */
    if (pmt_crc == pmt_crc0) mprintf2( stdout, "PMT CRC %08X OK\n", pmt_crc);

    cni = 1 & r[5];
    sn = r[6];
    lsn = r[7];

    mprintf2( stdout, " MPEG PMT PID %04X: SLen %03X VN %02X CNI %01X SN %02X LSN %02X\n",
	    pid, sl, vn, cni, sn, lsn );

    pcrpid = 0x1FFF & ( (r[ 8] << 8) | r[ 9] ); /* PID of stream with PCR */
    pilen  = 0xFFF  & ( (r[10] << 8) | r[11] ); /* program info len */
    r += 12;

    mprintf2( stdout, "  Program %04X PCRPID %04X Program Info Len %03X\n",
		pgn, pcrpid, pilen);

    mprintf2( stdout, "   PROGRAM MAP DESCRIPTORS:\n");

    b = pmp->payload + 11 + pilen;

    for ( ; r < b; ) {


#if 0
/* this is broken now, use -f option to build_new_pmt instead */
/* only single packet PMTs can be rewritten */
	if ((sl < 180) && (0xAA == *r)) {
	    unsigned int pmt_crc1;
	    unsigned char *r1;

/* BROKEN: do not know why this construct does not work. Errors are: */
/* invalid operand to binary - */
/* invalid operand to binary + */
/* I can't explain why the operation on the pointer is wrong. use -f. */
	    r1 = r - pmp->payload;
	    r1 += p + 5;

	    mprintf2( stdout, "     Tag %02X Dlen %02x RC ", r1[0], r1[1]);

/* FIXME: r is pointing to pmp->payload instead of p+12+desc */
	    *r1 = 0x80; /* change to stuffing */
	    pmt_crc0 = calc_crc32( p+5, sl-1 ); /* recompute CRC */
	    p[ sl+4 ] = 0xFF & (pmt_crc0 >> 24);
	    p[ sl+5 ] = 0xFF & (pmt_crc0 >> 16);
	    p[ sl+6 ] = 0xFF & (pmt_crc0 >> 8);
	    p[ sl+7 ] = 0xFF & pmt_crc0; /* store CRC */
	    pmt_crc1 = calc_crc32( p+5, sl+3 ); /* extra verify */

/* mprintf2 */
	    mprintf2( stdout, " Now Tag %02X CRC32 %08X New %08X %s\n",
		     r1[0], pmt_crc, pmt_crc0, (0 == pmt_crc1) ? "OK":"BAD" );
	}
#endif
	test_pmt_descriptor( r );
	r += r[1]+2;
    }

    acn = 0;
    vpid = 0;
    apid = 0;
    memset( apids, 0, sizeof(apids));

    mprintf2( stdout, "\n   PROGRAM MAP STREAM TYPES AND DESCRIPTORS:\n\n");

    b = pmp->payload + (sl-1);

    for ( ; r < b; ) {

/* break out if these reserved bits are not set */
	if  (  (0xE0 == (0xE0 & r[1]))
	    && (0xF0 == (0xF0 & r[3])) )
	{
	    unsigned char *s;
	    st = r[0];
	    espid  = 0x1FFF & ( (r[1] << 8) | r[2] );
	    esilen = 0x0FFF & ( (r[3] << 8) | r[4] );

	    /* check for video pids against list and add if missing */
	    if (0x02 == st) {
		add_vid_pid( espid ); /* counting video separate */
		vpid = espid;
	    }

	    /* check for audio pids against list and add if missing */
	    if (0x81 == st) {
		add_aud_pid( espid ); /* counting audio separate */
		apids[acn++] = espid;
	    }

	    s = "Reserved";
	    if (0x7F < st)  s = "User Private";
	    if (0x01 == st) s = "MPEG1 Video";
	    if (0x02 == st) s = "MPEG2 Video";
	    if (0x03 == st) s = "MPEG1 Audio";
	    if (0x04 == st) s = "MPEG2 Audio";
	    if (0x05 == st) s = "MPEG2 Private";

/*	    if ( 6 == st) s = "MPEG2 PES Private"; /  13818-1 has this */

	    if (0x06 == st) s = "A/90 PES sync";

/* rest are not used */
	    if (0x07 == st) s = "MHEG";
	    if (0x08 == st) s = "DSM CC";
	    if (0x09 == st) s = "H.222.1";
	    if (0x0A == st) s = "13818-6 A";
/*	    if (11 == st) s = "13818-6 B"; /  13818-1 has this */
	    if (0x0B == st) s = "A/90 DSM-CC async";
	    if (0x0C == st) s = "13818-6 C";
/*	    if (13 == st) s = "13818-6 D"; /  13818-1 has this */
	    if (0x0D == st) s = "A/90 DSM-CC addressable";
	    if (0x0E == st) s = "13818-1 Auxiliary";
	    if (0x81 == st) s = "A/53b Audio"; /* spec says A/53b */
	    if (0x95 == st) s = "A/90 Data Service Table";
	    if (0xC2 == st) s = "A/90 PES sync";
	    
	    mprintf2( stdout,
		"    Stream Type (%02X) %s ES PID %04X ES Info Len %04X\n",
			st, s, espid, esilen);

	    r += 5; /* skip 5 bytes to get to descriptors */

	    /* might be multiple descriptors, so do til reach esilen */
	    if (0 < esilen) mprintf2( stdout, "     STREAM DESCRIPTORS\n");
	    for (i=0; i < esilen; ) { /* bump i manually here */
		test_pmt_descriptor( r );
		i += r[1]+2; /* add header + data len to i */
		r += r[1]+2; /* add header + data len to r */
	    }
	    mprintf2( stdout, "\n");
	} else break; /* out of reserved bits */
    }

    if (0 != arg_epn) {
	if (pgn == arg_epn) {
	    if (arg_ean < acn) {
		apid = apids[ 7 & arg_ean ];
		if (0 == apid) apid = apids[0]; /* main if wrong audio */
		if (0 != apid) keep_pids[ apid ] = ~0;
	    }
	    if (0 != vpid) keep_pids[ vpid ] = ~0;

/* can only rewrite single packet PMT at this time */
	    if (sl < 180)
/* rebuild PMT for one video one audio */
		if (0 != arg_forge)
		    build_new_pmt( p, vpid, apid );
	}
    }
}
/****************************************************************************/


#ifdef USE_A52_PAYLOAD
/************************************************************** A/52 Audio */
// build audio payload may have to adjust for AFC in middle of payload
static
void
build_audio_payload( unsigned char *p )
{
    unsigned int psi, pid;
    unsigned char afc;
//    unsigned char afl;
    unsigned char *r, pl;
//    unsigned char i, j;
    int ok;
    
    pid = 0x1FFF & ( (p[1] << 8) | p[2] );	// packet id
    psi = 1 & (p[1]>>6);			// payload start indicator
    afc = 3 & (p[3]>>4);			// adaptation field control

    r = p+4;					// skip transport header

#if 0
// won't be needed unless somehow audio is the clock ref PID
// KTRK is sending AFC 3 on audio and AFC 3 on video. Yikes!
//
    if (afc > 1) {
	afl = *r;				// adaptation field length
	if (afl > 0)				// afc=2 = afl=0xB7 says spec
	    test_mpeg2_adaptation_field( p );	// parse the field
	if (2 == afc) return;			// no payload follows
	r++;					// skip field length byte
	r += afl;				// skip past entire field
    }
#endif

    pl = (p+188) - r; // wouldnt 184 be easier? oh adaptation header above...

#ifdef USE_ES_EXTRACT
// FIXME: broken elsewhere, -y option should extract ES to .es file
    if (pid == arg_espid) {
#ifdef USE_BUFFER
	ok = write_buffer(es_buf, r, pl); /* fast */
#else
	ok = write(out_file, r, pl);
#endif

/* writes should always pass */
	if (ok != pl) {
	    fprintf(stdout, "%s ES write %s failed\n", WHO, es_name);
	    exit(1);
	}
    }
#endif

    if (1 == psi)
    {
	if (1 == aud.payok)
	{
	    // only if no continuity counter error
	    if (0 == pid_cc[ pid ])
	    {
		mprintf6( stdout,
			    "  DONE A/52 Audio PID %04X Payload %05X bytes\n",
			    pid, aud.payoff );

		mprintf3( stdout, "\n A/52 Audio # %8d PID %04X # %8d\n\n",
			    pkt.aud, pid, pids[ pid ]);

// do these BEFORE it gets overwritten by next packet

	    } else {
		mprintf6( stdout, " *DROP previous discontinuous A/52 payload\n");
	    }
	}

	mprintf6( stdout, " START A/52 Audio PID %04X Payload\n", pid);
	aud.payok = 0;
	aud.payoff = 0;
	memset( aud.payload, 0, sizeof(aud.payload) );
	memcpy( aud.payload, r, pl);
	aud.payoff += pl;
    } else {
	if (0 == aud.payoff) {
	    mprintf6( stdout, " *DROP A/52 Audio PID %04X w/o psi\n", pid);
	} else {
	    aud.payok = 0;
	    if ( (aud.payoff+pl) >= sizeof(aud.payload) ) {
		mprintf3( stdout, "\n  A/52 Audio PID %04X payoff %d is over %d bytes\n",
			pid, aud.payoff+pl, sizeof( aud.payload ) );
		return;
	    }

//	    j = 0; // zero detect not needed for A/52
//	    for (i=0; i<pl; i++) j |= r[i];

	    if (0 == pid_cc[ pid ] ) {
		memcpy( &aud.payload[ aud.payoff ], r, pl );
		mprintf6( stdout,
			"  COPY A/52 Audio PID %04X Payload offset %05X\n",
			pid, aud.payoff);
		aud.payoff += pl;
		aud.payok = 1;
	    } else {
		mprintf6( stdout,
			" *DROP A/52 Audio PID %04X continuity error\n", pid);
		aud.payoff = 0;
		aud.payok = 0;
	    }
	}
    }
}
#endif

/**************************************************************** A/52 Audio */
/*no afc on these, should be PES AC-3 only. could extract by itself. */
static
void
test_a52_audio( unsigned char *p )
{
    unsigned short pid;

    pkt.aud++; // total audio, have to use pid lut for individual audio

    if (0 != arg_epn) {
	if (0 == pkt.pmt) return; /* no audio until pmtpid found */
	if (0 == pkt.vid) return; /* no audio until first video */
    }
    pid = 0x1FFF & ( (p[1] << 8) | p[2] );	// packet id

#if USE_A52_PAYLOAD
    build_audio_payload( p );
#endif

}
/****************************************************************************/


/**************************************************************** MPEG2 Video */
/* build video payload adjusts for AFC at start of payload */
static
void
build_video_payload( unsigned char *p )
{
    int ok, pl;
    unsigned int psi, pid, afc, afl;
    unsigned char *r;
    unsigned int i;
    unsigned int j;
//    unsigned char *t;
    
    pid = 0x1FFF & ( (p[1] << 8) | p[2] );	// packet id
    psi = 1 & (p[1]>>6);			// payload start indicator
    afc = 3 & (p[3]>>4);			// adaptation field control

#if 0
    if (0 != psi) {
	fprintf( stdout, "\n PSI ");
	for( i= 0 ; i < 20; i++) fprintf( stdout, "%02X ", p[i] );
	fprintf( stdout, "AFC%d\n", afc);
    }
#endif

    r = p+4;					// skip transport header

/* AFC can occur in the middle of any payload? */
    if (afc > 1) {
	afl = *r;				// adaptation field length
	if (afl > 0)				// afc=2 = afl=0xB7 says spec
	    test_mpeg2_adaptation_field( p );	// parse the field (PCR)
	if (2 == afc) return;			// no payload follows
	r++;					// skip field length byte
	r += afl;				// skip past entire field
    }
//    return;

    pl = (p+188) - r; // payload count after ts and adaptation headers

#ifdef USE_ES_EXTRACT
// WORKS: -y option extracts Video ES to .es file, no TS header or AFC
// It does leave some PES data that confuses Xine, but not mpeg2dec
    if (pid == arg_espid) {
#ifdef USE_BUFFER
	ok = write_buffer(es_buf, r, pl);
#else
	ok = write(es_file, r, pl);
#endif

/* writes should always pass */
	if (ok != pl) {
	    fprintf(stdout, "%s ES write %s failed\n", WHO, es_name);
	    exit(1);
	}
    }
#endif

/* payload start indicator set */
    if (1 == psi)
    {
	ppsbytes_in = psbytes_in;
	psbytes_in = bytes_in;

/* look for sequence header in first packet, after AFC */
	test_mpeg2_video_seq( r, pl );
	if (0 != arg_seqwr) return; /* -s option doesn't need more */
	
/* TESTME: vid.payok is 0 first time through? should be 1 */
	if (1 == vid.payok)
	{
	    if (0 == pid_cc[ pid ])
	    { // but only if no CC error
		for (i = vid.payoff; i > 0; i-- ) {
		    if (0 != vid.payload[i]) break; // non zero is non stuffing
		}
		if ( (i + 4) < vid.payoff) i += 4; // point forwards a bit
		j = vid.payoff - i; // stuffing count
		mprintf6( stdout,
	    	    "  DONE MPEG Video PID %04X Payload %05X, data %05X, zeroes %5X %c\n",
//	    		pid, vid.payoff, i, j, (( j<<1) < vid.payoff) ? ' ':'*');
	    		pid, vid.payoff, i, j, (j < 188) ? ' ':'*');

		mprintf4( stdout, "\n MPEG Video # %8d PID %04X # %8d\n\n",
			    pkt.vid, pid, pids[ pid ]);

		/* -s and -q options do not fully parse video headers */

/* -m8 option needed to parse rest of payload */
		if (0 != (8 & arg_mdump))
		    test_mpeg2_video_es();


	    } else {
		mprintf6( stdout, " *DROP previous MPEG2 payload\n" );
	    } // but still have to copy current new start payload
	}

	mprintf6( stdout, "\n START MPEG Video PID %04X Payload\n", pid);
	vid.payok = 0;
	vid.payoff = 0;

	// clear it so zero detect works
	memset( vid.payload, 0, sizeof(vid.payload) );
	memcpy( vid.payload, r, pl);
	vid.payoff += pl;
    } else {
	// payload start indicator not set
	if (0 == vid.payoff) {
	    mprintf6( stdout, " *DROP MPEG Video PID %04X w/o psi\n", pid);
	} else {
	    vid.payok = 0;
	    if ( (vid.payoff+pl) >= sizeof(vid.payload) ) {
		mprintf4( stdout,
			"\n  MPEG PID %04X payoff %d is over %d bytes\n",
			pid, vid.payoff+pl, sizeof( vid.payload ) );
		return;
	    }

/* -m8 to keep building payload, otherwise skip all the cycles */
	    if (0 == (8 & arg_mdump)) return;


// zero detect to end payload early
// will have 0-187 bytes of 0 stuffing per payload	    
	    j = 0;
	    for (i=0; i < pl; i++) j |= r[i];

// now cc error here in middle of payload is bad news,
// so will drop entire payload to wait for next psi.
// make this optional. decoder might have a few tricks.
//	    if ( 0 == pid_cc[ pid ] )
	    if (1)
	    {
		memcpy( &vid.payload[ vid.payoff ], r, pl );
		mprintf6( stdout,
			"  COPY MPEG Video PID %04X Payload offset %05X %s\n",
			pid, vid.payoff, (j==0)?"ZERO":"");
		vid.payoff += pl;
		vid.payok = 1;
	    } else {
		mprintf6( stdout,
			" *DROP MPEG Video PID %04X continuity error\n", pid);
		vid.payoff = 0; // will inhibit this until next psi
		vid.payok = 0; // will inhibit previous bad payload write
	    }
	}
    }
}

/*
	no crc available as far as i know. mpeg2 has a provision for it,
	but ATSC spec claims it should be 0 (not used?)
	or didn't understand what i was reading.
	these two not sure how to handle, can't do CRC on them.
	CABLE: these have a very good chance of being scrambled eggs useless.
	adaptation field might have interesting values in it and there may be
	some mpeg2 letterbox/pillarbox indicators too, but none seen so far
*/

/* gprof: reduce number of calls if not doing slice level */

static
void
test_mpeg2_video ( unsigned char *p )
{
    int psi;

    psi = 1 & (p[1]>>6);

    pkt.vid++;

//    if (0 != arg_redux) return;

    if (0 < arg_epn)
	if (0 == pkt.pmt) /* wait until first PMT for this program */
	    return;

    if (0 != arg_espid) {
	build_video_payload( p );
	return;
    }
/* -s option needs to generate sequence byte offset and picture type if psi */
    if (0 != arg_seqwr) {
	if (0 != psi) {
	    build_video_payload( p );
	}
    } else {
/* -q option is quick scan, doesn't need build payload */
	if (0 != (8 & arg_mdump))
		build_video_payload( p );
    }

//    pkt.vid++;				/* count all */
}
/****************************************************************************/

static
int
find_pa_pid( unsigned int pid )
{
    int i;
    for (i = 0; i < pat_ncs; i++)
	if (pid == pa[i].pmtpid)
	    return i;
    return -1;
}


/**************************************************************** MPEG2 PID */
/*ATSC parsed out, whats left is PAT, CAT, PMT, MPEG Video and A/52 Audio ES
  tei should have been checked in test packet. control PID are PAT CAT PMT
  no need to explore MPEG2 here, check CRC of control PID and log result
*/
static
void
test_mpeg2_packet( unsigned char *p )
{
	unsigned int psi, pid, afc;
	int pan;
    
	pid = 0x1FFF & ( (p[1] << 8) | p[2] );	// packet id
	psi = 1 & (p[1]>>6);			// payload start indicator
	afc = 3 & (p[3]>>4);			// adaptation field control

// update counters
	pkt.mpeg2++;

//////////////////////////////////////////////////////////// MPEG2 PAT PMT 
// first two table type pids for mpeg2 transport. cat is ignored

	// Program Association Table, should always be single packet?
	if (pid == 0) {
		test_pat( p ); // crc check at least
		return;
	}

	// Conditional Access Table, could be multiple packet?
	if (pid == 1) {
		test_cat( p ); // crc check at least
		return;
	}

/* is the pmt on the pa list? will force pmt parse to after first PAT */
	pan = find_pa_pid( pid );
	if (-1 < pan) {
		test_pmt( p, pan );
		return;
	}


/////////////////////////////////////////////////////////// MPEG2 PAT & CAT

/////////////////////////////////////////////////////////////////// MPEG2 PES
/* -q output redux option doesn't need MPEG data */
//	if ( (0 == (8 & arg_mdump)) && (0 == arg_seqwr) ) return;
	if ( (0 != arg_redux) && (0 == arg_seqwr) && (0 == arg_espid))
		return;

	if (0 == test_vid_pid( pid )) {
		test_mpeg2_video( p );
	} else {
		if (0 == test_aud_pid( pid )) {
			test_a52_audio( p );
		}
	}

	pkt.pes++;
	return; // match returns
}
//////////////////////////////////////////////////////////////////// MPEG2 PES



/////////////////////////////////////////////////////////////////////// VCT

// A/65b Table 6.29 Service Location Descriptor
static
void
test_vct_svloc( unsigned char *r)
{
    unsigned short pcpid;
    unsigned char nelm;
    unsigned char i;
    
    if ( 0xE0 == (r[0] & 0xE0))
    {
	pcpid = ((0x1F & r[0])<<8) | r[1];	// PCR PID
	nelm = r[2];				// number of elementary streams
	aprintf2( stdout, "PCR PID %04X, num ES %02X\n", pcpid, nelm);
	r += 3;
	for (i = 0; i < nelm; i++) {
	    if (0xE0 == (0xE0 & r[1])) {
		unsigned short epid;
		unsigned char strt;
		unsigned char *strx;
		unsigned char lang[4];

		strx = "*unparsed*";

		memset(lang, 0, sizeof(lang));
		strt = *r++;			// stream type
		epid = ((0x1F & r[0])<<8) | r[1]; // elementary program id
		r += 2;
		memcpy( lang, r, 3 );
		r += 3;
		lang[4] = 0;			// language
		if (strt == 0x02) strx = "MPEG Video";
		if (strt == 0x81) strx = "A/52 Audio";
		// rest is all A/90 spec that isn't used
				
		aprintf2( stdout,
			"      Stream Type (%02X) %s ES PID %04X [%s]\n",
			strt, strx, epid, lang);
//		fflush(stdout);
	    }
	}	
    }
}


// A/65b Table 6.38 Multiple String Structure
// does do huffman decomp
static
void
test_vct_mss( unsigned char *r)
{
    unsigned int nstr, nseg, comp, mode, nchr;
    unsigned int i,j,k;
    unsigned char lang[4];
    unsigned char s[256];
    unsigned char d[512];
//    unsigned char *t;

    memset( s, 0, sizeof(s));
    nstr = r[0];

    nstr = 1; /* force to one */
    for (i = 0; i < nstr; i++) {
	memcpy(lang, &r[1], 3);
	lang[3] = 0;
	r += 4; // 3?
	nseg = *r++;

	nseg = 1; /* force to one */
	for (j = 0; j < nseg; j++) {
	    comp = *r++;
	    mode = *r++;
	    nchr = *r++;
	    for (k = 0; k < nchr; k++) s[k] = *r++;

	    // show non-compressed
	    if ( (0 == comp) && (0 == mode) ) {
		aprintf2( stdout, "[%s] [%-s]", lang, s);
	    }

	    // show huffman compressed
	    if  (
//		 (mode == 0xFF) && // spec requires, KPXB ignores
		 (comp > 0) &&
		 (comp < 3)
		)
	    { 
		    huffman_decode( d, s, sizeof(d), nchr, comp);
		    aprintf2( stdout, "[%s] [%-s]", lang, d);
	    }
	    // show as unknown compress
	    if (comp > 2) aprintf2( stdout, "[%s] [L%02X C%02X M%02X]",
				    lang, nchr, comp, mode);
	}
    }
    aprintf2( stdout, "\n");
}


// r points to start of descriptors
// do until len exhausted
static
void
test_vct_descriptor( unsigned char *p, unsigned int len )
{
    unsigned char dtag, dlen;
    unsigned char *r, *t;

    r = p;
    dtag = dlen = 0;

    t = "*unknown*";
	
    while ( len > 0 ) {
	dtag = *r++;
	dlen = *r++;
	len -= 2;
//	aprintf2( stdout, "   VCT DESCRIPTORS:\n");
	switch( dtag ) {

	    // extended channel name descriptor is mulitple string structure
	    case 0xA0:
		t = "Extended Channel Name:\n     ";
		aprintf2( stdout, "    VCT Tag %02X DLen %02X %s", dtag, dlen, t);
		test_vct_mss(r);
		break;

	    // service location descriptor
	    case 0xA1:
		t = "Service Location";
		aprintf2( stdout, "     VCT Tag %02X DLen %02X %s: ", dtag, dlen, t);
		test_vct_svloc(r);
		break;

	    // time shifted service descriptor
	    case 0xA2:
		t = "Time Shifted Service";
		aprintf2( stdout, "     VCT Tag %02X DLen %02X %s: ", dtag, dlen, t);
		break;

	    // got some weird ones showing up here need to parse?
	    default:
		t = "ignored";
		aprintf2( stdout, "     VCT *Tag %02X DLen %02X: %s\n", dtag, dlen, t);
		break;
	}
	r += dlen;
	len -= dlen;
    }
}


// not going to implement. verify top bits and count only
static
void
test_dccsct( unsigned char *p )
{
	pkt.dccsct++;
	return;
}

// not going to implement. verify top bits and count only
static
void
test_dcct( unsigned char *p )
{
	pkt.dcct++;
	return;
}



// A/65b Table 6.38 Multiple String Structure
// does do huffman decomp
static
void
test_rrt_mss( unsigned char *r)
{
    unsigned int nstr, nseg, comp, mode, nchr;
    unsigned int i,j,k;
    unsigned char lang[4];
    unsigned char s[256];
    unsigned char d[512];

    memset( s, 0, sizeof(s));
    nstr = r[0];
    for (i = 0; i < nstr; i++) {
	memcpy(lang, &r[1], 3);
	lang[3] = 0;
	r += 4; // 3?
	nseg = *r++;
	for (j = 0; j < nseg; j++) {
	    comp = *r++;
	    mode = *r++;
	    nchr = *r++;
	    for (k = 0; k < nchr; k++) s[k] = *r++;

//	    aprintf5( stdout, "\n       RRT L%02X C%02X M%02X ", nchr, comp, mode);
	    // show non-compressed
	    if ( (0 == comp) && (0 == mode) ) {
		aprintf5( stdout, "[%s] [%-s] ", lang, s);
	    }
	    // show huffman compressed
	    if  (
//		 (mode == 0xFF) && // spec requires, KPXB ignores
		 (comp > 0) &&
		 (comp < 3)
		)
	    { 
		    huffman_decode( d, s, sizeof(d), nchr, comp);
		    aprintf5( stdout, "[%s] [%-s] ", lang, d);
	    }
	    // show as unknown compress
	    if (comp > 2) aprintf5( stdout, "[%s] [L%02X C%02X M%02X]",
				    lang, nchr, comp, mode);

	}
    }
}


// payload assembly for RRT
static
unsigned int
build_rrt_payload( unsigned char *p )
{
    unsigned char *r;
    unsigned char psi;
    //, vn;
    
    r = p+5;

    psi = 1 & ( p[1] >> 6);

    // handle payload start packet by copying start to RRT payload buffer
    if (psi != 0)
    {
	if ( (0xCA == r[0]) 			// table id RRT
	  && (0xF0 == (0xF0 & r[1]) )		// syntax,private,reserved bits
	  && (0xC1 == (0xC1 & r[5]) )		// reserved bits
	   )
	{
	    rrt_vn = 0x1F & ( r[5] >> 1);
	    rrt.sl = ((0xF & r[1])<<8) | r[2];

	    rrt.payoff = 183;
	    aprintf6( stdout,
		" START RRT Version %02X SLen %03X Payoff %03X\n",
		rrt_vn, rrt.sl, rrt.payoff);

	    memset( rrt.payload, 0, sizeof(rrt.payload) );
	    memcpy( rrt.payload, r, rrt.payoff );

	    if ( rrt.payoff > rrt.sl+3 ) {
		aprintf6( stdout, "   DONE RRT\n");
		return 0;
	    }
	}
	return 1;
    }

    r = p+4;

    // additional non start payloads append to RRT payload buffer
    if ( psi == 0 ) {
	memcpy( &rrt.payload[rrt.payoff], r, 184);
	rrt.payoff += 184;
	aprintf6( stdout, 
		"  COPY RRT Version %02X SLen %03X Payoff %03X\n",
		    rrt_vn, rrt.sl, rrt.payoff);

	if ( rrt.payoff > rrt.sl+3 ) {
	    aprintf6( stdout, "   DONE RRT\n");
	    return 0;
	}
	return 1;
    }

    aprintf6( stdout, "RRT not parsed\n");
    return 1;
}



// Content Advisory description text in EIT makes this redundant.
// but has to be parsed for completeness for stations w/o description
static
void
test_rrt( unsigned char *p )
{
    unsigned char *r;
    unsigned short sl, dl;
    unsigned char vn, cni, sn, lsn, pv;
    unsigned char i, j, rr, rrnl, dd, ddnl, arvl, rvl, gs, vd;
    
    rrt.payok = build_rrt_payload( p );
    if (0 == rrt.payok) {

	pkt.rrt++;
	r = rrt.payload;
    
	if ((0xCA != r[0]) || (0xFF != r[3]) ) {
	    aprintf5( stdout, "  RRT has bad TT/RB\n");
	    return;
	}

	sl = 0xFFF & ((r[1]<<8) | r[2]);		// section len
	if (sl > 1021) return; // not allowed

	rrt_crc = calc_crc32( r, sl+3);		// crc check
	if (0 != rrt_crc) {
	    pkt.crcrrt++;
	    aprintf5( stdout, "  RRT CRC ERROR\n");
	    //rrt_crc, r[sl], r[sl+1], r[sl+2], r[sl+3]);
	    return;
	}

	aprintf5( stdout, "RRT CRC OK\n");
	vn = 0x1F & (r[5]>>1);
	cni = 1 & r[5];
	sn = r[6];
	lsn = r[7];
	pv = r[8];

	rr = r[4];

	r += 9; // skip parsed
	aprintf5( stdout, " RRT # %d SLen %03X Ver %02X CNI %d SN %02X LSN %02X PV %02X\n",
	pkt.rrt, sl, vn, cni, sn, lsn, pv);
	
	aprintf5( stdout, "  Rating Region: ");
	rrnl = r[0];
	r++;
	test_rrt_mss( r );
	aprintf5( stdout, "\n");
	
	r += rrnl;

	dd = r[0];
	r++;
	// dimension names
	for (i = 0; i < dd; i++) {
	    ddnl = r[0];
	    r++;
	    aprintf5( stdout, "   Dimension Index %d Name: ", i);
	    test_rrt_mss( r );
	    aprintf5( stdout, "\n");
	    r += ddnl;
	    if (0xE0 == (0xE0 & r[0])) {
		gs = 1 & (r[0] >> 4);
		vd = 0xF & r[0];
		r++;
		aprintf5( stdout, "    %sRating Values %d:\n",
			( 0 == gs) ? "":"Graduated ", vd );
		for (j = 0; j < vd; j++) {
		    arvl = r[0];
		    r++;
		    aprintf5( stdout, "      Index %d ", j);
		    // abbreviated rating values
		    test_rrt_mss( r );
		    r += arvl;
		    rvl = r[0];
		    r++;
		    aprintf5( stdout, "\t");
		    // rating value
		    test_rrt_mss( r );
		    r += rvl;
		    aprintf5( stdout, "\n");
		}
//		aprintf5( stdout, "\n" );
	    }
	}	
	
	// final descriptors
	if ( 0xFC == r[0] ) {
	    dl = 0x3FF & ( ( r[0]<<8) | r[1] );
	    aprintf5( stdout, "RRT DESCRIPTORS LEN %03X\n", dl);	    	    
	    // rrt descriptors, not sure what kind they are
	}
    }

    aprintf5( stdout, "\n");
}


/***************************************************************** ATSC ETT */

#if 0
/* look up vc# 0-n by program number */
static
int
find_vc_pgm( unsigned short pn )
{
    int i;
    if (vct_ncs < 1) return -1;

    for (i=0; i < vct_ncs; i++)
	if (pn == vc[i].pgm)
	    return i;
    return -1;
}
#endif

/* Return VC index 0-n from source id (top 16 bits of ETMID), or return -1. */
static
int
find_vc_src( unsigned short sr )
{
    int i;
    if (vct_ncs < 1) return -1;

    for (i=0; i < vct_ncs; i++)
	if (sr == vc[i].src)
	    return i;
    return -1;
}


/* Extract source id from etmid to limit search length to events on this VC.
 * Return pgm[] index of matching etmid found or return -1.
 * TODO: Export this change to atscap, it can save some CPU.
 */
static
int
find_pgm_etmid( unsigned int etmid )
{
	int i, j, z, vcn, sr;
    
	sr = etmid>>16;
	vcn = find_vc_src( sr );

	z = EIZ * PEZ;
	j = vcn * z;

/* 0 to 1024 search, could be reduced to 0-768 search. 769-1023 are blanks */
	for (i = 0; i < z; i++)
		if (etmid == pgm[ i + j ].etmid) return i;
	return -1;
}


/* A/65b Table 6.38 Multiple String Structure */
/* does do huffman decomp */
/* n is ETT n */
static
void
test_ett_mss( unsigned char *r, unsigned int n, unsigned int etmid, int pgo)
{
    unsigned int nstr, nseg, comp, mode, nchr, onchr, sr, i, j;
    unsigned char lang[4];
    unsigned char s[PDZ];
    unsigned char d[PDZ];
    unsigned char *t;		/* points to uncompressed */
    	
    nstr = r[0];
    nstr = 1; /* override */

    sr = 0x7 & (etmid>>16);

    n &= 0x7f;

    for (i = 0; i < nstr; i++) {
	memcpy(lang, &r[1], 3);
	lang[3] = 0;
	r += 4; /* 3? */
	nseg = *r++;

	nseg = 1; /* override */

	for (j = 0; j < nseg; j++) {
	    
	    comp = *r++;
	    mode = *r++;
	    nchr = *r++;

	    onchr = nchr;
	    

	    /* each seg n has chars n */
	    memset( s, 0, PDZ); /* clear old */
	    memset( d, 0, PDZ);
	    onchr = nchr;

	    memcpy( s, r, nchr); /* keep last nul */
	    
	    t = s; /* point to source, it may be uncompressed */
	    aprintf4( stdout, "  ETT-%02X pg[%04X] ETMID %08X ", n, pgo, etmid);
	    aprintf4( stdout, "[L%02X C%02X M%02X] ", onchr, comp, mode);

	    /* non-compressed */
	    if ( (0 == comp) && (0 == mode) ) {
		aprintf4( stdout, "[%s]\n   [%-s]", lang, s);
	    }

	    /* huffman compressed */
	    if  (
/*		 (mode == 0xFF) &&     spec requires, KPXB ignores */
		 (comp > 0) &&
		 (comp < 3)
		)
	    { 
		t = d; /* point to decomp text */
		huffman_decode( d, s, sizeof(d), onchr, comp);
		aprintf4( stdout, "[%s] Huffman\n   [%-s]", lang, d);
	    }
	    /* unknown compression as numeric detail */
	    if (comp > 2) {
		t = d; /* point to detail */
		snprintf( d, sizeof(d)-1, "[%s] [L%02X C%02X M%02X]",
				    lang, onchr, comp, mode);
	    }

/* if pgo was not found, can't update the description */
	    if (-1 == pgo) {
		aprintf4( stdout,
		    "\n   *ignored ETT-%02X ETMID %08X, no EIT pgm[] match",
			    n, etmid);

/* FIXME: -q version redux needs to clear saved version if no EIT pgm match */
// this is not enough
//		if (vcn >= 0) ettvn[ (vcn << 7) + n ] = 0xFF; /* vcn * 128 */

	    } else {

/* copy uncompressed description to program guide */
		memcpy( pgm[ pgo ].desc, t, PDZ );
/*		fprintf( stdout, "\npgo %04X ETMID %08X %s <- %s", */
/*			pgo, pgm[pgo].etmid, pgm[pgo].name, pgm[pgo].desc); */
	    }
	    r += onchr; /* got chars n, get next seg */
	}
    }
    aprintf4( stdout, "\n");
}


/* add to ett payload. returns 0 if done, or 1 if not done */
static
unsigned int
build_ett_payload( unsigned char *p, unsigned int n )
{
    unsigned int psi, tei;
    unsigned char *r;
    
    psi = 1 & (p[1]>>6);
    tei = 1 & (p[1]>>7);
    if (tei != 0) return 1;
    
    r = p+5;
/* handle payload start packet by copying start to ETT payload buffer */
    if (psi != 0) {
/*	ett[n].payoff = 0; */
	if ( (0xCC == r[0]) 
	  && (0xF0 == (0xF0 & r[1]) )
	  && (0xC1 == (0xC1 & r[5]) )
	   )
	{
	    ett[n].sl = ((0xF & r[1])<<8) | r[2];
	    ett[n].payoff = 183;
	    ett_vn = 0x1F & (r[5]>>1);

	    aprintf6( stdout,
		    " START ETT-%02X Version %02X SLen %03X Payoff %03X\n",
		    n, ett_vn, ett[n].sl, ett[n].payoff);
	    memset( ett[n].payload, 0, sizeof(ett[n].payload) ); /* reset */
	    memcpy( ett[n].payload, r, ett[n].payoff);

	    if ( ett[n].payoff > ett[n].sl+3 ) {
		aprintf6( stdout, "  DONE ETT-%02X\n", n);
		return 0;
	    }
	    return 1;
	}
    }

    r = p+4;

    /* additional non start payloads append to ETT payload buffer */
    if ( (ett[n].sl > 179) && (0 == psi) ) {
	memcpy( &ett[n].payload[ett[n].payoff], r, 184);

	ett[n].payoff += 184;

	aprintf6( stdout, "  COPY ETT-%02X Version %02X SLen %03X Payoff %03X\n",
		n, ett[n].vn, ett[n].sl, ett[n].payoff );

	if ( ett[n].payoff > ett[n].sl+3 ) {
	    aprintf6( stdout, "   DONE ETT-%02X\n", n);
	    return 0;
	}
	return 1;
    }
    aprintf6( stdout, " ETT-%02X not parsed\n", n);
    return -1;
}


/* process ett packet based on payload start indicator */
static
void
test_ett ( unsigned char *p, unsigned int n )
{
	unsigned char *r;
	unsigned short sl, etidx, sr;
	unsigned char vn, cni, sn, lsn, pv;
	unsigned int etmid;
	unsigned int ett_crc = 0;
	int vcn, pgo;
/*
    p.0 is 0x47 sync
    p.1 is 3 bits tei psi pri, 5 high bits pid
    p.2 is 8 bits low bits pid
    p.3 is tsc afc cc 
    p.4 is 0 if table and payload start indicator 1 else data
    p.5 is table id if payload start indicator 1 else data
*/
	/* returns 0 if payload complete, else nz */
	ett[n].payok = build_ett_payload( p, n );
	if (0 != ett[n].payok) return;

	r = ett[n].payload;

	if (0xCC != r[0]) {
	    aprintf4( stdout, "  Table ID not ETT 0xCC but 0x%02X\n", r[0]);
	    return;
	}	

	sl = ((0xF & r[1])<<8) | r[2];		/* section len */
	ett_crc = calc_crc32( r, sl+3);		/* crc check */
	if (0 != ett_crc) {
	    pkt.crcett++;
	    aprintf4( stdout, "  ETT-%02X CRC ERROR\n", n);
	    return;
	}

	pv = r[8];
	if (pv != 0) {
	    aprintf4( stdout, "  ETT-%02X Protocol %02X not 0\n", n, pv);
	    return;				/* protocol 0 only */
	}
	etidx = (r[3] << 8) | r[4];		/* should be 0 */

/*
 dont treat as fatal to packet processing
 CBS KHOU seems to use table id extension bits and is allowed by spec

	if (etidx != 0) {
	    aprintf4( stdout,
		"\n  ETT-%02X table id extension %04X is not 0\n", n, etidx);
	    return;
	}
 */

	vn = (0x3E & r[5]) >> 1;

	cni = 1 & r[5];
	sn = r[6];
	lsn = r[7];

	etmid = (r[9]<<24) | (r[10]<<16) | (r[11]<<8) | r[12];
	sr = etmid>>16;

	pkt.ett++;

	vcn = find_vc_src( sr ); /* get 0-n index for VC # from source # */

/* holds down ETT display until VCT seen */
	if (vcn < 0) return;
	pgo = find_pgm_etmid( etmid );

/* OUTPUT REDUX: */
	if (0 != arg_redux) {
		/* do nothing if no ETMID from previously reeived EIT */
		if (pgo < 0) return;
		/* do nothing if version stays the same */
		if (vn == ettvn[ (vcn << 7) + n ]) return;
	}

/* only update version if previous etmid found, -g -q missing desc fix */
	ettvn[ (vcn << 7) + n ] = vn; /* vcn * 128 */

	aprintf4( stdout, "ETT-%02X CRC OK\n", n);

	aprintf4( stdout," ETT-%02X VER %02X SLen %03X CNI %d "
			 "SecN %02X LSecN %02X etmID %08X pgo %d\n",
	    n, vn, sl, cni, sn, lsn, etmid, pgo );

	r += 13; /* skip parsed */
	
	test_ett_mss( r, n, etmid, pgo );

	aprintf4( stdout, "\n");
	return;
}


/***************************************************************** ATSC EIT */
/* Event Information Table Content Advisory (Rating Region Table text) */
/* A/65b Table 6.38 Multiple String Structure */
/* does do huffman decomp */
static
void
test_eit_rr_mss ( unsigned char *r )
{
    unsigned int nstr, nseg, comp, mode, nchr;
    unsigned int i,j,k;
    unsigned char lang[4];
    unsigned char s[256];
    unsigned char d[512];
/*    unsigned char *t; */

    nstr = r[0];
//    aprintf3( stdout, "%s nstr %d\n", __FUNCTION__, nstr); // debug

/* ATSC constrained to 1 string, 1 segment */
//    nstr = 1;
    for (i = 0; i < nstr; i++) {
	memcpy(lang, &r[1], 3);
	lang[3] = 0;

	r += 4;
	nseg = *r;
	r++;

/* ATSC constrained to 1 string, 1 segment */
//	nseg = 1;
	for (j = 0; j < nseg; j++) {
	    comp = *r++;
	    mode = *r++;
	    nchr = *r++;


/* if no characters in this segment, loop until out of segments */
	    if (nchr < 1 ) continue;

	    memset( s, 0, sizeof(s));
/* copy string but keep nul term */
	    for (k = 0; k < nchr; k++)
		if (k < (sizeof(s)-1))
			s[k] = r[k];

/* show non-compressed */
	    if ( (0 == comp) && (0 == mode) ) {
		    aprintf3( stdout,
		     "[%02X][%02X] L%02X C%02X M%02X [%s] [%-s] ",
			    nstr, nseg, nchr, comp, mode, lang, s);
	    }
/* huffman compressed, comp 1 or 2 only (should be 1, title comp only) */
	    if ( (comp > 0) &&
		 (comp < 3)
/* KPXB ignores the mode == 0xFF at least a year ago */
/*	      && (mode == 0xFF) */
		)
	    { 
		    huffman_decode( d, s, sizeof(d), nchr, comp);
		    aprintf3( stdout, "[%s] huff [%-s] ", lang, d);
	    }

/* show as unknown compress */
	    if (comp > 2)
		    aprintf3( stdout, "[%s] unk [L%02X C%02X M%02X]",
			    lang, nchr, comp, mode);

/* unused in ATSC; multi segment string would need this with nseg > 1 */
	    r+= nchr;    
	}
    }
    aprintf3( stdout, "\n");
}



/* A/65b Table 6.38 Multiple String Structure */
/* does do huffman decomp */
/* program index is computed by caller, stores result in pgm[] struct */
static
void
test_eit_mss ( unsigned char *r, unsigned int pgo, unsigned int etmid,
		unsigned int est, unsigned int els, unsigned char n )
{
    unsigned int nstr, nseg, comp, mode, nchr;
    unsigned int i,j,k;
    unsigned char lang[4];
    unsigned char s[PNZ*2];
    unsigned char d[PNZ];
    unsigned char *t;

    memset( s, 0, sizeof(s));
    nstr = r[0];
    for (i = 0; i < nstr; i++) {
	memcpy(lang, &r[1], 3);
	lang[3] = 0;
	r += 4;
	nseg = *r++;

	for (j = 0; j < nseg; j++) {
	    comp = *r++;
	    mode = *r++;
	    nchr = *r++;

/* don't show blank entry? */
	    if (nchr < 1) continue;

	    memset( s, 0, sizeof(s));
/* copy string but keep nul term */
	    for (k = 0; k < nchr; k++)
		if (k < (sizeof(s)-1))
			s[k] = r[k];

/* don't store last duplicate in pgm? will only be 1 string?
	    if (i == (nstr-1)) continue;
 
*/

/* only show if no compression */
	    aprintf3( stdout, "  EIT-%02X pg[%04X] ETMID %08X ",
			 n, pgo, etmid);
	    aprintf3( stdout, "[L%02X C%02X M%02X] ",
			 nchr, comp, mode);

	    t = s;

	    /* show non-compressed */
	    if ( (0 == comp) && (0 == mode) ) {
		aprintf3( stdout, "[%s]\n    [%-s] ", lang, s);
	    }

/* show huffman compressed */
	    if  (
/*		 (mode == 0xFF) &&     spec requires, KPXB ignores */
		 (comp > 0) &&
		 (comp < 3)
		)
	    { 
		t = d;
		huffman_decode( d, s, sizeof(d), nchr, comp);
		aprintf3( stdout, "[%s] Huffman\n   [%-s] ", lang, d);
	    }
	    /* show as unknown compress */
	    if (comp > 2) {
		    t = d;
		    snprintf( d, sizeof(d)-1, "[%s] [L%02X C%02X M%02X]",
			    lang, nchr, comp, mode);
	    }

	    pgm[ pgo ].st = est;
	    pgm[ pgo ].ls = els;
	    pgm[ pgo ].etmid = etmid;
	    memset( pgm[ pgo ].name, 0, PNZ);
	    memcpy( pgm[ pgo ].name, t, PNZ);

/* unused in ATSC; multi segment string would need this with nseg > 1 */
	    r+= nchr;    

	}
    }
    aprintf3( stdout, "\n");
}


/* r points to start of descriptors */
/* do until len exhausted */
static
void
test_eit_descriptors( unsigned char *p, unsigned int len )
{
	unsigned char *q, *r, *t;
	unsigned char n, l, i, j;
	unsigned int dlen;

	r = p;
	
	aprintf3( stdout, " EIT DLen %03X:\n\n", len);
	if (len == 0) aprintf3( stdout, "\n");
/*
    for (i = 0; i<len; i++) aprintf3(stdout, "%02X ", r[i]);
    if (len !=0 ) aprintf3( stdout, "\n\n");
*/
	if (len > 160) return; /*len = 160;  */
    
	if (len < 2) return;

	dlen = len;

	while ( dlen > 2 ) {

	    n = *r++;
	    l = *r++;
	    dlen -= 2;

	    switch( n ) {

	    case 5:
		t = "Registration";
		aprintf3( stdout, "     EIT Tag %02X DLen %02X %s: ", n, l, t);
		aprintf3( stdout, "Format ID [%c%c%c%c]\n",
			    r[0],r[1],r[2],r[3]);
		if (l != 4)
		    aprintf3( stdout, 
				"    add'l reg info exists but not parsed\n");
		aprintf3( stdout, "\n");
		break;

	    case 0x80:
		t = "Stuffing";
		aprintf3( stdout, "     EIT Tag %02X DLen %02X %s: ", n, l, t);
		for (i=0; i < l; i++) aprintf3( stdout, "%02X ", r[i] );
		aprintf3( stdout, "\n");
		break;

/* REMINDER: Don't get too excited about this descriptor. It's probably not
    going to have the information necessary to determine the actual stream
    info, such as how many channels will be in use at the time of this event.

    Very few stations send it in the EIT and the ones that do send it, set
    it to the exact same values as live VCT/PMT version of this descriptor.
    This means no future EPG 5.1 indicator for anyone until bcasters get it
    right. About all that can be done is show current cap 5.1 from ac3 parse.
*/
	    case 0x81:
		t = "A/52 Audio";
		{
/*		    unsigned char lc, lc2, mid, asvcf, tl, tc, tb; ?? */
		    unsigned char src, bsid, brc, sm, bsm, nc, fs, lc;
		    unsigned char *srt, *brt, *brt1, *smt, *nct, *lft, *bsmt;

		    src = 0x07 & (r[0]>>5);
		    srt = "Reserved";
		    if (0 == src) srt = "48 kHz";
		    if (1 == src) srt = "44.1 kHz";
		    if (2 == src) srt = "32 kHz";

/* must be the can't decide section nyuk */
		    if (4 == src) srt = "48 or 44.1 kHz";
		    if (5 == src) srt = "48 or 32 kHz";
		    if (6 == src) srt = "44.1 or 32 kHz";
		    if (7 == src) srt = "48, 44.1 or 32 kHz";

		    bsid= 0x1F & r[0];		/* bsid */
		    brc = 0x3F & (r[1]>>2);	/* bit rate code */
		    sm  = 0x03 & r[1];		/* dsurmod */
		    bsm = 0x07 & (r[2]>>5);	/* bsmod */
		    nc  = 0x0F & (r[2]>>4);	/* num chans */
		    fs  = 0x01 & r[2];		/* full service */
		    lc  = r[3];			/* langcode */

		    brt1 = "Exact";
		    if (0x20 == (0x20 & brc)) brt1 = "Maximum";
		    brc &= 0x1F; /* limit it now brt1 has been extracted */

		    brt = "Reserved";
		    if ( 0 == brc) brt = " 32 kbits/second";
		    if ( 1 == brc) brt = " 40 kbits/second";
		    if ( 2 == brc) brt = " 48 kbits/second";
		    if ( 3 == brc) brt = " 56 kbits/second";
		    if ( 4 == brc) brt = " 64 kbits/second";
		    if ( 5 == brc) brt = " 80 kbits/second";
		    if ( 6 == brc) brt = " 96 kbits/second";
		    if ( 7 == brc) brt = "112 kbits/second";
		    if ( 8 == brc) brt = "128 kbits/second";
		    if ( 9 == brc) brt = "160 kbits/second";
		    if (10 == brc) brt = "192 kbits/second";
		    if (11 == brc) brt = "224 kbits/second";
		    if (12 == brc) brt = "256 kbits/second";
		    if (13 == brc) brt = "320 kbits/second";
		    if (14 == brc) brt = "384 kbits/second";
		    if (15 == brc) brt = "448 kbits/second";
		    if (16 == brc) brt = "512 kbits/second";
		    if (17 == brc) brt = "576 kbits/second";
		    if (18 == brc) brt = "640 kbits/second"; /* hear that! */

		    smt = "Reserved";
		    if (0 == sm) smt = "Not Indicated";
		    if (1 == sm) smt = "NOT Dolby";
		    if (2 == sm) smt = "Dolby";

		    nct = "Reserved";
		    lft = "";
		    if ( 8 == (8 & nc)) lft = " + LFE";
		    if ( 0 == nc) nct = "1+1";
		    if ( 1 == nc) nct = "1/0";
		    if ( 2 == nc) nct = "2/0";
		    if ( 3 == nc) nct = "3/0";
		    if ( 4 == nc) nct = "2/1";
		    if ( 5 == nc) nct = "3/1";
		    if ( 6 == nc) nct = "2/2";
		    if ( 7 == nc) nct = "3/2";

/* LFE (Low Frequency Effects aka subwoofer) is +1 channel in the count. */
		    if ( 8 == nc) nct = "1";
		    if ( 9 == nc) nct = "<=2";
		    if (10 == nc) nct = "<=3";
		    if (11 == nc) nct = "<=4";
		    if (12 == nc) nct = "<=5";
		    if (13 == nc) nct = "<=6";

		    bsmt = "Reserved";
		    if ( 0 == bsm) bsmt = "Main Audio: Complete Main";
		    if ( 1 == bsm) bsmt = "Main Audio: Music and Effects";
		    if ( 2 == bsm) bsmt = "Associated: Visually Impaired";
		    if ( 3 == bsm) bsmt = "Associated: Hearing Impaired";
		    if ( 4 == bsm) bsmt = "Associated: Dialogue";
		    if ( 5 == bsm) bsmt = "Associated: Commentary";
		    if ( 6 == bsm) bsmt = "Associated: Emergency";
		    if ( 7 == bsm) bsmt = "Associated: Voice Over";

		    aprintf3( stdout,
		     "     EIT Tag %02X DLen %02X %s: Sample Rate %s, "
		    "bsid %02X langcod %02X\n",
			    n, l, t, srt, bsid, lc);
		    aprintf3( stdout,
		     "      %s Bit Rate %s (%02X) Surround %s (%02X)\n",
			    brt1, brt, brc, smt, sm);
		    aprintf3( stdout,
		    "      Service %s (%d), Channels %s%s (%d), Full Service %u\n",
			    bsmt, bsm, nct, lft, nc, fs);

		    aprintf3( stdout, "\n");
		}

		break;

	    case 0x86:
		t = "Caption Service";
		if ( 0xC0 == (0xC0 & r[0]) ) { /* reserved bits check */
		    unsigned char nos, cct, l21f, csn, er, war;
		    unsigned char lang[4];

		    q = r;
		    aprintf3(stdout,"     EIT Tag %02X DLen %02X %s: ",n, l, t);
		    nos = 0x1F & q[0]; /* number of services */
/* skip parsed */
		    q++;
		    aprintf3( stdout, "Number of Services %u\n", nos);
		    for (i = 0; i < nos; i++) {
			if ( 0x40 == (0x40 & q[3]) ) { /* reserved bit */
			    unsigned char *cctt;
			    cctt = "ATSC CC";
			    memcpy( lang, q, 3 );
			    lang[3] = 0; /* 3 char lang code per ISO 639.2/B */
			    q += 3; /* skip lang */
			    cct = 1 & (q[0]>>7); /* caption type */
/*			    aprintf3( stdout, "cct %d\n", cct); */
			    if (0 == cct) {
/* don't care about EIA/CEA-608-B. that's NTSC */
				cctt = "NTSC CC";
				if ( 0x3E == (0x3E & q[0]) ) { /* reserved */
				    l21f = 1 & q[0];
				    aprintf3( stdout,
				     "      [%s] Type %s Line21 %s Field ",
					lang, cctt,
					(l21f == 0) ? "Even" : " Odd" );
				}				
			    } else {
/* might be EIA-708-A ATVCC spec, not testing */
				csn = 0x3F & q[0]; /* caption svc number */
				aprintf3( stdout,
				 "      [%s] Type %s Service Number %02X ",
				    lang, cctt, csn);
			    }
			    if ( (0x3F == (0x3F & q[1])) && (0xFF == q[2]) ) {
				er = 1 & (q[1]>>7);
				war = 1 & (q[1]>>6);
				aprintf3( stdout,
				 "Easy Reader %u Wide Aspect Ratio %u",
					er, war); 
				q += 3;
			    }			    
//			    q += 3; /* was here but may be wrong */
			}
			aprintf3( stdout, "\n");
		    }
		    aprintf3( stdout, "\n");
		}
		break;

	    case 0x87:
		t = "Content Advisory";
		q = r;
		if ( 0xC0 == (0xC0 & q[0]) ) {
		    unsigned char rrc, rr, rd, rdj, rv, rdl;

		    aprintf3(stdout,"     EIT Tag %02X DLen %02X %s: ",n, l, t);
		    rrc = 0x3F & q[0];
		    q++; /* skip parsed */
		    aprintf3( stdout, " Rating Region Count %u\n", rrc);
		    for (i = 0; i < rrc; i++) {
			rr = q[0]; /* rating region */
			rd = q[1]; /* rated dimensions */
			q += 2;

/* one dimension will try to print on one line */
			aprintf3( stdout,
				"      Region %u Dimensions %u:%s",
				rr, rd, (rd > 1) ? "\n":"");
			    
			for (j = 0; j < rd; j++) {
			    if ( 0xF0 == (0xF0 & q[1]) ) {
				rdj = q[0]; /* rating dimension j */
				rv = 0xF & q[1]; /* rating value */
				q += 2;
				aprintf3( stdout, " Dimension j %u Value %u Abbrev [%s]\n",
				    rdj, rv, ratings[ (rdj * 10) + rv + 1] );
			    }
			}
			rdl = q[0];
			q++;
//			if (rdl != 0)
/* need at least 8 bytes of header for 1 byte string */
			if (rdl > 8)
			{
			    aprintf3( stdout, "       mss dlen %d: ",
					rdl );
			    test_eit_rr_mss( q );
			}
		    }
		    aprintf3( stdout, "\n");
		}
		break;

/* this one was struck down by the judge, but some stations still sending it */
	    case 0xAA:
		t = "Redistribution Control";
		aprintf3( stdout, "     EIT Tag %02X DLen %02X %s: ", n, l, t);
		for (i=0; i<l; i++)	aprintf3( stdout, "%02X ", r[ i ] );
		aprintf3( stdout, "\n");
		break;

/* needs parsing if shows up here */
	    default:
		aprintf3( stdout, "     EIT *Tag %02X len %02X ignored\n", n, l);
		aprintf3( stdout, "\n");
		break;
	    }

/* skip parsed */
	    r += l;
	    dlen -= l;
	}
}


/* add to eit payload. returns 0 if done, or 1 if not done */
static
unsigned int
build_eit_payload( unsigned char *p, unsigned int n )
{
    unsigned char *r;
    unsigned int psi;
    
    psi = 1 & (p[1]>>6);
    r = p + 5;

    /* handle payload start packet by copying start to EIT payload buffer */
    if (psi != 0) {
	if ( (0xCB == r[0]) 
	  && (0xF0 == (0xF0 & r[1]) ) /* syntax,private,reserved */
	  && (0x01 == (0x01 & r[5]) ) /* check cni only, reserved is broken? */
	   )
	{
	    /* top two are reserved see A/65b page 41 */
	    if (0xC0 != (0xC0 & r[5]) ) {
		aprintf6( stdout, " ERROR EIT-%02X *** ignoring reserved bits 0xC0 not set %02X\n", n, 0xC0 & r[5]);
	    }

	    eit[n].sl = ((0xF & r[1])<<8) | r[2];
	    eit_vn = 0x1F & (r[5])>>1;

	    eit[n].payoff = 183;
	    aprintf6( stdout,
		    " START EIT-%02X Version %02X SLen %03X Payoff %03X\n",
		    n, eit_vn, eit[n].sl, eit[n].payoff);

	    memset( eit[n].payload, 0, sizeof(eit[n].payload) ); /* reset */
	    memcpy( eit[n].payload, r, eit[n].payoff);
	    if ( eit[n].payoff > eit[n].sl+3 ) {
		aprintf6( stdout, "  DONE EIT-%02X\n",n);
		return 0;
	    }
	    return 1;
	}
    }

    r = p + 4;

    /* additional non start payloads append to eit payload buffer */
    if ( (eit[n].sl > 179) && (pkt.psi == 0) ) {
	memcpy( &eit[n].payload[eit[n].payoff], r, 184);

	eit[n].payoff += 184;

	aprintf6( stdout, "  COPY EIT-%02X Version %02X SLen %03X Payoff %03X\n",
		n, eit_vn, eit[n].sl, eit[n].payoff);
	if ( eit[n].payoff > eit[n].sl+3 ) {
	    aprintf6( stdout, "   DONE EIT-%02X\n", n);
	    return 0;
	}

	return 1;
    }
    aprintf6( stdout, "  *ignored*\n");
    return -1;
}

/* FOX fix: put the received EIT in chronological order.
   A65/b mentions it should be. Guess FOX didn't read that part.
    Called by test eit after reading the entries into pgm[]
*/
static
void
sort_eit_pgm( unsigned int pgo, unsigned int nev ) {
    unsigned int i, j, a, b;
    struct pgm_s c;

//    fprintf( stdout, "%s %u %u\n", __FUNCTION__, pgo & 0x1FF8, nev);

/* only sorting 8, at offset pgo & ~8 */
    a = pgo & 0x1FF8;
    b = a + nev;

/* FIXME: will move all the 0 start times to the top, want them at bottom */
    for (i = a; i < (b-1); i++) {
	if (0 == pgm[i].st) break; /* stop at first zero start time */
	for (j = i+1; j < b; j++) {
	    if (0 == pgm[j].st) break;
	    if ( pgm[j].st < pgm[i].st) {
//		fprintf( stdout, "swapped %d %d (%u %u)\n", i, j, pgm[i].st, pgm[j].st );

/* would be much faster to swap pointers */
		memcpy( &c, &pgm[i], sizeof(c));
		memcpy( &pgm[i], &pgm[j], sizeof(c));
		memcpy( &pgm[j], &c, sizeof(c));
	    }	    
	}
    }

/* erase stale entries at end, 8 is PEZ */
    for (i = b; i < (a+8); i++) memset( &pgm[i], 0, sizeof(c) );

/* debug sort output */
//    for (i = a; i < b; i++) fprintf( stdout, "pgo[%04X] st %u\n", i, pgm[i].st);
}

/* process eit[n] packet based on payload start indicator */
static
void
test_eit ( unsigned char *p, unsigned int n )
{
	unsigned char *r;
	unsigned char vn, sn, lsn, pv, numevs, cni;
	unsigned short sl, sr, pn;
	unsigned int eit_crc = 0;
	unsigned int pgo;	/* program guide offset */
	int i, vcn; /* , j; */

	n &= 0x7f;			/* limit EIT-n to 0-127 */
	r = p;
/*
    p.0 is 0x47 sync
    p.1 is 3 bits tei psi pri, 5 high bits pid
    p.2 is 8 bits low bits pid
    p.3 is tsc afc cc 
    p.4 is 0 if table and payload start indicator 1 else data
    p.5 is table id if payload start indicator 1 else data
*/
	/* returns 0 if done, else 1 */
	eit[n].payok = build_eit_payload( r, n );

/* if payok is nz, eit payload is not done yet */
	if (eit[n].payok != 0) return;

	r = eit[n].payload;

	if (0xCB != r[0]) {
	    aprintf3( stdout, "  EIT-%02X bad table type %02X\n", n, r[0]);
	    return;
	}

	sl = ((0xF & r[1])<<8) | r[2];	/* section length */

	eit_crc = calc_crc32( r, sl+3 );
	if (eit_crc != 0) {
	    pkt.crceit++;
	    aprintf3( stdout, 	"  EIT-%02X CRC ERROR "
				"CALC %08X RCVD %02X%02X%02X%02X\n",
			n, eit_crc, r[sl], r[sl+1],r[sl+2],r[sl+3]);
/*	    return; */
	}

	sr = r[3]<<8 | r[4];		/* source id */
	vn = (0x3E & r[5])>>1;
	eit[n].vn = vn;			/* not used */

	vcn = find_vc_src( sr );
	pn = 0;

/* holds down EIT display until VCT seen */
	if (vcn < 0) return;

	pn = vc[vcn].pn;

/* OUTPUT REDUX: */
	if (0 != arg_redux)
	    if (vn == eitvn[ (vcn << 7) + n ])
		return;				/* only if version changes */
	eitvn[ (vcn << 7) + n ] = vn;

	aprintf3( stdout, "EIT-%02X CRC OK\n", n);

	pkt.eit++;

	cni = 1 & r[5];			/* current/next indicator */
	sn = r[6];			/* section number */
	lsn = r[7];			/* last section number */
	pv = r[8];			/* eats cycles, more bits */
	if (pv != 0) {
	    aprintf3( stdout, "  EIT Protocol %02X not 0\n", pv);
	    return;			/* protocol version 0 only */
	}

	numevs = r[9];			/* number of events in section */
	r += 10;			/* skip parsed for loop */

	aprintf3( stdout,
	    "\n EIT-%02X VER %02X SLen %03X Src %d "
	    "CNI %d SN %d LSN %d NumEv %d Program %04X\n\n",
		n, vn, sl, sr, cni, sn, lsn, numevs, pn );

/* event 0 is link to previous event, always it seems. no ETT either */
/* only keep event zero if it is EIT-00 */
/* NOTE: FOX has nationwide broken EPG so event is might not be previous */

	pgo = ~0;

	for (i=0; i<(numevs); i++)
	{
	    if ( (0xC0 == (0xC0 & r[0])) && (0xC0 == (0xC0 & r[6])) )
	    {
		unsigned short eid;	/* event id */
		unsigned char etmloc;	/* etm location */
		unsigned int els;	/* length in seconds */
		unsigned char tlen;	/* title length	*/
		unsigned char d[32];	/* date string */
		unsigned int etmid;	/* extended text message id */
		struct tm dtm;		/* date in tm format */
		time_t est;		/* event start time */

    		eid = ((0x3F & r[0]) << 8) | r[1];

/* SEE A/65b Table 6.14 EIT ETM ID */
		etmid = (sr<<16) | (eid<<2) | 2;

		est = (r[2] << 24) | (r[3] << 16) | (r[4] << 8) | r[5];
		etmloc = (0x30 & r[6]) >> 4;
		els = ((0xF & r[6]) << 16) | (r[7] << 8) | r[8];
		tlen = r[9];
		r += 10;
		
/* recalibrate for unix time */
		est += utc_offset;

		n &= 0x7f;

/* compute program offset, should look like this:  */
/* F  E  D| C  B  A| 9  8  7  6  5  4  3| 2  1  0 */
/* 0  0  0|  SRC   |      EIT-N         |   EVN   */

/* sr 0 is NTSC. This code doesn't care about NTSC, so skip it. */
		if (0 == sr) continue;

/* WRONG: can ignore source id or program # only need vc offset, see below */
#if 0
/* make index match atscap epg [n] key numbers */
		so = sr - 1;
		pgo = (so<<10) | (n<<3) | (7 & i);
/* KPXB fix for their stupid idiotic source numbers above 8 */
		pgo %= PGZ;
#endif
		pgo = (vcn<<10) | (n<<3) | (7 &i);
		pgm[pgo].pn = pn;

/* don't worry about overwriting anything */
/* any new data should overwrite and be near where it belongs */
/* sort will clean up what is left if MGT changes during cap */

		/* make event start time tm structure from it */
		localtime_r( &est, &dtm );
		/* make string from tm structure */
		asctime_r( &dtm, d);
		d[19] = 0; /* strip year+nl from start time text */
		test_eit_mss( r, pgo, etmid, est, els, n );
		aprintf3( stdout, "    %s %02d:%02d EID %04X ",
				d, els/60, els%60, eid);
		r += tlen;

		if ( 0xF0 == (0xF0 & r[0]) )
		{
		    unsigned int dlen;
			    
		    dlen = ((0xF & r[0])<<8) | r[1];
		    r += 2;
		    test_eit_descriptors( r, dlen );		    
		    r += dlen;
		}
	    }
	}

/* pgm[] has been updated but the (up to 8) new entries need sorting */
/* they should all be based around (pgo & 0x3F8), sort by time */
	if (~0 != pgo) sort_eit_pgm( pgo, numevs );

/*	aprintf3( stdout, "\n"); */
}
/***************************************************************** ATSC EIT */


/***************************************************************** ATSC ETM */
/* A/65b Table 6.38 Multiple String Structure */
/* does not do huffman decomp, but should */
static
void
test_channel_ett_mss( unsigned char *r )
{
    unsigned int nstr, nseg, comp, mode, nchr;
    unsigned int i,j,k;
    unsigned char lang[4];
    unsigned char s[512];
	
    memset( s, 0, sizeof(s));
    nstr = r[0];

    nstr = 1; /* force to one */
    for (i = 0; i < nstr; i++) {
	memcpy(lang, &r[1], 3);
	lang[3] = 0;
	r += 4; // 3?
	nseg = *r++;

	nseg = 1; /* force to one */
	for (j = 0; j < nseg; j++) {
	    comp = *r++;
	    mode = *r++;
	    nchr = *r++;
	    for (k = 0; k < nchr; k++) s[k] = *r++;

	    /* only show if no compression */
	    if ( (0 == comp) && (0 == mode) ) {
		aprintf8( stdout, "[%s] [%-s]\n", lang, s);
	    } else {
		aprintf8( stdout, "[%s] [*COMPRESSED*]\n", lang);
	    }
	}
    }
}

static
void
test_channel_ett( unsigned char *p )
{
    unsigned int sl, idx, vn, etmid;
    unsigned char *r;

    r = p+5;

    if (0xF0 != (0xF0 & r[1])) return;
    if (0xC1 != (0xC1 & r[5])) return;
    
    sl = ((0xF & r[1]) << 8) | r[2];
    idx = (r[3] << 8) | r[4];
    vn = (0x3E & r[5]) >> 1;
    if (r[6] != 0) return; //sn
    if (r[7] != 0) return; //lsn
    if (r[8] != 0) return; //pv
    etmid = r[9]<<24 | r[10]<<16 | r[11]<<8 | r[12];
    r += 13;
    aprintf8( stdout, "Channel ETM SLen %03X ", sl);
    test_channel_ett_mss( r );
}
/////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////// VCT Payload
// add to vct payload. returns 0 if done, or 1 if not done
static
unsigned int
build_vct_payload( unsigned char *p )
{
    unsigned char *r;
    unsigned char psi, vn;
    
    r = p+5;

    psi = 1 &   ( p[1] >> 6);
    // handle payload start packet by copying start to VCT payload buffer
    if (psi != 0)
    {
	if ( (0xC8 == (0xFE & r[0] )) 		// C8 (TVCT) or C9 (CVCT)
//	  && (0xF0 == (0xF0 & r[1]) )		// syntax,private,reserved bits
	  && (0xC0 == (0xC0 & r[1]) )		// syntax,private
	  && (0xC1 == (0xC1 & r[5]) )		// reserved top 2 bits, cni = 1
	   )
	{
	    vn = 0x1F & ( r[5] >> 1);
	    vct.vn = vn;
	    vct.sl = ((0xF & r[1])<<8) | r[2];

	    vct.payoff = 183;

	    aprintf6( stdout,
		" START VCT Version %02X SLen %03X Payoff %03X\n",
		vct.vn, vct.sl, vct.payoff);
	    memset( vct.payload, 0, sizeof(vct.payload) ); // reset
	    memcpy( vct.payload, r, vct.payoff );

	    if ( vct.payoff > vct.sl+3 ) {
		aprintf6( stdout, "  DONE VCT\n");
		return 0;
	    }
	}
	return 1;
    }

    r = p+4;

    // additional non start payloads append to VCT payload buffer
    if ( (vct.sl > 179) && (psi == 0) ) {
	memcpy( &vct.payload[vct.payoff], r, 184);

	vct.payoff += 184;

	aprintf6( stdout, "  COPY VCT Version %02X SLen %03X Payoff %03X\n",
		    vct.vn, vct.sl, vct.payoff);

	if ( vct.payoff > vct.sl+3 ) {
	    aprintf6( stdout, "   DONE VCT\n");
	    return 0;
	}
	return 1;
    }
    aprintf6( stdout, " VCT not parsed\n");
    return -1;
}




/**************************************** Terrestrial Virtual Channel Table */
/* This gives information on which PIDs are in use for this Channel */
static
void
test_tvct ( unsigned char *p )
{
	unsigned char *r;
	unsigned short sl, tsi, adl;
	unsigned char vn, cni, sn, lsn, pv, ncs, i, j;
    
//	fprintf(stdout, "TVCT\n");

	vct.payok = build_vct_payload( p );
	if (0 != vct.payok) return; /* payload is not done yet */

//	fprintf(stdout, "TVCT OK\n");
	r = vct.payload; /* section pointed to by r now */

	/* all these values have to be true or nothing done */
	if  (  (0xC8 != r[0])		/* table id */
//	    || (0xF0 != (r[1] & 0xFC))	/* reserved,syntax,private all 1 */
	    || (0xC0 != (r[1] & 0xC0))	/* reserved,syntax,private all 1 */
	    || (0xC1 != (r[5] & 0xC1))	/* reserved top 2 bits, cni = 1 */
	    || (0x00 != r[8])		/* protocol version must be 0 */
	    )
	{
	    aprintf2( stdout, "TVCT bad TT/RB\n" );
	    return;
	}

/*	r = p + 5; / * still have to check sl */

	sl = ((0xF & r[1]) <<8) | r[2];	/* section length may not be > 1021 */

        if (sl > 1021) {
	    aprintf2( stdout, "TVCT too large %d\n", sl);
	    return;		/* if it is > 1021, ignore packet */
	}

	/* crc check to qualify before doing anything else */
	vct_crc = calc_crc32( r, sl+3 );
	if (vct_crc != 0) {
	    pkt.crctvct++;
	    aprintf2( stdout, "TVCT CRC ERROR\n");
	    return;	/* bad packet do not process further */
	}
	pkt.tvct++;			/* good packet gets counted */

	tsi = (r[3] <<8) | r[4];	/* transport stream id */

	vn = (0x3E & r[5]) >> 1;	/* version number */

/* OUTPUT REDUX: */
	if (0 != arg_redux) 
	    if (vn == vct_vn)
		return;			/* don't update until vn changes */
	vct_vn = vn;

	aprintf2( stdout, "TVCT CRC OK\n");
	
	cni = r[5] & 1;			/* current/next indicator */
	sn  = r[6];			/* section number */
	lsn = r[7];			/* last section number */

	pv  = r[8];			/* protocol version */
	if (pv != 0) {
	    aprintf2( stdout, "TVCT Protocol not 0\n");
	    return;		/* only protocol 0 */
	}
	ncs = r[9];		/* number of channels in section */
	vct_ncs = ncs;

	if (ncs > 7) {
	    aprintf2( stdout, "TVCT NCS limit exceeded, %d\n", ncs);
	     return;		/* sanity limit do not process further */
	}

	r += 10;

	aprintf2( stdout,
	    " TVCT #%d  SLen %03X TSID %04X Ver# %02X CNI %d Sec# %02X "
	    "LSec# %02X Chan/Sect %02X\n",
	    pkt.tvct, sl, tsi, vn, cni, sn, lsn, ncs);

	for (i=0; i < ncs; i++)
	{
	    unsigned char t[256];

	    /* all these reseved bits have to be set or nothing done */
	    if  ( (0xF0 == (0xF0 & r[14] ))	/* after short name */
	      && (0x0D == (0x0D & r[26] ))	/* +12 more */
	      && (0xC0 == (0xC0 & r[27] ))	/* +1 more */
		)
	    {
		memcpy( &vc[i].name, r, 14); /* 7 chars unicode */
		for (j=1;j<14;j++) {
		    if (0 == vc[i].name[j]) vc[i].name[j] = ' ';
		}
		r += 14;
		vc[i].major = ((0xF & r[0]) << 6) | ((0xFC & r[1]) >> 2);
		vc[i].minor = ((0x3 & r[1]) << 8) | r[2];
		vc[i].mode  = r[3];
		vc[i].freq  = (r[4]<<24) | (r[5]<<16) | (r[6]<<8) | r[7];
		vc[i].ctsid = (r[8]<<8) | r[9];
		vc[i].pn    = (r[10]<<8) | r[11];
		vc[i].etm   = (0xC0 & r[12]) >> 6;
		vc[i].actrl = (0x20 & r[12]) >> 5;
		vc[i].hide  = (0x10 & r[12]) >> 4;
		vc[i].hideg = (0x02 & r[12]) >> 1;
		vc[i].svct  = (0x3F & r[13]);
		vc[i].src   = (r[14]<<8) | r[15];
		vc[i].dlen  = ((0x3 & r[16])<<8) | r[17];
		r += 18; /* skip parsed */

/* show channel name */
		aprintf2( stdout, "\n  VC%02d [", i);
		for (j=0; j<14;) {
		    aprintf2( stdout, "%c", vc[i].name[j+1] );
		    j += 2;	/* the low byte is the one that's ASCII */
		}
		aprintf2( stdout, "] ");

/* default is ATSC type, change if necessary */
		snprintf( t, sizeof(t)-1, "ATSC DTV %d.%d", vc[i].major, vc[i].pn);
		if (vc[i].pn == 0xFFFF)
		    strncpy( t, "NTSC", sizeof(t)-1);
		if (vc[i].pn == 0)
		    strncpy( t, "DISABLED", sizeof(t)-1);
		aprintf2( stdout,
		 "%d.%d Modulation %d TSID %04X Program %s\n"
		 "   ETM Location %d Access Control %d Hidden %d Hide Guide %d\n"
		 "   Service Type %d Source %d Program %d DLen %02X\n",
			vc[i].major, vc[i].minor, vc[i].mode,
			vc[i].ctsid, t, vc[i].etm, vc[i].actrl,
			vc[i].hide, vc[i].hideg, vc[i].svct,
			vc[i].src, vc[i].pn, vc[i].dlen
			);

/* test the descriptor */
		if (vc[i].dlen != 0) {
		    aprintf2( stdout, "   TVCT DESCRIPTORS: DLen %02X\n", vc[i].dlen);
/* use test vct descriptor because of different output control */
		    test_vct_descriptor( r, vc[i].dlen );

		    r += vc[i].dlen;
		}
	    }
	}

/* look for additional descriptors, spec says may be 0
    spec said it may be a single descriptor?
    doesn't seem to fit in with the other descriptors if so
*/
	if (0xFC == (r[0] & 0xFC)) {
	    adl = ((0x3 & r[0]) << 8) | r[1];
	    r++;
	    r++;

	    if (adl != 0) {
		aprintf2( stdout, "    TVCT add'l desc: ");
		for (j=0; j < adl; j++) {
		    
		    aprintf2( stdout, "%02X", *r );
    		    r++; /* skip descriptor to get to crc */
		}
		aprintf2( stdout, "\n");
	    }
	}

	aprintf2( stdout, "\n");
}



/******************************************** Cable Virtual Channel Table */
/* This gives information on which PIDs are in use for this Channel. */
static
void
test_cvct( unsigned char *p )
{
	unsigned char *r;
	unsigned short sl, tsi, adl;
	unsigned char vn, cni, sn, lsn, pv, ncs, i, j;
/* service type */
	char *st[5] = {
	    "Rsvd", "NTSC TV", "ATSC DTV", "ATSC Audio", "ATSC Data"
	};
/* modulation modes */
	char *mt[6] = {
	    "Rsvd", "Analog", "QAM64", "QAM256", "VSB8", "VSB16"
	};
	int msr;

	fprintf(stdout, "CVCT\n");
	vct.payok = build_vct_payload( p );
	if (0 != vct.payok) return; /* payload is not done yet */

	fprintf(stdout, "CVCT OK\n");
	r = vct.payload; /* section pointed to by r now */

	/* all these must test true or nothing is done */
	if  (  (0xC9 != r[0])		/* table id */
//	    || (0xF0 != (r[1] & 0xFC))	/* reserved,syntax,private all 1 */
	    || (0xC0 != (r[1] & 0xC0))	/* reserved,syntax,private all 1 */
	    || (0xC1 != (r[5] & 0xC1))	/* reserved 1 */
	    || (0x00 != r[8])		/* protocol version must be 0 */
	    )
	{
	    aprintf2( stdout, "CVCT bad TT/RB\n" );
	    return;
	}

/*	r = p + 5; / * still need section length */

	sl = ((0xF & r[1]) <<8) | r[2];	/* section len may not be > 1021 */
	fprintf(stdout, "CVCT SL %03X\n", sl);

        if (sl > 1021) {
	    aprintf2( stdout, "CVCT too large %d\n", sl);
	    return;		/* if it is > 1021, ignore packet */
	}

	/* crc check to qualify before doing anything else */
	vct_crc = calc_crc32( r, sl+3 );
	if (vct_crc != 0) {
	    pkt.crccvct++;
	    aprintf2( stdout, "CVCT CRC ERROR\n");
	    return;	/* bad packet do not process further */
	}
	aprintf2( stdout, "CVCT CRC OK\n");
	pkt.cvct++;			/* good packet gets counted */

	tsi = (r[3] <<8) | r[4];	/* transport stream id */

	vn = (0x3E & r[5]) >> 1;	/* version number */

/* OUTPUT REDUX: */
	if (0 != arg_redux)
	    if (vn == vct_vn)
		return;	/* don't update until vn changes */
	vct_vn = vn;
	
	cni = r[5] & 1;			/* current/next indicator */
	sn  = r[6];			/* section number */
	lsn = r[7];			/* last section number */

	pv  = r[8];			/* protocol version */
	if (pv != 0) {
	    aprintf2( stdout, "CVCT Protocol not 0\n");
	    return;			/* only protocol 0 */
	}
	ncs = r[9];			/* number of channels in section */

	if (ncs > 7) {
	    aprintf2( stdout, "CVCT NCS limit exceeded, %d\n", ncs);
	     return;		/* sanity limit do not process further */
	}

	r += 10;

	aprintf2( stdout,
	    " CVCT #%d  SLen %03X TSID %04X Ver# %02X CNI %d Sec# %02X "
	    "LSec# %02X Chan/Sect %02X\n",
	    pkt.tvct, sl, tsi, vn, cni, sn, lsn, ncs);

	for (i=0; i < ncs; i++)
	{
	    unsigned char t[256];
	    unsigned short opn;

	    /* all these reseved bits have to be set or nothing done */
	    if  ( (0xF0 != (0xF0 & r[14] )) /* after short name 14 bytes */
//	      || (0x0D != (0x0D & r[26] )) /* +12 more bytes */
	      || (0xC0 != (0xC0 & r[27] )) /* +1 more byte */
		) {
		fprintf( stdout, "CVCT VC %d has bad RB %02X %02X %02X\n",
			i, r[14], r[26], r[27]);
		break;
	    }

	    {
		memcpy( &vc[i].name, r, 14); /* 7 chars unicode */
		for (j=1;j<14;j++) {
		    if (0 == vc[i].name[j]) vc[i].name[j] = ' ';
		}
		r += 14;
		vc[i].major = ((0xF & r[0]) << 6) | ((0xFC & r[1]) >> 2);
		vc[i].minor = ((0x3 & r[1]) << 8) | r[2];
/* one part number? */
		opn = 0;
		if (0x3F0 == (0x3F0 & vc[i].major)) {
		    opn = (0x0F & vc[i].major) << 10;
		    opn |= vc[i].minor;
		    fprintf(stdout, "OPN %d\n", vc[i].minor);
		}
		vc[i].mode  = r[3];
		vc[i].freq  = (r[4]<<24) | (r[5]<<16) | (r[6]<<8) | r[7];
		vc[i].ctsid = (r[8]<<8) | r[9];
		vc[i].pn    = (r[10]<<8) | r[11];
		vc[i].etm   = (0xC0 & r[12]) >> 6;
		vc[i].actrl = (0x20 & r[12]) >> 5;
		vc[i].hide  = (0x10 & r[12]) >> 4;
		vc[i].path  = (0x08 & r[12]) >> 3;
		vc[i].oob   = (0x04 & r[12]) >> 2; /* rsvd in TVCT */
		vc[i].hideg = (0x02 & r[12]) >> 1; /* rsvd in TVCT */
		vc[i].svct  = (0x3F & r[13]);
		vc[i].src   = (r[14]<<8) | r[15];
		vc[i].dlen  = ((0x3 & r[16])<<8) | r[17];
		r += 18; /* skip parsed */
		/* look for descriptors, spec says may be 0 */
/*
{ int k; for (k=0; k< vc[i].dlen; k++) aprintf2(stdout, "%02X ", r[k]);}
aprintf2( stdout, "\n"); fflush(stdout);
 */
		/* show channel name */
		aprintf2( stdout, "  VC%d [", i);
		for (j=0; j<14;) {
		    aprintf2( stdout, "%c", vc[i].name[j+1] );
		    j += 2;	/* the low byte is the one that's ASCII */
		}
		aprintf2( stdout, "] ");

		/* default is ATSC type, change if necessary */
//		snprintf( t, sizeof(t)-1, "ATSC DTV %d.%d", vc[i].major, vc[i].pn);
		snprintf( t, sizeof(t)-1, "ATSC .%d", vc[i].pn);
		if (vc[i].pn == 0xFFFF)
		    strncpy( t, "NTSC", sizeof(t)-1);
		if (vc[i].pn == 0)
		    strncpy( t, "DISABLED", sizeof(t)-1);
		if (vc[i].mode > 5) vc[i].mode = 0;
		if (vc[i].svct > 4) vc[i].svct = 0;

		msr = 0;
/* qam is around 5 megasymbols */
		if (2 == vc[i].mode) msr = 5056941;
		if (3 == vc[i].mode) msr = 5360637;

/* vsb is around 10 megasymbols */
		if (4 == vc[i].mode) msr = 10762237;
		if (5 == vc[i].mode) msr = 10762237;

		if (0x3F0 == (0x3F0 & vc[i].major)) {
		    aprintf2( stdout, "OPN %d\n", opn);
		} else {
		    aprintf2( stdout, "M.n %d.%d\n", vc[i].major, vc[i].minor);
		}

		aprintf2( stdout,
			"   Modulation %s (%d) Symbol Rate %d Frequency %d\n",
			    mt[vc[i].mode], vc[i].mode, msr, vc[i].freq);
		aprintf2( stdout,
			"   TSID %04X Program %s ETM Location %d Path %d OOB %d\n",
			    vc[i].ctsid, t, vc[i].etm, vc[i].path, vc[i].oob );
		aprintf2( stdout,
			"   Access Control %d Hidden %d Hide Guide %d\n",
			    vc[i].actrl, vc[i].hide, vc[i].hideg );
		aprintf2( stdout,
			"   Service Type %s Source %d DLen %02X\n",
			    st[vc[i].svct],  vc[i].src, vc[i].dlen );

		/* test the descriptor */
		if (vc[i].dlen != 0) {
		    aprintf2( stdout, "   VCT DESCRIPTORS: DLen %02X\n", vc[i].dlen);
		/* use test vct descriptor because different output control */
		    test_vct_descriptor( r, vc[i].dlen );

		    r += vc[i].dlen;
		}
	    }
	}

	/* look for additional descriptors, spec says may be 0 */
	/* spec said something about this, it may be a single descriptor? */
	/* doesn't seem to fit the other descriptors if so */
	if (0xFC == (r[0] & 0xFC)) {
	    adl = ((0x3 & r[0]) << 8) | r[1];
	    r++;
	    r++;

	    if (adl != 0) {
		aprintf2( stdout, "    TVCT add'l desc: ");
		for (j=0; j < adl; j++) {
		    
		    aprintf2( stdout, "%02X", *r );
    		    r++; /* skip descriptor to get to crc */
		}
		aprintf2( stdout, "\n");
	    }
	}

	aprintf2( stdout, "\n");
}



/************************************************************** MGT Payload */
/* add to mgt payload. returns 0 if done, or 1 if not done */
static
unsigned int
build_mgt_payload( unsigned char *p )
{
    unsigned char *r;
    unsigned char psi; /*, vn; */
    
    r = p+5;

    psi = 1 &   ( p[1] >> 6);

    /* handle payload start packet by copying start to MGT payload buffer */
    if (psi != 0)
    {
	if ( (0xC7 == r[0]) 
//	  && (0xF0 == (0xF0 & r[1]) )		// syntax,private,reserved bits
	  && (0xC0 == (0xC0 & r[1]) )		// syntax,private
	  && (0xC1 == (0xC1 & r[5]) )		// reserved bits top 2, cni=1
	   )
	{
	    mgt.sl = ((0xF & r[1])<<8) | r[2];
	    mgt.vn = 0x1F & ( r[5] >> 1);

	    mgt.payoff = 183;

	    aprintf6( stdout,
		" START MGT Version %02X SLen %03X Payoff %03X\n",
		mgt.vn, mgt.sl, mgt.payoff);
	    memset( mgt.payload, 0, sizeof(mgt.payload) ); // reset
	    memcpy( mgt.payload, r, mgt.payoff);

	    if ( mgt.payoff > mgt.sl+3 ) {
		aprintf6( stdout, "  DONE MGT\n");
		return 0;
	    }
	}
	return 1;
    }

    r = p+4;

    // additional non start packet payloads append to MGT payload buffer
    if (psi == 0) {
	memcpy( &mgt.payload[mgt.payoff], r, 184);
	mgt.payoff += 184;
	aprintf6( stdout, "  COPY MGT Version %02X SLen %03X Payoff %03X\n",
		    mgt.vn, mgt.sl, mgt.payoff);

	if ( mgt.payoff > mgt.sl+3 ) {
	    aprintf6( stdout, "   DONE MGT\n");
	    return 0;
	}
	return 1;
    }
    aprintf1( stdout, " MGT not parsed\n");
    return -1;
}


static
void
test_mgt ( unsigned char *p )
{
	unsigned short sl, tidx, td, dl, i; /* j; */
	unsigned char vn, cni, sn, lsn, pv;
	unsigned char *r;
    
//	fprintf(stdout, "MGT\n");

	mgt.payok = build_mgt_payload( p );
	if (0 != mgt.payok) return;

//	fprintf(stdout, "MGT OK\n");

	r = mgt.payload;
	if (0xC7 != r[0]) {
	    aprintf1( stdout, "table id not MGT 0xC7 but 0x%02X\n", r[0]);
	    return;
	}

#if 0
	if  ( (0xF0 != (0xF0 & r[1])) || (0xC1 != (0xC1 & r[5])) ) {
	    aprintf1(stdout, "MGT has bad TT/RB\n");
	    return; /* reserved must be set */
	}
#endif
	if  ( (0xC0 != (0xC0 & r[1])) || (0xC1 != (0xC1 & r[5])) ) {
	    aprintf1(stdout, "MGT has bad TT/RB\n");
	    return; /* reserved must be set */
	}
	
	sl = ((0xF & r[1]) <<8) | r[2];	/* 12 bit section length */
	if (sl > 4093)
	{
	    aprintf1(stdout, "MGT SLen > 4093\n");
	    return;		/* if sl > 4093, ignore packet */
	}
	mgt_crc = calc_crc32( r, sl+3);

	if (mgt_crc != 0) {
	    pkt.crcmgt++;
	    aprintf1( stdout, "MGT CRC32 ERROR\n");
	    return;
	}
	
	pkt.mgt++;

	tidx = (r[3]<<8) | r[4];

	vn  = (0x3E & r[5]) >> 1;	/* 5 bit version number */

/* OUTPUT REDUX: */
	if (0 != arg_redux)
	    if (vn == mgt_vn)
		return;			/* don't update until vn changes */
	mgt_vn = vn;


	aprintf1( stdout, "MGT CRC OK\n");

	cni = 1 & r[5];			/* current/next indicator */
	sn  = r[6];			/* 8 bit section number */
	lsn = r[7];			/* 8 bit last section number */

	pv = r[8];			/* 8 bit protocol version */
	if (pv != 0) {
	    aprintf1( stdout, "MGT Protocol Version not 0\n");
	    return;			/* should always be zero */
	}
	td = r[9]<<8 | r[10];		/* 16 bit tables defined */
	r += 11;			/* skip parsed */

	aprintf1(stdout, " MGT C7 VER %02X SLen %03X Tables %d CNI %d SN %d LSN %d\n",
		vn, sl, td, cni, sn, lsn );

/* version staying same should skip all the rest */
	memset( mg, 0, sizeof( mg ) );

/* new mgt should reset pg */
	memset( pg, 0, sizeof( pg ) );

	mgt_tt_pidx = 0;
	mgt_eit = 0;

	for (i=0; i<td; i++)
	{
	    if ( ( 0xE0 == (0xE0 & r[2]))
	      && ( 0xE0 == (0xE0 & r[4]))
	      && ( 0xF0 == (0xF0 & r[9]))
	      )
	    {
		unsigned short tt;
		unsigned short ttpid;
		unsigned char ttvn;
		unsigned int nb;
		unsigned short ttdl;
		unsigned char tx[8];
		
		tt = (r[0]<<8) | r[1];
		ttpid = ((0x1F & r[2])<<8) | r[3];
		ttvn = 0x1F & r[4];
		nb  = (r[5]<<24) | (r[6]<<16) | (r[7]<<8) | r[8];
		ttdl = ((0xF & r[9])<<8) | r[10];
		r += 11;

		/*  0000-1FFF table types EIT 100-17F ETT 200-27F,
		    1500-1FFF reserved
		 */
		mg[ mgt_tt_pidx ].tt = tt;	/* table type */

		/* 0-1FFF PID range. coincidentally near same size as tt */
		mg[ mgt_tt_pidx ].ttpid = ttpid;  /* table type PID */

		mg[ mgt_tt_pidx ].ttvn = ttvn;	/* version */
		mg[ mgt_tt_pidx ].nb = nb;	/* bytes in table */
		mg[ mgt_tt_pidx ].ttdl = ttdl;	/* bytes in descriptor */
		mgt_tt_pidx++;

		strncpy( tx, "RESERV",  sizeof(tx));
		/* see A/65b Table 6.3 page 26 */
		if (tt == 0) strncpy( tx, "TVCT-C1", sizeof(tx)-1);
		if (tt == 1) strncpy( tx, "TVCT-C0", sizeof(tx)-1);
		if (tt == 2) strncpy( tx, "CVCT-C1", sizeof(tx)-1);
		if (tt == 3) strncpy( tx, "CVCT-C0", sizeof(tx)-1);
		if (tt == 4) strncpy( tx, "PTC ETT", sizeof(tx)-1);
		if (tt == 5) strncpy( tx, "DCCSCT ", sizeof(tx)-1);
		if ( (tt >= 0x100) && (tt <= 0x17f) ) {
		    snprintf( tx, sizeof(tx)-1, "EIT-%02X ", tt & 255);
		    mgt_eit = 1;
		}
		if ( (tt >= 0x200) && (tt <= 0x27f) )
		    snprintf( tx, sizeof(tx)-1, "ETT-%02X ", tt & 255);
		if ( (tt >= 0x301) && (tt <= 0x3ff) )
		    snprintf( tx, sizeof(tx)-1, "RRT-%02X ", tt & 255);
		if ( (tt >= 0x1400) && (tt <= 0x14ff) )
		    snprintf( tx, sizeof(tx)-1, "DCC-%02X ", tt & 255);

		aprintf1( stdout,
			"  Table #%3d Type %-7s %04X PID %04X Ver %02X NumB %06X TTDLen %d\n",
			i+1, tx, tt, ttpid, ttvn, nb, ttdl);
		    
		/* -k mgt specified, so keep all PIDs listed in MGT */
		if (arg_mgtpids != 0) keep_pids[ ttpid ] = ~0;
	    }
	}

	/* more reserved bit validate */
	if (0xF0 != (0xF0 & r[0])) return;
	
	dl = ((0xF & r[0])<<8) | r[1];
	r += 2;
	aprintf1( stdout, "  Additional Descriptor Len %03X\n", dl);

/* spec indicates it should be one descriptor only, no additional */
#if 0
	for (j=0; j<dl; j++) {
	    test_mgt_descriptor( r, dl );
	}
#endif
	r += dl;
	aprintf1( stdout, "\n");
}

/* return -1 if pid is not in mgt_tt_pid table, else return index */
static
int
test_mgt_tt_pid( unsigned int pid )
{
	int i;
	for (i=0; i<mgt_tt_pidx; i++) {
//	    aprintf1( stdout, "test mgt pid %04X ttpid %04X\n", pid, mg[i].ttpid );
	    if ( pid == (0x1FFF & mg[i].ttpid ) ) return i;
	}
	return -1;
}


/* packets that match MGT table */
static
void
test_mgt_tt_packet( unsigned char *p, unsigned int pid )
{
    int ttidx, ttpid, tt;

    ttidx = test_mgt_tt_pid( pid );
    if (ttidx == -1) {
	aprintf1( stdout, "PID %04X table not found in MGT\n", pid);
	return; // no match found
    }
    pkt.atsc++;
    ttpid = 0x1FFF & mg[ ttidx ].ttpid; /* pid ranges len coincidental to */
    tt    = 0x1FFF & mg[ ttidx ].tt;	/* table ranges. dont get confused */

    if (tt == 0) {
	dprintf6( stdout, " PID %04X Table Type %04X Terrestrial VCT CNI1\n", ttpid, tt);
	return;
    }
    if (tt == 1) {
	dprintf6( stdout, " PID %04X Table Type %04X Terrestrial VCT CNI0\n", ttpid, tt);
	return;
    }
    if (tt == 2) {
	dprintf6( stdout, " PID %04X Table Type %04X Cable VCT CNI1\n", ttpid, tt);
	return;
    }
    if (tt == 3) {
	dprintf6( stdout, " PID %04X Table Type %04X Cable VCT CNI0\n", ttpid, tt);
	return;
    }
    if (tt == 4) {
	dprintf6( stdout, " PID %04X Table Type %04X Channel ETT ", ttpid, tt);
	test_channel_ett( p );
	return;
    }
    if (tt == 5) {
	dprintf6( stdout, " PID %04X Table Type %04X DCCSCT\n", ttpid, tt);
	return;
    }

/* Event Information Table */
    if ( (tt > 0xFF) && (tt < 0x180) ) {
	dprintf1( stdout, " PID %04X Table Type %04X EIT-%02X\n", ttpid, tt, tt-0x100);
	test_eit( p, tt & 0x7F );
	return;
    }

/* Extended Text Table */
    if ( (tt > 0x1FF) && (tt < 0x280) ) {
	dprintf1( stdout, " PID %04X Table Type %04X ETT-%02X\n", ttpid, tt, tt-0x200);
	test_ett( p, 0x7F & tt );
	return;
    }

/* Rating Region Table */
    if ( (tt > 0x2FF) && (tt < 0x400) ) {
	dprintf1( stdout, " PID %04X Table Type %04X RRT-%02X\n", ttpid, tt, tt-0x300);
	test_rrt( p );
	return;
    }

/* Directed Channel Change Table */
    if ( (tt > 0x13FF) && (tt < 0x1500) ) {
	dprintf1( stdout, " PID %04X Table Type %04X DCCT-%02X\n", ttpid, tt, tt-0x1400);
	test_dcct( p );
	return;
    }

/* see A/65b Table 6.3 Table Types */
    dprintf1( stdout, " PID %04X RESERVED TT %04X\n", ttpid, tt);
}
/****************************************************************************/

/******************************************************** System Time Table */
static
void
test_stt ( unsigned char *p )
{
    static unsigned int stt_prev;
    unsigned char *r;
    char e = ' '; /* error flag */
        
    r = p+5;
    
    /* all these have to be true or nothing happens */
    if (   (0xCD == r[0])		/* table_id is STT table     = 0xCD */
	&& (0xF0 == (r[1] & 0xF0))	/* syntax,private,reserved      = 1 */
	&& (0x00 == r[3])		/* table_id_extension msB	= 0 */
	&& (0x00 == r[4])		/* table_id_extension lsB	= 0 */
	&& (0xC1 == r[5])		/* rsvd, current_next = 1,  ver = 0 */
	&& (0x00 == r[6])		/* section number		= 0 */
	&& (0x00 == r[7])		/* last section number		= 0 */
	)
    {	
	stt.sl = ((0xF & r[1]) <<8) | r[2];

	/* don't process any further if the CRC is bad */
	stt.crc = calc_crc32( r, stt.sl+3 );
	if (stt.crc != 0) {
	    pkt.crcstt++;
	    aprintf0( stdout, "STT CRC ERROR\n");
	    return;
	}

	stt.st = (p[14]<<24) | (p[15]<<16) | (p[16]<<8) | p[17];
	stt.guo = p[18];		/* gps utc offset */

	stt.ds = (p[19]<<8) | p[20]; /* unused for now? always 06D0 */

/* daylight savings status A/65b Annex A pp 84-85 */
	if (0x60 == (0x60 & p[19])) {
	    stt.dss = 1 & (stt.ds >> 15);
	    stt.dsd = 0x1F & (stt.ds >> 8 );
	    stt.dsh = 0x1F & stt.ds;
	} else {
/* DST ctl reserved bits not set */
	    stt.dss = 0;
	    stt.dsd = 0;
	    stt.dsh = 0;
	}
	/* GPS UTC offset correction, Bureau of Standards sets + / - */
	stt.st -= stt.guo;

/* add 10 years + 5 days for ATSC epoch starting at Jan 6 1980 */
/* is really 10 years + 7 days counting 1972 & 1976 leap years */
	stt.st += utc_offset;

	if (last_stt > stt.st) e = '*';

	if (0 == first_stt) first_stt = stt.st;
	last_stt = stt.st;
	
/* fill stt_tm structure with corrected time */
/* FIXME: DST not handled yet? */
	localtime_r( &stt.st, &stt_tm );

	pkt.stt++;

/* ouput redux, but actually want this one as speedometer */
/*	if (0 == (stt.st % 60)) */
/*	    if (stt_prev != stt.st) */
	    {
		time_t tt;
		struct tm lt;	/* local time */
		char ds1[32];	/* date string */
		char ds2[32];

		tt = time( NULL );
		localtime_r( &tt, &lt);
		asctime_r( &stt_tm, stt_text );
		asctime_r( &stt_tm, ds1 );
		asctime_r( &lt, ds2);
		ds1[24] = 0;
		ds2[24] = 0;
		aprintf7( stdout,
		    "STT %04d%02d%02d %02d:%02d:%02d %c o%d ds%04X s%d d%d h%d\n",
			stt_tm.tm_year + 1900,
			stt_tm.tm_mon + 1,
			stt_tm.tm_mday,
			stt_tm.tm_hour,
			stt_tm.tm_min,
			stt_tm.tm_sec,
			e, stt.guo, stt.ds, stt.dss, stt.dsd, stt.dsh );
	    }

	stt_prev = stt.st;
    }
/*    aprintf0( stdout, "\n"); */
}

/* handle PID 1FFB, single or multi-packet payload */
static
void
test_atsc_packet( unsigned char *p )
{
    unsigned int psi, pid;
    
    psi = 1 & (p[1]>>6);
    pid = 0x1FFF & ((p[1]<<8) | p[2]);
    pkt.atsc++;

    // payload start indicator?
    if (psi != 0)
    {
//	pkt.atsctid = p[5]; // save last tid that got payload unit start bit
//	switch( pkt.atsctid ) {
	psit[ pid ] = p[5];
	switch( psit[ pid ] ) {
	    case 0xCD:
		test_stt( p );
		break;

	    case 0xC7:
		test_mgt( p );
		break;

	    case 0xC8:
		test_tvct( p );
		break;

	    case 0xC9:
		test_cvct( p );
		break;		

	    case 0xCA:
		test_rrt( p );
		break;

	    case 0xD3:
		test_dcct( p );
		break;

	    case 0xD4:
		test_dccsct( p );
		break;

	    default:
		aprintf0( stdout, "  unknown ATSC table id %02X*\n", p[5]);
		break;
	}
	return;
    }

    // if psi indicates rest of payload, see what last pkt.tid needs payload
    // this pretty much requires the ATSC packets be sequential, so that
    // no other ATSC PID comes down before last PID in current table done.
    // not sure if spec spells it out. maybe left to the implementation.
    if (psi == 0)
    {
//	switch( pkt.atsctid ) {
	switch( psit[ pid ] ) {
	    case 0xCD:
		test_stt( p );
		break;

	    case 0xC7:
		test_mgt( p );
		break;

	    case 0xC8:
		test_tvct( p );
		break;

	    case 0xC9:
		test_cvct( p );
		break;

	    case 0xCA:
		test_rrt( p );
		break;

	    default:
		break;
	}
    }
    return;
}


static
void
test_packet( unsigned char *p )
{
    unsigned int tei, psi, pid, pri, cc, tsc, afc, tid, i;
    unsigned char c;
    unsigned char cct;

    cct = ' ';

/* -d option duty cycle control, limits bandwidth used by program
    this tries to force the program data transfer rate down
    to prevent problems with any in-progress captures
    -d50 looks to write near same speed as broadcast rate, on my system.
*/

    if (arg_duty != 100) 
    {
	i = pkt.count % duty_cycle_packet;

	// sleeps at begin
	//if (i == 0) usleep( duty_cycle_sleep );

	// sleeps at end until ideal packet rate expired
	if ( i == (duty_cycle_packet-1) ) usleep( duty_cycle_sleep );
    }

    pkt.count++;
    tei = 1 & (p[1] >> 7);			// transport error indicate

    psi = 1 & (p[1] >> 6);			// payload start indicator
    pkt.psi = psi;				// use for multi-packet table

    pri = 1 & (p[1] >> 5);			// transport priority
    pid = 0x1FFF & ( (p[1] << 8) | p[2] );	// program id
    tsc = 3 & (p[3]>>6);			// transport scramble control
    afc = 3 & (p[3]>>4);			// adaptation field control
    cc = 15 & p[3];				// continuity counter
    tid = p[5];


    if (0 != *arg_newpat) {
	if (0 == pid)
	    memcpy( p, newpat, 188 );
	    p[3] &= 0xF0;
	    p[3] |= cc; /* keep the continuity counter */
    }


    
    // ignore reserved AFC packets
    if (afc == 0) return;

    // ignore transport error indicator set packets
    if (tei != 0) {
	pkt.errtei++;
//	keep_pkt = 0;
	dprintf4(stdout, "PKT # %8u TEI\n", pkt.count);
	return;
    }

    // ignore scramble control non zero packets
    if (tsc != 0) {
	pkt.errscr++;
//	keep_pkt = 0;
	dprintf4(stdout, "PKT # %8u scramble bits = %u\n", pkt.count, tsc);
	return;
    }

    // continuity counter finally works. looks simple but wasn't obvious
    pid_cc[ pid ] = 0; // payload process will use to see if drop time
    if (pid != 0x1FFF) {
	if (0xFF != last_cc[ pid ]) {
    	    if (afc == 2) {
		// cc doesnt change for afc 2
		if (cc != last_cc[ pid ]) {
		    pkt.errcce++;
		    cct = '!';
		    pid_cc[ pid ] = ~0;
		}
	    } else {
		// cc should change for everything else except NULLs
		//   no duplicates seen in local ATSC broadcasts
		if (cc != (0xF & (last_cc[ pid ]+1))) {
		    pkt.errcce++;
		    cct = '!';
		    pid_cc[ pid ] = ~0;
		    cc_err[ pid ]++;
		    if (pid < 0x1000) {
			pkt.mpegce++;
		    } else {
			pkt.atscce++;
		    }
		}
	    }
	}
    }

    last_cc[ pid ] = cc; // for next time thru

    pids[pid]++; // count pid's of good non null packets

    c = '#';
    if (arg_pids != 0) {
	c = '-';
	if (0 != keep_pids[pid]) {
	    c = '+';
	}
    }

    // full stream cut to full stream should keep NULLs
    // but most times doing full stream cut to 1 video and 1 audio
    if (pid == 0x1FFF) {
	pkt.null++;
	if (0 != arg_nulls) keep_pids[pid] = ~0;
	return;
    }

    /* + means on PID list, - is not on list, # is not checking */
    /* also masking START NO unless -v bit 4 (16 val) set */
    if ( 0 == psi ) {
	/* bit 4 of -v option makes START NO appear */
	if (0 != (16 & arg_verbose)) {
	    dprintf1(stdout, "PKT %c %8d PID %04X SC %d AF %d CC %01X%c START NO  ",
			c, pkt.count-1, pid, tsc, afc, cc, cct );
	    for (i = 4; i < 12; i++)  dprintf1(stdout, "%02X ", p[i]);
	    dprintf1( stdout, "\n");
	}
    } else {
	    dprintf1(stdout, "PKT %c %8d PID %04X SC %d AF %d CC %01X%c START YES ",
			c, pkt.count-1, pid, tsc, afc, cc, cct );
	    for (i = 4; i < 12; i++)  dprintf1(stdout, "%02X ", p[i]);
	    dprintf1( stdout, "\n");
    }

    /* count pes packet type but don't do anything with it yet */
    if ( pid < 0x1000 ) {
	test_mpeg2_packet( p );
    }

    // ATSC PSIP always has AFC=1
    if (afc == 1) {
	if (pid == 0x1FFB) {		// handle 1FFB PID PSIP types
	    test_atsc_packet( p );	// handles MGT TVCT STT, not RRT CVCT
	} else {
	    // >= 0x1000 is possible ATSC PSIP packet
	    if ( (pid > 0xFFF) && (pid < 0x1FFF)) // handle non 1FFB/1FFF PID
		test_mgt_tt_packet ( p, pid ); // from MGT PSIP lut
	}
    }

/* write buffer used for:
	-k PID extract
	-e program extract
	-r pat replacement
	-t pmt replacement
*/
    if (0 != arg_pids)
	if (0 != keep_pids[pid])		/* in the lookup table? */
#ifdef USE_BUFFER
		write_buffer( out_buf, p, TSPZ);
#else
		write( out_file, p, TSPZ);
#endif
    return;
}


static
void
read_loop( void )
{
    int len, ok, i;

    len = TSBUFZ;
    ok = 0;

    fprintf( stdout, "Testing packets in %s\n", in_name);
    if (0 != *arg_newpat)
	fprintf( stdout, "Replacing PAT packets with %s\n", arg_newpat);
    if (0 != arg_mgtpids)
	fprintf( stdout, "Extracting PIDs for ATSC and MPEG tables\n");
    if (0 != arg_seqwr)
	fprintf( stdout, "Extracting Frames to %sx\n",
		    in_name);
    if (0 != arg_epn)
	fprintf( stdout, "Extracting PIDS for MPEG Program number %d\n",
		 arg_epn);
	
    if ( (0 == arg_epn)
      && (0 == arg_epn)
      && (0 != arg_pids)
      && (0 == arg_mgtpids) ) {
	fprintf( stdout, "Extracting by this PID list: ");
	for (i=0; i<0x2000; i++)
	    if (0 != keep_pids[i])
		fprintf( stdout, "%04X ", i); 
	fprintf( stdout, "\n");
    }

    fprintf( stdout, "\n");

    bytes_out = 0;

    while ( 0 != len )
    {
	// read a packet
#ifdef USE_BUFFER
	len = read_buffer( in_buf, ts_buf, TSPZ);
#else
	len = read( in_file, ts_buf, TSPZ);
#endif
	if (len < 0) {
		fprintf(stdout, "Could not read %s\n", in_name);
		perror("");
		exit(1);
	}
	if (0 == len) {
	    fprintf( stdout, "EOF\n");
	    break;
	}

	bytes_in += len;

/* packet traffic director */
	test_packet( ts_buf );
#if 0
	if (0 != arg_crc) {
/* Could use as a packet hash for unique ID but has multiple solutions. */
/* Perhaps could be useful for future stream-restoration project. */
	    unsigned int crc;
	    crc = calc_crc32( ts_buf, TSPZ );
	    fprintf( stdout, "%08X\n", crc );
	}
#endif

    }

    return;    
}

/* ATSC uses 32 bit unsigned from 00:00:00 UTC, Jan 6, 1980
   unix uses 32 bit unsigned from 00:00:00 UTC, Jan 1, 1970
   difference is saved in global utc_offset
*/
static
void
calc_epoch( void )
{
    struct tm as = {0,0,0,6,0,80,0,0,0}; // Jan 6, 1980 00:00:00 UTC
    unsigned long utd;
    unsigned char d[32];

    utd = (unsigned int)mktime( &as );
    snprintf( d, sizeof(d)-1, "%s", asctime(localtime( &utd )) );
    
    d[24] = 0;
    if (utd != -1)
	fprintf( stdout, "ATSC timebase %s: unix diff %u TZ diff %ld\n",
		d, (unsigned int)utd, as.tm_gmtoff );

    utc_offset = utd + as.tm_gmtoff;
}


static
void
parse_args( int argc, char **argv)
{
    struct stat fs;
    char bn[256];
    int arg_idx;	// copy of argc, decremented
    int c;
    int sr, ok;		// stat returns
    int adump, mdump, vdump;
    char inz[32], inp[32];

    in_size = 0;
    arg_idx = 0;

    arg_adump = arg_mdump = arg_verbose = 0;

    if (argc == 1) {
	fprintf(stdout, usage, argv[0]);
	exit(0);
    }

    while ((c = getopt( argc, argv, "cfghnqs" "a:d:e:k:m:o:p:r:v:y:")) != EOF)
    {
	switch(c)
	{
	    case 'c':
		arg_crc = ~arg_crc;
		break;

    	    case 'f':
		arg_forge = ~arg_forge;
		break;

	    case 'q':
		arg_redux = ~arg_redux;
		break;

	    case 'r':
		{
		    int npf;
		    sscanf( optarg, "%s", arg_newpat );
		    npf = open( arg_newpat, RD_MODE );
		    if (npf > 2) {
			ok = read( npf, newpat, 188);
			if (188 != ok) {
			    fprintf(stderr, "%s < 188 bytes\n", optarg);
			    close(npf);
			    exit(0);
			}
			close( npf );

		    } else {
			 *arg_newpat = 0;
		    }
		}
			    
		break;

	    case 'h':
		fprintf(stdout, usage, argv[0]);
		exit(0);
		break;

	    case 'g':
		arg_guide = ~arg_guide;
		break;

	    case 's':
		arg_seqwr = ~0;
		break;
				
	    case 'n':
		arg_nulls = ~0;
		break;

	    case 'e':
		arg_pids = ~0;
		sscanf( optarg, "%d,%d", &arg_epn, &arg_ean);
		if (arg_epn < 1) arg_epn = 0;		/* ignore bogus */
		if (arg_epn > 65534) arg_epn = 0;	/* program number */
		arg_ean &= 7;

		break;

	    case 'y':
		sscanf( optarg, "%x", &arg_espid);
		arg_espid &= 0x1FFF;
		fprintf( stdout, "Extracting ES PID %04X\n", arg_espid);
		break;

	    // duty cycle from 1 to 100
	    case 'd':
		sscanf( optarg, "%d", &arg_duty);
		if (arg_duty < 1) arg_duty = 1;
		if (arg_duty > 100) arg_duty = 100;
		break;

	    // ATSC detail level
	    case 'a':
		adump = 0;
		if (NULL != optarg) adump = atoi( optarg);
		arg_adump |= adump;
		break;

	    // MPEG detail level
	    case 'm':
		mdump = 0;
		if (NULL != optarg) mdump = atoi( optarg );
		arg_mdump |= mdump;
		break;

	    // verbose and packet assembly detail level
	    case 'v':
		vdump = 0;
		if (optarg != NULL) vdump = atoi( optarg );
		if (vdump & 2) arg_adump = 0xFF;
		if (vdump & 4) arg_mdump = 0xFF;
		arg_verbose |= vdump;
		break;
			    
	    // add a PID to the list to test for keeping
	    // -k mgt will keep all PIDs from Master Guide Table
	    //  without the audio and video, for testing ATSC PSIP only
	    case 'k':
		if (optarg != NULL) {
		    int pid;
		    arg_pids = ~0;

		    // -k mgt option to keep all ATSC Master Guide
		    if ( 0 == strncasecmp(optarg, "mgt", strlen(optarg)) ) {
			arg_mgtpids = ~0;
		    } else {
			sscanf( optarg, "%x", &pid);
//			fprintf( stdout, "-k %d option\n", pid);
			keep_pids[ 0x1FFF & pid ] = ~0;
		    }
		}
		break;

	    case 'o':
		if (optarg != NULL) {
		    snprintf( arg_name, sizeof(arg_name)-1, "%s", optarg);

/* null extraction needs to trigger file write somehow */

		}
		break;

/* output path, tests for IFDIR */
	    case 'p':
		if (optarg != NULL) {
		    struct stat s;
		    snprintf( arg_path, sizeof(arg_path)-1, "%s", optarg);

		    // remove trailing slash if any
		    if (arg_path[strlen(arg_path)-1] == '/')
			arg_path[strlen(arg_path)-1] = 0;
			
		    stat( arg_path, &s );
		    if ( (s.st_mode & S_IFDIR) == 0 ) {
			fprintf(stderr, "\n%s is not a directory\n", arg_path);
			exit(1);
		    }
		    snprintf( out_path, sizeof(out_path)-1, "%s", arg_path);
		}
		break;
	    

	    default:
		break;
	}	    

    }

// duty cycle packet is % of packets per ideal second
    duty_cycle_packet = (12898 * arg_duty) / 100;

// FIXME: don't use constant pktrate
// duty cycle sleep is how long until end of ideal second
// see notes in test packet for usage

    duty_cycle_sleep = 12898 - duty_cycle_packet;
    duty_cycle_sleep *= 77; // 77 microseconds/packet


//    fprintf( stderr, "optind %d argc %d\n", optind, argc);

    arg_idx = argc - optind;
    arg_idx = (arg_idx < 0) ? 0 : arg_idx;

//    fprintf( stderr, "arg_idx %d argv[optind] %s\n",arg_idx, argv[optind]);
    
    if (arg_idx < 1) {
	fprintf( stderr, "\nMissing a parameter? See %s -h\n\n", argv[0]);
	exit(1);
    }

// filename is first on list after options, optind stays pointing to it
    snprintf( in_name, sizeof(in_name)-1, "%s", argv[optind]);

    memset( &fs, 0, sizeof( fs ) ); // clear stat struct

// get file size of input file
    sr = stat( in_name, &fs);
    if (sr != 0) {
	fprintf( stderr, "%s ", in_name);
	perror("stat");
	exit(1);
    }

    in_size = fs.st_size; // file size, used for packet rate compute
    
    lltoasc( inz, in_size );
    lltoasc( inp, in_size / 188 );

    fprintf( stdout, "Input file name %s\nInput size %s bytes, %s packets\n",
		in_name, inz, inp );

    in_file = open( in_name, FILE_RMODE );
    if (in_file < 3) {
	fprintf( stderr, "\nerror opening file %s: ", in_name );
	perror("");
	exit(1);
    }

#ifdef USE_BUFFER
    in_buf = init_buffer( in_file, 4096, in_name );
    if (NULL == in_buf) {
	fprintf(stderr, "\nCan't allocate buffer for %s\n\n", in_name);
	exit(1);
    }
#endif

    file_size = fs.st_size;
//    fprintf( stdout, "%s has %lld bytes\n", in_name, file_size);

    // create basename for the cut(s)
    strncpy( bn, argv[optind], sizeof(bn) );

/* -o output name override, but only overrides base name */
    if (0 != *arg_name)
	strncpy( bn, arg_name, sizeof(arg_name) );
    filebase( base_name, bn, F_TFILE );

    arg_idx--; // filename done

/* -k numeric extract is -x.ts */
    if (0 != arg_pids) {
	snprintf( out_name, sizeof(out_name)-1, "%s/%s-x.ts",
			  out_path, base_name);
	/* -k mgt subset extract is -mgt.ts */
	if (0 != arg_mgtpids) {
	    snprintf( out_name, sizeof(out_name)-1, "%s/%s-mgt.ts",
		    out_path, base_name);
	}

/* -e program subset extract is .p.ts */
	if (0 != arg_epn) {
	    if (0 == arg_ean) {
		snprintf( out_name, sizeof(out_name)-1, "%s/%s.%d.ts",
			out_path, base_name, arg_epn);
	    /* with non-main audio is .p,a.ts */
	    } else {
		snprintf( out_name, sizeof(out_name)-1, "%s/%s.%d,%d.ts",
			out_path, base_name, arg_epn, arg_ean);
	    }
	}
	
	out_file = open( out_name, FILE_WMODE, FILE_PERMS );
	if (out_file < 3) {
	    perror("");
	    exit(1);
	}

#ifdef USE_BUFFER
	out_buf = init_buffer( out_file, 4096, out_name );
	if (NULL == out_buf) {
	    fprintf(stderr, "\nCan't allocate buffer for %s\n\n", out_name);
	    exit(1);
	}
#endif

	fprintf( stdout, "PIDs will be extracted to %s\n", out_name);
    }	

/* -y Video Elementary Stream extract, (still has a PES header @ SEQ) */
    if (0 != arg_espid) {
	snprintf( es_name, sizeof(es_name)-1, "%s/%s-%02X.es", 
		    out_path, base_name, arg_espid);

	es_file = open( es_name, FILE_WMODE, FILE_PERMS );
	if (es_file < 3) {
	    perror("");
	    exit(1);
	}

#ifdef USE_BUFFER
	es_buf = init_buffer( es_file, 4096, es_name );
	if (NULL == es_buf) {
	    fprintf(stderr, "\nCan't allocate buffer for %s\n\n", es_name);
	    exit(1);
	}
#endif
	fprintf( stdout, "PID %04X ES output to %s\n", arg_espid, es_name);

    }
}


#if 0
// xenoterms word wrapper, could be used for long description display...
static
void
word_wrap_tab( unsigned char *s, int cl )
{
    unsigned char *t;
    unsigned char tab[80];
    
    int ts, ns, cc, cl, i, len;

    ts = cc = ns = 0;

    t = strchr( s, '|'); // first | is tab stop
    if (t != NULL) ts = t - s;
    memset( tab, 0, sizeof(tab));
    ts += 2;
    memset( tab, ' ', ts);
    len = strlen(s);
    fprintf( stdout, "\n");
    for (i = 0; i < len; i++) {
	// if this char not a space, find next space
	if (s[i] != ' ') {
	    t = strchr( &s[i], ' ');
	    if (t != NULL) {
		ns = t - &s[i];
		if ((ns + cc) > cl)
		{
		    fprintf( stdout, "\n%s", tab);
		    cc = ts;
		}
	    }
	}    
	fprintf( stdout, "%c", s[i]);
	cc++;
    }
}
#endif


static
void
remove_dups( void )
{
    unsigned int i, j;

// first make pg list of unsorted entries and count pgm_idx
    pgm_idx = 0;
    for (i = 0; i < PGZ; i++) if (pgm[ i ].etmid != 0) pg[ pgm_idx++ ] = i;
    fprintf( stdout, "pgm idx %d\n", pgm_idx);

    pg_idx = pgm_idx;
    
// if the EIT and ETT are parsed correctly you wont need to sort
// however, sort makes duplicate removal a lot easier, if less efficient
// sort by VC# and time, since sort by ETMID is broken?

#if 0
    for (i = 0; i < (pg_idx-1); i++) {
	for (j = (i+1); j < pg_idx; j++) {
	    if ( pgm[ pg[ j ] ].etmid < pgm[ pg[ i ] ].etmid ) {
		int k;
		k = pg[j];
		pg[j] = pg[i];	// old sort of sort
		pg[i] = k;		// do not need anything faster
	    }
	}
    }
#endif

// now can remove dups
/* sieve method of removal, more accurate, less efficient */
    for (i = 0; i < (pg_idx-1); i++) {
	if (0xFFFF == pg[ i ]) continue; /* skip already removed */
	for (j = (i+1); j <pg_idx; j++) {
	    if (0xFFFF == pg[ j ]) continue; /* skip already removed */
	    if ( pgm[ pg[ i ] ].etmid == pgm[ pg[ j ] ].etmid ) {
		fprintf( stdout, "Remove DUP of pgm[%04X] @ pgm[%04X]\n",
		    pg[ i ], pg[ j ] );
		memset( &pgm[ pg[ j ]], 0, sizeof( pgm1 ) ); // clear it
		pg[ j ] = 0xFFFF; /* mark it inactive */
	    }
	}
    }

/* create pg1 index list, will be modified by [sort and?] dup removal */
//    pg1_idx = 0;
//    for (i = 0; i < pg_idx; i++) if (0xFFFF != pg[i]) pg1[pg1_idx++] = pg[i];

// dump guide uses pg1[ 0...pg1_idx-1 ] for sorted with no dups
    fprintf( stdout, "Old pg %u new no-DUP pg1 %d\n\n", pg_idx, pg1_idx);
}

static
void
dump_guide( void )
{
	short pn, sr, ap; // atsc program number lookup from source id
	int i, j, k;
	unsigned int lpt;
	unsigned int cpt; // last program time+len
	unsigned char s[1024];
	unsigned char d[32]; // date string
	unsigned char td; // to do
	struct tm stm; // start time as tm
	time_t st; // start time as time_t

	if (arg_guide == 0) return;

	i = j = k = 0;
	lpt = cpt = 0;

	// sort guide by etmid. it gets unruly otherwise.
	// various PSIP generators do various things differently
	remove_dups(); // sets pg1_idx

	fprintf( stdout, "PSIP guide dump, last STT %s\n", stt_text);
	fprintf( stdout,
		"   pg  ETMID    Pgm# Date       Time   Len Name"
		"                             Description\n");

//        for (i = 0; i < pg1_idx; i++) {
//	    k = pg1[i]; // shorthand for sorted no-dup pg1 index

	for (i = 0; i < pg_idx; i++) {
	    k = pg[i];
	    if (0xFFFF == k) continue;		/* skip removed */
	    if (0 == pgm[k].etmid) continue;	/* skip empty */

	    td = ' ';

// only bother with pgm entry if etmid set
//    	    if (0 != pgm[k].etmid)
	    {

		pn = 0;
		sr = pgm[k].etmid>>16;
		ap = find_vc_src( sr );
		if (ap >= 0) {
		    pn = vc[ap].pn;
		}
		st = pgm[k].st;

		localtime_r( &st, &stm);
		asctime_r( &stm, d);

		d[16] = 0;
	
		if (strlen(pgm[k].name) > 30) {
		    pgm[k].name[30]='>';
		    pgm[k].name[31]=0;
		}
		cpt = pgm[k].st;

		// first one doesn't get a star, but rest out of sync do
		if ( (i != 0) && (cpt != lpt) ) td = '*';

		snprintf( s, sizeof(s)-1,
			    "%c"
			    " %04X "
			    "%08X "
			    "%04X "
			    "%s "
			    "%4d "
			    "%-32s "
			    "%s ",
			td,
			k,
			pgm[k].etmid,
			pn,
			d,
			pgm[k].ls / 60,
			pgm[k].name,
			pgm[k].desc
		     );

//		if (strlen(s) > 78) {
//		    s[78] = '>';
//		    s[79] = 0;
//		}

		// output line of text
		fprintf( stdout, "%s\n", s);
//		word_wrap_tab( s );
		// fix last program time to current + len
		lpt = cpt + pgm[k].ls;
	    }
	}
}


static
void
dump_counts( void )
{
    int i, j, k;
    unsigned char p1[32], p2[32], p3[32];

    j = 0;

    // sum up transport errors
    pkt.errors = pkt.errsyn + pkt.errtei + pkt.errscr + pkt.errcce;
    // then atsc errors
    pkt.errors += pkt.crcstt + pkt.crcmgt + pkt.crctvct + pkt.crceit
		+ pkt.crcett + pkt.crcrrt + pkt.crcdcct + pkt.crcdccsct;
    // then mpeg errors
    pkt.errors += pkt.crcpat + pkt.crccat + pkt.crcpmt;
	
    lltoasc(p1, pkt.count);
    lltoasc(p2, pkt.pes);
    lltoasc(p3, pkt.errors);

    fprintf( stdout, "\nPackets %s, PES packet %s, Errors %s\n", p1, p2, p3);

    fprintf( stdout, "Transport Errors: %d: Sync %d Scrambled %d Continuity %d\n\n",
	    pkt.errtei, pkt.errsyn, pkt.errscr, pkt.errcce);

    if (0 != pkt.errcce) {
	if (0 != pkt.mpegce) {
	    fprintf( stdout, "MPEG Continuity errors %d, PIDs:\n", pkt.mpegce);
	    for (i = 0; i < 0x1000; i++) {
		if (0 == cc_err[i]) continue;
		fprintf( stdout, "    %04X # %8d", i, cc_err[i]);
		j++;
		j &= 3;
		if (0 == j) fprintf( stdout, "\n");
	    }
	    fprintf( stdout, "\n");
	}
	if (0 != pkt.atscce) {
	    fprintf( stdout, "ATSC Continuity errors %d, PIDs:\n", pkt.atscce);
	    for (i = 0x1000; i < 0x2000; i++) {
		if (0 == cc_err[i]) continue;
		fprintf( stdout, "    %04X # %8d", i, cc_err[i]);
		j++;
		j &= 3;
		if (0 == j) fprintf( stdout, "\n");
	    }
	    fprintf( stdout, "\n");
	}
        fprintf( stdout, "\n");
    }

    if (0 != pkt.mpeg2) {
	fprintf( stdout, "MPEG PIDs by Table Type:\n");
	fprintf( stdout, "MPEG---- -----PAT -----CAT -----PMT -----VID -----AUD ----NULL\n");
	fprintf( stdout, "%8d %8d %8d %8d %8d %8d %8d\n",
		pkt.mpeg2+pkt.null, pkt.pat, pkt.cat, pkt.pmt,
		pkt.vid, pkt.aud, pkt.null );

	k = pkt.crcpat + pkt.crccat + pkt.crcpmt;
	if (0 != k)
	    fprintf( stdout, "CRC32ERR %8d %8d %8d\n",
		pkt.crcpat, pkt.crccat, pkt.crcpmt);
        fprintf( stdout, "\n");
    }
    	    
    if (0 != pkt.atsc) {
	fprintf( stdout, "ATSC PIDs by Table Type:\n");
	fprintf( stdout, "ATSC---- -----MGT -----VCT -----EIT -----ETT -----RRT -----STT\n");
	fprintf( stdout, "%8d %8d %8d %8d %8d %8d %8d\n",
		pkt.atsc, pkt.mgt, pkt.tvct, pkt.eit,
		pkt.ett, pkt.rrt, pkt.stt);

	k = pkt.crcmgt + pkt.crctvct + pkt.crceit + pkt.crcett + pkt.crcrrt + pkt.crcstt;
	if (0 != k)
	    fprintf( stdout, "CRC32ERR %8d %8d %8d %8d %8d %8d\n",
		pkt.crcmgt, pkt.crctvct, pkt.crceit, pkt.crcett,
		pkt.crcrrt, pkt.crcstt);
	fprintf( stdout, "\n");
    }

//    if (0 == arg_pids) return;

    if (0 != pkt.mpeg2) {
	fprintf( stdout, "MPEG PID counts:");
	j = 0;
	k = pkt.null;
	for (i = 0; i < 0x1000; i++) {
	    if (pids[i] > 0) {
		char *s;
		k += pids[i];
		s = "    ";
		if (i > 0x1FF0)     { s = "\n    "; j = 0; }
		if (0 == (0xF & i)) { s = "\n    "; j = 0; }
		if (3 == (3 & j))   { s = "\n\\   "; j = 0; }
		j++;
		fprintf(stdout, "%s%04X #%9d", s, i, pids[i]);
	    }
	}
	fprintf( stdout, "\n");
	fprintf( stdout, "MPEG PID total: %8d\n\n", k);
    }

    if (0 != pkt.atsc) {
	k = 0;
	fprintf( stdout, "ATSC PID counts:\n");
	for (i = 0x1000; i < 0x1FFF; i++) {
	    if (pids[i] > 0) {
		char *s;
		k += pids[i];
		s = "    ";
		if (i > 0x1FF0)     { s = "\n    "; j = 0; }
		if (0 == (0xF & i)) { s = "\n    "; j = 0; }
		if (3 == (3 & j))   { s = "\n\\   "; j = 0; }
		j++;
		fprintf(stdout, "%s%04X #%9d", s, i, pids[i]);
	    }
	}
	fprintf( stdout, "\n");
	fprintf( stdout, "ATSC PID total: %8d\n\n", k);
    }
}

/* write .tsx file:
    sequence[] long long byte offsets, list terminated by -1LL,
    frames[] picture types and packet counts per picture

FIXME: seems to choke on files > 2hr long. spews out bad frame data.

*/
static
void
dump_sequences( void )
{
	unsigned char n[256];
	unsigned char o[256];
	ssize_t x, y;
	FILE *f;
	long long m1 = -1LL;

	printf( "\n");

	if (0 == arg_seqwr) return;
//	if (0 == arg_write) return;
	if (0 == frame_idx) return;
	if (0 == sequence_idx) return;

/* truncate name */
	filebase( n, in_name, F_PFILE );
/* add .tsx */
	snprintf( o, sizeof(o), "%s.tsx", n);
/* open file */
	f = fopen( o, "w");
	if (NULL == f) {
		fprintf( stderr, "error opening %s ", o);
		perror("");
/* error on open returns */
		return;
	}

//	fprintf( stdout, "writing sequence indices to %s\n", o);
	x = sequence_idx;

/* terminate list with -1LL */
	sequences[x] = m1;

/* FIXME: should check return to write the rest if it didn't finish it */
	y = fwrite( sequences, sizeof(m1), x+1, f);

	fprintf( stdout, "Wrote %lld out of %lld sequences to %s\n",
		    (long long)y-1, (long long)x, o );
	bytes_out += y * sizeof( m1 );


//    fprintf( stdout, "Writing frame packet indices to %s\n", o );
	x = frame_idx;

/* FIXME: should check return to write the rest if it didn't finish it */
	y = fwrite( frames, sizeof(struct frame_s), x, f);

	fprintf( stdout, "Wrote %lld out of %lld frames to %s\n",
		    (long long)y, (long long)x, o );

	bytes_out += y * sizeof( struct frame_s );
	fflush( f );
	fclose( f );
	printf( "\n");
}


static
void
dump_elapsed( void )
{
	double etf;
	double tpf;
	double flf;
	char bt[32];

	fprintf( stdout, " IN %11lld bytes\n", bytes_in);
	fprintf( stdout, "OUT %11lld bytes\n", bytes_out);

	bytes_total = bytes_in + bytes_out;

	etf = (double)cap_et.tv_sec;
	etf += ((double)cap_et.tv_nsec / 1000000000.0);

	tpf = (double)bytes_total;
	tpf /= etf;
	tpf /= 1000000.0;

	lltoasc( bt, bytes_total );

	fprintf( stdout, "\n");
	fprintf( stdout,
		 "processed %s bytes in %.2f seconds, %.2f MB/s\n",
		    bt, etf, tpf );
/*
		ap_et.tv_sec, cap_et.tv_nsec,
		(long long)( (bytes_total / (long long)cap_et.tv_sec) >> 20) );
*/

	if (last_pcr != 0) {
	    fprintf( stdout,
		"MPEG Stream Elapsed %llu %llu/90000 Seconds from AFC PCR\n", 
		(last_pcr - first_pcr) / 90000ULL, 
		(last_pcr - first_pcr) % 90000ULL );
	}

	if (last_stt != 0) {
		flf = (double)last_stt - (double)first_stt;

		if (etf < 0.1) etf = 0.1;

		fprintf( stdout,
		     "ATSC Stream Elapsed %u Seconds from STT, Rate: %.2fx\n",
			last_stt - first_stt, flf/etf);
	}
}


static
void
start_timer( void )
{
	memset( &cap_start, 0, sizeof(cap_start));	/* w/o librt, start is 0 */

#ifdef USE_LIBRT
	clock_gettime(  clock_method, &cap_start);
#endif

}

static
void
stop_timer( void )
{
	cap_stop.tv_sec = 1;			/* w/o librt, ET is 1s */

#ifdef USE_LIBRT
	clock_gettime( clock_method, &cap_stop);
#endif
	memcpy( &cap_et, &cap_stop, sizeof(cap_et) );

// if nanos subtracts borrow
	if (cap_start.tv_nsec > cap_stop.tv_nsec) {
		cap_et.tv_sec--;
		cap_et.tv_nsec += 1000000000;
	}
	cap_et.tv_sec  -= cap_start.tv_sec;
	cap_et.tv_nsec -= cap_start.tv_nsec;
    
// minimum is always one second, bypass short cut /0 moth
	if (cap_et.tv_sec < 1) cap_et.tv_sec = 1;
}

static
void
alloc_frames( void )
{
	frames = malloc( FRAME_MAX * sizeof(struct frame_s)  );
	sequences = malloc( SEQUENCE_MAX * sizeof(long long) );
}

static
void
free_frames( void )
{
	free(frames);
	free(sequences);
}


static
void
init_arrays( void )
{
    int i;

/* initialize some arrays */
	memset( pgm,	0,	sizeof( pgm )    );
	memset( pg,	0,	sizeof( pg )	 );
	memset( pg1,	0,	sizeof( pg1 )	 );
    
	memset( &pkt,	0,	sizeof( pkt )    );    
	memset( pids,	0,	sizeof( pids )   );
	memset( pid_cc,	0xFF,	sizeof( pid_cc ) );
	memset( pid_vn,	0xFF,	sizeof( pid_cc ) );
	memset( cc_err, 0,      sizeof( cc_err ) );

	memset( psit,	0xFF,	sizeof( psit )   );
	memset( keep_pids, 0,	sizeof( keep_pids ) );

/* EIT/ETT version numbers set invalid for update */
	memset( eitvn,	0xFF,	sizeof(eitvn));
	memset( ettvn,	0xfF,	sizeof(ettvn));

/* continuity counters set invalid for update */
	for (i=0; i < 0x2000; i++) last_cc[i] = 0xFF;

}

int
main( int argc, char **argv)
{

#ifdef USE_LIBRT
    test_clock_res();	/* find most accurate clock for clock_gettime */
#endif

    start_timer();

    fprintf( stdout, "\n"NAME" "VERSION" "LASTEDIT" "COPYRIGHT" "EMAIL"\n");
    fprintf( stdout, "Released under the %s\n", LICENSE);
    fprintf( stdout, "This software is supplied AS-IS, with NO WARRANTY.\n");
    fprintf( stdout, "Compiled on %s at %s for %s\n",
		    __DATE__, __TIME__, PLATFORM_NAME );

    calc_epoch();	/* ATSC vs Unix epoch */

    init_allocs();

    alloc_frames();	/* -s uses this */

    init_arrays();	/* clear the baffles */

    parse_args( argc, argv ); /* hey homer, have a powdered jelly doh-nut */

    show_allocs();

    read_loop();	/* reads until EOF */
    
    if (in_file > 2) {
	close( in_file );
#ifdef USE_BUFFER
	reset_buffer( in_buf );
	free_buffer( in_buf );
#endif
    }

    if (es_file > 2) {
	close( es_file );
	es_file = 0;
#ifdef USE_BUFFER
	flush_buffer( es_buf, 1 ); /* sync */
	bytes_out += es_buf->num;
	fprintf(stdout, "\n ES->num %lld\n", es_buf->num);
	free_buffer( es_buf );
#endif
    }

    if (out_file > 2) {
	close( out_file );
	out_file = 0;
#ifdef USE_BUFFER
	flush_buffer( out_buf, 1 ); /* sync */
	bytes_out += out_buf->num;
	fprintf(stdout, "\n TS->num %lld\n", out_buf->num);
	free_buffer( out_buf );
#endif
    }

    stop_timer();

    if (0 != arg_seqwr) {
	dump_sequences();	/* -s option writes frames/sequences */
    } else {
	dump_counts();	/* counts will not be valid without read loop */
	dump_guide();	/* guide will not be valid unless ATSC PSIP */
    }

    free_frames();

    dump_elapsed();

    show_allocs();
    fprintf( stdout, "\n");

    fflush( stdout );

    return 0;
}
