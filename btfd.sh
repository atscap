#!/bin/sh
PREFIX=/usr/local
# backtrace symbols .bt file dump helper for atscap-1.1
# Usage: $0 <FILENAME
#
# Application name is AN
# Application signal handler name is SN
#
# CO PO PC BO BC P1 P2 B1 B2 are checks for various extracts
#
AN="atscap"
SN="signal_handler"
CO="\#"
PO="\("
PC="\)"
BO="\["
BC="\]"

while read line

do

  NAME=""
  FUNC=""
  ADDR=""
   
# extract file name, (function name) and [address]
  C1=`expr index "$line" "$CO"`
  if [ "$C1" -eq "1" ]
    then   echo "$line"; continue
  fi

#  echo "RAW $line"

  N1="0"
  N2="1"

  P1=`expr index "$line" "$PO"`
  P2=`expr index "$line" "$PC"`

  B1=`expr index "$line" "$BO"`
  B2=`expr index "$line" "$BC"`

# echo "  " $C1 $P1 $P2 $B1 $B2

  if [ "$P1" -gt "0" ]
    then if [ "$P2" -gt "0" ]
      then FUNC=${line:$P1:$P2-$P1-1}
    fi
  fi

  if [ "$B2" -gt "$B1" ]
      then ADDR=${line:$B1:$B2-$B1-1}
  fi

  N2=$P1

  if [ "$N2" -eq "0" ]
    then N2=$B1;
  fi

  if [ "$N2" -eq "0" ]
    then N2=$P1;
  fi

#  echo N1 $N1 N2 $N2 P1 $P1 P2 $P2 B1 $B1 B2 $B2

  if [ "$N2" -gt "$N1" ]
    then NAME=${line:$N1:$N2-$N1-1}
  fi

# if [ "$N2" -gt "1" ]
#   then echo NAME $NAME
# fi

# if [ "$P2" -gt "1" ]
#   then echo FUNC $FUNC
# fi

# if [ "$B2" -gt "1" ]
#   then echo ADDR $ADDR
# fi
#  echo "+++" NAME $NAME FUNC $FUNC ADDR $ADDR

# only want AM matches to application name AN to generate addr2line
  AM=`expr index "$NAME" "$AN"`
# only want SM non-match to signal_handler function to generate addr2line
  SM=`expr index "$NAME" "$SN"`
#  echo $NAME $AN $AM
#  echo $NAME $SN $SM

  if [ "$AM" -ne "1" ]
    then continue
  fi

  if [ "$SM" -eq "0" ]
    then continue
  fi

#  echo DO addr2line.new -s -f -e $PREFIX/bin/atscap $ADDR

  addr2line -s -f -e $PREFIX/bin/atscap $ADDR

done
