/*****************************************************************************
 * common.h (c) Copyright 2007 by inkling@users.sourceforge.net
 *
 * This software is part of the atscap 1.1 distribution codebase.
 *
 * atscap is free software; you may only redistribute it and/or modify
 * it under the terms of the GNU General Public License, Version 2 or later,
 * as published by the Free Software Foundation.
 *
 * atscap is distributed to you AS-IS, in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License Version 2
 * along with this program; if not, write the Free Software Foundation, Inc.,
 * at 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */


#include <stdarg.h>

#ifndef COMMON_H
#define COMMON_H

#ifndef WHO
#define WHO (char *) __FUNCTION__
#define WHERE (int) __LINE__
#endif

/******************************** alloc functions ***************************/
#define ALLOC_MAX 32
#define ALLOC_NAME_MAX 40
typedef struct {
	char * name;
	void * addr;
	int size;
} alloc_t;

alloc_t allocs[ ALLOC_MAX ];
int alloc_idx = 0;


//static
void
init_allocs ( void )
{
        int i;

        memset( allocs, 0, sizeof(allocs));

        for (i = 0; i < ALLOC_MAX; i++) {
                allocs[i].addr = NULL;
                allocs[i].name = NULL;
        }
}


//static
void
show_allocs( void )
{
	int i, t;
	alloc_t *m;

//	WHOAMI;

	m = allocs;

	t = 0;
	fprintf(stdout, "\nPointers       Size Name\n");

	for (i = 0; i < ALLOC_MAX; i++) {
	    if (NULL != m[i].addr) {
		fprintf( stdout, "0x%08X %8d %s\n",

/* broken for 64 bits? use off_t instead? */
			(unsigned int)m[i].addr,

			m[i].size,
			m[i].name );

		t += m[i].size;	
	    }
	}

	fprintf(stdout, "Pointers  %9d total bytes\n\n", t);
}


//static
void *
ifree ( void * p, char *n )
{
	int i;
	alloc_t *m;

//	WHOAMI;

	m = allocs;

	if (NULL == p) return NULL;

	for (i = 0; i < ALLOC_MAX; i++)
		if (p == m[i].addr) break;

	if (i == ALLOC_MAX) {
#ifdef ATSCAP
		dvrlog( LOG_INFO,
			"ifree %p not found for %s", p, n);
#else
		fprintf( stdout,
			 "ifree %p not found for %s\n", p, n);
#endif
		return NULL;
	}
	m = &allocs[i];

//	fprintf( stdout, "ifreed %s %d\n", m->name, m->size);

	free(m->addr);
	m->addr = NULL;
	m->size = 0;
	free(m->name);
	m->name = NULL;
	alloc_idx--;

	return NULL;
}


//static
void *
imalloc ( size_t z, char *n )
{
	int i;
	void *p;
	alloc_t *m;

	m = allocs;

	for (i = 0; i < ALLOC_MAX; i++)
		if (NULL == m[i].addr ) break;

	if (i == ALLOC_MAX) {
#ifdef ATSCAP
		dvrlog( LOG_INFO,
			 "ialloc full %s %d, increase ALLOC_MAX",
			 n, z);

#else
		fprintf( stdout,
			 "ialloc full %s %d, increase ALLOC_MAX\n",
			 n, z );
#endif
		return NULL;
	}

	p = malloc( z );
	if (NULL == p) return NULL;
	m->addr = p;
	m->size = z;
	m->name = calloc( 1, ALLOC_NAME_MAX );
	strncpy( m->name, n, ALLOC_NAME_MAX );
	m->name[ ALLOC_NAME_MAX - 1 ] = 0;
	alloc_idx++;

	return p;
}


//static
void *
icalloc( int x, int y, char *n )
{
	void * p;
	int i;
	alloc_t *m;

//	WHOAMI;
//	if (0 != arg_fmsg)
//		fprintf( stdout, "%s %d %s\n", WHO, z, n );

	m = allocs;

	for (i = 0; i < ALLOC_MAX; i++)
		if (NULL == m[i].addr ) break;

	if (i == ALLOC_MAX) {
#ifdef ATSCAP
		dvrlog( LOG_INFO,
			"icalloc full %s %dx%d, increase ALLOC_MAX",
			n, x, y );

#else
		fprintf( stdout,
			 "icalloc full %s %dx%d, increase ALLOC_MAX\n",
			 n, x, y );
#endif
		return NULL;
	}

	p = calloc( x, y );
	if (NULL == p) return NULL;

	m = &allocs[i];
	m->addr = p;
	m->size = x * y;

	m->name = calloc( 1, ALLOC_NAME_MAX );
	strncpy( m->name, n, ALLOC_NAME_MAX );
	m->name[ ALLOC_NAME_MAX - 1 ] = 0;

	alloc_idx++;

	return p;
}

// incremental realloc, does not shrink, only grows by n bytes
//static
void *
irealloc ( void * p, int z, char *n )
{
	int i;
	alloc_t *m;

//	WHOAMI;
//	fprintf( stdout, "%s p %p z %d\n", WHO, p, z );

	m = allocs;


	if (NULL == p) return NULL;
	if (0 == z) return p;

	for (i = 0; i < ALLOC_MAX; i++)
		if (p == m[i].addr) break;

	if (i == ALLOC_MAX) {
#ifdef ATSCAP
		dvrlog( LOG_INFO,
			"irealloc %p not found for %s %d", p, n, z);
#else
		fprintf( stdout,
			 "irealloc %p not found for %s %d\n", p, n, z);
#endif
		return NULL;
	}

	m = &allocs[i];

	strncpy( m->name, n, ALLOC_NAME_MAX );
	m->name[ ALLOC_NAME_MAX - 1] = 0;

	m->size += z;
	m->addr = realloc( m->addr, m->size );

	return m->addr;
}
/*************************** end of alloc functions *************************/


/********************************* text functions ***************************/
#define F_PATH 1
#define F_PFILE 2
#define F_TFILE 3
#define F_FILE 4

//static
void
filebase ( char *d, char *s, int o )
{
	char *t;

	*d = 0;

/* limit option flags to non-bogus */
	if (0 == o) return;
	if (0 > o) return;

/* limit string handling to non-bogus */
	if (NULL == d) return;
	if (NULL == s) return;
	if (0 == *s) return;

/* mutually exclusive options for how to truncate the source name */
	switch( o )
	{
/* path only, with / */
	case F_PATH:
		strcpy( d, "./" );
		t = strrchr( s, '/' );
		if (NULL != t) {
			strcpy( d, s );
			t = strrchr( d, '/');
			t++;
			*t = 0;
		}
		break;

/* path and file, without last .ext */
	case F_PFILE:
		strcpy( d, "./" );
		t = strrchr( s, '/' );
		if (NULL == t) d += 2;
		strcpy( d, s );
		t = strrchr( d, '.');
		if (NULL != t) *t = 0;	    /* truncate at last . */
		break;

/* truncated file only, without last .ext */
	case F_TFILE:
		t = strrchr( s, '/' ); /* skip path */
		if (NULL != t) {
			t++;
			strcpy( d, t );

/* truncate at last . */
			t = strrchr( d, '.');
			if (NULL != t) *t = 0;
		} else {
/* no path, truncate at last . */
			strcpy( d, s );
			t = strrchr( d, '.');
			if (NULL != t) *t = 0;
		}
		break;

/* file only with .ext */
	case F_FILE:
		t = strrchr( s, '/' );
		if (NULL != t) {
			t++;
			strcpy( d, t );
		} else {
			strcpy( d, s );
		}
		break;

	default:
		break;
	}
}


/* long long to ascii comma delimited to make big numbers easier to read.
    Assumes dest has enough bytes for result (19 digits + 6 commas + nul).
    Largest number is around +/- 9,000,000,000,000,000,000 (US quintillion).

    PROOF:

    value       sl              dest            dl      dl computation

    1           1               1               1       (0 * 4) + 1 = 1
    10          2               10              2       (0 * 4) + 2 = 2
    100         3               100             3       (1 * 4) - 1 = 3
    1000        4               1,000           5       (1 * 4) + 1 = 5
    10000       5               10,000          6       (1 * 4) + 2 = 6
    100000      6               100,000         7       (2 * 4) - 1 = 7
    1000000     7               1,000,000       9       (2 * 4) + 1 = 9
    10000000    8               10,000,000      10      (2 * 4) + 2 = 10
    100000000   9               100,000,000     11      (3 * 4) - 1 = 11
    1000000000  10              1,000,000,000   13      (3 * 4) + 1 = 13

    NOTE: atoll() breaks after 2^30 on 32-bit with some older compilers.
	Auto-verification using atoll on char c[] doesn't always work.
	This function also depends on snprintf getting it right.

    10000000000 11              10,000,000,000  14      (3 * 4) + 2 = 14
*/
#define COMMA_MAX 32

//static
void
lltoasc ( char *dest, long long value )
{
	char *s, *b, *d;		/* source and bottom boundary for d */
	int i, k, sl, dl;		/* loops, source len, destination len */
	char c[COMMA_MAX];		/* value in ascii */

	d = dest;

/* Don't want the sign confusing the comma calculation so fix it here. */
	if ( value < 0LL ) {
//		value ^= -1LL;			/* one */
//		value++;			/* way */ 
		value = 0LL - value;		/* or another */
/* if still negative, it is largest negative and will not complement. */
		if (value >= 0LL) *d++ = '-';
	}

	snprintf( c, COMMA_MAX, "%lld", value );

	if (value > 999LL) {			/* commas needed ? */
		b = d;				/* save stop pointer */
		sl = strlen( c );		/* src len */
		dl = ((sl / 3) * 4) + (sl % 3);	/* dest len */
		if ( 0 == ( sl % 3)) dl--;	/* fixup for last , */

		d[dl] = 0;			/* NUL term */
		dl--;				/* and back up */

		if ( dl < COMMA_MAX ) { 	/* will added commas fit? */

/* copy bytes from s to d in reverse order, adding , to d every 3 bytes */

			k = sl - 1;		/* loop limit */
			s = c + k;		/* point: 1's place end of c */
			d += dl;		/* point: computed end of d */
        
			for ( i = sl; i > 0; ) {
    			    if ( (d < b) || (s < c) ) break;	/* boundary */
			    i--;
			    *d-- = *s--;		/* copy & decrement */
			    if ( (d < b) || (s < c) ) break;	/* boundary */

			    if (0 == ((sl-i) % 3)) *d-- = ',';
			    if (d < b) break;		/* boundary check */
			}
		} else {
			strncpy( d, c, COMMA_MAX ); /* commas won't fit */
		}
	} else {
		strncpy( d, c, COMMA_MAX );	/* no commas needed */
	}
	return;
}


/* build a list of pointers up to n pointers from string s using delim c */
/* s is modified with NUL terms replacing each delim c */
//static
void
build_args( char **p, int n, char *s, char c, char *caller )
{
	int i;
	char *r, *t;

	for (i = 0; i < n; i++) p[i] = NULL;

	r = s;

	for (i = 0; i < n; i++) {
		if (0 == *r) break;
		p[i] = r;
		t = strchr( r, c );
		if (NULL == t) break;
		*t = 0;
		t++;
		r = t;
	}
}


/* Similar to but different from strncpy:
    No string greater than 16k allowed to be copied, log > 16k and NULLs.
    Does not pad destination with 0's if length of source is shorter than n.
    There is no valid reason to spend cpu cycles filling memory with zeros.
    This will always null terminate at n-1 even if s is n len bytes long.
    d result will always be n-1 characters long, at most.
*/
/* static */
void
astrncpy ( char *d, char *s, unsigned int n )
{
        int c = n;
        char *t = d;
/* log nulls */
        if ( (NULL == d) || (NULL == s) ) {
#ifdef ATSCAP
		dvrlog( LOG_INFO, "astrncpy() %p %p NULL", d, s);
#else
		fprintf( stdout, "\nastrncpy() %p %p NULL\n", d, s);
#endif
		return;
        }
/* log 0 and > 65535 len */
        if ( (0 == n) || (n > 16383) ) {
#ifdef ATSCAP
                dvrlog( LOG_INFO, "astrncpy() %d chars", n);
#else
                fprintf( stdout, "\nastrncpy() %d chars\n", n);
#endif
                return;
        }

/* count auto decrements, pointers auto increment */
	while (c-- > 0) if (0 == (*t++ = *s++)) break;

/* always force last byte 0 */
	d[n-1] = 0;
}


/* maximum amount to text to append with each call */
#define PRINTF_MAX 16384
/* Incremental snprintf, appends to string 'a' until size 'b' reached.
    This keeps 'b' limited to 'a' string alloc instead of blind melon limit.
    First call should have a strlen 0, *a = 0 or you get variable junk.
 */
//static
void
asnprintf ( char *a, size_t b, const char *fmt, ... )
{
	char *p, t[PRINTF_MAX];
	int la;
	int lt;
	va_list ap;

	if (NULL == a) return;
	if (b < 1) return;

	memset(t, 0, sizeof(t));
	va_start(ap, fmt);
/* variable arg macro start, pass fmt and ap, variable arg macro end */
	vsnprintf( t, sizeof(t)-1, fmt, ap );
	va_end(ap);

	la = strlen(a);
	lt = strlen(t);

/* bit bucket does not overflow? */
	if ( (la + lt + 1) < b ) {
		p = &a[la];
/* nul term it */
		astrncpy( p, t, lt + 1 );
	}
/* if bit bucket does overflow, silently fail and don't exceed boundary */
}
/**************************** end of text functions *************************/


/******************************* librt functions ****************************/
#ifdef USE_LIBRT
#define RTC_MAX 3
char *clock_text[ RTC_MAX ] = {
        "REALTIME",
        "PROCESS_CPUTIME_ID",
        "THREAD_CPUTIME_ID"
};

clockid_t clock_ids[ RTC_MAX ] = {
        CLOCK_REALTIME,
        CLOCK_PROCESS_CPUTIME_ID,
        CLOCK_THREAD_CPUTIME_ID
};
clockid_t clock_method = CLOCK_PROCESS_CPUTIME_ID;
long long clock_res;            /* (tv.sec<<32) | tv.nsec */
int clock_idx;

/* find the most accurate clock and set clock_method global */
//static
void
test_clock_res ( void )
{
	struct timespec res;
	int i, j;
	long long r, s;

	j = -1;
	r = 1000000000LL; /* worst case scenario is 1 clock per second */

	for (i = 0; i < RTC_MAX; i++) {

/* is clock res return valid? */
	    if (0 == clock_getres( clock_ids[i], &res)) {
		s = res.tv_sec;
		s <<= 32;
		s |= res.tv_nsec;

/* is clock res non-zero? */
		if (0LL != s)
		    if (s < r) {
			j = i;
			r = s;
		    }
	    }
	}
    
	if (-1 == j) {
	    fprintf(stderr, "%s has no clock method? check librt\n", NAME);
	    exit(1);
	}

/* use last lowest value in l for the syslog */
	clock_method = clock_ids[j];
	clock_idx = j;
	clock_res = r;
	fprintf(stdout, "CLOCK_METHOD is %d %s\n",
		clock_idx, clock_text[clock_idx]);
}
/* USE_LIBRT */
#endif
/*************************** end of librt functions *************************/


/******************************* file functions *****************************/
#ifdef ATSCAP
/* Copy source file s to destination file d, a is 0 copy or 1 append:
    If source contains '*', source is used for glob list.
    If destination term with '/', [glob] source appends to dest.
    NOTE: It could *always* glob, however glob() uses a lot of cycles.
    Also, it tries to behave like cp -a and keep file date and mode bits.
    TODO? alloc buf fs.blocksize or page-size? 4k is OK
*/

//static
int
filecopy ( char *d, char *s, int a )
{
	char buf[4096], ni[512], no[512], fb[256], *p;
	int i, j, k, o, r, w, z, e, g, f, rc;
	struct utimbuf ut;
	int upm;
	glob_t globt;
	struct stat64 fs;

	z = sizeof(buf);

/* sanity checks */
	if (NULL == d) return -1;
	if (NULL == s) return -1;
	if (0 == *d) return -1;
	if (0 == *s) return -1;
    
	e = 0;
	f = 1;
	g = 0;

/* glob needed for * in filename? */
	p = strchr( s, '*' );
	if (NULL == p) p = strchr( s, '?' );
	if (NULL != p) g = ~0;
	
	if (0 != g) {
/* WARNING: no returns until globt is freed, only continues and breaks */
	    glob( s, 0, NULL, &globt );

	    f = globt.gl_pathc;
	    advrlog( LOG_INFO, "%s glob %s files %d", WHO, s, f);
	}

/* any more glob files left to do, or is single file? */
	for (j = 0; j < f; j++) {

/* set name in and name out */
	    astrncpy( ni, s, sizeof(ni) );
	    astrncpy( no, d, sizeof(no) );
	    
/* glob will change source name to glob index if found */
	    if (0 != g) astrncpy( ni, globt.gl_pathv[ j ], sizeof(ni) );

/* remove any trailing / */
	    p = no + strlen( no );
	    p--;
	    if ('/' == *p) *p = 0;

/* destination exists? */
	    rc = stat64( no, &fs );
	    if (0 == rc) {
		if (0 != (S_IFDIR & fs.st_mode)) {
		    filebase( fb, ni, F_FILE );
		    asnprintf( no, sizeof(no), "/%s", fb );
		}
	    }

/* keep source metadata: cp -a emulation sets mtime and mode after copy */
	    rc = stat64( ni, &fs);

/* skip bad file for whatever reason */
	    if (rc < 0) {
		dvrlog( LOG_INFO, "%s can't stat %s", WHO, ni);
		continue;
	    }

/* clear other time stamps */
	    memset( &ut, 0, sizeof(ut) );
	    ut.modtime = fs.st_mtime;
	    ut.actime = fs.st_atime;

	    upm = fs.st_mode;

	    advrlog( LOG_INFO, "%s %s %s", (0 == a)?"copy":"append", ni, no );

/* peter knaggs says read only helps luser mode */
	    i = open( ni, FILE_ROMODE );
	    if (i < 3) {
		dvrlog( LOG_INFO, "%s can't open %s err %d", WHO, ni, errno);
		e = -1;
		continue;
	    }

/* copy, mode is octal */
	    if (0 == a)
		o = open( no, FILE_WMODE, 0644 );

/* append. seeks end of file instead of O_APPEND flag with NFS problems */
	    if (0 != a)
		o = open( no, O_RDWR | O_CREAT, 0644 );

	    if (o < 3) {
		dvrlog( LOG_INFO, "%s can't open %s", WHO, no);
		close ( i );
		e = -1;
		continue;
	    }

/* append sets file position to end, should avoid NFS problems?
   no. append is broken. don't use it here.
*/
	    if (0 != a) lseek( o, 0, SEEK_END );

	    r = 1;
	    e = 0;
	    k = 0;
	    while (r > 0) {
		r = read( i, buf, z );
		if (r > 0) {
			w = write( o, buf, r );
			k += r;
			if (w < 1) {
			    dvrlog( LOG_INFO, "%s can't write %s", WHO, no);
			    e = -1;
			    break;
			}

			if (w != r) {
				dvrlog( LOG_INFO, "%s wrote %d of %d to %s",
					WHO, w, r, no);
				e = -1;
				break;
			}

			if (r != z) {
				advrlog( LOG_INFO, "%s EOF %s", WHO, ni);
				break;
			}
		}

		if (r < 0) {
			dvrlog( LOG_INFO, "%s can't read %s", WHO, no);
			e = -1;
			break;
		}
	    }
	    close( i );
	    fsync( o );
	    close( o );

/* Change file permissions after close to match original mode then set
    mtime after chmod. This prevents guide from reloading every time it
    restarts with recent EPGs, and keeps auto-EPG reload on 3hr meridian.

   NOTE: It is not expected that you will have to restart it very often.
    This is mostly to make it easier if you're trying to add new features.
    The wait for guide load can be very annoying after a few dozen times. :>
*/
	    chmod( no, upm );
	    utime( no, &ut );
	}

/* don't forget to free what glob allocated */
	if (0 != g) globfree( &globt );

	return e;
}

/* ATSCAP */
#endif
/**************************** end of file functions *************************/


/* COMMON_H */
#endif
