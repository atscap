#!/bin/bash
# Changes mozilla supplied mpeg:// url to:
#   a) http:// for xine/mplayer http playback over httpd
# or:
#   b) directory plus basename for playback over mounted filesystem
#
# mplayer can use httpd Range: to seek within file. no slider in xine http://
#
# xine player options can be set to:
#  -V xvmc overrides to xvmc mode; -p starts playback;
#  --enqueue adds to mrl list and starts playing if not loaded yet
PLAYER=xine
#PLAYOPT="-p -V xvmc --enqueue"
PLAYOPT="-p -G960x544"
#
# mplayer options set to xvmc mode playback
#PLAYER=mplayer
#PLAYOPT="-vo xvmc -vc ffmpeg12mc"
#
# replace mpeg:// with http:// for URL
URI=$1
PLAYURL=http${URI:4}
CUT1=`expr "$URI" : 'mpeg://'`
HOSTFILE=${URI:CUT1}
CUT2=`expr index "$HOSTFILE" '/'`
NAME=${HOSTFILE:CUT2}
#
# change this to local mount used for your remote capture directory
DIR=/dtv
PLAYFILE=$DIR/$NAME
#
# uncomment this for playback over local mount directory/filename.ts
$PLAYER $PLAYOPT $PLAYFILE
# uncomment this for playback over HTTP
#$PLAYER $PLAYOPT $PLAYURL
