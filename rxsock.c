/*
    receive multicast socket test
    by inkling@nop.org 2-dec-2005
    released to public domain
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

#define MULTICAST_ADDRESS "224.1.2.3"
#define MULTICAST_PORT 1234

int
main( int argc, char ** argv)
{
    struct sockaddr_in sockin;
    struct ip_mreq im;
    unsigned char tsbuf[188];
    int sockfd, bindok, ruok, imok, bc, rx;
    unsigned int yes = 1;
    int sockin_len;

    bc = 0;
    sockin_len = sizeof(sockin);


    /* open socket */
    sockfd = socket( AF_INET, SOCK_DGRAM, IPPROTO_UDP );
    if (-1 == sockfd) {
	perror( "socket open ");
	exit(1);
    }

    /* set socket as re-usable */
    ruok = setsockopt( sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
    if (-1 == ruok) {
	perror( "socket reuse ");
	exit(1);
    }

    /* set socket as multicast group */
    im.imr_multiaddr.s_addr = inet_addr( MULTICAST_ADDRESS );
    im.imr_interface.s_addr = htonl( INADDR_ANY );
    imok = setsockopt( sockfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &im, sizeof(im));
    if (-1 == imok) {
	perror( "socket multicast ");	
	exit(1);
    }

    /* bind socket AFTER setting socket options */
    /* gives re-use error if you try bind first */
    memset( &sockin, 0, sockin_len );
    sockin.sin_family = AF_INET;
    sockin.sin_addr.s_addr = htonl( INADDR_ANY );
    sockin.sin_port = htons( MULTICAST_PORT );
    bindok = bind( sockfd, (struct sockaddr *)&sockin,  sockin_len );
    if (-1 == bindok) {
	perror( "socket bind ");
	exit(1);
    }
    while(1) {
/*	memset( &sockin, 0, sizeof(sockin) ); */
	rx = recvfrom( sockfd,
			tsbuf, sizeof(tsbuf),
			0,
			(struct sockaddr *)&sockin,
			&sockin_len
			);

	if( -1 == rx) {
	    perror( "recvfrom ");
	    exit(1);
	}
	
	bc += rx;
	fprintf( stdout, "received %d bytes %d total\n", rx, bc);
    }    	

    return 0;
}
