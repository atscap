/* loop on a command until the return code is non-zero */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int
main( int argc, char **argv )
{
	int ok, i,p;
	char c[80];
	if (argc < 2) { printf("Supply a command line to run\n"); exit(0); }
	*c = 0;
	for (i = 1; i < argc; i++) {
		p = strlen(c);
		if (p + strlen(argv[i]) > sizeof(c))
			break;
    		snprintf( &c[p], sizeof(c), "%s ", argv[i] );
	}
	printf( "Command line: %s", c);
	i = 0;
	ok = 0;
	while( 0 == ok ) {
		ok = system( c );
		printf( "\n\n# %d, %d = %s\n", i++, ok, c );
		sleep(4);
	}
	return ok;
}
