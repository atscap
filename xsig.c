#define NAME "xsig"
#define AUTHOR "inkling"
#define EMAIL "inkling@nop.org"
#define WEBPAGE "http://www.nop.org/inkling/dtv"
#define COPYRIGHT "Copyright (C) 2004-2007"
#define LICENSE "GNU General Public License"
#define LASTEDIT "20071007"
#define VERSION "1.4"
#warning ignore any OpenGL in this file.
/*
 * xsig.c (c) Copyright 2004-2005 by inkling@nop.org
 *  V4L/V4L2/DVB API Signal Strength Display
 *
 * xsig is free software; you may only redistribute it and/or modify
 * it under the terms of the GNU General Public License, Version 2,
 * as published by the Free Software Foundation.
 *
 * xsig is distributed to you in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write the Free Software Foundation, Inc.,
 * at 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* Peter Knaggs Peter.Knaggs@gmail.com submitted code for DVB API 2005-Sep-7 */

/*
    WHAT IT DOES

        This program reads V4L, V4L2 or DVB API signal strength
    and displays it as a running graph of signal versus time.

	By default, it will try to draw, in X, a polar chart, where
    latitude is signal strength and longitude is time, clockwise.

	Optional display in text mode is available with -t, but it only
    works with console or xterm, not aterm because of scrolling region.

*******************************************************************************
************ NOTE: EDIT THE TABLE atsc_bcast[] BELOW FOR YOUR AREA ************
*******************************************************************************


*/

/*
    EXAMPLE USAGE:

	sig -i0 19

    Will set device /dev/dtv0 to UHF channel 19 and begin signal scan

	sig -a -i0 19
	
    Will set device /dev/dtv0 to UHF channel 19 and begin signal scan,
    with variable audio pitch feedback for signal strength.
*/

/*
    CHANGELOG

    sig		original experiment to get HD2000 LED status in software

	0.1	first version based on dtvsignal from JSK, driver 1.4
	0.2	use LEDs from GPIO on HD2000 with custom driver 1.4

    atscsig	pcHDTV ATSC drivers only

	0.3	use V4L2 with stock driver 1.6 and above
	0.4	chg switched to signal bar like atscap, scrapped LEDs
	0.5	add set scrolling region, xterm | console OK, aterm broken
	0.6	add audio indication of signal strength. bad sigs odd melodies
	0.7	add X histogram for signal strength. drab and plain.
	0.8	chg to polar plot power colors vs time circle, eye candy
	0.9	add openGL support for polar plot (work in progress)
	1.0	chg to using V4L2 or V4L1 interface for driver 2.0 or 1.6


    xsig	name is now xsig to reflect support for V4L / V4L2 / DVB API

	1.1	add DVB interface, -1 -2 -3 options set video API
	1.2	chg reduce DVB API ioctls, fix -t mode, msgs, add % to polar
	1.3	chg for backwards compatibility test on V4L1 with 2.4.26
	1.4	add DVB FE_TUNE_MODE_ONESHOT ioctl for i2c collision avoidance
*/
/* NOTES:

	Ignore all the Open GL code. I'll flesh it out someday.
*/

/* BUGS:
	died on 2.4.26 with V4L driver 1.6: floating point exception, but
	works ok with DVB API. I can't support that old V4L code anymore.
*/ 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include <inttypes.h>
#include <time.h>
#include <math.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/cursorfont.h>

#include <sys/soundcard.h>

#if USE_GL
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#ifdef USE_DVB
#include <linux/dvb/frontend.h>
#include <linux/dvb/dmx.h>
#endif

/* V4L is deprecated as of june 2006, do not use */
#ifdef USE_V4L
#warning obsolete V4L API via -1 -2 options
#include <linux/videodev.h>
#include <linux/videodev2.h>
#endif

#define WHO fprintf(stderr, "%s:\n", __FUNCTION__)


#define xprintf if (0!=arg_xmsg) fprintf

static int arg_once = 0;	// exit after first loop, give scans
static int arg_adsp = 0;	// audio enable
static int arg_athr = 0;	// audio threshold
static int arg_xdim = 0;	// X dimensions, height/width are same
static int arg_xmsg = 0;	// NZ show X debug messages
static int arg_otxt = 0;	// NZ text output only
static int arg_ox11 = ~0;	// NZ X output only
static int arg_idev = 0;	// dtv0-3
static int arg_loop = 1;	// 1 360 loop by default cuz dvb so slow
static int arg_modu = 3;	// loop modulus, get sig once every modulus
static int arg_time = 0;	// sleep time in nanos
static int arg_wipe = 0;	// wipe display after each loop

#ifdef USE_DVB
static int arg_vapi = 3;	// 1 = V4L1  2 = V4L2  3 = DVB
#endif

#ifdef USE_V4L
static int arg_vapi = 1;	// 1 = V4L1  2 = V4L2  3 = DVB
#endif

/******************************************************** X related globals */
static Display *mydisplay;
static Window mywindow;
static XWindowAttributes attribs;
static XEvent xev;
static XSetWindowAttributes xswa;
static int myscreen; /* X screen from init */
static int xinit = 0; /* X window initialized */
static int xbpp = 0; /* X bits per pixel depth */
static int xtbh = 0; /* X title bar height */
static int xmaxi = 0; /* NOTE: X window not maximized by default */
static int xlbt0 = 0; /* X left button time 0 */
static int xlbtd = 0; /* X left button time delta */
static int xrszlock = 0; /* don't interrupt x resize */
static unsigned int fg, bg; /* x foreground and background colors */
static GC mygc;
static XGCValues xgcv;
static XImage *myximage;
static unsigned char *ximagedata;
static XFontStruct * myxfonts;
static unsigned char xtitle[256];
static unsigned char xtext[256];
static XSizeHints hint;

unsigned int *xcolors;

unsigned int xcolors565[] = {
    (0x07 << 11) | (0x0F << 6) | 0x07, // lgrey
    (0x00 << 11) | (0x00 << 6) | 0x1F, // B
    (0x1F << 11) | (0x00 << 6) | 0x1F, // M
    (0x1F << 11) | (0x00 << 6) | 0x00, // R
    (0x1F << 11) | (0x3F << 6) | 0x00, // Y
    (0x00 << 11) | (0x3F << 6) | 0x00, // G
    (0x00 << 11) | (0x3F << 6) | 0x1F, // C
    (0x1F << 11) | (0x3F << 6) | 0x1F, // W
    (0x1F << 11) | (0x3F << 6) | 0x1F, // W
    (0x03 << 11) | (0x03 << 6) | 0x03, // dgrey
};

unsigned int xcolors888[] = {
    (0x40 << 16) | (0x40 << 8) | 0x40, // lgrey
    (0x00 << 16) | (0x00 << 8) | 0xFF, // B
    (0xFF << 16) | (0x00 << 8) | 0xFF, // M
    (0xFF << 16) | (0x00 << 8) | 0x00, // R
    (0xFF << 16) | (0xFF << 8) | 0x00, // Y
    (0x00 << 16) | (0xFF << 8) | 0x00, // G
    (0x00 << 16) | (0xFF << 8) | 0xFF, // C
    (0xFF << 16) | (0xFF << 8) | 0xFF, // W
    (0xFF << 16) | (0xFF << 8) | 0xFF, // W
    (0x10 << 16) | (0x10 << 8) | 0x10, // dgrey
};

struct vw_s {
    int x;
    int y;
    int width;
    int height;
};

static struct vw_s vw  = { 512, 0,  314, 314 };	// initpos, x changed so far
static struct vw_s vw0 = { 0, 0,    0,    0 };	// max/min toggle

double fstrength = 0.0;
double fdegrees  = 0.0;
double fxscale, fyscale;
int xorigin, yorigin, xend, yend;

double pi  = 3.1415926;
double pi2 = 6.2831852;
    
/********************************************************* v4l/tuner globals */


static int in_file;

static unsigned char in_name[256];
static unsigned char in_sname[8];

static int chan = 2;
struct chan_s {
    char *sid;
    int   freq;
};


static int strength = 0;		// signal strength
static unsigned long freq = 1;		// current frequency
static unsigned long pfreq = 0;	// previous frequency
static int sig1 = 0, sig2 = 0, siglock = 0, sigper = 0;
static int ledG = 0, ledR = 0;


unsigned int use_bar = ~0;


#ifdef USE_DVB
// dvb uses to tune to a channel (frequency)
struct dvb_frontend_parameters dvb_frontend_param;
#endif


#ifdef USE_V4L
static struct video_signal vsig;
static struct v4l2_tuner v2sig;

struct video_channel vch_ATSC = {
 0,
 "chan --",
 1,
 VIDEO_VC_TUNER,
 VIDEO_TYPE_TV,
 VIDEO_MODE_ATSC
};

// DELETEME: why is this here? leftover?
struct video_channel vch_NTSC = {
 0,
 "",
 1,
 VIDEO_VC_TUNER,
 VIDEO_TYPE_TV,
 VIDEO_MODE_NTSC
};
#endif

// count number of times sig scan actually done

// jostle the scheduler
struct timespec tune_sleep = { 0, 250000000 };  // quarter second for tuner
struct timespec sig_sleep  = { 0, 25000 };	// 25 microseconds
struct timespec loop_start = { 0, 0 };		// time spent in get sig
struct timespec loop_stop  = { 0, 0 };
struct timespec loop_diff  = { 0, 0 };

/* NOTE: you need to edit these for your area */
static struct chan_s atsc_bcast[] = {
    { " 0",     43250 },
    { " 1",     49250 },
    { " 2",	55250 },
    { " 3",	61250 },
    { " 4",	67250 },
    { " 5",	77250 },
    { " 6",	83250 },

    { " 7",	175250 },
    { " 8",	181250 },
    { " 9",	187250 },
    { "10",	193250 },
    { "11",	199250 },
    { "12",	205250 },
    { "13",	211250 },

    { "14",	471250 },
    { "15",	477250 },
    { "16",	483250 },
    { "17",	489250 },
    { "18",	495250 },
    { "19",	501250 },
    { "20",	507250 },
    { "21",	513250 },
    { "22",	519250 },
    { "23",	525250 },
    { "24",	531250 },
    { "25",	537250 },
    { "26",	543250 },
    { "27",	549250 },
    { "28",	555250 },
    { "29",	561250 },
    { "30",	567250 },
    { "31",	573250 },
    { "32",	579250 },
    { "33",	585250 },
    { "34",	591250 },
    { "35",	597250 },
    { "36",	603250 },
    { "37",	609250 },
    { "38",	615250 },
    { "39",	621250 },
    { "40",	627250 },
    { "41",	633250 },
    { "42",	639250 },
    { "43",	645250 },
    { "44",	651250 },
    { "45",	657250 },
    { "46",	663250 },
    { "47",	669250 },
    { "48",	675250 },
    { "49",	681250 },
    { "50",	687250 },
    { "51",	693250 },
    { "52",	699250 },
    { "53",	705250 },
    { "54",	711250 },
    { "55",	717250 },
    { "56",	723250 },
    { "57",	729250 },
    { "58",	735250 },
    { "59",	741250 },
    { "60",	747250 },
    { "61",	753250 },
    { "62",	759250 },

// not supposed to have any stations up here but let user try for themselves
    { "63",	765250 },
    { "64",	771250 },
    { "65",	777250 },
    { "66",	783250 },
    { "67",	789250 },
    { "68",	795250 },
    { "69",	801250 },
 
    { "70",	807250 },
    { "71",	813250 },
    { "72",	819250 },
    { "73",	825250 },
    { "74",	831250 },
    { "75",	837250 },
    { "76",	843250 },
    { "77",	849250 },
    { "78",	855250 },
    { "79",	861250 },
    { "80",	867250 },
    { "81",	873250 },
    { "82",	879250 },
    { "83",	885250 },
};

static int scan_list[82];
static int scan_count = 0;

static int strength_count = 0;
static int strength_sum = 0;
static int strength_avg = 0;


#define OSS_DEVICE "/dev/dsp0"
#define OSS_BUF_SIZE 4096
#define SAMPLE_RATE 44100
#define SAMPLE_CHANNELS 1
#define SAMPLE_BYTES 2
// 1/16th of a second
#define SAMPLE_SIZE ((SAMPLE_RATE * SAMPLE_CHANNELS) >> 4)
// 1 second
//#define SAMPLE_SIZE (SAMPLE_RATE * SAMPLE_CHANNELS)

static int audio_fd;
//unsigned char audio_buf[OSS_BUF_SIZE];
static signed short audio_sample[ SAMPLE_SIZE ];
static int sample_index = 0;

#define CHROMATIC_SCALE 12
static int chromatic_scale[] =
{
// n * 2 to the power of 1/12, 101 entries for 0-100% (practical 22-84?)
//  A A#/Bb     B     C C#/Db     D D#/Eb     E     F F#/Gb     G G#/Ab
   55,   58,   62,   65,   69,   73,   78,   82,   87,   92,   98,  104,
  110,  117,  123,  131,  139,  147,  156,  165,  175,  185,  196,  208,
  220,  233,  247,  262,  277,  294,  311,  330,  349,  370,  392,  415,
  440,  466,  494,  523,  554,  587,  622,  659,  698,  740,  784,  831,
  880,  932,  988, 1047, 1109, 1175, 1245, 1319, 1397, 1480, 1568, 1661,
 1760, 1865, 1976, 2093, 2217, 2349, 2489, 2637, 2794, 2960, 3136, 3322,
 3520, 3729, 3951, 4186, 4435, 4699, 4978, 5274, 5588, 5920, 6272, 6645,
 7040, 7459, 7902, 8372, 8870, 9397, 9956,10548,11175,11840,12544,13290,
14080,14917,15804,16744,17740
};

// based on above
static int diatonic_scale[] =
{
//    A   A#   B    C   C#   D   D#   E    F  F#    G   G#
//   -1,  0,  -1,  -1,  0,  -1,  0,  -1,  -1,  0,  -1,  0
      0,       2,   3,       5,       7,   8,       10,
     12,      14,  15,      17,      19,  20,       22, 
     24,      26,  27,      29,      31,  32,       34, 
};

/************************************************************** BEGIN GLOBALS */

int fullscreen = 0;


#undef USE_GL
#ifdef USE_GL
struct mouse_s {
    int called;
    int button;
    int updown;
    int x;
    int y;
};
static struct mouse_s mouse;

static struct {
    int w;
    int h;
    int x;
    int y;
} origin;
static struct timespec gl_sleep = { 0, 5000000 }; // 5 mS

// define a point in space as x,y,z and set the color value for it
struct point3f_s {
    GLfloat x;
    GLfloat y;
    GLfloat z;
    GLfloat r;
    GLfloat g;
    GLfloat b;
};
//static struct point3f_s point;

GLfloat spin = 0.0;
GLuint theGrid;

/*************************************************************** PROTOTYPES */
void glutCloseFunc( void * );
void glutLeaveMainLoop( void );
#endif

static void c_exit( int ev );

/******************************************************************* TIMERS */
// put timespec nanosecond time difference from z - y into x
static
void
time_diff( struct timespec *x, struct timespec *y, struct timespec *z)
{
    time_t sec;
    long nsec;

    sec = z->tv_sec;
    nsec = z->tv_nsec;

    if (y->tv_nsec > nsec) { // borrow implicit second if/when clock wraps
	nsec += 1000000000;
	sec--;
    }
    x->tv_nsec = nsec - y->tv_nsec;
    x->tv_sec = sec - y->tv_sec;
}


#ifdef USE_GL
/******************************************************** GL/glut FUNCTIONS */

static
void
GLGrid( void )
{
    theGrid = glGenLists( 1 );
    glNewList( theGrid, GL_COMPILE );

    glColor3f ( 0.0, 1.0, 0.0 );
    glPushMatrix();
    glBegin( GL_LINES );
	glVertex3f( -1.0, -1.0, 0.0 );
	glVertex3f( 1.0, 1.0, 0.0 );
    glEnd();
    glPopMatrix();

    glColor3f ( 1.0, 0.0, 0.0 );
    glPushMatrix();
    glBegin( GL_LINES );
	glVertex3f( 1.0, -1.0, 0.0 );
	glVertex3f( -1.0, 1.0, 0.0 );
    glEnd();
    glPopMatrix();

    glColor3f ( 1.0, 1.0, 0.0 );
    glPushMatrix();
    glBegin( GL_LINES );
	glVertex3f( 0.0, 1.0, 0.0 );
	glVertex3f( 0.0, -1.0, 0.0 );
    glEnd();
    glPopMatrix();

    glColor3f ( 1.0, 0.0, 1.0 );
    glPushMatrix();
    glBegin( GL_LINES );
	glVertex3f( 1.0, 0.0, 0.0 );
	glVertex3f( -1.0, 0.0, 0.0 );
    glEnd();
    glPopMatrix();

    glEndList();
    
}

/* initialize rgba mode with antialias, alpha blend, hint, and line width */
static
void
GLInit( void )
{
// if you need to know granularity and max line width
//    GLfloat v[2];
//    glGetFloatv( GL_LINE_WIDTH_GRANULARITY, v);
//    glGetFloatv( GL_LINE_WIDTH_RANGE, v);

    GLGrid();

    glEnable( GL_LINE_SMOOTH ); // anti-alias

    glEnable( GL_BLEND ); // alpha blend
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); // alpha blend
    
    glHint( GL_LINE_SMOOTH_HINT, GL_DONT_CARE ); // hint is dont care?

    glLineWidth( 1.0 );	// line width
    glClearColor ( 0.0, 0.0, 0.0, 0.0 ); // background pixel color
}

/*///////////////////////////////////////////////////////////////////////////*/

static
void
GLDisplay( void )
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    glPushMatrix();  // viewpoint matrix push
    glTranslatef ( 0.0, 0.0, -5.0 ); // viewpoint translate

    glRotatef( spin, 1.0, 1.0, 1.0 );

    glCallList( theGrid );
    
    glPopMatrix(); // viewpoint pop
//    glFlush();
    glutSwapBuffers();
//    nanosleep( &gl_sleep, NULL );
}


static
void
GLReshape(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
    gluPerspective( 20.0, (GLfloat)w / (GLfloat)h, 1.0, 20.0);
    glMatrixMode( GL_MODELVIEW );
}


static
void
GLLeftMouseButton( void )
{
    fprintf( stdout, "Left Mouse Down\n");
}

static
void
GLMiddleMouseButton( void )
{
    fullscreen = ~fullscreen;
    fprintf( stdout, "Middle Mouse Down -- ");
    fprintf( stdout, "Fullscreen %s\n", (0==fullscreen)?"NO":"YES");
    if (0 != fullscreen) {
	glutFullScreen();
    } else {
	GLReshape( origin.w, origin.h );
    }
}

static
void
GLRightMouseButton( void )
{
    fprintf( stdout, "Right Mouse Down\n");
}

static
void
GLMouse( int b, int u, int x, int y)
{
    GLint w;
    w = glutGetWindow();
    mouse.called = 1;
    mouse.button = b;
    mouse.updown = u;
    mouse.x = x;
    mouse.y = y;
    fprintf( stdout, "Mouse: w %d b %d u %d x %d y %d\n", w, b, u, x, y  );
    if ( (0 == b) && (0 == u) ) GLLeftMouseButton();
    if ( (1 == b) && (0 == u) ) GLMiddleMouseButton();
    if ( (2 == b) && (0 == u) ) GLRightMouseButton();
}


static
void
GLKeyboard( unsigned char c, int i, int j )
{
    fprintf( stdout, "Keyboard: c '%c' i %d j %d\n", c, i, j );

    if ( 'q' == c )
	glutLeaveMainLoop();

    if ( 'f' == c ) { 
	fullscreen = ~fullscreen;
	if (0 != fullscreen) {
	    glutFullScreen();
	} else {
	    GLReshape( origin.w, origin.h );
	}
    }
	
}


static
void
GLClose( void )
{
    GLint w;
    w = glutGetWindow();
    fprintf( stdout, "Close: w %d\n", w);
    glutLeaveMainLoop();
}


// this function gets called when not doing anything.
// could make it change the location of the line
static
void
GLIdle( void )
{
    // change parameters for the line
    spin += 1.0;
    // ...
    /// and display then sleep
    GLDisplay(); // display needed here? think so
//    nanosleep( &gl_sleep, NULL );        
}


/************************************************************ END FUNCTIONS */


/****************************************************************** GL MAIN */
int
gl_main(int argc, char** argv)
{
    int glut_window;
    
    origin.w = origin.h = 300;
    origin.x = origin.y = 0;
    
    glutInitWindowSize( origin.w, origin.h );
    glutInitWindowPosition( origin.x, origin.y );

    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInit( &argc, argv );

    glut_window = glutCreateWindow( "Polar Plot: Strength vs. Time");
    GLInit();
    
    // hook in various glut event callbacks
    glutReshapeFunc( GLReshape );
    glutKeyboardFunc( GLKeyboard );
    glutMouseFunc( GLMouse );
    glutCloseFunc( GLClose );
    glutIdleFunc( GLIdle );
    glutDisplayFunc( GLDisplay );

    glutMainLoop( ); // waits until closed or [q]uit key hit

    return 0;
}

#endif /* USE_GL */


/********************************************************************* AUDIO */
static
void
open_audio( void )
{
    WHO;
    audio_fd = open( OSS_DEVICE, O_WRONLY, 0 );
    if (audio_fd == -1) {
	perror( "OPEN ERROR " OSS_DEVICE );
	exit(1);
    }
    fprintf(stderr, "Audio open\n");
}

static
void
close_audio( void )
{
    int ok;
    WHO;
    ok = close(audio_fd);
    if (ok == -1) {
	perror( "CLOSE ERROR " OSS_DEVICE );
	exit(1);
    }    
    fprintf(stderr, "Audio closed\n");
}

// build a tone in audio sample buffer
// if clear is 0, values from new tone added to old tone
static
void
build_frequency( int afreq, int clear )
{
    double pi1 = 3.1415926;
    double sample_scale;
    double samples_cycle;
    int i, j;

    if (0 != clear)
	memset( audio_sample, 0, sizeof(audio_sample) );

    samples_cycle = SAMPLE_RATE / afreq;
    sample_scale = (2 * pi1) / samples_cycle;
//    fprintf( stdout, "Samples per cycle %f scale %f\n", samples_cycle, sample_scale);

// i bumped below
    for (i = 0; i < (sizeof(audio_sample)>>1); ) {
	for (j = 0; j < samples_cycle; j++) {
	    sample_index = i + j; // current sample
	    // want -32768 to +32767 and 0
	    if ( sample_index < (sizeof(audio_sample)>>1) )
	    {
		int k;
		k = 0x8001;
		// new values
		if (0 != clear) {
		    k = 32767 * sin( j * sample_scale );
		} else {
		    k = audio_sample[ sample_index ];
		    // try additive for now but probably wrong
//		    k += (32767 * sin( j * sample_scale ));
		}		    

		// clamp
		if (k < -32767) k = -32767;
		// clamp
		if (k > 32767) k = 32767;

		audio_sample[ sample_index ] = k;

//	    	fprintf( stdout, "sample %d value %d\n", i+j, audio_sample[i+j] );
	    }
	}
	i+= j;
    }
}


// use normal table index to build semitone n (0-11 for first octave)
static
void
build_semitone( int n )
{
    build_frequency( chromatic_scale[ n ], 1 );
}

// use diatonic table index to build full tone n (0-7 for first octave)
static
void
build_tone( int n )
{
    build_frequency( chromatic_scale[ diatonic_scale [ n ] ], 1);
}


// set audio device parameters: Signed 16 bit Any Endian at 44100 Hz monaural
// device should be open prior to call
static
void
setup_audio( void )
{
    int format, channels, rate, ir;
    format = AFMT_S16_NE;
    channels = SAMPLE_CHANNELS;
    rate = SAMPLE_RATE;

    WHO;
    ir = ioctl(audio_fd, SNDCTL_DSP_SETFMT, &format);
    if (ir == -1) {
	perror( "SNDCTL_DSP_SETFMT ERROR " OSS_DEVICE );
	exit(1);
    }
    fprintf( stderr, "%s SNDCTL_DSP_SETFMT\n", OSS_DEVICE);

    if (AFMT_S16_LE != format) {
	fprintf( stderr, "SNDCTL_DSP_SETFMT NOT S16LE %s\n", OSS_DEVICE );
	exit(1);
    }
    fprintf( stderr, "%s SNDCTL_DSP_SETFMT AFMT_S16_LE\n", OSS_DEVICE);
    
    ir = ioctl(audio_fd, SNDCTL_DSP_CHANNELS, &channels);
    if (ir == -1) {
	perror( "SNDCTL_DSP_CHANNELS ERROR " OSS_DEVICE );
	exit(1);
    }
    fprintf( stderr, "%s SNDCTL_DSP_CHANNELS\n", OSS_DEVICE);

    if (1 != channels) {
	fprintf( stderr, "SNDCTL_DSP_CHANNELS NO MONO %s\n", OSS_DEVICE);
	exit(1);
    }
    fprintf( stderr, "%s SNDCTL_DSP_CHANNELS has monaural\n", OSS_DEVICE);

    ir = ioctl(audio_fd, SNDCTL_DSP_SPEED, &rate);
    if (ir == -1) {
	perror( "SNDCTL_DSP_SPEED ERROR " OSS_DEVICE );
	exit(1);
    }
    fprintf( stderr, "%s SNDCTL_DSP_SPEED %d\n", OSS_DEVICE, rate);

// ignore the rate being wrong, unpatched es1371 drive reports 44101
//    if (44100 != rate) {
//	fprintf( stderr, "SNDCTL_DSP_SPEED %d\n", rate);
//    }

    build_tone( 12 ); // should be 110Hz A, 0-11 is inaudible for some
}

// send tone to device
static
void
play_audio( int duration ) {
    int len;
    int samp_len;

#if 0
    samp_len = (sample_index * duration)/100;
    if (samp_len < 0) samp_len = 0;
    if (samp_len > sample_index) samp_len = sample_index;
//    samp_len = sample_index - samp_len;
    
//    fprintf( stderr, "sample index %d duration %d samplen %d\n",
//		sample_index, duration, samp_len);
#endif
    samp_len = sample_index;

    // most likely will fail and have to check len for how much written
    if (samp_len != 0) {
	len = write( audio_fd, audio_sample, samp_len );
	if (len == -1) {
	    perror( "AUDIO WRITE " OSS_DEVICE);
	    exit(1);
	}
    }
//    fprintf( stderr, "Wrote %d bytes\n", len);
}



static void init_audio( void ) {
    open_audio();
    setup_audio();
}


/**************************************************************** X functions */

/* compute vertical x offset from fontsize and text line (row) number */
/* myxfonts is what font was loaded */
/* FIXME: doesn't check for offscreen */
static
int
x_fontline( int row )
{
	int ytot;

//	if (0 == xinit) return;

	/* total font height = cell height (ascender) + descender + blank */
	ytot = myxfonts->ascent + myxfonts->descent + 1; /* 1 or 2 blanks? */

	/* however, font baseline does not count descender (leave a blank?) */
	/* should already have ytot correct with ascent+descent+blank */
	return (row * ytot) + myxfonts->ascent + 1;
//	return (row * ytot);
}

static
void
x_title( void )
{
	if (0 == xinit) return;

	/* tell window manager about this window: title, icon, hw, xy */
	/* XSetWMProperties is more hassle than is needed here */
	XSetStandardProperties(	mydisplay,
				mywindow,
				xtitle,
				xtitle,
				None,
				NULL,
				0,
				&hint
			    );
}


static
void
x_draw_filled_rectangle( int c, int x, int y, unsigned int w, unsigned int h)
{
	if (0 == xinit) return;

	XSetForeground( mydisplay, mygc, c );
//	XDrawRectangle( mydisplay, mywindow, mygc, x,y,w,h); // not needed
	XFillRectangle( mydisplay, mywindow, mygc, x,y,w,h);
}

static
void
x_display_grid( void )
{
	int c, j, x, y, h, w;
	int xoffset = 0;
	int yoffset = 0;

	if (0 == xinit) return;

	x = vw.width/2;
	y = vw.height/2;
	h = vw.width;
	w = vw.height;

#if 0
// crosshairs then circles
	XSetForeground( mydisplay, mygc, 0xFFFF);
	XDrawLine( mydisplay, mywindow, mygc, 0, 0, vw.width-1,vw.height-1);
	XDrawLine( mydisplay, mywindow, mygc, 0, vw.height-1,vw.width-1,0);
	XDrawLine( mydisplay, mywindow, mygc, vw.width/2, 0, vw.width/2, vw.height-1);
	XDrawLine( mydisplay, mywindow, mygc, 0, vw.height/2, vw.width-1, vw.height/2);
	XFlush( mydisplay );
#endif

#define STEP 12
	for (j=0; j < 100;) {
	    c = xcolors[j / STEP];
	    XSetForeground( mydisplay, mygc, c );

	    XDrawArc(   mydisplay, mywindow, mygc,
		    x - ((j/100.0)*(w / 2)), y - ((j/100.0)*(h / 2)),
		    (h * j) / 100.0, (w *j) / 100.0, 0, 360 * 64
		);
	    j += STEP;
	}
	XFlush( mydisplay );

	// show signal percentage too

	memset( xtext, 0 , sizeof(xtext) );
	snprintf( xtext, sizeof(xtext)-1, "%3d%%", strength ); 
	xoffset = vw.width - 46;
	yoffset = x_fontline(1);
		
	/* XDrawString draws fg only, XDrawImageString draws fg + bg */
	XDrawImageString(   mydisplay,
			    mywindow,
			    mygc,
			    xoffset,
			    yoffset,
			    xtext,
			    strlen(xtext)
			);


	XFlush( mydisplay );


}


// shows which channel is being charted
static
void
x_display_channel( void )
{
	int xoffset = 0;
	int yoffset = 0;
	unsigned char *sid;

	if (0 == xinit)	return;

	sid = atsc_bcast[chan].sid;

	/* create string, e.g. 19 KTXH UPN */
	memset( xtext, 0, sizeof(xtext));
	snprintf( xtext, sizeof(xtext)-1, "%-11s", sid );


	/* top left corner, second font line */
	xoffset = 0;
	yoffset = x_fontline(0);

	XSetBackground( mydisplay, mygc, bg );
	fg = xcolors[7];
	XSetForeground( mydisplay, mygc, fg );

	/* XDrawString draws fg only, XDrawImageString draws fg + bg */
	XDrawImageString(   mydisplay,
			    mywindow,
			    mygc,
			    xoffset,
			    yoffset,
			    xtext,
			    strlen(xtext)
			);

	XFlush( mydisplay );

	memset( xtext, 0 , sizeof(xtext) );
	snprintf( xtext, sizeof(xtext)-1, "%s", in_sname ); 
//	xoffset = 0;
//	yoffset = x_fontline(1);
	xoffset = vw.width - 46;
	yoffset = x_fontline(0);
		
	/* XDrawString draws fg only, XDrawImageString draws fg + bg */
	XDrawImageString(   mydisplay,
			    mywindow,
			    mygc,
			    xoffset,
			    yoffset,
			    xtext,
			    strlen(xtext)
			);


	XFlush( mydisplay );



	// the rest belongs in display grid, except it flickers there
	memset( xtext, 0 , sizeof(xtext));
	snprintf( xtext, sizeof(xtext)-1, "Bad"); 
	xoffset = 0;
	yoffset = vw.height - 2;

	/* XDrawString draws fg only, XDrawImageString draws fg + bg */
	XSetForeground( mydisplay, mygc, fg );
	XDrawImageString(   mydisplay,
			    mywindow,
			    mygc,
			    xoffset,
			    yoffset,
			    xtext,
			    strlen(xtext)
			);
	// draw rectangles for bad color
	yoffset -= x_fontline(0)+9;
//	xoffset += 44;
	x_draw_filled_rectangle( xcolors[0], xoffset, yoffset, 10, 10 );
	xoffset += 11;
	x_draw_filled_rectangle( xcolors[1], xoffset, yoffset, 10, 10 );
	xoffset += 11;
	x_draw_filled_rectangle( xcolors[2], xoffset, yoffset, 10, 10 );
	xoffset += 11;
	x_draw_filled_rectangle( xcolors[3], xoffset, yoffset, 10, 10 );
	XFlush( mydisplay );

	memset( xtext, 0 , sizeof(xtext));
	snprintf( xtext, sizeof(xtext)-1, "Good"); 
	xoffset = vw.width - 46;
	yoffset = vw.height - 2;
		
	/* XDrawString draws fg only, XDrawImageString draws fg + bg */
	XSetForeground( mydisplay, mygc, fg );
	XDrawImageString(   mydisplay,
			    mywindow,
			    mygc,
			    xoffset,
			    yoffset,
			    xtext,
			    strlen(xtext)
			);

	// draw rectangles for good color
	yoffset -= x_fontline(0)+9;
//	xoffset -= 55;
	x_draw_filled_rectangle( xcolors[4], xoffset, yoffset, 10, 10 );
	xoffset += 11;
	x_draw_filled_rectangle( xcolors[5], xoffset, yoffset, 10, 10 );
	xoffset += 11;
	x_draw_filled_rectangle( xcolors[6], xoffset, yoffset, 10, 10 );
	xoffset += 11;
	x_draw_filled_rectangle( xcolors[7], xoffset, yoffset, 10, 10 );

	XFlush( mydisplay );
}


static
void
x_clear_display( void )
{
	if (0 == xinit) return;

        XClearWindow( mydisplay, mywindow );
	XFlush( mydisplay );
}


static
void
x_redraw_display( int clear )
{
	if (0 == xinit) return;

	if (0 != clear) x_clear_display();
	x_display_channel(); // new channel gets new title
	x_display_grid();
}


/***********************************************************************
 *  start of main block of X window support functions
 *  FIXME: all of these functions need some kind of error handling
 ***********************************************************************/

static void
x_resize_display( void )
{
//	int aspect = 0;
	XSetWindowAttributes nxswa;
	unsigned long nxswamask;
	Cursor fscur;
	XColor fgnd;
	XColor bgnd;

	if (0 == xinit) return;

	nxswamask = CWOverrideRedirect;
	
	if (xrszlock != 0) return; /* do nothing if in middle of resize */

	xrszlock = 1; /* lock against click happy */

	if (0 != xmaxi)
	{
		/* unmap the window so we can change attributes */
		/* loses focus? unmap sent back to previous window? */
		XUnmapWindow( mydisplay, mywindow );
		/* wait and eat notify events until we get UnmapNotify */
		do
		{
		} while ( False == XCheckTypedWindowEvent(  mydisplay,
							    mywindow,
							    UnmapNotify,
							    &xev
							  )
			 );

		/* turn off window manager dressing */
		nxswa.override_redirect = True;
		XChangeWindowAttributes( mydisplay,
					mywindow,
					nxswamask,
					&nxswa
					);

		/* fullscreen cursor has fg/bg same color (colorkey) */
		fscur = XCreateFontCursor( mydisplay, XC_crosshair );

		/* now color and define the fullscreen cursor */
		/* fixme, needs depth check and r<< g<< b<< appropriate */
		fgnd.pixel = 1;
		fgnd.red = 65535;
		fgnd.blue = 65535;
		fgnd.green = 65535;
		fgnd.flags = DoRed | DoGreen | DoBlue;

		bgnd.pixel = 0;
		bgnd.red = bgnd.blue = bgnd.green = 0;
		bgnd.flags = DoRed | DoGreen | DoBlue; 

		XRecolorCursor( mydisplay, fscur, &bgnd, &fgnd);
		XDefineCursor( mydisplay, mywindow, fscur );

		/* map the window back onscreen with new attributes */
		XMapWindow( mydisplay, mywindow );

		/* wait and eat notify events until we get MapNotify */
		do
		{
		} while ( False == XCheckTypedWindowEvent(  mydisplay,
							    mywindow,
							    MapNotify,
							    &xev
							  )
			);

		/* set focus to the fullscreen window so xinput works.
		   NOTE: doesn't seem to work if it's in x_init only */
		XSetInputFocus(	mydisplay,
				mywindow,
				RevertToNone,
				CurrentTime
			    );

		/* maximize the window */
		XMoveResizeWindow(	mydisplay,
					mywindow,
					(attribs.width-attribs.height)/2,
					attribs.y,
					attribs.height,
					attribs.height
				);

		/* wait for ConfigureNotify event
		   have to do this because otherwise x_event() will see
		   the resize and try to change the BES aspect ratio */
		do {
			// nano sleep?
		} while ( False == XCheckTypedWindowEvent(  mydisplay,
							    mywindow,
							    ConfigureNotify,
							    &xev
							  )
			);

		/* save current overlay settings for later restore,
		   update current overlay w/ root window attribs, full-screen
		   Hey Joe!
		*/
		vw0.x = vw.x;
		vw0.y = vw.y;
		vw0.width = vw.width;
		vw0.height = vw.height;

		vw.x = attribs.x;
		vw.y = attribs.y;
		vw.width = attribs.height; /* root window size */
		vw.height = attribs.height; /* root window size */

		/* full-screen integer aspect ratio adjustment */
//		aspect = (vw0.width<<10)/vw0.height;
//		aspect = 1<<10;
//		vw.height = (vw.width<<10)/aspect;
//		vw.y = ((attribs.height-vw.height)>>1);
		vw.x = (attribs.width - attribs.height)/2;

		xprintf(stderr,"\tMaximized window x%d y%d w%d h%d\n",
			attribs.x, attribs.y, attribs.width, attribs.height);
		xprintf(stderr,"\tOverlay window x%d y%d w%d h%d\n",
			vw.x, vw.y, vw.width, vw.height);

		/* no send_event on XMoveResize, so update BES */
//		set_overlay();

	} else {
		/* restore overlay and window to previous saved.
		   have to adjust for titlebar height again too */
		vw.x = vw0.x;
		vw.y = vw0.y;
		vw.width = vw0.width;
		vw.height = vw0.height;
		/* unmap the window so we can change its attributes */
		XUnmapWindow( mydisplay, mywindow);

		/* wait and eat notify events until we get UnmapNotify */
		do
		{
			// nano sleep?
		} while ( False == XCheckTypedWindowEvent(  mydisplay,
							    mywindow,
							    UnmapNotify,
							    &xev
							  )
			);

		/* turn on window manager dressing */
		nxswa.override_redirect = False;
		XChangeWindowAttributes(    mydisplay,
					    mywindow,
					    nxswamask,
					    &nxswa
					);

		/* put cursor back to normal for windowed */
		XUndefineCursor( mydisplay, mywindow);

		/* map the window back onscreen with restored attributes */
		XMapWindow( mydisplay, mywindow );

		/* wait and eat notify events until MapNotify */
		do
		{
			// nano sleep?
		} while ( False == XCheckTypedWindowEvent(   mydisplay,
							    mywindow,
							    MapNotify,
							    &xev
							)
			);

		/* restore window size to previously saved BES size,
		   minus the titlebar height */
		XMoveResizeWindow(	mydisplay,
					mywindow,
					vw.x,
					vw.y-xtbh,
 					vw.width,
					vw.height
					);
//#if 0
		/* wait for and eat the ConfigureNotify event.
		   have to do this because otherwise x_check_events() will see
		   the resize and try to change the aspect ratio */
		do
		{
			// nano sleep?
		} while ( False == XCheckTypedWindowEvent(  mydisplay,
							    mywindow,
							    ConfigureNotify,
							    &xev
							  )
			);

//#endif
		/* no send_event on XMoveResize, so update BES manually */
//		set_overlay();


		xprintf(stderr,"\tRestored x%d y%d w%d h%d\n",
			vw.x, vw.y, vw.width, vw.height);
	}
	x_redraw_display(1);

	xrszlock = 0; /* unlock this */
}

/* connect to server, create and map window */
static void
x_init_display( void )
{
	char *dispname = ":0"; /* need to make this option */
	XVisualInfo vinfo;

	Colormap xcmap;
	unsigned long xswamask;

	snprintf( xtitle, sizeof(xtitle)-1,
		"Strength vs Time Polar Plot %s", in_sname);

	/* if the window already exists, return immediately */
	if (0 != xinit) {
		fprintf(stderr, "X display already open!\n");
		return;
	}

	/* environment variable DISPLAY will override above definition */
	if ( NULL != getenv("DISPLAY")) dispname = getenv("DISPLAY");

	/* open the x display */
	mydisplay = XOpenDisplay( dispname );

	if (mydisplay == NULL) {
		perror("opening X display");
		exit(1);
	}

	/* get default screen number from XOpenDisplay */
	myscreen = DefaultScreen( mydisplay );

	/* setup x window hints for initial size and position */

	hint.x = vw.x;
	hint.y = vw.y;
	// override hints based on -i value, 0-3, to make window
	// start at different x value for each card
	hint.x = (16+vw.width) * (1+arg_idev); // scoot it out a bit

	hint.width = vw.width;
	hint.height = vw.height;

	hint.flags = PPosition | PSize;

	/* set foreground and background colors */
	bg = BlackPixel( mydisplay, myscreen );
	fg = WhitePixel( mydisplay, myscreen );

	XGetWindowAttributes(	mydisplay,
				DefaultRootWindow( mydisplay ),
				&attribs
			    );

	xbpp = attribs.depth;
	xprintf( stderr, "bpp %d\n", xbpp );
	xcolors = xcolors565;
	if (24 == xbpp) xcolors = xcolors888;
	if (32 == xbpp) xcolors = xcolors888;

	/* find a matching visual for the display */
	XMatchVisualInfo( mydisplay,
			  myscreen,
			  xbpp,
			  TrueColor,
			  &vinfo
			);

	xprintf( stderr, "visual %lx\n",vinfo.visualid );

	/* create the color map for the window */
	xcmap = XCreateColormap( mydisplay,
				 RootWindow( mydisplay, myscreen ),
				 vinfo.visual,
				 AllocNone
			      );

	xswa.background_pixel = BlackPixel( mydisplay, myscreen );
//	xswa.foreground_pixel = WhitePixel( mydisplay, myscreen );

	xprintf(stderr, "X background %06lX\n", xswa.background_pixel );

	xswa.border_pixel     = WhitePixel( mydisplay, myscreen );
	xswa.colormap         = xcmap;

	/* create window with background pixel, border pixel and colormap */
	xswamask = CWBackPixel
		 | CWBorderPixel
		 | CWColormap;

	/* create the window on the display */
	mywindow = XCreateWindow( mydisplay,
				  RootWindow( mydisplay, myscreen ),
				  hint.x,
				  hint.y,
				  hint.width,
				  hint.height,
				  0,
				  xbpp,
				  CopyFromParent,
				  vinfo.visual,
				  xswamask,
				  &xswa
				);

/* set the type of events we will allow in this window
		ConfigureNotify		move/resize
		UnmapNotify		overlay off
		MapNotify		overlay on
		KeyPress		playback mode key
		ButtonPress1		double click to maximize/restore
	*****	ButtonPress2		mouse button for cfg dialog (not done)

	   NOTE: the minimal closer you get isn't event-able, but does close
			if you hit it hard enough, use q to quit smoothly
*/
	XSelectInput(	mydisplay,
			mywindow,
			StructureNotifyMask
			| KeyPressMask
			| ButtonPressMask
		    );

	/* put the window on the display */
	XMapWindow(mydisplay, mywindow);

//	x_title();
	
	/* wait for map event to indicate the map finished. you can't use
	   it until then. it will happen after 2 configure events.
	   (configure size and configure position)
	 */
	do
	{
		/* wait for window structure events in overlay window */
		XWindowEvent(	mydisplay,
				mywindow,
				StructureNotifyMask,
				&xev);

		/* looking for configure notify events to set size/position */
		if (xev.type == ConfigureNotify)
		{
/* window manager may change the actual dimension/position from our hints,
   so get where wm actually put window. that's why they're called 'hints' */

			xprintf(stderr,"XEvent:\tConfigureNotify\n");

			/* send_event indicates window moved, so udpate x,y */
			if (xev.xconfigure.send_event)
			{
				/* FIXME: this is asking for trouble */
				/* get the titlebar height */
				xtbh = xev.xconfigure.y - 1;

				vw.x = xev.xconfigure.x;
				vw.y = xev.xconfigure.y;
				vw.width = xev.xconfigure.width;
				vw.height = xev.xconfigure.height;

				xprintf(stderr,
				    "\tSend Event x%d y%d w%d h%d th%d\n",
				    vw.x, vw.y, vw.width, vw.height, xtbh);

			} else {
			/* otherwise it's a window resize, so update w,h */
				vw.width = xev.xconfigure.width;
				vw.height = xev.xconfigure.height;

				xprintf(stderr,"\tUser w%d h%d\n",
				vw.width, vw.height);
			}
		}
	/* keep checking events until MapNotify event says window is mapped */
	}  while (xev.type != MapNotify );

	xprintf(stderr,"XEvent:\tMapNotify\n");

	/* flush the output buffer and wait until all requests processed.
	   this is probably not needed. see man XSync for reason why */
	XSync(mydisplay, True); /* discard rest of events in output queue */


	/* GC needed for any kind of text/gfx overlay, like play status etc */

	/* create a graphic context */
	mygc = XCreateGC(mydisplay, mywindow, 0L, &xgcv);

	XSetBackground(mydisplay, mygc, bg);
	XSetForeground(mydisplay, mygc, fg);

	/* this is a pretty sharp and legible font */
	myxfonts = XLoadQueryFont(mydisplay, "10x20");
	if ( myxfonts == (XFontStruct*)NULL ){
		fprintf(stderr,"can't load 10x20 font trying fixed\n");
		myxfonts = XLoadQueryFont(mydisplay,"fixed");
		if ( myxfonts == (XFontStruct*)NULL) {
			fprintf(stderr,"can't load fixed font\n");
		}
	} else {
		XSetFont(mydisplay, mygc, myxfonts->fid);
		xprintf(stderr,"X font loaded\n");
	}
	
/* don't need this for anything yet */
#ifdef NEVER
	/* get pointer to graphic context so we can get address of
	   pixmap data area (possible uses: some type of pixmap overlay) */
	myximage = XGetImage(	mydisplay,
				mywindow,
				0,
				0,
				vw.width,
				vw.height,
				AllPlanes,
				ZPixmap
			    );
	ximagedata = myximage->data;
#endif

	xprintf(stderr,"X created GC %p @ %p\n",
		 myximage, ximagedata);

	/* show the results of what the overlay and X window were set to */
	xprintf(stderr,"X display init: %0dx%0d @ %0d,%0d\n",
		vw.width, vw.height, vw.x, vw.y);

	/* indicate the window is already initialized */
	xinit = 1;

	x_title();

	/* set focus so xinput works when in fullscreen mode */
	XSetInputFocus(	mydisplay,
			mywindow,
			RevertToNone,
			CurrentTime
		      );

	if (xmaxi) {
		fprintf(stderr,"going full screen\n");
		x_resize_display();
	}
}

/* destroy the window, close the display and clear the init flag */
static void
x_close_display( void )
{
	XDestroyWindow(mydisplay, mywindow);
	XCloseDisplay(mydisplay);
	xinit = 0;
}



/* check for the following events:
	resize/move = update BES
	iconify/restore = disable/enable BES (and pause)
	double click = toggles maximize on/off
	keypress = set playback mode
*/
static void
x_check_events( void )
{
	/* look for UnmapNotify for minimized/disable overlay */
	if ( XCheckTypedWindowEvent(	mydisplay,
					mywindow,
					UnmapNotify,
					&xev) )
	{

		xprintf(stderr,"\nXEvent:\tUnmapNotifyEvent  BES OFF");
	}

	/* look for ConfigureNotify for resize or move */
	if ( XCheckTypedWindowEvent(	mydisplay,
					mywindow,
					ConfigureNotify,
					&xev) )
	{

		xprintf(stderr,"\nXEvent:\tConfigureNotify\n");

		/* ConfigureNotify event indicates size/position change */
		if (xev.type == ConfigureNotify) {
			/* send_event indicates window manager move/resize,
			   so udpate w,h,x,y with new location */
			if (xev.xconfigure.send_event) {
				vw.x = xev.xconfigure.x;
				vw.y = xev.xconfigure.y;
// not square
//				vw.width = xev.xconfigure.width;

				vw.width = xev.xconfigure.width;
				vw.height = xev.xconfigure.height;

				xprintf(stderr,"\tSend Event x%d y%d w%d h%d\n",
				vw.x, vw.y, vw.width, vw.height);
			} else {
			/* otherwise it's a user resize, so update w,h */
// not square
//				vw.width = xev.xconfigure.width;

				vw.width = xev.xconfigure.height;
				vw.height = xev.xconfigure.height;

				xprintf(stderr,"\tUser w%d h%d\n",
				vw.width, vw.height);
			}

			x_redraw_display(1);
		}
	}


/* it's ordered like this so the ConfigureNotify above puts overlay
   back in right location before enabling it. looks smoother.
*/
	/* look for MapNotify for maximized/enable overlay */
	if ( XCheckTypedWindowEvent(	mydisplay,
					mywindow,
					MapNotify,
					&xev) )
	{
		xprintf(stderr,"\nXEvent:\tMapNotify  BES ON\n");
	}

	/* look for double click events */
	if ( XCheckTypedWindowEvent(	mydisplay,
					mywindow,
					ButtonPress,
					&xev) )
	{
		/* FIXME: need to check for the left button only */
		if (xev.xbutton.button == 1) {

			xprintf(stderr,"\nXEvent:\tButtonPress\n");

			xlbtd = xev.xbutton.time - xlbt0;
			xlbt0 = xev.xbutton.time;
			if (abs(xlbtd) < 200) {
				xprintf(stderr,"\tclick delta %d\n", xlbtd);

				xmaxi = ~xmaxi;
				x_resize_display();
			}
		}
	}

	/* look for keypress events */
	if ( XCheckTypedWindowEvent(	mydisplay,
					mywindow,
					KeyPress,
					&xev)
				    )
	{
		/* trying a larger buffer for holding down single step */
		int key_buffer_size = 256;
		char key_buffer[key_buffer_size-1];
		XComposeStatus compose_status;
		KeySym key_sym;
		int keyval;

		/* try to convert key event into ASCII */
		XLookupString(	(XKeyEvent *)&xev,
				key_buffer,
				key_buffer_size,
				&key_sym,
				&compose_status
				);

		keyval = key_buffer[0];

		xprintf(stderr,"\nXEvent:\tKeyPress '%c' = 0x%X\n",
			keyval, keyval);

// NOTE: ADD MORE KEYS HERE
		if (keyval > 0) {
		    switch(keyval) {

		    case 'q':
		    case 27:
			c_exit(0);
			break;

		    default:
			break;

		    }
		}
	}
}

#ifdef USE_DVB
/************************************************************** DVB setup */
static
int
init_dvb_frontend (int fe_fd, struct dvb_frontend_parameters *frontend)
{
    struct dvb_frontend_info fe_info;

    if (ioctl(fe_fd, FE_GET_INFO, &fe_info) < 0) {
        fprintf( stderr, "FE GET INFO fail %s\n", strerror(errno));
	return -1;
    }

    if (fe_info.type != FE_ATSC) {
        fprintf( stderr, "FE not an ATSC (VSB/QAM) device\n" );
       return -1;
    }

    /* No inversion, whatever that is. */
    frontend->inversion = INVERSION_OFF;

    /* Make sure we have dvb_frontend_info.u.vsb.modulation correctly set, 
       to get a valid stream.
     */
    frontend->u.vsb.modulation = VSB_8;
    /* TODO_DVB: does this need to be configurable? */
    // not for cable, no VSB 16 planned for terrestrial AFAIK -ink
    return 0;
}

static
int
set_dvb_channel( int i )
{
    int ir = 0;
    unsigned int f = 0;

    freq = atsc_bcast[i].freq;

    if (freq == pfreq) return 0;
    pfreq = freq;

    /* See dtvsignal.c in:
     * http://www.pchdtv.com/downloads/dvb-atsc-tools-0.tar.gz
     * for where the "+ 1750" calculation comes from. 
     */
    f = ( freq + 1750 ) * 1000;  /* freq is in kHz. We need it in Hz for DVB API */

    dvb_frontend_param.frequency = f;
    ir = ioctl( in_file, FE_SET_FRONTEND, &dvb_frontend_param );
    if (ir != 0)
        fprintf( stderr, "FE SET FRONTEND fail %d %s\n", ir, strerror( errno ) );

    return ir;
}
#endif

#ifdef USE_V4L
static
int
set_v4l2_channel( int i )
{
    int ir;	    
    struct v4l2_frequency v2f;

    freq = ((atsc_bcast[i].freq/10)*16)/100;  /* divide by 62.5 */
    // inhibit multiple calls so tuner tank remains stabilized
    if (pfreq == freq) return 0;
    pfreq = freq;
    ir = 0;

//    fprintf( stdout, "channel = %d freq*16 = %ld  ",i ,freq);
    // v4l2 needs some slight init
    v2f.frequency = freq;
    v2f.type = V4L2_TUNER_ANALOG_TV;
    v2f.tuner = 0; // one tuner
    ir = ioctl( in_file, VIDIOC_S_FREQUENCY, &v2f );
    return ir;
}

static
int
set_v4l1_channel( int i )
{
    int ir;	    

    freq = ((atsc_bcast[i].freq/10)*16)/100;  /* divide by 62.5 */
    // inhibit multiple calls so tuner tank remains stabilized
    if (pfreq == freq) return 0;
    pfreq = freq;
    ir = 0;

//    fprintf( stdout, "channel = %d freq*16 = %ld  ",i ,freq);
    ir = ioctl( in_file, VIDIOCSFREQ, &freq );
    return ir;
}
#endif

static
int
set_channel( int i )
{
    int ir;

    ir = 0;

#ifdef USE_V4L
    if (1 == arg_vapi) ir = set_v4l1_channel( i );
    if (2 == arg_vapi) ir = set_v4l2_channel( i );
#endif

#ifdef USE_DVB
    if (3 == arg_vapi) ir = set_dvb_channel( i );
#endif

    nanosleep( &tune_sleep, NULL );
    return ir;
}

#ifdef USE_DVB
static
void
get_dvb_strength( void )
{
    int ir = 0;
    fe_status_t status;
    unsigned short sig;

    ledG = sig1 = sig2 = siglock = sigper = sig = 0;
    ledR = 1;
    ir = 0;
    strength = 0;

    ir = ioctl( in_file, FE_READ_STATUS, &status );
    if (ir < 0) {
        fprintf( stderr, "FE READ STATUS fail %s\n", strerror(errno) );
	return;
    }

    siglock = (status & FE_HAS_LOCK) ? 1:0;

    ir = ioctl(in_file, FE_READ_SIGNAL_STRENGTH, &sig);
    if (ir < 0) {
    	fprintf( stderr, "FE_READ_SIGNAL_STRENGTH fail %d %s\n",
		     ir, strerror(errno));
	return;
    }

    // without a sleep, will display fast and nothing if no lock
    // nanosleep( &sig_sleep, NULL);

    // scale to 0-100 range
    strength = sig / 655;

    if (strength > 100) strength = 100;

    sigper = strength;
    strength_count++; // good ioctl
    strength_sum += strength; // add the percentage values for average

    return;
}

#endif


#ifdef USE_V4L
static
void
get_v4l2_strength( void )
{
    int ir;

    ledG = sig1 = sig2 = siglock = sigper = 0;
    ledR = 1;
    ir = 0;
    strength = 0;

    memset( &v2sig, 0, sizeof( v2sig ) );
    ir = ioctl( in_file, VIDIOC_G_TUNER, &v2sig );
    if (0 != ir ) return; // bad ioctl

    strength = v2sig.signal; // V4L2 has better math
    if (strength > 40) {
        siglock = ledG = 1; // fake
        ledR = 0; // fake
    }
    sigper = strength;

    strength_count++; // good ioctl
    strength_sum += sigper; // add the percentage values for average

    return;
}

static
void
get_v4l1_strength( void )
{
    int ir;

    ledG = sig1 = sig2 = siglock = sigper = 0;
    ledR = 1;
    strength = 0;

    ir = 0;

    memset( &vsig, 0, sizeof( vsig ) );
    ir = ioctl( in_file, VIDIOCGSIGNAL, &vsig );
    if (0 != ir) return; // bad ioctl

    strength = (0xFF0000 & vsig.strength) >> 16;
    siglock = ( (0x43 & vsig.strength) == 0x43 ) ? 1:0;
    sig1 = 0xFF & (0x100 - ((0xFF00 & vsig.strength)>>8));
    sig2 = 0xFF & (0x100 - (0xFF & (vsig.aux)));

    ledR = 1 & (vsig.aux>>31); // custom hd2000 driver only
    ledG = 1 & (vsig.aux>>30); // custom hd2000 driver only

    sigper = (sig1 * 100) / 240; // bad math but gives something

    strength_count++; // good ioctl
    strength_sum += sigper; // add the percentage values for average
}
#endif

// sets global strength variables and sum and count for averaging
static
void
get_strength( void )
{

#ifdef USE_V4L
    if (1 == arg_vapi) get_v4l1_strength();
    if (2 == arg_vapi) get_v4l2_strength();
#endif

#ifdef USE_DVB
    if (3 == arg_vapi) get_dvb_strength();
#endif

}


static
void
scan_loop( void )
{
    int i, ir;
    int j, k, m, n;

    unsigned char bar[132]; // 80 cols at least
    unsigned int barx;
    double fm;
    int sfreq;

    ir = 0;

    if (scan_count != 0) {
	k = scan_count;
    } else {
	k = 1;
    }

    if (arg_otxt) {
/*
	fprintf( stderr, "\033[1;1H\033[2J"); //clear
	fprintf( stderr, "\033[4;24r"); // set scroll region
	fprintf( stderr, "\033[1;1H i  Ch Call Net MHz  dB%%  SS%%  SNR  AGC IR     STR2     STR1 LGR ERRORS %s", in_sname );
	fprintf( stderr, "\033[3;1H   %%|     NONE     JUNK     ECHO     SKIP     POOR     FAIR     GOOD     BEST\n");
*/
    }

    x_redraw_display(1);
    while(1)
    {
	for (j = 0; j < k; j++)
	{
	    i = scan_list[j];
	    chan = i;
	    set_channel( chan );

	    x_redraw_display(1);
//	    fprintf( stderr, "\033[4;1H\033[J");

	    // clear old junk and redraw text and grid
	    strength = 0;
	    
#define PI360 1131

//	    fflush(stdout); // move move move

	    for( m = 0; m < (PI360 * arg_loop); m++ )
	    {
//		nanosleep( &sig_sleep, NULL );

		// loop start resets time stats
		if (0 == (m % PI360)) {

		    x_redraw_display( arg_wipe );

		    memset( &loop_start, 0, sizeof(loop_start));
		    memset( &loop_stop,  0, sizeof(loop_start));
		    memset( &loop_diff,  0, sizeof(loop_start));
	    

/* Peter Knaggs Peter.Knaggs@gmail.com reports the following odd glibc error:
 *  Debian seems to return zero from PROCESS_CPUTIME_ID, even though my
 *  kernel is compiled with CONFIG_HPET_TIMER. Using the syscall directly
 *  via syscall(__NR_clock_gettime, PROCESS_CPUTIME_ID, &loop_start) works. 
 *  Perhaps it's simpler to use CLOCK_REALTIME in this case, though.
 */
//		    clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &loop_start );
		    clock_gettime( CLOCK_REALTIME, &loop_start );
	    
		    strength_count = 0;
		    strength_sum = 0;
		    strength_avg = 0;

		}

		// loop stop displays time stats
		if ( (PI360 - 1) == (m % PI360) ) {
		    long long nanos;
		    long long snano;
		    nanos = snano = 0;
// see above
//		    clock_gettime( CLOCK_PROCESS_CPUTIME_ID, &loop_stop );
		    clock_gettime( CLOCK_REALTIME, &loop_stop );

		    time_diff( &loop_diff, &loop_start, &loop_stop );

//		    fprintf( stdout, "start %d.%09ld stop %d.%09ld\n",
//			    (int)loop_start.tv_sec, loop_start.tv_nsec,
//			    (int)loop_stop.tv_sec,  loop_stop.tv_nsec );
		

		    // TESTME: forcing type, may be bad for 64 bit
		    nanos = 1000000000ULL * (unsigned long)loop_diff.tv_sec;
		    nanos += (unsigned long)loop_diff.tv_nsec;

		    /* avoid divide by zero */
		    if (0 != strength_count) {
			snano = nanos / strength_count;
			snano = 1000000000 / snano; // per second
//			if (0 != arg_otxt) fprintf( stderr, "\033[25;1H" );
			fprintf( stderr,
			    "\n%s channel %2d, sig %2d avg, %2lld scans/s, 360 time %d.%09lds\n\n",
			    in_sname, chan, strength_sum / strength_count, snano,
			    (int)loop_diff.tv_sec, loop_diff.tv_nsec );
			if (0 == arg_otxt) fprintf( stderr, "\n");
//			if (0 != arg_otxt) fprintf( stderr, "\033[24;1H" );
		    }

		}


		if (0 == (m % arg_modu))
		    get_strength();

		sfreq = atsc_bcast[i].freq / 1000;
		if (3 == arg_vapi)  sfreq = (atsc_bcast[i].freq+1750) / 1000;
		
		if (0 != arg_otxt) {
#if 0
		    fprintf( stderr,
			"\033[s\033[2;1H"
			"%3d %-11s %3d %3d%% %3d%% %04X %04X %2d "
			"%08X %08X %1X%1X%1X %2s%2s%2s"
			"\033[K\033[u",
			m % 999, atsc_bcast[i].sid, sfreq,
			strength, sigper, sig1, sig2, ir, 

// FIXME no leds, shorten, unjunk display
#ifdef USE_V4L
			v2sig.signal, vsig.strength, siglock, ledG, ledR,
#else
			-1, -1, siglock, ledG, ledR,
#endif 
			(siglock==0) ? "-L":"",
			(ledG==0) ? "-G":"",
			(ledR!=0) ? "+R":"" );
#endif

		    if (strength < 0) strength = 0;
		    if (strength > 100) strength = 100;
		    barx = (strength * 75) / 100; // 75 chars max
		    if (barx > 0) memset( bar, '*', barx);
		    bar[barx] = 0;
		    fprintf( stderr, "%3d%% %s\n", strength, bar);
		}

		if (0 != arg_adsp) {
		    
		    n = strength / 2;
		    n += 24;
//		    if (n > 47) n = 47;

		    if (n < 0) n = 0;

		    build_semitone( n );

		// no audio until past 22%
		    if (strength > arg_athr) play_audio( strength );
		}

//		strength = (m/12) % 100; // calibrate check


// X11 functions conditional
		if (arg_ox11) {
		    x_check_events();

		    if (strength > 99) strength = 99;
		    fstrength = ((double)strength) / 100.0;
		    fxscale = (double)vw.width / 2.0;
		    // room for 3 lines of 20 pels
		    fyscale = (double)vw.height / 2.0;
		    xorigin = vw.width / 2;
		    yorigin = vw.height / 2;

// dummy max for roundness check
//		xend = xscale * sin( m / 180.0 );
//		yend = yscale * cos( m / 180.0 );

		    // black/grey line ahead of next line
		    fm = 0.0;
		    fm = ((double)(m+1.0)) / 180.0;
		    xend = fxscale * sin( fm );
		    yend = fyscale * cos( fm );

		    bg = xcolors[9];		// even circles dark grey
		    if (1 == (1 & (m/PI360))) {
			bg = 0;			// odd circles black
//		    	while(1); // calibration check
		    }
		    XSetBackground( mydisplay, mygc, bg );
		    XSetForeground( mydisplay, mygc, bg );
		    XDrawLine(  mydisplay, mywindow, mygc,
			    xorigin,
			    yorigin,
			    xorigin + xend,
			    yorigin - yend
			    );
#if 0
		    // second line?
		    fm = 0.0;
		    fm = ((double)m) / 180.0;

		    xend = fxscale * sin( fm );
		    yend = fyscale * cos( fm );
//		    XSetForeground( mydisplay, mygc, fg );
		    XDrawLine(  mydisplay, mywindow, mygc,
			    xorigin,
			    yorigin,
			    xorigin + xend,
			    yorigin - yend
			    );
#endif

		    fg = xcolors[0];
		
		    if (strength > 12) fg = xcolors[1];
		    if (strength > 24) fg = xcolors[2];
		    if (strength > 36) fg = xcolors[3];
		    if (strength > 48) fg = xcolors[4];
		    if (strength > 60) fg = xcolors[5];
		    if (strength > 72) fg = xcolors[6];
		    if (strength > 84) fg = xcolors[7];
		
		    xend = fstrength * fxscale * sin( fm );
		    yend = fstrength * fyscale * cos( fm );
		
		    //xorigin += fxscale*sin(fstrength);
		    //yorigin -= fyscale*cos(fstrength);
		
		    XSetForeground( mydisplay, mygc, fg );
		    XDrawLine(  mydisplay, mywindow, mygc,
			    xorigin,
			    yorigin,
			    xorigin + xend,
			    yorigin - yend
			    );

		    x_display_grid();

#if 0
// this works ok, for horizontal overwriting histogram, also is lame
		    XSetForeground( mydisplay, mygc, bg );
		    // blank the line ahead
		    if (m < (vw.width-1))
			XDrawLine( mydisplay, mywindow, mygc,
				m+1, vw.height, m+1, vw.height-yscale);

		    XDrawLine( mydisplay,mywindow,mygc,
			    m, vw.height, m, vw.height-(yscale * fstrength) );

		    XFlush( mydisplay );
#endif
		}
//		nanosleep( &sig_sleep, NULL );

	    }


	    if (arg_once) c_exit(0);

//	    fprintf(stderr, "\n");
	}
	// fprintf(stderr, "\033[%dA", k);
    }
}

#if 0
// 2 to the power of 1/12 is semitone step on chromatic scale
static
void
build_chromatic_root_A_55hz( void ) {
    double chromatic = 1.059463094; // 2 to power of 1/12
    double semitone  = 55.0;
    int i,j;

    // create 8 octaves of 12 chromatic semi tones, starting at 220 A
    fprintf( stdout, "   A     A#/Bb B     C     C#/Db D     D#/Eb F     F#/Gb G     G#/Ab\n");
    for (i = 0; i<8; i++) {
	//		      220.00   
	for (j = 0; j<12; j++) {
	    fprintf( stdout, "%5.0f,", semitone); 
	    semitone *= chromatic;
	}
	fprintf( stdout, "\n");
    }
}
#endif
static
void
usage( char ** argv )
{
    fprintf( stdout,
"	Usage:\n"
"		%s [options] channel [channel2 ... channel82]\n"
"\n"
"	Where:\n"\
"		channel is numeric 2 through 82\n"
"		# is numeric 1 to n\n"
"\n"
"	Keys:\n"
"		q	quit program\n"
"			double click button 1 (LMB) for maximize\n"
"\n"
"	Options:\n"
#ifdef USE_V4L
"		-1	use V4L API for signal strength\n"
"		-2	use V4L2 API\n"
"		-3	use DVB API [default]\n"
"		-i #	set input device number [/dev/dtv#]\n"
#endif
"		-i #	set input device number [/dev/dvb/adapter#]\n"
"		-b #	signal strength below which audio is silent\n"
"		-r #	radians per scan\n"
"		-l #	number of 360 degree loops\n"
"		-w #	width and height for window\n"
"		-s #	sleep time per scan\n"
"		-a	use audio feedback\n"
"		-t	text feedback only, no X display\n"
"		-x	X visual feedback only, no text [default]\n"
"		-y	debug X events\n"
"		-c	clear display after each loop\n"
"\n", argv[0]);
}

static
void
c_exit( int ev )
{
    if (0 != xinit) x_close_display();
    if (0 != arg_adsp) close_audio();
    if (in_file > 0) close(in_file);
    // reset term to clear scrolling region
    if (0 != arg_otxt) fprintf( stderr, "\033c");
    exit( ev );
}

// arg_vapi determines correct device and init method
static
void
init_device( void )
{
    int ir;
    ir =0;

    // set device name
    if ( 3 == arg_vapi ) {
	snprintf( in_name, sizeof(in_name)-1, "/dev/dvb/adapter%d/frontend%d", arg_idev, 0);
    } else {
	snprintf( in_name, sizeof(in_name)-1, "/dev/dtv%d", arg_idev);
    }

    // open device
    in_file = open (in_name, O_RDWR );
    if ( 0 > in_file ) {

	fprintf( stderr,
		"Could not open file %s - %s\n",
		in_name, strerror( errno ));

	exit( 1 );
    }

#ifdef USE_DVB
    // DVB init
    if ( 3 == arg_vapi ) {
	ir = init_dvb_frontend( in_file, &dvb_frontend_param );
	if ( ir < 0 )
	{
	    fprintf( stderr, "Could not set up frontend %s\n", in_name);
    	    exit( 1 );
	}
	/* latest DVB API uses this method to fix driver i2c lock-out */

#ifdef FE_SET_FRONTEND_TUNE_MODE
	ioctl( in_file, FE_SET_FRONTEND_TUNE_MODE, FE_TUNE_MODE_ONESHOT);
#endif

    }
#endif

#ifdef USE_V4L
    if ( 3 != arg_vapi ) {
    // V4L needs to set the input source (CHAN is not frequency but source)
    // V4l2 is using V4L1 for this until I figure out V4L2 replacement
    // HD2000 has two antenna inputs, but not sure how to set them anymore
	ir = ioctl( in_file, VIDIOCSCHAN, &vch_ATSC );
	if ( 0 != ir ) {
	    fprintf( stderr, "V4L1 Set Input Source Channel error\n");
	    exit(1);
	}
    }
#endif

}


static
void
parse_args( int argc, char ** argv ){
    int i,j,k;
    int c;
    char *t;

    if (1 == argc) {
	usage( argv );
	fprintf( stdout, "Can't scan without a channel.\n\n");
	exit( 1 );
    }

    while ( EOF != ( c = getopt( argc, argv, "123cqatxyb:w:i:r:s:l:" ) ) )
    {
	switch(c) {
	case '1':
	case '2':
	case '3':
	    arg_vapi = c & 3;
	    if ( 0 == arg_vapi ) {
		fprintf( stderr, "Invalid Video API: use 1 2 or 3\n");
		exit(1);
	    }
	    break;

	case 'c':
	    arg_wipe = ~0;
	    break;

	case 'q':
	    arg_once = ~0;
	    arg_loop = 1;
	    break;

	// enable audio
	case 'a':
	    arg_adsp = ~0;
	    break;

	// signal percent threshold before audio output
	case 'b':
	    if (NULL != optarg) {
		sscanf( optarg,"%d",&arg_athr );
	    }
	    break;

	// X event display
	case 'e':
	    arg_xmsg = ~0;
	    break;

	case 'w':
	    if (NULL != optarg) {
		sscanf( optarg, "%d", &arg_xdim );
	    }
	    vw.height = arg_xdim;
	    vw.width  = arg_xdim;
	    break;

	case 'i':
	    if (NULL != optarg) {
		sscanf( optarg, "%d", &arg_idev );
	    }
	    break;

	case 'l':
	    if (NULL != optarg) {
		sscanf( optarg, "%d", &arg_loop );
	    }
	    break;

	case 's':
	    if (NULL != optarg) {
		sscanf( optarg, "%d", &arg_time );
		if (0 != arg_time) sig_sleep.tv_nsec = arg_time;
	    }
	    break;

	/* radians */
	case 'r':
	    if (NULL != optarg) {
		sscanf( optarg, "%d", &arg_modu );
		/* arg_modu = 3.1415926 * arg_modu; */
	    }
	    break;
	    
    	case 't':
	    arg_otxt = ~0;
	    arg_ox11 = 0;
	    break;

    	case 'x':
	    arg_otxt = 0;
	    arg_ox11 = ~0;
	    break;

	default:
	    break;
	}
    }


    memset( in_sname, 0, sizeof( in_sname ) );
    switch(arg_vapi) {
	case 1:
	    t = "Video4Linux";
	    snprintf( in_sname, sizeof( in_sname), "dtv%d", arg_idev);
	    break;
	
	case 2:
	    t = "Video4Linux2";
	    snprintf( in_sname, sizeof( in_sname), "dtv%d", arg_idev);
	    break;

	case 3:
	    t = "DVB";
	    snprintf( in_sname, sizeof( in_sname), "dvb%d", arg_idev);
	    break;

	default:
	    fprintf(stderr, "\nNO VIDEO API CHOSEN\n");
	    exit(1);
	    break;
    }
    
    fprintf( stdout, "Using %s API on %s\n", t, in_sname );

    k = optind;

    if (0 == (argc - k)) {
	usage( argv );
	fprintf( stderr, "Can't scan without a channel.\n\n");
	exit( 1 );
    }

//    fprintf( stdout, "argc - k = %d\n", argc-k);

    chan = 0;
    if (k < argc) {
      chan = atoi(argv[k]);
//      fprintf (stdout, "Using channel %d\n",chan);
    }

    memset( scan_list, 0, sizeof( scan_list ) );
  /* parse arg channel list if exists */
    if (k < argc) {
      for (i=0; i < 81; i++) {
        j = 0;
        if (k > argc) break;

        if ( argv[ k ] == NULL ) break;

        j = atoi( argv[ k++ ] );
        if ( (j < 2) || (j > 83) ) {
          fprintf( stderr, "Invalid channel %d: range is 2 thru 83\n", j );
	  continue; // not fatal, skip it
        }

//        fprintf( stdout, "scan %d\n", j );
        scan_list[ i ] = j;
        scan_count++;
      }
    }

    if (chan < 0) chan = 0;

    in_file = 0;
    init_device();
}

int
main( int argc, char ** argv )
{

    fprintf(stdout, NAME" "VERSION" "COPYRIGHT" "EMAIL"\n");
    fprintf(stdout, "Released under the "LICENSE"\n");

    parse_args( argc, argv );
    
    if (0 != arg_adsp) {
	init_audio();
    }

    // if X not started, fall back to text mode
    if ( NULL == getenv( "DISPLAY" ) ) {
	arg_ox11 = 0;
	arg_otxt = ~0;
    }

    if (0 != arg_ox11) {
	x_init_display();
	x_redraw_display(1);
    }

    scan_loop();

    fprintf( stderr, "\n" );

    if (0 != arg_ox11) {
	x_close_display();
    }

    if (0 != arg_adsp) {
	close_audio();
    }

    close( in_file );


    return 0;
}

