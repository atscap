#!/bin/bash
# main capture directory (use XFS filesystem for speed)
DIR=/dtv
# cut directory (symlink to another physical hd for speed)
CUTDIR=/dtv/cut
EDITOR=xtscut
# cut files are written here
EDITOPT="-p "$CUTDIR
URI=$1
# remove xtc://host:port/ and feed DIR/[dir/]name to xtscut
CUT1=`expr "$URI" : 'xtc://'`
HOSTFILE=${URI:CUT1}
CUT2=`expr index "$HOSTFILE" '/'`
NAME=${HOSTFILE:CUT2}
#
# change this to local mount used for your remote capture directory
EDITFILE=$DIR/$NAME
# changes URI to basename for playback from mounted filesystem
#
# uncomment this for playback over local mount directory/filename.ts

XTERMCFG="xterm -fn 10x20 -fg white -bg black -n xtscut -e"
$XTERMCFG $EDITOR $EDITOPT $EDITFILE
