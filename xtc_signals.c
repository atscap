/*****************************************************************************
 *
 * signals.c    (c) Copyright 2004-2007 by inkling@nop.org
 *       part of the ATSC Transport Stream Capture Application Programs
 *
 * atscap is free software; you may only redistribute it and/or modify
 * it under the terms of the GNU General Public License Version 2, or later,
 * as published by the Free Software Foundation.
 *
 * atscap source code is distributed to you in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY OR SUPPORT; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  Please see the
 * GNU General Public License Version 3 for more details.
 *
 * You should have received a copy of the GNU General Public License Version 2
 * along with this program; if not, write me or the Free Software Foundation,
 * Inc., at 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifdef USE_CONSOLE
#warning using console_* functions
/* console scan esc uses
 kb remap of special esc keys to single byte values
 move these around to anything above 127 to suit yourself
 vt102 1980 standard stuff? maybe

 KR_ is keyboard raw return byte string from read(0,...)

 KB_ is keyboard cooked to something simpler for console_scan()
*/

/* ESC [ A */
#define KR_UP 0x1B5B41
#define KB_UP 0xF1
/* ESC [ B */
#define KR_DN 0x1B5B42
#define KB_DN 0xF2
/* ESC [ C */
#define KR_RT 0x1B5B43
#define KB_RT 0xF3
/* ESC [ D */
#define KR_LF 0x1B5B44
#define KB_LF 0xF4

/* things get a bit odder here. may have to redefine if not xterm/aterm. */
/* ESC [ 1 ~ */
#define KR_HM 0x1B5B317E
#define KB_HM 0xF5
/* ESC [ 2 ~ */
#define KR_IN 0x1B5B327E
#define KB_IN 0xF6 
/* ESC [ 3 ~ */
#define KR_DL 0x1B5B337E
#define KB_DL 0xF7
/* ESC [ 4 ~ */
#define KR_EN 0x1B5B347E
#define KB_EN 0xF8
/* ESC [ 5 ~ */
#define KR_PU 0x1B5B357E
#define KB_PU 0xF9
/* ESC [ 6 ~ */
#define KR_PD 0x1B5B367E
#define KB_PD 0xFA
#include <termios.h>
int console_init = 0;

#endif /* KR_UP */

/* will need for signal actions, sigterm mostly */
#ifdef USE_SIGNALS
#warning using POSIX signals
#include <signal.h>

volatile int sig_val = 0;
int sig_kill = 0;
char *sig_text[32] = {
        "NULL", "HUP", "INT", "QUIT", "ILL", "TRAP", "ABRT",  "BUS",
        "FPE", "KILL", "USR1", "SEGV", "USR2", "PIPE", "ALRM", "TERM",
        "STKFLT", "CHLD", "CONT", "STOP", "TSTP", "TTIN", "TTOU", "URG",
        "XCPU", "XFSZ", "VTALRM", "PROF", "WINCH", "IO", "PWR", "SYS"
};
/* prototype */
void signal_test( void );
#endif


#ifdef USE_CONSOLE
/* put the console back to a usable state */
/* static */
void
console_reset ( void )
{
	struct termios modes;
	int f;
    
	f = 0;
	fcntl( 0, F_GETFL, &f );
	f &= ~O_NONBLOCK;
	fcntl( 0, F_SETFL, f );

/* indicate the console will need init before nonblock nonecho use */
	console_init = 1;
	if ( tcgetattr( 0, &modes ) < 0 )
		fprintf( stderr, "c_reset tcgetattr" );

	modes.c_lflag |= ICANON;
	modes.c_lflag |= ECHO;
	if ( tcsetattr( 0, TCSAFLUSH, &modes ) < 0 )
		fprintf( stderr, "c_reset tcsetattr" );
}

/* static */
void console_exit ( int err )
{
	console_reset();
	console_reset();
	c_exit(err);
}

/* non-blocking non-echoing single-char stdin, from mzplay.c */
/* static */
int
console_getch ( void ) {
	unsigned c = 0;

#ifdef USE_SIGNALS
#warning console_getch checks sig_kill
	signal_test();	/* any flags worth noticing? */
	if (0 != sig_kill) console_exit(0);
#endif

	if (console_init) {
		struct termios modes;
		int f;

		console_init = 0;

		if ( tcgetattr(0, &modes) < 0 )
			fprintf( stderr, "c_getch tcgetattr" );

		modes.c_lflag &= ~ICANON;
		modes.c_lflag &= ~ECHO;

		if ( tcsetattr(0, TCSAFLUSH, &modes) < 0 )
			fprintf( stderr, "c_getch tcsetattr" );

		f = 0;
		fcntl( 0, F_GETFL, &f );
		f |= O_NONBLOCK;
		fcntl( 0, F_SETFL, f );
	}
	if ( read( 0, &c, 1 ) < 1 ) c = 0;
	return c;
}

/* when console scan gets ESC, look for [ and if so do these
 make easier to use with following keys: up/dn/lt/rt/in/dl/hm/en/pu/pd
*/
/* static */
void
console_scan_esc ( unsigned char *kb )
{
    unsigned char k;
    int arrow;

    *kb = console_getch();	/* get char after ESC */
    if (0 == *kb) return;	/* nothing pending from function keys */
    if ('[' != *kb) return;	/* no [ means no arrows or function keys */


/*    utstpg = utsnow + PROGRAM_GUIDE_TIMEOUT; */

/*    nanosleep( &console_read_sleep, NULL); */
    *kb = console_getch();	/* get what follows ESC [ */

    /* should be cursor/arrow keys */
/*    fprintf( stdout, "ESC[ %02X", *kb); */

    arrow = 0;
    /* "ESC [" and 4 choices for arrows are A B C D */
    switch( *kb ) {
	/* no chars to clear after these */
	case 'A':
	    *kb = KB_UP;	/* UP ARROW */
	    arrow = ~0;
	    break;

	case 'B':
	    *kb = KB_DN;	/* DOWN ARROW */
	    arrow = ~0;
	    break;

	case 'C':
	    *kb = KB_RT;	/* RIGHT ARROW */
	    arrow = ~0;
	    break;

	case 'D':
	    *kb = KB_LF;
	    arrow = ~0;
	    break;

	case 'H':
	    *kb = KB_HM;	/* HOME */
	    arrow = ~0;
	    break;

	case 'F':
	    *kb = KB_EN;	/* END */
	    arrow = ~0;
	    break;

    }

    /* have an arrow so get out, no need for ~ term */
    if (arrow != 0) return;
    
    /* checked for arrows, kb still has numeric, so check for ~ term char */
    k = console_getch();
    if (0 == k)   { *kb = 0; return; } /* it's incomplete so abort */
    if ('~' != k) { *kb = 0; return; } /* no ~ term char so abort */
    
    /* check char after ESC [ again */
    switch ( *kb ) {

	case '7':		/* newer aterm doing this? */
	case '1':
	    *kb = KB_HM;	/* HOME */
	    break;

	case '2':
	    *kb = KB_IN;	/* INSERT */
	    break;

	case '3':
	    *kb = KB_DL;	/* DELETE */
	    break;

	case '8':		/* newer aterm doing this? */
	case '4':
	    *kb = KB_EN;	/* END */
	    break;

	case '5':
	    *kb = KB_PU;	/* PAGE UP */
	    break;
	case '6':
	    *kb = KB_PD;	/* PAGE DOWN */
	    break;
	default:
	    *kb = 0; /* try to prevent false triggers if no conditions met */
	    break;
    }

    return;
}

/* wrapper to return keyboard special keys as single byte */
/* static */
void
console_getch_fn ( unsigned char *kb )
{
	*kb = console_getch();
	if (0x1B != *kb) return; /* let normal keys slide */
	console_scan_esc( kb ); /* try to parse function keys */
}
#endif /* USE_CONSOLE */


#ifdef USE_SIGNALS
/* Dump backtrace to file and to stderr after clearing screen */
/* Check ALL pointers before output in case it's toast. */
/* NOTE: It should be pointed out that this is a Bad Idea. */
#ifdef USE_GNU_BACKTRACE_SCRIPT
#warning using GNU backtrace() script
/* static */
void
dump_backtrace ( char **bts, size_t z, void *addr )
{
	FILE *f;
	char a[128+3]; /* address, up to 64 bits, plus 3 for 0x and nul */
	char o[256]; /* dump output name */
	char t[256]; /* temp copy of backtrace string */
	char n[256]; /* app or lib name */
	char s[256]; /* source function after address + name extract */
	char *p, *r;
	size_t y;
	unsigned int i;
	char has_name, has_addr, has_func;

	if (0 == z) return;
	if (NULL == bts) return;

	has_name = has_addr = has_func = 0;

/* Crash log script is /dtv/atscap#-pid.sh, calling addr2line for line numbers.
    NOTE: This only works if crash log matches current version compiled.
*/
	snprintf( o, sizeof(o), "%s%s-%d.sh",
		  out_path, NAME, (int)pid_m);

	f = fopen( o, "wb");

/* make .sh executable to call addr2line.sh to help debug */
	if (NULL != f) chmod( o, 0755 );

	if (NULL != f) {
	    fprintf( f, "# %s\n", o);
	    fprintf( f, "echo %s%d-%s SIG%s at address %p on %s.\n",
			NAME, arg_devnum, VERSION,
			sig_text[sig_val], addr, date_now );
	    fprintf( f, "# addr2line helps find backtrace() line number.\n");
	    fprintf( f, "# Comments below are strings from backtrace().\n");
	    fprintf( f, "# Only %s lines are used for line numbers.\n", NAME);
	    fprintf( f, "# Found %d stack frames.\n", z);
	    fprintf( f, "#\n");

/* start at 1, because 0 is always signal_handler */
	    for (y = 1; y < z; y++) {
		i = 0;

/* zero strings */
		memset( t, 0, sizeof(t) );
		memset( s, 0, sizeof(s) );
		memset( n, 0, sizeof(n) );
		memset( a, 0, sizeof(a) );

/* copy backtrace string for editing, first time */
		astrncpy( t, bts[ y ], sizeof(t) );

		if ( '[' != *t) has_name = ~0;

/* has name ? */
		if (0 != has_name) {
		    astrncpy( n, bts[ y ], sizeof(n));
		    if (NULL != strstr( n, NAME )) {
			strcpy( n, NAME );
		    }
/* remove anything after name, work backwards from [ ( and blank */
		    p = strchr( n, '[' );		/* remove addr */
		    if (NULL != p) *p = 0;
		    p = strchr( n, '(' );		/* remove func */
		    if (NULL != p) *p = 0;
		    p = strchr( n, ' ' );		/* remove blank */
		    if (NULL != p) *p = 0;
		}

/* copy backtrace string for editing, again */
		astrncpy( t, bts[ y ], sizeof(t) );
		p = strchr( t, '[' );
		if (NULL != p) has_addr = ~0;

/* has address? */
		if (0 != has_addr) {
		    p = strchr( t, '[' );		/* point to addr */
		    if (NULL != p) {
			p++;
			sscanf( p, "%x", &i );		/* get addr */

/* convert all hexadecimal displays to upper case */
			snprintf( a, sizeof(a), "0x%08X", i);
		    }
		}

/* copy backtrace string for editing, again */
		astrncpy( t, bts[ y ], sizeof(t) );
		p = strchr( t, '(' );
		if (NULL != p) has_func = ~0;

		*s = 0;

/* has function? */
		if (0 != has_func) {
		    p = strchr( t, '(' );
		    if (NULL != p) {
			p++;
			r = strchr( p, ')' );
			if (NULL != r) {
			    *r = 0;
			    astrncpy( s, p, sizeof(s) );
			}
		    }
		}

/* atscap default install location */
#define USE_PREFIX_BIN "/usr/local/bin/"

/* if file open */
		if (NULL != f) {
		    fprintf( f, "# %s\n", bts[ y ] );
		    if (0 != *n) {
			if ( '/' != *n) 
			    fprintf( f, "addr2line -s -f -e %s%s %s # %s\n",
				USE_PREFIX_BIN, n, a, s );
		    }
		}

	    }
	    if (NULL != f) fprintf( f, "# EOF\n");
	}

	if (NULL != f) fclose(f);

/* let user know it's all bad */
	fprintf( stdout, "Running %s to get backtrace lines:\n\n", o);
	fflush( stdout );

	nanosleep( &console_read_sleep, NULL );
	console_reset();
	console_reset();
	system( o );
	free( bts );
	exit( 252 );
	fprintf( stdout, "\n");
}
#endif

/* See http://www.linuxjournal.com/article/6391 Listing 3. */
/* NOTE: stack crashes will prevent most of this from working right,
    but simpler errors like NULL pointers may be more easily found.
*/
/* static */
void
/* signal_handler ( int sigval ) */ /* old style */
signal_handler ( int sigval, siginfo_t *info, void *secret )
{
#ifdef USE_GNU_BACKTRACE
	void *bt[BTZ];
	size_t z;
	char t[256];
	int f;
#ifdef USE_GNU_BACKTRACE_SCRIPT
	char **bts = (char **) NULL;
#endif
#endif
	ucontext_t *uc;
	void *addr;
	char n[256];

//	return;

	*n = 0;

	addr = NULL;

/* EIP only works with GNU/Linux on intel/amd x86 32-bit arch */
#if defined(REG_EIP)
#warning using x86 arch uc_mcontext.gregs REG_EIP
	uc = (ucontext_t *)secret;
	addr = (void *) uc->uc_mcontext.gregs[ REG_EIP ];
#endif
/* RIP only works with GNU/Linux on intel/amd x86_64 64-bit arch */
#if defined(REG_RIP)
#warning using x86_64 arch uc_mcontext.gregs REG_RIP
	uc = (ucontext_t *)secret;
	addr = (void *) uc->uc_mcontext.gregs[ REG_RIP ];
#endif

	sig_val = sigval; /* save signal for exit processing */

	switch (sig_val)
	{

/* ignore sigpipe, is likely to be web server aborted connection */
	    case SIGPIPE:
		return;
		break;

/* only sets flag for signal test called from console scan */
	    case SIGWINCH:
		return;
		break;
	    case SIGTERM:
		return;
		break;
	    case SIGQUIT:
		return;
		break;
	    case SIGINT:
		return;
		break;

#ifdef USE_GNU_BACKTRACE
	    case SIGUSR1:
		z = backtrace( bt, BTZ);
		if (0 == z) break;

/* addr will be arch dependent on x86 and derivatives with newer GCC only */
		bt[2] = addr;

		snprintf( n, sizeof(n), "%s%s%d-%d.bt",
			  out_path, NAME, arg_devnum, pid_m);
		snprintf( t, sizeof(t),
			"# %s%d-%s SIG%s at addr %p pid %d tid %d (%d) on %s\n\n",
			NAME, arg_devnum, VERSION,
			sig_text[sig_val], addr, pid_m,
			(int) pthread_self(),
			0x3FFF & (int) pthread_self(),  /* ignore if NPTL */
			date_now );

		f = open( n, O_RDWR | O_CREAT, 0644 );

		if (f > 2) {
		    write( f, t, strlen(t) );

/* use btfd.sh to extract line numbers */
		    backtrace_symbols_fd( bt, z, f );
		    fsync( f );
		    close( f );
		}
		return;
		break;
#endif
	
/* fatal errors */
	    case SIGSEGV:
	    case SIGILL:
	    case SIGFPE:
	    case SIGBUS:
	    case SIGIOT:

#ifdef USE_GNU_BACKTRACE
/* if backtrace or backtrace_symbols fail, give some exit indication */
		z = backtrace( bt, BTZ);
		if (0 == z) {
		    fprintf( stderr, CLS BN SCV
			     "Fatal error at addr %p, no backtrace.\n", addr );
		    console_reset();
		    console_reset();
		    exit(254);
		}
	
/* This is typical place where stack trace goes wrong, for pthreads. All
    examples of how to use have bt[1] instead, but are not pthread apps.
*/
		bt[2] = addr;
		
#ifdef USE_GNU_BACKTRACE_SCRIPT
/* Save backtrace to addr2line script. Is problematic if stack is crashed. */
		bts = backtrace_symbols( bt, z );
		if (NULL == bts) {
		    fprintf( stderr, CLS BN SCV
			     "Fatal error at addr %p, no backtrace.\n", addr );
		    console_reset();
		    console_reset();
		    exit(255);
		}

/* If you're here, it must have crashed. LET ME KNOW or send me a patch.  */
/* It logs what can be logged about crash. addr2line will help, somewhat. */
		fprintf( stderr, CLS SCV BN );
#if 0
		fprintf( stderr, "%s%d-%s SIG%s at addr %p on %s.\n",
			NAME, arg_devnum, VERSION,
			sig_text[sig_val], addr, date_now );
#endif

		dump_backtrace( bts, z, addr ); /* does not return, exit252 */
#else
/* Save backtrace to file instead. Is problematic if stack is crashed. */
/*	snprintf( n, sizeof(n), "%s%s-%d.bt", out_path, NAME, pid_m ); */
		snprintf( n, sizeof(n), "%s%s%d-%d.bt",
			    out_path, NAME, arg_devnum, getpid());
		snprintf( t, sizeof(t),
			"# %s%d-%s SIG%s pid %d tid %d (%d) on %s\n",
			NAME, arg_devnum, VERSION,
			sig_text[sig_val], pid_m,
			(int) pthread_self(),
			0x3FFF & (int) pthread_self(), /* ignore if NPTL */
			date_now );

		f = open( n, O_RDWR | O_CREAT | O_TRUNC, 0644 );
		if (f > 2) {
		    write( f, t, strlen(t) );
		    backtrace_symbols_fd( bt, z, f );
		    fsync(f);
		    close(f);
		}

/* USE_GNU_BACKTRACE_SCRIPT */
#endif
/* USE_GNU_BACKTRACE */

/* NOTE: if stack is crashed, these won't help and may make it worse */
		fprintf( stderr, CLS SCV BN 
			"%s%d-%s SIG%s at addr %p on %s.\n",
			NAME, arg_devnum, VERSION,
			sig_text[sig_val], addr, date_now );

		fprintf(stderr, "Run btfd.sh <%s for stack trace.\n\n", n);
#endif
		fprintf(stderr, "SIG%s at addr %p\n", sig_text[sig_val], addr);

		console_reset();
		console_reset();
		exit(253); // 0xFC
		break;

/* the rest get logged */
	    default:
		if (sig_val < 32) {
		    fprintf( stdout, "%s %s(%d) tid %d ignored",
			WHO, sig_text[ sig_val ], sigval, getpid() );
		} else {
		    fprintf( stdout, "%s %d", WHO, sig_val);
		}
		break;
	}

	fprintf( stderr, "%s-%s unhandled SIG%s at addr %p\n\n",
		    NAME, VERSION, sig_text[sig_val], addr);
	console_exit( sig_val );
}

/* global signal init. got rid of -ansi -pedantic porting errors */
/* static */
void
signal_init ( void )
{
	struct sigaction act;

/* 	act.sa_handler = signal_handler;  */
/* old style doesn't give EIP */
	
/* sigemptyset is required */
	sigemptyset( &act.sa_mask);
	act.sa_flags = SA_RESTART | SA_SIGINFO;
	act.sa_sigaction = signal_handler;

/* signals quit and term can be used to terminate it gracefully */
	sigaction( SIGQUIT, &act, NULL );
	sigaction( SIGTERM, &act, NULL );

/* control c will terminate it gracefully if not capturing */
	sigaction( SIGINT, &act, NULL );

/*	act.sa_handler = SIG_IGN;  */

/* console resize, using something else */
	sigaction( SIGWINCH, &act, NULL );

/* HTTP remote closed connection is usual cause of this.
   Example: refresh the page before it's done loading.
*/
	sigaction( SIGPIPE, &act, NULL );

/* these are all fatal */
	sigaction( SIGSEGV, &act, NULL );
	sigaction( SIGILL, &act, NULL );
	sigaction( SIGFPE, &act, NULL );
	sigaction( SIGBUS, &act, NULL );
	sigaction( SIGIOT, &act, NULL );

/* SIGUSR1 will dump a backtrace log of current threads */
	sigaction( SIGUSR1, &act, NULL );
}


/* SIGINT enable 1, disable 0 */
/* static */
void
sigint_set ( int s )
{
	struct sigaction act;

/* sigemptyset is required */
	sigemptyset( &act.sa_mask );
	act.sa_flags = SA_RESTART;
	act.sa_handler = SIG_IGN; 

/*	if (0 != s) act.sa_handler = signal_handler;  */ /* old style */
	if (0 != s) act.sa_sigaction = signal_handler; 

	sigaction( SIGINT, &act, NULL );
}

/* USE_SIGNALS */
#endif



#ifdef USE_SIGNALS
/* want to close the device properly if possible */
/* static */
void
signal_test ( void )
{
        if (0 == sig_val) return; /* nothing to do? */
        if (sig_val < 32)
                fprintf( stderr, "received SIG%s", sig_text[sig_val] );

        switch( sig_val )
        {

/* these three indicate user requested program terminate */
        case SIGINT:
        case SIGTERM:
        case SIGQUIT:
                sig_kill = ~0;
                fprintf( stderr, "exiting on signal %d", sig_val);
                break;

/* GNU screen changed the term size, or xterm was resized */
        case SIGWINCH:
                fprintf( stderr, "%s tty dimension changed", WHO);
                break;

        case SIGPIPE:
                fprintf( stderr, "%s broken pipe or socket", WHO);
                break;

/* even with SEGV still want to try to close device properly if possible */
	case SIGSEGV:
		sig_kill = ~0;
		break;

        default:
                if (sig_val < 32)
		    fprintf( stderr, "%s signal %s ignored",
			WHO, sig_text[sig_val] );
                break;
	}
	sig_val = 0;
}

#endif


#if 0
#ifdef USE_SIGNALS
/* disable control c */
    sigint_set( 0 );
#endif

#ifdef USE_SIGNALS
/* enable control c */
    sigint_set( 1 );
#endif

#ifdef USE_SIGNALS
/* initialize signal handler */
    signal_init(); /* sigaction setup for control c trap */
#endif
    signal_timeout = 0;
#endif

