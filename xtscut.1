.TH Xtscut 1 "2008-Jan-02" "Xtscut-1.3.5" "(C) 2005-2008 inkling@users.sourceforge.net"
.SH NAME
.B Xtscut \- ATSC Transport Stream Cutter for X

.SH SYNOPSIS
.PP
.B xtscut infile
.IP
.B [\-1dgknr]
.B [\-fix]
.B [\-hv]
.br
.B [\-a FPUbits]
.br
.B [\-b framecount]
.br
.B [\-c even|odd|all]
.br
.B [\-o outname]
.br
.B [\-p outpath]
.br
.B [\-w divisor]
.br

.SH DESCRIPTION
.PP
This manual page describes the options for
.B Xtscut,
the Xlib graphical tool to cut ATSC MPEG Transport Streams at Sequence headers.

.SH BACKGROUND
MPEG2 usually needs a Non-Linear Editor (NLE) to handle simple cuts.
.PP
NLEs can be slow if they don't have some hardware assist because they have
to render the entire Sequence, all of the frames, and then encode a new
Sequence of frames that has the cut frames removed. This is much easier
if you have a hardware encoder, but not many people have HDTV grade Main
Profile @ High Level hardware encoders available to them. Software encoders
are very slow for HDTV, even on new systems.

.SH THEORY OF OPERATION
.B Xtscut
avoids all of the demux, decode, encode, and remux hoops that you have to
jump through for the typical NLE hardware or software.
.PP
.B Xtscut
doesn't concern itself with precise frame cuts, but instead only cuts at
the start of a Sequence. The first video frame after a Sequence header
is an Intra frame in ATSC. This Intra frame is a still picture that doesn't
need any other P or B frames to describe it. These make very good preview
and cut points for the ATSC Transport Stream.
.PP
.B Xtscut
tracks the byte offsets of the Sequence headers and uses these for the cut
points. Aside from the Intra frame preview, which isn't used when writing the
cuts, no MPEG2 decoding of the stream is done.
.PP
The advantages to this method not only apply to speed, but also reliability.
Since the stream isn't parsed during cuts, a few (or even a lot) of bad packets
will not affect the cutting process. This method performs the cuts with a few
seeks and a lot of reads and writes. This makes
.B Xtscut
one of the fastest, if not the fastest, ATSC Transport Stream cutters
available today. It's also less expensive than other solutions because:
.IP
a) it's free software and
.IP
b) it's fast.
.PP
Time is like money. The more you save, the more you have.
.SH DISCLAIMER
.B Xtscut
is not designed nor intended to be a perfect substitute for a Non-Linear
Editor. If you have the need for frame accurate editing, or special effects,
there are various hardware and software solutions that may suit you better.
Intra frame cuts are good enough for me.

.SH OPTIONS WITHOUT PARAMETERS
These options act mostly as toggles for the compiled defaults (arg_*).

.TP
.B \-h
Help. This may be more up-to-date than this manual page.

.TP
.B \-v
Show program banner and exit.

.TP
.B \-1
Cut to multiple files, -r -cX modifies this.
.br
Default is to cut to one, possibly giant, file. The [w] key in the GUI will
reset this to writing multiple files. The [m] key in the GUI will toggle this 
between multiple and single file cut writes.

.TP
.B \-d
Delay writes to reduce I/O load on one drive, default no.

.TP
.B \-n
Write NULL packets at the start of each cut. This may or may not help with
initial playback stuttering. Still testing it. Consider it experimental.
The [n] key in the GUI will toggle NULL packet generation on and off.
.TP
.B \-r
Renumber odd/even cuts to .01.ts ... .99.ts, default no.
.br
See also
.B -1 -c

.SH OPTIONS WITH PARAMETERS
These options set user supplied parameters.

.TP
.B \-a method
Default is to let libmpeg2 autodetect which FPU method to use for the IDCT.
If you need to override this, you can use one of the numerical values found
in mpeg2.h. The default should work well for most people.

.TP
.B \-b decimal
Benchmark the Intra frame render. It's only useful for comparative testing
with option -w1 vs other -w sizes, and for Imlib1 vs Imlib2 vs XVideo testing.
Give it file.ts and it will try to render num Intra frames as fast as it can.
It doesn't do the histogram render, so it will be faster than actual use.

.TP
.B \-c e|o|a
If *.tsc file exists, cut odd, even or all and exit.
Use with options for scripts that don't need the GUI.
.br
See also:
.B -1 -r -c

.TP
.B \-o name
Basename to write cuts as
.B name.00.ts ... name.99.ts

.TP
.B \-p path
Save output *.ts to
.B path.

.TP
.B \-w divisor
Default video window is 960x544 (16x9). Specifying this option will instead
use the source material frame size with this divisor. The divisors are
limited to 1, 2, 4 and 8 to prevent interlacing artifacts (banding). If you
have a 1920x1200 display, you can use -w1 for a big 1920x1080 window. You
may also use -w1 to present SD material at the correct aspect ratio. You
may use -w0 to reset the video window to 960x544.

.TP
.B \-k n
By default the last video Sequence is discarded because this is usually cruft.
However, if you're re-cutting a previous cut, or are using source material
(i.e., a camcorder or a previous cut) that has a known good final Sequence,
use the -k1 option to keep the last Sequence instead of discarding it. This
prevents any cutting-related generational losses of the last Sequence.
.SH WARNING: Xtscut will crash if you try to preview a corrupted Sequence.
The crashes may occur in bad streams, or at last Sequence preview.
.PP
The Good News is this happens very seldom. The Bad News is this happens very
far down into libmpeg2 and crashes it before mpeg2_parse() can return
STATE_INVALID_END. Currently there is no workaround for this other than
discarding the last Sequence, which is the default.
.B Xtscut
may also crash if it previews a corrupted Sequence anywhere, not only at
the end of the stream.
.TP
.B NOTE: Cuts are auto-saved so you shouldn't lose much if it crashes.


.SH OPTIONS FOR DEBUGGING
These options can be used when modifying the source code. You probably
won't have to use these unless I missed a really bad bug somewhere.

.TP
.B \-f
Function name verbosity

.TP
.B \-i
Intra Frame processing verbosity

.TP
.B \-x
X function verbosity

.SH USER INTERFACE DESCRIPTION
.B Xtscut
cuts ATSC Transport Stream (and possibly some camcorder) captures, .ts files,
using Sequence and Frame data files, .tsx files, generated by:
.B atscap -m
or
.B atscut -s
to determine where to make the cuts.
.PP
If you don't have a .tsx file for the stream you want to cut,
.B Xtscut
will try to generate the .tsx file for you. If that fails, you may try:
.IP
.B $ atscut -s file.ts
.PP
If it does need to create the .tsx file, please be patient. Depending on the
speed of your system, it may take longer than 30 seconds per gigabyte of
source material before the
.B Xtscut
GUI is able to start.
.PP
The histogram shows frame number as X-offset. The height of the histogram
gives a general overview of the bandwidth usage for each frame type.
.PP
You can use middle mouse, button2, as coarse stream navigation by percentage
of horizontal video window area to set the percentage of stream. For example,
clicking MMB in the middle of the video window will put you about halfway
into the stream. Think of it as an invisible always available slider.
.PP
The cyan lines are Sequence/Intra-frame starts where you can make a cut.
The right mouse button, button3, previews that Sequence start. The left
mouse button, button1, inserts or removes a cut at that Sequence start.
.PP
The magenta and yellow lines are P & B frames, respectively. These are
not selectable for cuts, but can help to quickly find a cut point by
graphically representing the packet drop-off that occurs in a blank spot.
.PP
You may use the left mouse button, button 1, to click on the IPB buttons to
enable or disable the frame histogram for each of the 3 frame types.
.PP
Generally speaking, you cut at the nearest blank Sequence, or as close as
you can get, to what you want to cut. It does Sequence-accurate cuts, not
Frame-accurate cuts. Cut wide if you want to edit B and P frames later
with a proper NLE.

.PP
Cuts are indicated by a red line with a number at the bottom, which
is the number of the cut to the
.B LEFT
of the red line.

.PP
If you have a scroll-wheel mouse you can use that for navigation.
When navigating, the white line in the center is the preview position.
If or when the scroll-wheel is too slow, you may use the following keys:
.IP
.B NAVKEYS --- FUNCTIONS
.IP
[  HOME  backward to start of stream + half screen width
.br
y  PGUP  backward 2 screen widths
.br
u  UP    backward 1 screen width
.br
MB4      backward 1/2 screen width (scrollwheel up)
.br
j  LEFT  backward 1 Intra frame
.IP
k  RIGHT forward 1 Intra frame
.br
MB5      forward 1/2 screen width
.br
i  DOWN  forward 1 screen width
.br
o  PGDN  forward 2 screen widths
.br
]  END   forward to end of stream - half screen width
.br
.SH MORE KEYS
These keys set various options that control cutting and display.
.IP
d     Display detailed cut information
.br
w     Write all cuts to separate files
.br
e     Write even numbered cuts
.br
r     Write odd numbered cuts
.br
a     Jump to previous cut
.br
s     Jump to next cut
.br
c     Clear all cuts
.br
-     Decrease histogram scale
.br
+     Increase histogram scale
.br
m     Toggle multiple/single file cut writes   
.br
n     Toggle writing of NULL packets at start of each cut
.br
g     Toggle writing of GOP broken flag at start of each cut
.br
t     Toggle time-code/frame count display
.br
F1    Toggle histogram display on/off
.br
F2    Toggle IPB button display on/off
.br
F3    Toggle frame number/timecode at top on/off
.br
F4    Toggle slider bead (visual position) on/off
.br
F5    Toggle shading and rounded corners on/off
.br
q     Quit
.br
.SH EXAMPLE USAGE
.PP
You may want to start
.B Xtscut
from a script so you can see the data in the xterm, otherwise all the stdout
will wind up on the console that started X. A script like this will work:
.IP
#/bin/bash
.br
xterm -fn 10x20 -fg white -bg black -n Xtscut -e xtscut $*

.PP
Edit a file so what you want has even numbered cuts, and hit [d] key to view
the cut-list. You can then either hit [e] to write the even cuts, or [q] key
to quit for processing the cuts later.
.PP
Scripted processing can be done to extract even cuts to one file for playback:
.IP
.B $ xtscut -ce file.ts
.PP
Where
.B file.ts
is the name of the capture file.

.SH CONFIGURATION FILE
.B Xtscut
stores the current configuration in /etc/atscap/xtscut.conf. You may
safely delete this file to revert to the compiled defaults. It will
be regenerated for you the next time you start
.B Xtscut.


.SH PERFORMANCE
The performance can be improved by cutting to a different physical
volume with the
.B -p path
option. 
.PP
Modern systems and hard drives should be capable of an aggregate read/write
throughput of 80+ megabytes per second, if the system is otherwise idle.

.SH LIMITATIONS
.B Xtscut
only works with single virtual channel/program captures. If you have a
multiplexed stream with more than one video program, you will need to
extract a single program n by using the following:
.IP
.B $ atscut -e n file.ts

.PP
Bad Sequence/Intra-frame data has been known to crash the libmpeg2 library.
Future versions of the libmpeg2 library may address this issue. Cut points
are automatically saved when selecting them, in case it does crash.

.PP
If you change the window size, it may introduce some banding or other visual
artifacts. The default window size of 960x544 gives good results. The -w0
option may be used to reset the initial video window size to the default of
960x544. If you have a 1920x1200 display, try -w1 on HD 1080i content. SD
video will not look right with the default 960x544 window size, so use 
.B -w1
option to set the correct aspect ratio.

.PP
Benchmarking with -b mode doesn't take into account the histogram render time.

.SH BUGS
.PP
I use
.B Xtscut
every day, so a lot of effort has gone into making
.B Xtscut
a fast and reliable ATSC Transport Stream cutter.
.PP
If you think you have found a bug, please send a bug report via SourceForge,
either with the bug tracking system or post to atscap-users forum.

You may find a link to the latest atscap version here:
.IP
http://atscap.sourceforge.net
.PP
The main project page is here:
.IP
http://sourceforge.net/projects/atscap/

.SH COMPATIBILITY
.B Xtscut
has mostly been tested with ATSC broadcast streams. Cable and satellite
streams may not contain the MPEG2 video as one picture per payload and
will not work with
.B Xtscut,
however some cable streams are known to work OK. MPEG4 is not supported,
but could be added in the future if someone contributes the code for it.

.PP
If you have a camcorder that outputs an MPEG2 Transport Stream that does
use one picture per payload, it may work OK with
.B Xtscut,
but you will probably want to use the -k1 option to keep the last Sequence
or else you might wonder why the last frames are missing from the output.

.PP
.B Xtscut
should work with any X11R6 server. It has been tested on X11R6V3 and
X11R6V4 from X.org. Some users have reported that it works with third-party
X servers running on other "operating systems". PMX X11R5 anyone? :>

.SH ENVIRONMENT VARIABLES
.PP
This may fix the wobbly problem associated with the Compiz Window Manager.
.B Xtscut
1.2.9 and later will set the environment variable listed below.
.PP
Thanks to Peter Knaggs for finding and reporting this fix:
.IP
$ XLIB_SKIP_ARGB_VISUALS=1 Xtscut file.ts


.SH DEPENDENCIES
.B Xtscut
depends on a few libraries (and package versions):
.br
.IP
libImlib (1.9.14) handles RGB preview for XFree86 3.x+
.br
libImlib2 (1.4.0) handles RGB preview for XFree86 3.x+
.br
libXv (4.0.3+) handles XVideo YUV preview for XFree86 4.0.3
.br
libmpeg2 (0.4.1) handles MPEG2 preview image decoding
.br
libmpeg2convert (0.4.1) handles image conversion for RGB
.br
librt clock_gettime() handles benchmarking
.br
libX11 handles X processing
.br
.PP
You should install mpeg2dec if you haven't already. You may need to install
the relevant header files (-devel or -dev packages) too. The other
libraries should have been installed by your distribution, but will also
require the various header files to get Xtscut to compile.

.SH SEE ALSO
.PP
atscap(1), atscut(1), mpeg2dec(1)

.SH AUTHOR
.PP
The author of this manual page is <inkling@users.sourceforge.net>.
.SH COPYRIGHT
.PP
This manual page is part of the atscap-1.1 distribution software.
.PP
Xtscut is Copyright (C) 2005-2008 by
.B <inkling@users.sourceforge.net>

.PP
This GPL software contains GPL software from the libmpeg2 distribution:
.br
Copyright (C) 2000-2003 Michel Lespinasse <walken@zoy.org>
.br
Copyright (C) 2003      Regis Duchesne <hpreg@zoy.org>
.br
Copyright (C) 1999-2000 Aaron Holtzman <aholtzma@ess.engr.uvic.ca>


.SH LICENSE
.PP
Permission is granted to you to copy, distribute and/or modify this software
only under the terms of the GNU General Public License, Version 2.
.PP
A copy of the license with the file name COPYING or LICENSE should have been
included in this distribution. If it was not included, or you did not receive
the source code for this software, please contact me,
.B <inkling@users.sourceforge.net>,
or write to:
.IP
 The Free Software Foundation, Inc.
 51 Franklin Street, Fifth Floor
 Boston, MA 02110-1301, USA
