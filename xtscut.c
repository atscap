#define NAME "Xtscut"
#define AUTHOR "inkling"
#define EMAIL "inkling@users.sourceforge.net"
#define WEBPAGE "http://atscap.sourceforge.net"
#define COPYRIGHT "(C) 2005-2008"
#define LICENSE "GNU General Public License Version 2"
#define LASTEDIT "20080102"
#define VERSION "1.3.5"
/*
 * xtscut.c (C) Copyright 2005-2008 by inkling@users.sorceforge.net
 *  X Transport Stream Cut Utility
 *  xtscut is a graphical visual ATSC transport stream cutting utility, with
 *  IPB frame statistics display from atscap/atscut generated .tsx files.
 *
 * xtscut is free software; you may only redistribute it and/or modify
 * it under the terms of the GNU General Public License, Version 2 or later,
 * as published by the Free Software Foundation.
 *
 * xtscut is distributed to you in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License Version 2
 * along with this program; if not, write the Free Software Foundation, Inc.,
 * at 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/*
                           Xtscut.c version 1.3.5
                              January 1, 2008
                     X Transport Stream Cutting Utility
          Copyright (c) 2004-2008 by inkling@users.sourceforge.net
*/


/*
 * Compile xtscut with:
 *
 *   gcc -Wall -O3 -lrt -lImlib2 -lmpeg2 -lmpeg2convert -lX11 \
 *      (optional) -L/usr/lib/X11R6 or -L/usr/X11R6/lib      \
 *      -o xtscut xtscut.c
 *
 * Dependencies:
 *      librt                   real time clock for timing file i/o
 *				  you may have to change defined clock_method
 *
 *      imlib-config or imlib2-config --libs will show full dependency list
 *
 *      libImlib2               ImageMagick 2 library handles X images
 *                                tested with version 1.2.1.009
 *	libImlib		ImageMagick library handles X images
 *				  tested with version 1.9.14
 *
 *      libmpeg2                mpeg2dec package
 *      libmpeg2convert         mpeg2dec package
 *                                tested with version 0.4.1
 *
 *      libX11                  Xlib generic interface
 *
 */

/*
    CREDITS:

	The MPEG2 Intra frame parse is, more or less, sample2.c from libmpeg2.
    It needed a few changes, but not a whole lot, so the authors deserve
    credit for making it flexible and explanatory enough to get the job done.
    It would have taken longer w/o libmpeg2. Thank you libmpeg2 developers!

        The Imlib and X developers also deserve many thanks for their
    good documentation and example code to get the bitmap moves done easily.
*/

/*
    SYNOPSIS:

	A different visual approach to content extraction, that tries to have
    minimal impact on stream content aside from clock discontinuities at cuts.

	This is no nifty stream re-timing tool. It splits-em-apart and
    slaps them back together, without giving any stream cut indication.

	It will be up to your player to handle it properly through the cuts.
    xine-hd 0.8 from pcHDTV.com is what I use and it handles it well enough.
    Newer version of xine may also work fine. Mplayer acts a bit odd.    

	xtscut will open a .ts file and a window to display the packet counts
    used for each of the I, P and B frames. Cuts are done on sequence starts.
    .tsx sequence files can be auto generated with atscap -m option, or
    may be generated separately with atscut -s file.ts.

	Time is estimate from frame number, is also x dimension, or width.
    Short sequences will make this number useless, but it's only for display.
    Usually, it's either nearly correct or 2x too large. Needs a frame rate
    detection routine to set the nominal frame rate to make it more accurate.

	Packet count for each frame type is y dimension, or height. Each
    pixel represents a single packet used for that frame type. Each frame
    type has a different color, but they blend together to make other colors.

	The IPB legend at the bottom may be used to select or de-select each
    frame type packet count line from the display list to help identify frames.


    Scrollwheel moves forwards (down) and backwards (up) through the file:

    	 LMB selects cut point to nearest sequence start/Intra frame
	A vertical red line is used to indicate the cut, with the
	cut number at the bottom of the line. It's a toggle. Click
	it again to remove the cut if it's in the wrong place. Cuts
	are saved and renumbered automatically.

    	 MMB moves to percentage of viewable horizontal area as view-finder
	Grey bar below IPB legend area gives a visual positional indication.

    	 RMB is the preview, it renders the Intra frame under the mouse.
	The vertical white line indicates the current preview Intra frame.

	 MB4 and MB5 are backwards and forwards 480 frames, 1/2 window width.
	Jump distance will be proportional to the current window size.
	480 frames is half of the horizontal default video size of 960x544.

    Keys (X input focus is IPB graph window, not xterm invoked from):

  Control:
    w	write all cuts
    o	write odd cuts
    e	write even cuts
    c	clear all cuts
    d	dump cuts list to stdout and cut file
    q	quit (or control C the xterm invocation)
    g	list local memory allocations (not libvo or imlib)

    F1	toggles hide histogram lines
    F2  toggles hide IPB legend and buttons
    F3	toggles hide timecodes
    F4	toggles hide visual position bar


  Navigation:
    a	jump to previous cut
    s	jump to next cut

    j	or LeftArrow moves backward one Intra frame
    k	or RightArrow moves forward one Intra frame

    i	or UpArrow moves backward about 960 frames, or 1x window width
    o	or DownArrow moves backward about 960 frames, or 1x window width

    y	or PageUP moves backward about 1920 frames, 2x window width
    o	or PageDown moves forward about 1920 frames, 2x window width

    [	or Home moves to start of stream
    ]	or End moves to end of stream


    Keys are only active when not cutting, but the X keybuf remembers them.
*/

/* NOTES:

    EXTRA FREE BONUS X BACKWARDS COMPATIBILITY LIMITATION:

	Using RGB instead of YUV overlay means it will probably run on any X.
	The default is to use imlib and not the YUV overlay. If you have a new
	system, compile it with -DUSE_LIBVO to see a 2x render speed boost.

	You might be able to compile the XVideo version with "make xtscut3".
	Don't compile the -DUSE_LIBVO version if using XFree86 4.0.2 or older.
	XVideo and XShm were not well supported until XFree86 4.0.3 and later.
	Video devices older than appoximately GeForce2 may not work with YUV12.

	It was tested back to XFree86 3.3 (a couple of years ago), but because
	of the 	simple nature of the Xlib usage, it will probably work on any
	X server from the past 10 years, if you compile the right version.

	Two-button mouse and keyboard navigation should work fine, and has
	actually been tested with old 2-button mouse and trackball.

    XLIB EXTENSION "SHAPE" MISSING ON DISPLAY
	You might see this with OpenSSH. Use ssh -Y to start the session.
	Also, make sure X config has Load "extmod" in Section "Module". [?]
	You may want to use the -w option to reduce the resolution if you
	have limited bandwidth over the connection.

    XVIDEO IS LOCAL ONLY
	XVideo uses XShm. It will only work when server is same as client.
	Use imlib1 or imlib2 versions for remote client sessions.
	For remote sessions, you will probably want to turn off the extra
	eye-candy (shading, cornering) too, by hitting F5 key.
*/

/*
    CHANGELOG:

	1.3.5 	added Eye-Candy in time for Christmas!
		added x_draw_cutlist and supporting functions
		added 8x8 rounded corner algorithm to x_draw_quad_gradientv
		added gradient to timecode area at top of histogram
		added gradient to IPB button area
		added gradient to cut number area
		added cut area indication to slider bar coloration
		added F5 key to turn off gradientv eye candy (remote session)

TODO		change slider looks bad with eye-candy off, either
		    draw all 3 lines or only draw one if arg_ec clear

		changed x_draw_histogram P & B lines use XDrawSegments
		changed special keys use X11/keysym.h
		changed [d] key uses x_draw_cutlist
		changed -k0 tests last Sequence for missing data cruft
		changed -k1 doesn't test last Sequence for missing data cruft
		changed timecode colors for odd cuts is now red
		changed timecode font size to 10x20, format to hh:mm:ss
		changed timecode spacing to 120 frames to fit font+format

		disabled stdout show_scuts

		fixed input scan failed on very short file
		fixed draw_histogram case 1: missing break, I/P frame wrong
		fixed short streams were not starting render at frame 0
		fixed frames[].cut not cleared, confused find prev/next cut

		removed USE_BUFFER and buffer.h related code

------------------------------------------------------------------------------

	1.3.4	added show allocs to view local memory allocations
		added [g] key toggles GOP broken_link setting (is slower)
		added frames, iframes, and sequences use dynamic allocation
		added error return checking to alloc frames
		added input file extension check for '.e*' for Video ES
		added open/close buffer function, calls init/flush
		added parse ts header function and tsh_t typedef
		added x_draw_slider bead visual stream position feedback
		added -g option run test GOP broken link at start of cut
		added cursor pad and numpad keys for navigation
		added F1-F4 keys to toggle: histogram, IPB, timecodes, slider
		added load/save_config for /etc/atscap/xtscut.conf
		added x_draw_quad_gradientv for shaded fill quadrangles
		added show_keyhelp and show_cfghelp video overlays

TEST		added -z option to set buffer sizes, 188 to 128k bytes
		    [may be useless, 4096 always seem to be optimal]

		changed -n option not given strips (most) NULLs from output
		changed PSI0 audio packets at start of cut NULL'd or skipped
		changed test GOP broken link to work with larger buffers
		changed input scan uses parse ts header
		changed input scan looks for video and audio start codes
		changed build tsx uses parse ts header
		changed parse video es uses parse ts header
		changed read/write code to use buffers larger than 188 bytes
		changed [n] toggles nulls, [m] toggles multi/single write
		changed [j]/[k] now 1 Intra skip

		disabled libvo XShm and non YV12 XVideo modes, temporarily
		[YV12 gets by without mpeg2_convert, XShm and YUYV need it]

TODO		fix x resize display so keys 1 2 4 8 and 0 set display size
TODO		 and fix maximize to set non-native image aspect and offsets

		fixed button select can get two buttons at once
		fixed cut numbering wasn't correct, should not start at 0.

		removed buffer.h code to optional compile (is slower)
		removed -y option, uses input file extension .e* check now
		removed shifted navkeys, due to lack of use
		removed shifted option key duplicates, also unused
		removed +/- 30/60/90 frame navkeys

	1.3.3	added -y option for Video Elementary Stream (file.es)
		added build fsx, build tsx, build esx
		added generic buffer code in buffer.h
		added lltoasc to show scuts

		changed ts/es file I/O to use buffer.h functions
		changed 'system atscut -s' is fallback, uses build_fsx now

		fixed write disabled when input and output are same file
		fixed GOP broken link wasn't being set at start of cuts

	    Peter Knaggs sent in this modern system benchmark for XvYV12:
	     128 Intra frames in 3.06 seconds at 41.83 FPS   WOW!


	1.3.2	added [b] key for NULL blanks at start of each cut
		added discard Xvideo buffers to x close_display
		added mpeg2dec/libvo/video_out_x11 for faster video render
		    Benchmarks for 1280x720p on 1.6 GHz ATSC test system:
			    imlib1  IFPS 8
			    imlib2  IFPS 13
			    XvYV12  IFPS 23 WOW! (-m1 is default)

		changed back to [n]/[m] keys for Intra frame single-step
		changed Makefile:
		    make xtscut for new imlib2 version (is new default)
		    make xtscut1 for old imlib1 version (slowest)
		    make xtscut2 for new imlib2 version (medium)
		    make xtscut3 for new XVideo version (fastest & buggy?)


TODO		export signal and console code from atscap
TODO		fix missing PNG images for XVideo compiles (needs imlib2)
TODO		verify XVideo + libmpeg2 renders directly to video memory


	1.3.1	changed [w] key, -ca option writes multiple files [ivary]
REVERSED	changed [n] key to null toggle, single iframe step is [,][.]
		changed NULL packet generation code
		changed show scuts counts the NULLs if -n option used
		changed write block subs NULLs for initial PSI0 audio packets


	1.3.0	added support for imlib2, 10-25% faster render with -b
		added -b option for benchmark test of imlib1 vs imlib2
		added build outname function
		added [1] key for single/multi file write toggle
		added abort write and warn user if output same as input
		added button1 toggles legend area timecode/frame rate
		added define for librt clock_gettime and fallback

		changed start and stop timer to be more generic if no librt
		changed MMB is disabled for short streams with frames < width
		changed read_buffer switch cleanup
		changed how and where xtscut end png is rendered
		changed [n] & [m] keys use find prev/next_iframe function

		fixed x close display memory leak, not freeing image
		fixed test clock res had wrong clock method string indices
		fixed Makefile to try to use imlib1/2-config scripts
		fixed find nearest_iframe sometimes returns 0 [ivary]
		fixed preview on 'The End' doesn't redraw image after cut

		testing x_resize_display, aspect is OK, but has render bugs.
		    Need to scale the image w/o changing vw.* and leave
		    vw.* for window control only

		TODO? build outname should create new name if output exists


	1.2.9	added option -k to keep last sequence via fake last I-frame
		added option -a to set mpeg2 accel mode (default autodetects)
		added PNG_PATH define to set path for png files
		added pngs for splash, bad frame and end of stream
		added -n outputs 1/8s NULL packets to start of each cut
		added [a] key shows previous cut
		assed [s] key shows next cut
		added [t] key toggles frame count/time code

		changed global mpeg2 init and structures
		changed -n outbasename is now -o outbasename
		changed frame 0 cut selection disabled
		changed frame histogram stops at last I-frame line + 2
		changed display xtscut-end for last I-frame
		changed short stream starts preview at frame 0

		fixed generational loss of last sequence with -k option
		fixed skip initial P and B frames until first I-frame
		fixed repetitive redraws when scrolling hits container limits

		fixed disable x events debug code discarding events [rich123]


	1.2.8   added setenv "XLIB_SKIP_ARGB_VISUAL=1" [Peter Knaggs]
		added x button quad for mouse button position test
		added aspect ratio of source to GC setup
		added white line as current preview location
		added show current preview frame number above IPB legend
		added -w option to set intial screen width
		added xrender hold down to x_redraw_display

		changed button test logic is a bit cleaner
		changed ignore mouse events in frame count area above IPB
		changed button2 uses floating point to calc frame offset
		changed cut line is white if current preview frame
		changed X event VisibilityChangeMask to FocusChangeMask
		changed Expose event handling to conditional complile

		removed redundant calls to x_redraw_display

TODO		closer button and event handler for it [is it really needed?]
TODO		time-code toggle to frame number? [done in 1.3.0]
TODO		full-screen toggle is still broken, fix it or scrap it?
		    [slightly better results in 1.3.0, but still broken]


	1.2.7   added track last frame drawn to hold down parse intra_frame
		added bit rate scaling to histogram lines
		added write nulls to start of stream to help player sync

		fixed coarse navkeys do not redraw near start/end [ivary]
		

	1.2.6   added frame count for cuts and totals
		added frame rate calc from libmpeg2 info structure
		changed show ss_timecode to show frame_timecode
		changed timecode colors for odd cuts
		fixed input scan fails with certain test streams with nulls


	1.2.5	fixed -ce not writing even cuts, missing build scut_frames
		fixed draw cutnum uneven and obscured by IPB controls


	1.2.4	added percentage of source file for cuts and totals
		added save cuts each time a cut is inserted or deleted
		fixed crash on last sequence preview, can still select as cut
		changed whitespace
		

	1.2.3	added x_set_font
		changed x draw_time called every 2s of frames, not 4 i-frames
		changed x draw_cutnum larger font


	1.2.2	changed show_ss_timecode so [d]ump cuts is more accurate
		changed volume status checks arg_path free space


	1.2.1	disabled [f] key full screen because it's still too buggy


	1.2.0	added write patpmt and fixed write scuts to use it
		added payload start hold downs for cuts to smooth audio start
		changed input scan to autodetect of PAT + PMT pids
		fixed -p path wasn't being used for -1 multiple file cuts
	

	1.1.9	added system call to atscut -s if .tsx file missing
		added input scan autodetect video PID
		changed -v option is version instead of video PID
		
		TEST: -p -n from read-only source directory. will fail?
		
		Final throughput testing results for PATA 300G Maxtors:
		  Testing cuts to same volume ~ 30-40MB/second, EXT3 & XFS.
		  Testing XFS to EXT3 maxes out around 60MB/second.
		  Testing EXT3 to XFS maxes out around 80MB/second.
		  Testing XFS to XFS maxes out around 111MB/second. WOW!


	1.1.8	added [c] key to clear all cuts


	1.1.7	added cuts have gop broken link set if closed gop not set
		fixed cut from gui needs i/o block index reset. -ce is OK.


	1.1.6	added delays in lieu of duty-cycle limiter to output


	1.1.5	remap keys: o to r; y/o now 2x u/i jump, shift does 2x more
		added test clock res to set more accurate clock method


	1.1.4	fixed IPB legend/buttons above cut number, not covering it
		fixed -1 and even or odd cut write only wrote one file
		added -co/e/a option, if .tsc file exists, cut and exit
		added -r cut renumber option for 01 02 03 from odd/even


	1.1.3	added limit check for input sequences and frames


	1.1.2	reduced gcc pre-processor million item list with alloc frames
		use malloc not calloc to hold down page commit until use


	1.1.1	nfs read performance far exceeds samba read performance:
		added -o -n output options for read only media.


	1.1	changed to single .tsx file after changing atscap and atscut


	1.0r3	gcc audit: cleanups for -pedantic -std=c99 and gcc -ansi


	1.0r2	added -b output basename [changed to -n in 1.1.1]


	1.0r1	trimmed cruft for addition to atscut, which may not get done


	1.0	added buffered i/o for cut writes and video preview, faster now


	0.9	added MMB, added .cut save to [d]ump, fixed save, fixed expose

20060105 renamed to xtscut
	0.8	added MPEG2 Intra render behind histogram + RMB, fixed mem leak

	0.7	added load and save .cut file, even/odd/all save keys

20060106 merge of frames.c and ipb.c codebases
	0.6	added cut number to bottom of red Intra frame cut line

20051227 ipb.c theory works
	0.5	imported -i ipb cuts from atscut; added LMB mouse cut toggle

	0.4	added mouse buttons (4, 5) for scrollwheel support

	0.3	added legend buttons to toggle display of each frame type

20051222 ipb.c
	0.2	show I, P and B frames as colored lines, with times at top

	0.1	one histogram line per I frame. looks useful already!
*/


/* NOTES:   GOP broken_link and closed_gop

	#define USE_GOP_BROKEN_LINK

	The worst case scenario is user sees junk in video at cut point.
    Actually, the user saw a lot of junk TV way before the cut point. :>

	Editors should set Broken GOP flag at cut point to trigger decoder to
    reset the internal macroblocks to reduce artifacting at cut point.
    
	If you cut at fade to black this artifact problem may not appear.
    It all depends on what (last) next two B frames look like. GOP broken_link
    set tells decoder to ignore next B frames until a P frame. GOP closed_gop
    set tells decoder it's OK to use the B frames: one or other will be set.
    The spec doesn't say 'mutually exclusive' but the practice seems to be.

	The spec for these two bits seems obtuse, but the general idea is that
    the GOP broken_link flag is the one that should purge junk frames on cuts.
    Whether or not various decoders actually honor GOP broken_link is unknown.
    Hardware decoders may see less bad blocks with GOP broken_link flag set.
*/


/*
    TODO:

	This code does not set the Discontinuity Flags in the TS Adaptation
    Field Control, at the cut point, so it may be non-compliant MPEGv2-wise.
    It does try to set the GOP broken_link bit if closed_gop bit is clear.
    I may experiment with inserting 1 video packet at the start of each cut
    that has AFC = 2 and only a Discontinuity Flag as the payload.

     [ AFC Discontinuity Flag tells the decoder to use the new time, like DVD.
       Most players seem to handle getting the new time from the stream, so
       playing with the AFC DF bit is, maybe, not necessary. ]


	Buttons for the following:

	Threshold select/clear all cuts/process cuts(write even odd)/display:
    These may not be needed. Threshold set from mouse input could be useful.
*** [Threshold is for long abandoned auto-cutting idea, remove references]

	Status inside X frame for the following:
    During writes, file name and current sequence number updated in frame.
    Showing current cut frame is not useful because it should be blank. DOH!
    Maybe show 15 frames ahead instead.


	nramsey requested these:
DONE	    keys for nearest cut to left or right: [a]/[s] keys

	    .tsc file as ascii text, nl delimited for each cut

	    Can X detect shift key press as scroll wheel accelerator?
	    [ Better yet, can another key serve as mouse buttons? ]	    
*/


/* LIMITATIONS:

	There used to be a limit on the number of hours of video.  That
    limit has been removed with the new dynamic frame data allocation.
    The .tsx file generation for a 4 hour capture will take a while. If the
    atscap -m generated .tsx file is wrong, delete it to build a new one.

	It may crash on bad streams. Jumping around with the MMB to get part
    of the stream may help, but it's usually pointless on really bad captures.
    Other than making libmpeg2/mpeg2dec more robust there is no real solution.
    If the data isn't there, it isn't there and it's difficult to handle this.

	Good antennas that will help you receieve better are the easiest fix.
    Your current antenna may be good enough but only needs better placement.
*/


/* makes large files work right on 32 bit machines */
/* #define GNULINUX */

#define __USE_LARGEFILE64
#define __USE_FILE_OFFSET64
#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE

/* always try to set GOP broken_link */
#define USE_GOP_BROKEN_LINK
#define USE_TS_FIX

#include <features.h>

#ifdef USE_PLATFORM_H
#ifndef PLATFORM_NAME
#include <platform.h>
#endif
#endif

#ifndef PLATFORM_NAME
#define PLATFORM_NAME "Linux"
#endif

#define FILE_RMODE O_RDONLY
#define FILE_WMODE O_RDWR | O_CREAT | O_TRUNC
/* umask 022 makes this 0644 */
#define FILE_PERMS 0666

#define PNG_PATH "/dtv/pg/img/"

/* debug the MPEG2 parsing in build_esx/tsx */
#define PIC_DEBUG

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <sys/ioctl.h>
#include <inttypes.h>
#include <time.h>
/* #include <math.h>  */

/* libX* needs these */
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/cursorfont.h>
#include <X11/keysym.h>

#ifdef USE_LIBVO
/********************************************************** libmpeg2 libvo */

#include <inttypes.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <X11/extensions/XShm.h>
#include <X11/extensions/Xvlib.h>
#define FOURCC_YV12 0x32315659
#define FOURCC_UYVY 0x59565955
/* since it doesn't seem to be defined on some platforms */
int XShmGetEventBase (Display *);

/* slightly modified video_out_x11.c */
#include "video_out.h"
/********************************************************** libmpeg2 libvo */
#endif /* USE_LIBVO */

/* both Imlib1/2 need this? */
#include <X11/extensions/shape.h>

#ifdef USE_IMLIB2
#include <Imlib2.h>
#endif

#ifdef USE_IMLIB1
#include <Imlib.h>
#endif

/* libmpeg2 needs these */
#include <mpeg2dec/mpeg2.h>
#include <mpeg2dec/mpeg2convert.h>

/* statfs for volume free space */
#include <sys/vfs.h>
/****************************************************************** macros */

#define xprintf if (0 != arg_xmsg) fprintf
#define iprintf if (0 != arg_imsg) fprintf
#define dprintf if (0 != arg_dmsg) fprintf
#define WHOAMI if (0 != arg_fmsg) fprintf( stdout, "%s\n", __FUNCTION__ )

#define WHO (char *) __FUNCTION__
#define WHERE (int) __LINE__

/* 24hrs at 60fps, maximum page allocation will be around 45mb */

#define SCUT_MAX 1000

#define CUT_NONE 0
#define CUT_ALL 1
#define CUT_EVEN 2
#define CUT_ODD 3

#define MPEG_NULL 0x1FFF
#define MPEG_PAT 0
#define MPEG_PMT 2

#define MPEG_SYN 0x47
#define MPEG_PES 0xE0
#define MPEG_UPS 0xB2
#define MPEG_SEQ 0xB3
#define MPEG_EXT 0xB5
#define MPEG_GOP 0xB8
#define MPEG_PIC 0x00
#define A52_PS1  0xBD
#define ESZ 184
#define TSZ 188
//#define TSBZ (21 * TSZ)


/****************************************************************** globals */

/* reused for .tsx .tsc, .esx, .esc */
int in_file;
static char in_name[256] = "";
static ino_t in_inode;

/* used to save cuts */
int out_file;
static char out_name[256];
static char out_base[256];
static ino_t out_inode;
static long long in_size;

long long bytes_in;
long long bytes_out;
long long bytes_total;

static char *fxt[2] = { "ts", "es" };
static char *fxp = NULL;

static char pictypes[4] = "?IPB";
struct in_frame_s {
	int pct; /* picture coding type */
	int vpn; /* video packet number */
};

/* one entry for input file transform to larger structure */
/* static struct in_frame_s in_frame; */
/* static long long in_seq; */

/* 16 bytes */
typedef struct {
/* from input file */
	unsigned char pct;	/* picture coding type */
	unsigned char cut;	/* non-zero is a cut point */
	unsigned char cutnum;	/* build scut_frames computes */
	unsigned char tcolor;	/* non-zero is frame in odd cut, timecode rv */
	int vpn;		/* picture start video packet number */
	int fpc;		/* frame packet count */
	int num;		/* frame number for this picture type */
} frame_t ;

//typedef long long sequence_t;
//typedef int iframe_t;

struct timer_s {
	struct timespec start;
	struct timespec stop;
	struct timespec diff;
};

struct timer_s timer_gui;
struct timer_s timer_cut;
struct timer_s timer_bif;
struct timer_s timer_fsx;

static frame_t *frames;
static int *iframes;
static long long *sequences;

static int frame_idx;
static int iframe_idx;
static int sequence_idx;

static int ipbx[4];

/* these are set by sequence parse */
static unsigned int frame_rate = 0;
static unsigned int frame_period = 0;
static unsigned int byte_rate = 0;
static unsigned int bit_rate = 0;
static unsigned int null_pkts = 0;

static double hist_scale = 0.5;
//static int hist_hpn = 0;

/* each scut entry has index to sequence[] */
static int scuts[ SCUT_MAX ];
static int scut_idx = 0;
static int scut_done = 0;

/* 0 no cuts, 1 is all [w], 2 even scuts [e], 3 odd scuts [o] */
static int cut_type = 0;


static int nif = 0;			/* number of i frames rendered */

char *votypes[] = {	"imlib1 RGB",
			"imlib2 RGB",
			"XShm RGB",
			"XVideo YV12",
			"XVideo YUYV"
};

typedef struct {
/* TS header order */
	unsigned char tei;	/* Transport Error Indicator     1 bit */
	unsigned char psi;	/* Payload Start Indicator       1 bit */
	unsigned char pri;	/* Priority (unused)             1 bit */
	unsigned short pid;	/* Packet ID                    13 bits */
	unsigned char tsc;	/* Transport Scramble Control    2 bits */
	unsigned char afc;	/* Adaptation Field Control      2 bits */
	unsigned char cc;	/* Continuity Counter            4 bits */
	unsigned char afb;	/* Adaptation Field byte count   8 bits */

	unsigned char pso;	/* computed: TS packet payload start index */
	unsigned char psb;	/* computed: TS payload segment byte count */
} tsh_t;
static tsh_t tsh;

/************************************************************ CLI arguments */
#ifdef USE_LIBVO
static int arg_vo = 1;			/* default 0 x11, 1 xv, 2 xv2 */
#endif /* USE_LIBVO */
static int arg_hl = 0;			/* F1 histogram on or off */
static int arg_lb = 0;			/* F2 legend button toggle */
static int arg_tc = 0;			/* F3 timecode at top toggle */
static int arg_sb = ~0;			/* F4 visual position toggle */
static int arg_ec = ~0;			/* F5 eye candy toggle */
static int arg_ft = ~0;			/* frame code or time code */
static int arg_wd = 0;			/* video window divisor */
static int arg_es = 0;			/* 0 is de-packetize input */
static int arg_gop = 0;			/* default sets GOP broken_link */
static int arg_bfr = 0;			/* benchmark iframe render test len */
static int arg_kls = 0;			/* don't discard last sequence */
static int arg_mbz = 4096;		/* buffer size */
static int arg_jump = 0;		/* unused, start at this frame */
static int arg_xmsg = 0;		/* zero to disable x event text */
static int arg_imsg = 0;		/* frame IPB scan verbosity */
static int arg_dmsg = 0;
static int arg_fmsg = 0;		/* function name flow */
static int arg_nulls = 0;		/* nulls at start of cuts? */
static int arg_delay = 0;
static unsigned int arg_mx = MPEG2_ACCEL_DETECT; /* FPU select for IDCT */
static unsigned short arg_vpid = 0;
static unsigned short arg_apid = 0;

static char arg_base[256] = "";		/* basename */
static char arg_path[256] = ".";	/* output path, default is cwd */
/* [path]filename on command line */
/*
static unsigned char arg_name[256];
*/

#if 0
/* -i thresholds */
static int arg_ti = 265;
static int arg_tp = 64;
static int arg_tb = 64;
#endif

static char ** arg_v;
static int arg_c;

static int arg_cut = CUT_NONE;		/* -cx cut without gui */
static int arg_renum = 0;		/* -r renumber cut option */
static int arg_one = ~0;		/* -1 clears for multiple cuts */
static int arg_alpha = 0xFF;		/* alpha blend opacity, 0 - 255 */
static int pkt_vid = 0;
static int pkt_num = 0;

static unsigned int accel;		/* return from mpeg2_accel */
char *x86mx[] = { "NONE", "MMX", "3DNOW", "3DNOW+MMX",
		"MMX2", "MMX2+MMX", "MMX2+3DNOW", "MMX2+MMX+3DNOW" };

static unsigned char *ts_buf;
static unsigned char *es_buf;

#ifdef USE_LIBRT
//static clockid_t clock_method;
#endif

static char pat[188];		/* PAT is always one packet */
static char pmt[188*2];		/* PMT is usually one packet. KQED has two */
static int psib[0x2000];	/* payload start indicators by pid */
static short pmt_pid = 0;	/* input scan sets this */
static char pmt_pkts = 0;	/* input scan sets this */




//static int frame_threshold;	/* RMB sets, [a] [s] keys jump forward/back */


/******************************************************** X related globals */

static int uoffset = 0;				/* user frame offset */
static int xoffset = 0;				/* histogram draw offset */
static int coffset = 0;				/* current histogram offset */
static int toffset = 0;				/* target histogram offset */
static int xredraw = ~0;
static int xumove = ~0;				/* user moved position */
static int xlfd = 0;				/* last frame displayed */
static int xcfd = 0;				/* current frame displayed */

/* button area */
static int bax;
static int bay;
static int baw;
static int bah;

/* IPB button areas */
static int bxi; /* button x i */
static int bxp; /* button x p */
static int bxb; /* button x b */
static int bys; /* button y start */
static int byh; /* button y height */
static int tys; /* timecode y start */

/* IPB button color/line visible status */
static int bis = ~0;
static int bps = ~0;
static int bbs = ~0;

static int xinit = 0; /* X11 initialized */
static int m2init = 0; /* libmpeg2 initialized */
char *dispname = ":0";
static Display *xdisplay;
static Window xwindow;
static int xscreen; /* X screen from init */
static GC xgc;

static XWindowAttributes xattr;
static XEvent xev; /* generic event */
static unsigned long sattrmask;
static XSetWindowAttributes sattr;

#ifndef USE_LIBVO
static Visual *xvisual;
static XVisualInfo vinfo;
static Colormap xcmap;
static int xtbh = 0; /* X title bar height */
static XGCValues xgcv;
#endif

static int xbpp = 0; /* X bits per pixel depth */
static unsigned int xfg, xbg; /* x foreground and background colors */
static XFontStruct * xfonts;
static XSizeHints xhint;
static char xtitle[256];
//static Cursor fscur;

unsigned int *xcolors;
unsigned long expose_serial;	/* serial number of last expose event */

#define COLOR_BLACK   xcolors[8]
#define COLOR_GREY0   xcolors[9]
#define COLOR_GREY1   xcolors[0]
#define COLOR_WHITE   xcolors[7]
#define COLOR_CYAN    xcolors[6]
#define COLOR_MAGENTA0 xcolors[10]
#define COLOR_MAGENTA xcolors[2]
#define COLOR_YELLOW  xcolors[4]
#define COLOR_RED     xcolors[3]
#define COLOR_GREEN0   xcolors[11]
#define COLOR_GREEN   xcolors[5]
#define COLOR_BLUE    xcolors[1]

/* 16 bit visual primary color index */
unsigned int xcolors565[] = {
    (0x07 << 11) | (0x0F << 6) | 0x07, /* Bright Grey */
    (0x00 << 11) | (0x00 << 6) | 0x1F, /* Bright Blue */
    (0x1F << 11) | (0x00 << 6) | 0x1F, /* Bright Magenta */
    (0x1F << 11) | (0x00 << 6) | 0x00, /* Bright Red */
    (0x1F << 11) | (0x3F << 6) | 0x00, /* Bright Yellow */
    (0x00 << 11) | (0x3F << 6) | 0x00, /* Bright Green */
    (0x00 << 11) | (0x3F << 6) | 0x1F, /* Bright Cyan  */
    (0x1E << 11) | (0x3E << 6) | 0x1E, /* Bright White */
    (0x00 << 11) | (0x00 << 6) | 0x00, /* Black */
    (0x03 << 11) | (0x03 << 6) | 0x03, /* Dark Grey */
    (0x0F << 11) | (0x00 << 6) | 0x0F, /* Mid Magenta */
    (0x00 << 11) | (0x1F << 6) | 0x00, /* Mid Green */
};

/* 24/32 bit visual primary color index */
unsigned int xcolors888[] = {
    (0x80 << 16) | (0x80 << 8) | 0x80, /* Brigt Grey */
    (0x00 << 16) | (0x00 << 8) | 0xFF, /* Bright Blue */
    (0xFF << 16) | (0x00 << 8) | 0xFF, /* Bright Magenta */
    (0xFF << 16) | (0x00 << 8) | 0x00, /* Bright Red */
    (0xFF << 16) | (0xFF << 8) | 0x00, /* Bright Yellow */
    (0x00 << 16) | (0xFF << 8) | 0x00, /* Bright Green */
    (0x00 << 16) | (0xFF << 8) | 0xFF, /* Bright Cyan */
    (0xFF << 16) | (0xFF << 8) | 0xFF, /* Bright White */
    (0x00 << 16) | (0x00 << 8) | 0x00, /* Black */
    (0x40 << 16) | (0x40 << 8) | 0x40, /* Dark Grey */
    (0x7F << 16) | (0x00 << 8) | 0x7F, /* Mid Magenta */
    (0x00 << 16) | (0x7F << 8) | 0x00, /* Mid Green */
};

struct vw_s {
    int x;
    int y;
    int w;
    int h;
};

char *xevtypes[] = {
 "Event0",
 "Event1",
 "KeyPress",
 "KeyRelease",
 "ButtonPress",
 "ButtonRelease",
 "MotionNotify",
 "EnterNotify",
 "LeaveNotify",
 "FocusIn",
 "FocusOut",
 "KeymapNotify",
 "Expose",
 "GraphicsExpose",
 "NoExpose",
 "VisibilityNotify",
 "CreateNotify",
 "DestroyNotify",
 "UnmapNotify",
 "MapNotify",
 "MapRequest",
 "ReparentNotify",
 "ConfigureNotify",
 "ConfigureRequest",
 "GravityNotify",
 "ResizeRequest",
 "CirculateNotify",
 "CirculateRequest",
 "PropertyNotify",
 "SelectionClear",
 "SelectionRequest",
 "SelectionNotify",
 "ColormapNotify",
 "ClientMessage",
 "MappingNotify",
 "LASTEvent"
};


typedef struct {
    int w;
    int h;
    char *name;
} fnt_t;

/* pcf fonts found in /usr/share/fonts/misc */
fnt_t xpcf[] = {
 { 12, 24, "12x24" },
 { 10, 20, "10x20" },
 {  9, 18, "9x18" }, // 9x18 is broken here
 {  9, 15, "9x15" },
 {  8, 16, "8x16" },
 {  8, 13, "8x13" },
 {  7, 14, "7x14" },
 {  7, 13, "7x13" },
 {  6, 13, "6x13" },
 {  6, 12, "6x12" },
 {  6, 10, "6x10" },
 {  6,  9, "6x9" },
 {  5,  8, "5x8" }
};
static int xpcf_idx = 13;
static int xhelp = 0;

#define MPEG2_STAT_MAX 12
char *m2stat[] = {
 "BUFFER",
 "SEQUENCE",
 "SEQUENCE_REPEATED",
 "GOP",
 "PICTURE",
 "SLICE_1ST",
 "PICTURE_2ND",
 "SLICE",
 "END",
 "INVALID",
 "INVALID_END",
 "??"
};


/* default window size, works equally well for 720p or 1080i w/o artifacts */
static struct vw_s vw  = { 0, 0, 960, 544 };

#ifdef USE_MAXIMIZE
/* FIXME: maximize is broken */
/* maximize saves previous window size here */
static int xmaxi = 0;
static struct vw_s vw0 = { 0, 0, 0, 0 };
#endif

/* decoded Intra frame that is put in window before histogram drawn */
#ifdef USE_IMLIB2
#warning using Imlib version 2
static Imlib_Image *image = NULL;	/* temp mpeg render image */
#endif

#ifdef USE_IMLIB1
#warning using Imlib version 1
static ImlibImage *image = NULL;
static ImlibData *idata = NULL;
#endif
/************************************************************ X globals END */

/* libmpeg2 structures */
mpeg2dec_t * m2dec;
const mpeg2_info_t * m2inf;
const mpeg2_sequence_t * m2seq;
mpeg2_state_t m2sta;

/* nanosleeps */
struct timespec event_loop_sleep = { 0, 1 };	/* X event loop sleep */
struct timespec write_sleep = { 0, 77 };	/* write sleep -d option */
struct timespec splash_sleep = { 5, 0 };	/* splash sleep */
struct timespec endimg_sleep = { 0, 500000000 };/* splash sleep */

static unsigned int framew = 1920;
static unsigned int frameh = 1088;

static double aspectf = 0.0;

#ifdef USE_CONSOLE
static unsigned char ckey;		/* last cooked console key */
#endif

static unsigned int sc;			/* MPEG2 start code */

static char keyhelp[4096] = "Key Help";
static char cfghelp[4096] = "Config Help";
static char cutlist[8192] = "\n No cuts selected yet! \n\n";
static char outlist[8192] = "\n Nothing written yet! \n\n";

/************************************************************ GLOBALS END */


/*********************************************************** functions BEGIN */

/************************************************************* prototypes */
static void x_event( void );
static void c_exit( int );
/************************************************************* prototypes */

#include "common.h"

static char date_now[64];


static
void
get_time ( void )
{
	time_t tnow;
	struct tm tloc;
	tnow = time( NULL );
	localtime_r( &tnow, &tloc );
	asctime_r( &tloc, date_now );
}


/* write configuration to a text file */
static
void
save_config ( void )
{
	FILE *f;
	char *n = "/etc/atscap/xtscut.conf";
	char *t0 = "not ";
	char *t1 = "";

	WHOAMI;

	f = fopen(n, "w");
	if (NULL == f) {
		fprintf(stdout, "%s could not open %s\n", WHO, n);
		perror("");
		return;
	}

	get_time();

	fprintf(f, "# "NAME"-"VERSION"-"LASTEDIT" configuration saved on %s",
		    date_now);

	fprintf(f, "# Binary options, 0=off/1=on\n");
	fprintf(f, "H%d\t\t# Histogram lines are %svisible\n",
		    1 & arg_hl, (arg_hl)? t1:t0);

	fprintf(f, "L%d\t\t# IPB Buttons are %svisible\n",
		    1 & arg_lb, (arg_lb)? t1:t0);

	fprintf(f, "T%d\t\t# Timecodes are %svisible\n",
		    1 & arg_tc, (arg_tc)? t1:t0);

	fprintf(f, "E%d\t\t# Eye-Candy is %srendered\n",
		    1 & arg_ec, (arg_tc)? t1:t0);

	fprintf(f, "F%d\t\t# Timecodes are Intra frame %s\n",
		    1 & arg_ft, (arg_ft) ? "mmm:ss":"counts");

	fprintf(f, "S%d\t\t# Slider bead is %svisible\n",
		    1 & arg_sb, (arg_sb)? t1:t0);

	fprintf(f, "G%d\t\t# GOP broken_link will %sbe set\n",
		    1 & arg_gop, (arg_gop)? t1:t0);

	fprintf(f, "N%d\t\t# NULL packets will %sbe written\n",
		    1 & arg_nulls, (arg_nulls)? t1:t0);

	fprintf(f, "D%d\t\t# Writes will %shave delays\n",
		    1 & arg_delay, (arg_delay)? t1:t0);

	fprintf(f, "K%d\t\t# Last Sequence %s kept\n",
		    1 & arg_kls, (arg_kls)? "always":"may not be");

	fprintf(f, "R%d\t\t# Cuts will %sbe renumbered\n",
		1 & arg_renum, (arg_renum)? t1:t0 );

	t0 = "OFF";
	t1 = "ON";

	fprintf(f, "I%d\t\t# Intra frame lines are %s\n",
		    1 & bis, (bis)? t1:t0);

	fprintf(f, "P%d\t\t# Predictor frame lines are %s\n",
		    1 & bps, (bps)? t1:t0);

	fprintf(f, "B%d\t\t# Bidi frame lines are %s\n",
		    1 & bbs, (bbs)? t1:t0);

	fprintf(f, "M%d\t\t# Cuts will go to %s file%s\n",
		1 & arg_one, (arg_one)?"one":"multiple", (arg_one)?"":"s");

	fprintf(f, "# Non-binary options\n");

	fprintf(f, "A0x%08X\t# libmpeg2 acceleration bits (0x%X autodetect)\n",
		arg_mx, MPEG2_ACCEL_DETECT);

	fprintf(f, "O%s\t\t# Output path\n", arg_path);

	fprintf(f, "Q%f\t# Histogram scale, float 0.0 to 1.0\n", hist_scale);

//	fprintf(f, "U0x%02X\t\t# Histogram alpha blend opacity\n", arg_alpha);
//	fprintf(f, "J%d\t\t# Jump to last Intra displayed\n", xcfd);

	fprintf(f, "W%d\t\t# Video size divisor, or 0 default\n", arg_wd);

	fprintf(f, "X%d\t\t# Video X axis dimension\n", vw.w);

	fprintf(f, "Y%d\t\t# Video Y axis dimension\n", vw.h);

	fprintf(f, "Z%d\t\t# Buffer size\n", arg_mbz);

	fprintf(f, "\n");

	fflush(f);

	fclose(f);
}


static
void
load_config( void )
{
	FILE *f;
	char b[80];
	char *n = "/etc/atscap/xtscut.conf";
	char *r, *p;

	WHOAMI;

	f = fopen(n, "r");
	if (NULL == f) return;

	memset(b, 0, sizeof(b));
	r = n;
	while (1) {
		r = fgets( b, sizeof(b), f );
		if (NULL == r) break;
		if (0 == *r) break;

/* strip NL, TAB, comment and space */
		p = strchr(r, '\n');
		if (NULL != p) *p = 0;

		p = strchr(r, '\t');
		if (NULL != p) *p = 0;

		p = strchr(r, '#');
		if (NULL != p) *p = 0;

		p = strchr(r, ' ');
		if (NULL != p) *p = 0;

		if (0 == *r) continue;

		p = r;
		p++;

		switch (*r) {

/* 32 bits, top bit is auto-detect, rest of bits are force certain FPU type */
		case 'A':
		    p += 2;
		    sscanf( p, "%08X", &arg_mx);
		    break;

		case 'B':
		    bbs = atoi(p);
		    break;

		case 'C':				/* unused */
		    break;

		case 'D':
		    arg_delay = 1 & atoi(p);
		    break;

/* eye candy toggle, enabled uses more CPU, slows navigation a little bit */
		case 'E':
		    arg_ec = 1 & atoi(p);
		    break;

		case 'F':
		    arg_ft = 1 & atoi(p);
		    break;

		case 'G':
		    arg_gop = 1 & atoi(p);
		    break;

		case 'H':
		    arg_hl = 1 & atoi(p);
		    break;

		case 'I':
		    bis = 1 & atoi(p);
		    break;

		case 'J':				/* broken */
		    arg_jump = atoi(p);
		    break;

		case 'K':
		    arg_kls = 1 & atoi(p);
		    break;

		case 'L':
		    arg_lb = 1 & atoi(p);
		    break;

		case 'M':
		    arg_one = 1 & atoi(p);
		    break;

		case 'N':
		    arg_nulls = 1 & atoi(p);
		    break;

/* output path */
		case 'O':
		    if (0 != *p) {
			memset(arg_path, 0, sizeof(arg_path));
			strncpy(arg_path, p, sizeof(arg_path)-1);
			arg_path[ sizeof(arg_path)-1 ] = 0;
		    }
		    break;

		case 'P':
		    bps = 1 & atoi(p);
		    break;

		case 'Q':
		    hist_scale = atof(p);
		    break;

		case 'R':
		    arg_renum = 1 & atoi(p);
		    break;

		case 'S':
		    arg_sb = 1 & atoi(p);
		    break;

		case 'T':
		    arg_tc = 1 & atoi(p);
		    break;

/* Alpha blend is currently not implemented. It may use too much CPU. */
		case 'U':
		    arg_alpha = 0xFF & atoi(p);
		    break;

		case 'V':				/* unused */
		    break;

		case 'W':
		    arg_wd = 15 & atoi(p);
		    if (0 == arg_wd) { vw.w = 960; vw.h = 544; }
		    break;

		case 'X':
		    vw.w = atoi(p);
		    break;

		case 'Y':
		    vw.h = atoi(p);
		    break;

		case 'Z':
		    arg_mbz = atoi(p);
		    break;
		}
	}

/* no divisor unless it's 1, 2, 4, or 8 */
	if ( (1 != arg_wd)
	  && (2 != arg_wd)
	  && (4 != arg_wd)
	  && (8 != arg_wd) ) arg_wd = 0;

/* if either width or height are incorrect, reset to default video size */
	if ((vw.w < 160) || (vw.w > 1920)) {
		vw.w = 960;
		vw.h = 544;
	}

	if ((vw.h < 90) || (vw.h > 1088)) {
		vw.w = 960;
		vw.h = 544;
	}
}


/* dump config to stdout in human readable form */
//static
void
show_config ( void )
{
	FILE *f;
	char *t0 = "not ";
	char *t1 = "";
	char *s0 = "OFF";
	char *s1 = "ON";

	WHOAMI;

	f = stdout;

	fprintf(f, "\nCURRENT CONFIGURATION:\n");
	fprintf(f, "\tlibmpeg2 acceleration bits 0x%08X\n\n", arg_mx);
	fprintf(f, "\tIPB Histogram is %svisible,", (arg_hl)? t1:t0);
	fprintf(f, " Scale is %f\n", hist_scale);
	fprintf(f, "\tIPB Histogram buttons are %svisible\n",
		     (arg_lb)? t1:t0);
	fprintf(f, "\t\tI frames %s,", (bis)? s1:s0);
	fprintf(f, "  P frames %s,", (bps)? s1:s0);
	fprintf(f, "  B frames %s\n\n", (bbs)? s1:s0);

	fprintf(f, "\tCuts go to output path %s\n", arg_path);
	fprintf(f, "\t\tCuts to %s file%s,",
		    (arg_one)?"single":"multiple", (arg_one)?"":"s");

	fprintf(f, " Renumbering %s,", (arg_renum)? s1:s0 );
	fprintf(f, " Last Sequence %s,\n", (arg_kls)? s1:s0);
	fprintf(f, "\t\tGOP broken_link %s,", (arg_gop)? s1:s0);
	fprintf(f, " NULL packets %s,", (arg_nulls)? s1:s0);
	fprintf(f, " Delays %s\n", (arg_delay)? s1:s0);
	fprintf(f, "\t\tBuffer size %d\n\n", arg_mbz);

	fprintf(f, "\tVideo dimensions: Source %dx%d Display %dx%d",
		    framew, frameh, vw.w, vw.h);
	if (0 != arg_wd)
	    fprintf(f, ", Divisor %d\n", arg_wd);

	fprintf(stdout, "\n");
	fprintf(f, "\tSlider bead is %svisible\n\n", (arg_sb)? t1:t0);
	fprintf(f, "\tTimecodes are %svisible,", (arg_tc)? t1:t0);
	fprintf(f, " Format %s\n",
		    (arg_ft) ? "mmm:ss":"frames");
	fprintf(f, "\n");
}


static
void
build_keyhelp ( char *d, int z, int s )
{
    char *t;
    t = "\n";
    if (0 == s) t = "";

    memset(d, 0, z);

    asnprintf(d, z,
"KEY CONTROLS:\n"
"%s"
"    ?    This help text                  ESC     Refresh display\n"
"    F1   Toggles hide IPB histogram      F2      Toggles hide IPB buttons\n"
"    F3   Toggles hide timecodes at top   F4      Toggles hide slider bead\n"
"    [+]  Increase histogram scale        [-]     Decrease histogram scale\n"
"%s"
"    m    Toggle single/multiple cuts     d       Display cut list\n"
"    n    Toggle NULLs packets            c       Clear all cuts\n"
"    t    Toggle timecode format          w       Write all cuts\n"
"    g    Toggle GOP broken_link          e       Write even cuts\n"
"    q    Quit                            r       Write odd cuts\n"
"\n"
"KEY NAVIGATION: W is window width in frames (same as pixel width)\n"
"%s"
"    y or [PageUp] backward 2W            o or [PageDown] forward 2W\n"
"    u or [Up]     backward 1W            i or [Down]     forward 1W\n"
"    j or [Left]   back 1 Sequence        k or [Right]    forward 1 Sequence\n"
"    [ or [Home]   jump to start          ] or [End]      jump to end\n"
"    a             jump to previous cut   s               jump to next cut\n"
"\n"
"MOUSE CONTROLS:\n"
"%s"
"    LMB     Left Button selects/deselects a cut or IPB legend line\n"
"    RMB     Right Button previews the Intra-frame under the mouse\n"
"    MMB     Middle button jumps to stream position (invisible slider)\n"
"    SCROLL  Scroll Wheel navigates backward or forward by 1/2 W.\n"
"%s", t, t, t, t, t );

}


static
void
build_cfghelp ( char *d, int z, int s )
{
	char *at = "n/a";
	char *dt = "n/a";
	char *t0 = "not ";
	char *t1 = "";
//	char *s0 = "OFF";
//	char *s1 = "ON";
	char *y0 = "NO ";
	char *y1 = "YES";
	char *pok;
	int ok;
	char path[512];

	WHOAMI;

	memset(d, 0, z);

/* set the output path but truncate it after first 40 chars */
	memset(path, 0, sizeof(path));
	ok = 0;
	if (0 != *arg_path)
	    ok = chdir(arg_path);

	if (0 != ok) {
	    perror( WHO );
	    c_exit(1);
	}

	pok = getcwd(path, sizeof(path));
	if (pok == NULL) *path = 0;
	if (0 != *path) path[40] = 0;

	asnprintf(d, z, "\n");
	asnprintf(d, z,  " CURRENT CONFIGURATION:\n");
	at = "autodetect";
	if (0x80000000 != arg_mx) at = x86mx[7 & arg_mx];

#ifdef USE_LIBVO
	dt = votypes[2 + arg_vo];
#endif
#ifdef USE_IMLIB1
	dt = votypes[0];
#endif
#ifdef USE_IMLIB2
	dt = votypes[1];
#endif
	asnprintf(d, z,  "  libmpeg2 acceleration bits %s \n", at);
	asnprintf(d, z,  "  Video Source %dx%d Display %dx%d \n",
			    framew, frameh, vw.w, vw.h);
	asnprintf(d, z, "   Render Video with %s", dt);
	if (0 != arg_wd)
	    asnprintf(d, z,  ", Divisor %d ", arg_wd);
	asnprintf(d, z, "\n\n");

	asnprintf(d, z,  "  IPB Histogram is %svisible", (arg_hl)? t1:t0);
	if (arg_hl) asnprintf(d, z,  ", Scale is %.0f%% ", hist_scale * 100.0);
	asnprintf(d, z, "\n");

	asnprintf(d, z,  "  IPB Buttons are %svisible",
		     (arg_lb)? t1:t0);
	if (arg_lb) {
	    asnprintf(d, z,  ": I %s, ", (bis)? y1:y0);
	    asnprintf(d, z,  "P %s, ", (bps)? y1:y0);
	    asnprintf(d, z,  "B %s ", (bbs)? y1:y0);
	}
	asnprintf(d, z, "\n");

	asnprintf(d, z,  "  Cut%s to: ", (arg_one)?"":"s");
//	if (0 != *arg_path) astrncpy(path, arg_path, sizeof(path));
	asnprintf(d, z,  "%s \n", path);

	asnprintf(d, z,  "    Renumber cuts %s", (arg_renum)? y1:y0);
	asnprintf(d, z,  "    Delay %s\n", (arg_delay)? y1:y0);
	asnprintf(d, z,  "    Buffer size %d", arg_mbz);
	asnprintf(d, z,  "    Keep Cruft %s\n", (arg_kls)? y1:y0);
#ifdef USE_TS_FIX
	asnprintf(d, z,  "    GOP broken %s", (arg_gop)? y1:y0);
	asnprintf(d, z,  "    Keep NULLs %s\n", (arg_nulls)? y1:y0);
#endif
	asnprintf(d, z, "\n");

	asnprintf(d, z,  "  Slider bead is %svisible\n", (arg_sb)? t1:t0);
	asnprintf(d, z,  "    Timecodes are %svisible", (arg_tc)? t1:t0);
	if (arg_tc) asnprintf(d, z,
		    ", Format %s", (arg_ft) ? "mmm:ss":"frames");
	asnprintf(d, z, "\n");
	asnprintf(d, z, "\n");
}


/* build scut[] from frames[].cut, adding cut index to frames[].cutnum */
static
void
build_scut_frames ( void )
{
    int i, c;
    frame_t *f;

    WHOAMI;

    memset( scuts, 0, sizeof(scuts));
    scut_idx = 0;
    scuts[scut_idx++] = 0; /* first is sequence 0 */

    c = 1;

    for (i = 0; i < frame_idx; i++) {
	f = &frames[i];

	f->tcolor = c;

/* Intra frame type */
	if (1 == f->pct) {

/* reset in case f->cut changed */
	    f->cutnum = 0;

	    if (0 != f->cut) {
		    dprintf( stdout,
			"Cut %02d Frame %6d Intra %6d Byte %11llX Color %d\n",
			scut_idx, i, f->num, sequences[f->num], f->tcolor
		    );

		    f->cutnum = scut_idx;
		    scuts[scut_idx] = f->num;
		    scut_idx++;
		    c = 1 & scut_idx;
	    }
	}
    }
}


/* Compute the number of nulls to use at the start of each cut. */
/* NULLs are 1/8th packet rate, in case broadcaster screws up and
   sends a bogus stream bitrate of 80Mbits/sec. This will limit
   the maximum size of NULL packet pre-amble to about 1.25Mbytes.

    NOTE: mplayer is easily confused by a lot of initial NULL packets.
	If you use mplayer, you should not use the -n option.

    TODO: Put the above note into the man page for future reference.
*/
static
void
calc_null_packets ( void )
{
	WHOAMI;

	null_pkts = 0;
	if (0 == arg_nulls) return;
	null_pkts = byte_rate / TSZ;
	null_pkts /= 8;	/* fewer nulls, about 1/8th of a second */
}


/* i is frame index, divide by frame rate to determine the time code */
static
void
build_frame_timecode ( char *d, int z, int i )
{
	int m, s, p;

//	WHOAMI;

	p = i * 100;
	p /= frame_rate;
	s = p % 60;
	m = p / 60;
	asnprintf(d, z, " %03d:%02d", m, s );
}



static
void
build_cutlist( char *d, int z )
{
	int i, fce, fco, fca;
	long long cpt, cat, cet, cot, vbf;
	struct statfs fs;
	char anum[32];

// FIXME: disabled for debug, but enable for output redux on cl-usage
//	if (0 != arg_cut) return;

	WHOAMI;

	memset( cutlist, 0, sizeof(cutlist));

/* preamble byte count is PAT + PMT + optional NULLs */
	calc_null_packets();
	cpt = TSZ * (1 + pmt_pkts);
	cpt += TSZ * null_pkts;

	fce = fco = fca = 0;
	cet = cot = cat = 0;

	build_scut_frames();

/* if it only shows this one, this trick is ok */
	scuts[scut_idx] = sequence_idx-1;
    
	asnprintf(d, z, "Cut #  SEQ #      byte offset        file size  start  runtime frames   %%\n");
	asnprintf(d, z, "----- ------ ---------------- ---------------- ------- ------- ------ -----\n");

	if (0 == arg_es) {
	    asnprintf(d, z, " none   none %16lld %16lld   PAT+PMT%s pre-amble\n",
			0LL, cpt, (0 != arg_nulls) ?"+NULL":"");
	} else {
	    asnprintf(d, z, "Elementary Stream write has no pre-amble\n");
	}
	for (i = 0 ; i < scut_idx-1; i++) {
		long long cs, cz;
		int fcs, fcz;

/* don't need last I frame plus junk */
		if (0 == scuts[i+1]) break;

/* byte start of cut and byte count of cut */
		cs = sequences[ scuts[ i ] ];
		cz = sequences[ scuts[ i+1 ] ] - cs;

/* frame start of cut and frame count of cut */
		fcs = iframes[ scuts[ i ] ];
		fcz = iframes[ scuts[ i+1 ] ] - fcs;

/* cut all total */
		fca += fcz;
		cat += cz;
		cat += cpt;

/* off by one, list starts with cut 1 */
		if (i&1) {
/* cut even total */
			cet += cz;
			cet += cpt;
			fce += fcz;
		} else {
/* cut odd total */
			cot += cz;
			cot += cpt;
			fco += fcz;
		}

		lltoasc( anum, cz + cpt );

/* cut 001 to last cut: cut size includes pre-amble for each cut */
		asnprintf(d, z, "%3d @ %6d ", i+1, scuts[i]);
		lltoasc( anum, cs );
		asnprintf(d, z, "%16s ", anum );
		lltoasc( anum, cz + cpt);
		asnprintf(d, z, "%16s ", anum );

		build_frame_timecode(d, z, fcs);
		asnprintf(d, z, " ");
		build_frame_timecode(d, z, fcz);
		asnprintf(d, z, "%7d ", fcz);
		asnprintf(d, z, " %3lld%%", (cz * 100) / in_size);
		asnprintf(d, z, "\n");
	}

#if 0
/* data after last cut not displayed because it's irrelevant */
	asnprintf(d, z, "%3d @ %6d %11lld %11lld  End cruft\n",
		scut_idx,
		sequence_idx - 1, 
		sequences[ sequence_idx - 1 ],
		in_size - sequences[ scuts[ scut_idx - 1 ]]
	);
#endif

	statfs( arg_path, &fs);
	vbf = fs.f_bsize * fs.f_bavail;
	asnprintf(d, z, "Free space  %11lld bytes in volume %s:\n",
		 vbf, arg_path);

	asnprintf(d, z, " EVEN cut time");
	build_frame_timecode(d, z, fce);
	asnprintf(d, z, ", bytes %11lld, ", cet);
	asnprintf(d, z, "frames %6d (%2d%%), will %sfit.\n",
			fce, (100 * fce) / frame_idx, (cet < vbf) ?"":"NOT " );

	asnprintf(d, z, "  ODD cut time");
	build_frame_timecode(d, z, fco);
	asnprintf(d, z, ", bytes %11lld, ", cot);
	asnprintf(d, z, "frames %6d (%2d%%), will %sfit.\n",
			fco, (100 * fco) / frame_idx, (cot < vbf) ?"":"NOT " );

	asnprintf(d, z, "  ALL cut time");
	build_frame_timecode(d, z, fca);
	asnprintf(d, z, ", bytes %11lld, ", cat);
	asnprintf(d, z, "frames %6d (%2d%%), will %sfit.\n",
			fca, (100 * fca) / frame_idx, (cat < vbf) ?"":"NOT " );

	asnprintf(d, z, "\n" );
}


static
void
show_keys( int s )
{
	build_keyhelp( keyhelp, sizeof(keyhelp), s);
	fprintf( stdout, "%s", keyhelp);
}

/* TODO: needs control c handler to give it up properly */
static
void
free_frames ( void )
{
	WHOAMI;

	sequences = ifree(sequences, "sequences");
	iframes = ifree(iframes, "iframes");
	frames = ifree(frames, "frames");
}


#if 0
// error: dereferencing pointer to incomplete type
static
void
free_mpeg2 ( mpeg2dec_t * mpeg2dec )
{
	if (NULL == mpeg2dec) return;

	if (mpeg2dec->sequence->width != (unsigned)-1) {
    	    int i;

	    mpeg2dec->sequence.width = (unsigned)-1;
	    if (!mpeg2dec->custom_fbuf)
        	for (i = mpeg2dec->alloc_index_user;
                 i < mpeg2dec->alloc_index; i++) {
		    mpeg2_free (mpeg2dec->fbuf_alloc[i].fbuf.buf[0]);
		    mpeg2_free (mpeg2dec->fbuf_alloc[i].fbuf.buf[1]);
		    mpeg2_free (mpeg2dec->fbuf_alloc[i].fbuf.buf[2]);
		}
	    if (mpeg2dec->convert_start)
		for (i = 0; i < 3; i++) {
		    mpeg2_free (mpeg2dec->yuv_buf[i][0]);
		    mpeg2_free (mpeg2dec->yuv_buf[i][1]);
		    mpeg2_free (mpeg2dec->yuv_buf[i][2]);
		}
	    if (mpeg2dec->decoder.convert_id)
		mpeg2_free (mpeg2dec->decoder.convert_id);
	}
}
#endif


/* alloc frames, iframes and sequences arrays, one item until expanded */
static
void
alloc_frames ( void )
{
	char n[256];

	/* frame_s, 2x int */

	WHOAMI;

	frames = NULL;
	iframes = NULL;
	sequences = NULL;

/* frames frame_t */
	snprintf(n, sizeof(n), "%s[%d]", "frames", 0 );
	frames = icalloc( 2, sizeof(frame_t), n );
	if (NULL == frames) {
		fprintf(stdout, "%s failed *frames alloc\n", WHO);
		c_exit(1);
	}

/* iframes int */
	snprintf(n, sizeof(n), "%s[%d]", "iframes", 0 );
	iframes = icalloc( 2, sizeof(int), n );
	if (NULL == iframes) {
		fprintf(stdout, "%s failed *iframes alloc\n", WHO);
		c_exit(1);
	}

/* sequences long long */
	snprintf(n, sizeof(n), "%s[%d]", "sequences", 0 );
	sequences = icalloc( 2, sizeof(long long), n);
	if (NULL == sequences) {
		fprintf(stdout, "%s failed *sequences alloc\n", WHO);
		c_exit(1);
	}
}


static
void
alloc_streams( void )
{
	es_buf = icalloc( 1, arg_mbz + TSZ, "ES buffer" );
	if (NULL == es_buf) {
		fprintf(stdout, "%s failed es_buf alloc(%d)\n",
			    WHO, arg_mbz + TSZ );
		c_exit(1);
	}

	ts_buf = icalloc( 1, arg_mbz + TSZ, "TS buffer" );
	if (NULL == es_buf) {
		fprintf(stdout, "%s failed es_buf alloc(%d)\n",
			    WHO, arg_mbz + TSZ );
		c_exit(1);
	}
}


static
void
clear_arrays ( void )
{
	WHOAMI;

#if 0
/* FIXME: sizeof doesn't work on pointers, need to use data types */
	memset( frames, 0, sizeof( frames ));
	memset( iframes, 0, sizeof( iframes));
	memset( sequences, 0, sizeof( sequences));
#endif

	memset( scuts, 0, sizeof(scuts));

	frame_idx = 0;
	sequence_idx = 0;
	scut_idx = 0;
}


#ifdef USE_SIGNALS
#include "xtc_signals.c"
#endif

#ifdef USE_LIBVO
/*********************************************************** mpeg2dec libvo */
#warning using libvo for XShm and XVideo
#include "xtc_libvo.c"
vo_instance_t * xoutput;
vo_setup_result_t xsetup;
/******************************************************* mpeg2dec libvo END */
#endif /* USE_LIBVO */

static
void
free_sbufs ( void )
{
	WHOAMI;

	es_buf = ifree(es_buf, "ES buffer");
	ts_buf = ifree(ts_buf, "TS buffer");
}



#ifdef USE_MAXIMIZE
/* 1920/1088 = x/y, same as 1088/1920 = y/x */
/* y is returned after calc from width x */
static
unsigned int
calc_aspect_h ( unsigned int w )
{
	int y;

	WHOAMI;

	y = 0;
	if ( (0 == w) || (0 == framew) || (0 == frameh) ) return 0;
	    
	y = frameh * w;
	y /= framew;
    
	return y;
}

/* 1920/1088 = x/y, same as 1088/1920 = y/x */
/* y is returned after calc from width x */
static
unsigned int
calc_aspect_w ( unsigned int h )
{
	int x;

	WHOAMI;

	x = 0;
	if ( (0 == h) || (0 == framew) || (0 == frameh) ) return 0;
	    
	x = framew * h;
	x /= frameh;
    
	return x;
}
#endif



/* build a NULL packet filled with zeros at poiner p */
static
void
build_null_packet ( char *p )
{
	WHOAMI;

	memset( p, 0, TSZ );
	*p = MPEG_SYN;
	p++;
	*p = 0x1F;
	p++;
	*p = 0xFF;
	p++;
	*p = 0x10;
}


static
void
dump_packet ( long long o, unsigned char *p, int z, int e )
{
	int i;
	char *b;

	WHOAMI;

	for (i = 0; i < z; i++) {
		if (0 == (15 & i)) fprintf( stdout, "%9llX: ", o + i);
		b = " ";
		if (e == i) b = "*";
		fprintf(stdout, "%s%02X ", b, p[i]);
		if (15 == (15 & i)) fprintf( stdout, "\n");
	}
	fprintf( stdout, "\n\n");
}



#ifndef USE_LIBVO
/* Copy from libmpeg2 parsed intra frame buffer to imlib buffer.
    The image is not freed so that x draw_image may reuse it.
*/
inline
void
update_iframe ( int w, int h, unsigned char *b )
{
	WHOAMI;

	if (0 == xinit) return;


#ifdef USE_IMLIB2
/* free old img before creating new */
	if (NULL != image) imlib_free_image();
	image = imlib_create_image_using_data( w, h, (DATA32 *) b );
	if (NULL != image) imlib_context_set_image( image );
#endif

#ifdef USE_IMLIB1
/* free old img before creating new */
	if (NULL != image) Imlib_kill_image( idata, image );
	image = Imlib_create_image_from_data( idata, b, NULL, w, h );
#endif

}
#endif /* ~USE_LIBVO */

/* Parses Transport Stream header and store result in tsh_t

    Arguments:
	p	start of 188 byte packet, *p should be 0x47 or sync is lost
        h	tsh_t holds the parsed flags
	pid	0 returns all ES, nz returns only ES found in PID

    Returns:
	-1	if transport sync lost, *p isn't 0x47
	0	if no ES data in PID
	>0	ES byte count starting at packet offset h->pso


    Usage:
	input scan, called with 0 for pid
	build tsx, called with arg_vpid found by input scan
	parse video es, called with arg_vpid found by input scan

	Future ES-only write function could use this, if ES need arises.
	Future resync code could use to find new sync point, if needed.
*/
inline
unsigned int
parse_ts_header( unsigned char *p, tsh_t *h, unsigned short pid )
{
	unsigned char *r = NULL;
	unsigned int z = 0;

//	WHOAMI;
	r = p;

	h->afc = 0;
	h->afb = 0;
	h->pso = 4;
	h->psb = 0;
	h->pid = 0;

	if (MPEG_SYN != *r) {			/* sync lost? */
/* TODO: needs a way to resync */
		return -1;
	}

	r++;
	h->tei = 1 & (*r>>7);			/* transport error nz */

	h->psi = 1 & (*r>>6);			/* payload start 1 */
	h->pri = 1 & (*r>>5);			/* priority ignored */

	h->pid = 0x1F00 & (*r<<8);		/* top half */
	r++;
	h->pid |= *r;				/* bottom half */

	r++;					/* TSC/AFC/CC */
	h->tsc = 3 & (*r>>6);			/* scrambled error nz */

	h->afc = 3 & (*r>>4);			/* adaptation field control */
	h->cc  = 0x0F & *r;			/* continuity counter */
	r++;					/* AFC count or video */

	z = 0;

/* Payload[0] after header is for Adaptation Field Control size if afc > 1 */
	if (h->afc > 0) {

		if (1 == h->afc) {
			z = ESZ;		/* no AFC has 184 bytes ES */
			h->pso = 0xFF & (r - p);
		}
		if (2 == h->afc) z = 0;		/* AFC len, no ES payload */

		if (3 == h->afc) {		/* w/AFC, ES is <= 182 bytes */
			h->afb = *r;		/* AFC len */
			r++;
			r += h->afb;		/* skip AFC count + data */
			z = (ESZ - 1) - h->afb; /* subtract AFC len+data */
			h->pso = 0xFF & (r - p);
		}

	}
	h->psb = 0xFF & z;

	if (0 != h->tei) return 0;
	if (0 != pid)				/* zero PID doesn't */
	    if (pid != h->pid)			/* check for Video PID */
		return 0;

	if (0 != h->tsc) return 0;

	return z;
}


/* Parse transport stream to MPEG2 Elementary Stream, without AFC data.
    This is modified by arg_es global to not attempt to de-packetize it,
     but instead read the data as Video Elementary Stream.
*/
//static
inline
int
parse_video_es ( void )
{
	unsigned char *r, *p, *d;
	int i, z, c;
	tsh_t *h;

	h = &tsh;

//	WHOAMI;

/* -y or .es file specified only uses simple Elementary Stream data */
	if (0 != arg_es) {
	    z = read( in_file, es_buf, arg_mbz );
/* EOF or error? */
	    if (z < 1) fprintf( stdout, "%s ES EOF %s\n", WHO, in_name );
	    return z;
	}

/* TS requires some extraction of the packet data to the ES buffer */
	z = read( in_file, ts_buf, arg_mbz );
	if (z < 1) {				/* EOF or error? */
	    fprintf( stdout, "%s TS EOF %s\n", WHO, in_name);
	    return z;				/* return what was read */
	}

/* es_buf will always have enough for extracted ES from same size ts_buf */
	d = es_buf;
	p = ts_buf;

	c = z = 0;
	for (i = 0; i < arg_mbz; i += TSZ) {

	    r = p + i;
	    z = parse_ts_header( r, h, arg_vpid );

/* fatal abort on sync lost */
	    if (-1 == z) {
		    fprintf( stdout,
			"\nFATAL: %s Transport Stream Sync lost.\n\n", WHO);
		    c_exit(1);
	    }

//	    r += h->pso;

	    dprintf(stdout,
		"%s PID %04X PSI%d AFC%d i %4d es[%4d] afb %3d z %3d ts[%4d)\n",
		    WHO, h->pid, h->psi, h->afc, i, c, h->afb, z, r - p);

	    pkt_num++;
	    if (0 == z) continue;
	    if (h->pid == MPEG_NULL) continue;
	    if (h->pid != arg_vpid) continue;

	    pkt_vid++;		/* skip non video ES PIDs */
	    r += h->pso;
	    memcpy(&d[c], r, z );

/* counter should never exceed mbz */
	    c += z;				/* bump the byte counter */
	}
	return c;
}




static
void
x_draw_line ( unsigned int fgc, int xc1, int yc1, int xc2, int yc2 )
{
	if (0 == xinit) return;	

//	if (0 != arg_xmsg) WHOAMI;

#ifdef USE_IMLIB2
//#define USE_IMLIB2_DRAW
#ifdef USE_IMLIB2_DRAW
/* FIXME: doesn't take into account 16 bit colors, only 24 bit.
   Also, the histogram displays wrong. Disabled until debugged.
*/

	int c1, c2, c3;

	c1 = 0xFF & (fgc >> 16);
	c2 = 0xFF & (fgc >> 8);
	c3 = 0xFF & fgc;

	imlib_context_set_color( arg_alpha, c1, c2, c3 );
	imlib_image_draw_line( xc1, yc1, xc2, yc2, 0 );
	return;
#else
	XSetForeground( xdisplay, xgc, fgc );
	XDrawLine( xdisplay, xwindow, xgc, xc1, yc1, xc2, yc2);
	return;
#endif
#else
	XSetForeground( xdisplay, xgc, fgc );
	XDrawLine( xdisplay, xwindow, xgc, xc1, yc1, xc2, yc2);
	return;
#endif

}



static
void
x_clear_display ( void )
{
	if (0 == xinit) return;

#ifndef USE_LIBVO
	return;
#endif
	if (0 != arg_xmsg) WHOAMI;

        XClearWindow( xdisplay, xwindow );
	XFlush( xdisplay );
}

/* Draw a filled quadrangle, many examples, buttons, backgrounds, etc.
   The biggest visual problem these suffer is they're flat-color quads.
*/
static
void
x_draw_quad_fill ( unsigned int fgc, int x, int y, int w, int h )
{
	if (0 == xinit) return;	

//	if (0 != arg_xmsg) WHOAMI;

	XSetForeground( xdisplay, xgc, fgc );
	XFillRectangle( xdisplay, xwindow, xgc, x, y, w, h );
}

/* Draw a quadrangle, i.e. de-selected button border, or help text border. */
//static
void
x_draw_quad_nofill ( unsigned int fgc, int x, int y, int w, int h )
{
	if (0 == xinit) return;	

//	if (0 != arg_xmsg) WHOAMI;

	XSetForeground( xdisplay, xgc, fgc );
	XDrawRectangle( xdisplay, xwindow, xgc, x, y, w, h );
}

/* Draw a vertical gradient filled quad. g float gradient for line color.
   g positive starts at black and works way up to color fgc
   g negative starts at color fbc and works way down to black
    g is limited to the real numbers between -1.0 and 1.0, inclusive

*/
static
void
x_draw_quad_gradientv (
     unsigned int fgc, int x, int y, int w, int h, float e )
{
	int i, k, mr, mg, mb, r, r1, g, g1, b, b1, x1, w1;
	unsigned int c;
	float j, f, rf, gf, bf;

	int cx[8] = { 8, 6, 4, 3, 2, 2, 1, 1 };

	if (0 == xinit) return;	
	if (0 == arg_ec) {
	    x_draw_quad_fill( COLOR_BLACK, x, y, w, h );
	    return;
	}
//	if (0 != arg_xmsg) WHOAMI;

/* ignore bogus text box limit */
	if (0 == h) return;
	if (0 == w) return;

/* ignore bogus gradients */
	if (e < -1.0) return;
	if (e > +1.0) return;

/* divide gradient by number of rows for gradient step */
	j = e / (float) h;

	mr = mg = mb = 1 << (xbpp/3);
/* FIXME: need to know if 5 bit or 6 bit green for max green value */
//	mg = 1 << xgreenbits;
    
	r = (0xFF & (fgc >> 16));
	g = (0xFF & (fgc >> 8));
	b = (0xFF & fgc);

//	fprintf( stdout, "gradient step j %f\n", j);
	for (i = 0; i < h; i++) {
		f = j * (float) i;
		if (f < 0.0) f = 1.0 + f;

/* color change per color channel */
		rf = f * (float) r;
		gf = f * (float) g;
		bf = f * (float) b;

		r1 = (int) rf;
		g1 = (int) gf;
		b1 = (int) bf;
/* clamp color excursion to maximum color limit */
		if (r1 > mr) r1 = mr;
		if (g1 > mg) g1 = mg;
		if (b1 > mb) b1 = mb;
		c = (r1 << 16) | (g1 << 8) | b1;

// fprintf(stdout, "i %d fgc %08X f %.2f c %08X\n",i, fgc, f, c);
// x_draw_line( fc, x, i, x + w, i );

		XSetForeground( xdisplay, xgc, c );
		
/* if top or bottom 8 lines, change x1 w1 to give rounded corners */
		x1 = x;
		w1 = w;
		if ( (i < 8) || (i > (h-9)) ) {
		    if (i < 8) {
			k = i;
		    } else {
			k = (h - 1) - i;
		    }
		    x1 += cx[k];
		    w1 -= 2 * cx[k];

// fprintf( stdout, "i%4d k %d x'%4d w'%4d\n", i, k, x1 - x, w - w1 );
		}

		XDrawLine( xdisplay, xwindow, xgc, x1, y + i, x1 + w1, y + i );
	}
}

/*
   Open ts file, seek to offset and start parsing until Intra-frame arrives,
    then parsed intra is sent to imlib for display processing. The file is
    left open so the next seek shouldn't be impacted by file open delay.

   This is drawn before the histogram when redraw is called. The frame rate
    and scale factor for histogram are set before time-codes and lines drawn.

   Histogram scale factor is bit rate / 5 megabit to give 1-16 for 5-80 Mbit.
    This should draw the lines short enough to not take up the whole frame.
*/
static
void
parse_intra_frame ( long long offset, char *caller )
{
	int sz, pictype;
	long long ok;
//	uint8_t m2buf[ ESZ ];

	if (NULL == m2dec) {
		fprintf(stdout, "%s MPEG2 decoder not initialized.\n", WHO );
		exit(1);
	}

	WHOAMI;

	pictype = 0;
	iprintf(stdout, "%s %s offset %09llX\n", WHO, caller, offset);

/* last (fake) sequence can't do preview but can be selected for cut. */
	if (0 != xinit) {
	    if  (offset >= sequences[ sequence_idx-1 ]) {

/* image is not freed after load so x draw_image can do render */

#ifdef USE_LIBVO
/* Xvideo make the screen flash white momentarily when at 'The End' */
		x_draw_quad_fill( COLOR_RED, 0, 0, vw.w, vw.h);
#endif

#ifdef USE_IMLIB2
/* free old img before creating new */
		if (NULL != image) imlib_free_image();
		image = imlib_load_image( PNG_PATH "xtscut-end.png");
		if (NULL != image) imlib_context_set_image( image );
#endif

#ifdef USE_IMLIB1
/* free old img before creating new */
		if (NULL != image) Imlib_kill_image( idata, image );
		image = Imlib_load_image( idata, PNG_PATH "xtscut-end.png");
#endif
		return;
	    }
	}

	ok = lseek( in_file, offset, SEEK_SET );
	if (ok != offset) {
		fprintf( stdout, "%s seek error %lld\n", WHO, ok );
		perror("");
		exit(1);
	}
	iprintf( stdout, "%s->%s seek %09llX \n",
		    caller, WHO, offset );

	sz = 0;

/* reset decoder. 1 sets look for next Sequence (0 is next picture) */
	mpeg2_reset( m2dec, 1 );

	do {
		m2sta = mpeg2_parse( m2dec );
		dprintf( stdout, "m2sta %d %s\n", m2sta, m2stat[m2sta]);
		switch (m2sta) {

/* m2buf needs more data */
		case STATE_BUFFER:
			sz = parse_video_es();
			if (-1 != sz) /* if it was part of video, use it */
				mpeg2_buffer (m2dec, es_buf, es_buf + sz);
			break;

/* SEQUENCE needs to set output size and frame rate for i frame time codes,
    with bit rate and aspect ratio updates based on height and width.
*/
		case STATE_SEQUENCE:
		    iprintf( stdout, "%s xinit %d\n", m2stat[m2sta], xinit);
#ifdef USE_LIBVO
/* might set nb fbuf, convert format, stride */
/* might set fbufs */
		    if (0 != xinit) {
			iprintf( stdout, "Xoutput->setup Y %d %d UV %d %d\n",
				    m2seq->width,
				    m2seq->height,
				    m2seq->chroma_width,
				    m2seq->chroma_height);

			if ( xoutput->setup( xoutput,
				m2seq->width,
				m2seq->height,
				m2seq->chroma_width,
				m2seq->chroma_height,
				&xsetup) )
			{
				fprintf(stdout, "Xoutput->setup failed\n");
				c_exit(1);
			}

#ifdef USE_LIBVO
/* if this is disabled, XShm RGB and XVideo YUYV break */
			if (1 != arg_vo) {
			    if (xsetup.convert
			      && mpeg2_convert (m2dec, xsetup.convert, NULL))
			    {
				fprintf (stdout, "conversion setup failed\n");
				exit(1);
			    }
			}
#endif

			if (xoutput->set_fbuf) {
			    uint8_t * buf[3];
			    void * id;

			    dprintf(stdout, "Xoutput->set_fbuf\n");
			    mpeg2_custom_fbuf (m2dec, 1);
			    xoutput->set_fbuf (xoutput, buf, &id);
			    mpeg2_set_buf (m2dec, buf, id);
			    xoutput->set_fbuf (xoutput, buf, &id);
			    mpeg2_set_buf (m2dec, buf, id);
			} else if (xoutput->setup_fbuf) {
			    uint8_t * buf[3];
			    void * id;

			    dprintf(stdout, "Xoutput->setup_fbuf\n");
			    xoutput->setup_fbuf (xoutput, buf, &id);
			    mpeg2_set_buf (m2dec, buf, id);
			    xoutput->setup_fbuf (xoutput, buf, &id);
			    mpeg2_set_buf (m2dec, buf, id);
			    xoutput->setup_fbuf (xoutput, buf, &id);
			    mpeg2_set_buf (m2dec, buf, id);
			}
			mpeg2_skip(m2dec, (xoutput->draw == NULL));
		    }
#else

#ifdef USE_IMLIB2
			mpeg2_convert( m2dec, mpeg2convert_rgb32, NULL );
#endif

#ifdef USE_IMLIB1
			mpeg2_convert( m2dec, mpeg2convert_rgb24, NULL );
#endif

#endif /* USE_LIBVO */
			m2seq = m2inf->sequence;
			frame_period = m2seq->frame_period;
			frame_rate = 2700000000UL / frame_period;

			if (byte_rate != m2seq->byte_rate)
				fprintf(stdout,
					"Frame rate %u.%u, period %5.3f mS, "
					"Bit rate %u, Byte rate %u\n",
					frame_rate / 100,
					frame_rate % 100,
					(double)m2seq->frame_period
					    / (double)27000.0,
					m2seq->byte_rate << 3,
					m2seq->byte_rate );

			framew = m2seq->picture_width;
			frameh = m2seq->picture_height;
			aspectf = (double)framew / (double)frameh;

			byte_rate = m2seq->byte_rate;
			bit_rate = byte_rate << 3;
			break;


		case STATE_PICTURE:
/* might skip */
/* might set fbuf */
		    pictype = 0;
		    if (0 != xinit) {
			if (m2inf->display_fbuf)
			pictype = 3 & m2inf->display_picture->flags;

/* only output I-frames, pictype 1 */
			if (1 != pictype) break;

			iprintf( stdout, "PICTURE %c #%d\n",
					pictypes[pictype], nif++);
		    }

#ifdef USE_LIBVO
		    if (0 != xinit) {

/* discard should be done on picture header */
        		if (xoutput->discard && m2inf->discard_fbuf)
            			xoutput->discard( xoutput,
						    m2inf->discard_fbuf->buf,
						    m2inf->discard_fbuf->id );

        		if (xoutput->set_fbuf) {
            			uint8_t * buf[3];
            			void * id;

				xoutput->set_fbuf (xoutput, buf, &id);
				mpeg2_set_buf (m2dec, buf, id);
			}
			if (xoutput->start_fbuf)
				xoutput->start_fbuf( xoutput,
						m2inf->current_fbuf->buf,
						m2inf->current_fbuf->id);
		    }
#endif /* USE_LIBVO */

		    break;

/* normal end of picture or sequence end seen */
		case STATE_SLICE:
		case STATE_END:

			pictype = 0;
			if (m2inf->display_fbuf)
			    pictype = 3 & m2inf->display_picture->flags;

			if (1 == pictype) {
			    if (STATE_SLICE == m2sta)
				iprintf( stdout, " PICTURE %c SLICE\n",
					pictypes[pictype]);
			}

			if (STATE_END == m2sta) {
				iprintf( stdout, " PICTURE %c END\n",
					pictypes[pictype]);
				sz = 0;
				break;
			}

/* make parse loop stop at the intra frame */
			if (0 != xinit) {
			    if (m2inf->display_fbuf) {
				if (1 == pictype) {
				    xredraw = ~0;
				    sz = 0;
#ifdef USE_LIBVO
				    dprintf(stdout,
					     "x_draw_image will render\n");
/* draw has been moved to x_draw_image */

#if 0
/* draw has been moved to x_draw_image */
/* draw current picture */
/* might free frame buffer */
            			    if (xoutput->draw)
                			xoutput->draw (xoutput,
						m2inf->display_fbuf->buf,
						m2inf->display_fbuf->id);

/* discard has been moved to x_close_display */
        			    if (xoutput->discard && m2inf->discard_fbuf)
            				xoutput->discard( xoutput,
						m2inf->discard_fbuf->buf,
						m2inf->discard_fbuf->id);
#endif


#else
/* filter keeps iframes */
				    update_iframe(
						m2seq->width,
						m2seq->height,
						m2inf->display_fbuf->buf[0] );
#endif /* USE_LIBVO */

				}
			    }
			} else {

/* X not init yet only scans for sequence header info */
			    sz = 0;
			}

			break;

/* TESTME: why does STATE_INVALID trigger every time? */
		case STATE_INVALID:
		    dprintf( stdout, "STATE_INVALID\n" );
		    break;

/* error end of picture, show png if using X: free before use, not after */
		case STATE_INVALID_END:
		    fprintf( stdout, "STATE_INVALID_END\n" );
		    fprintf(stdout, "%s offset %lld has bad data\n",
				    WHO, offset);
		    if (0 != xinit) {
#ifdef USE_IMLIB2
/* free old img before creating new. new image is kept until reloaded */
			if (NULL != image) imlib_free_image();
			image = imlib_load_image( PNG_PATH "xtscut-bad.png" );
			if (NULL != image) imlib_context_set_image( image );
#endif
#ifdef USE_IMLIB1
/* free old img before creating new. new image is kept until reloaded */
			if (NULL != image) Imlib_kill_image( idata, image );
			image = Imlib_load_image( idata, PNG_PATH "xtscut-bad.png");
#endif
		    }

		    break;

		default:
			iprintf( stdout, "Unhandled %d %s\n",
				    m2sta, m2stat[m2sta]);
			break;

		}

	} while( sz );

	iprintf(stdout, "\n");

/* close it to reset it, even more than a reset would do? */
//	mpeg2_close( m2dec );
}


/* destroy the window, close the display and clear the init flag */
static
void
x_close_display ( void )
{
	WHOAMI;

#ifdef USE_LIBVO
/* discard any remaining buffers before closing */
        if (xoutput->discard && m2inf->discard_fbuf)
		xoutput->discard( xoutput,
				  m2inf->discard_fbuf->buf,
				  m2inf->discard_fbuf->id );
	xoutput->close( xoutput );
	xinit = 0;
	return;
#else

#ifdef USE_IMLIB2
	if (NULL != image) imlib_free_image();
#endif
#ifdef USE_IMLIB1
	if (NULL != image) Imlib_kill_image( idata, image );
#endif
	XFreeGC( xdisplay, xgc );
	XDestroyWindow( xdisplay, xwindow );
	XCloseDisplay( xdisplay );

#endif /* USE_LIBVO */
	xinit = 0;
}


/* exit, with X structure deallocate */
static
void
c_exit ( int ev )
{
	WHOAMI;

#ifdef USE_CONSOLE
	console_reset();
	console_reset();
#endif
	dprintf(stdout, "%s:%d\n", WHO, WHERE);
	free_frames();

	dprintf(stdout, "%s:%d\n", WHO, WHERE);
	if (0 != m2init) {
	    mpeg2_close( m2dec );
	}

	dprintf(stdout, "%s:%d\n", WHO, WHERE);
	if (0 != xinit) {
	    x_close_display();
	}

	close( in_file );
	in_file = 0;
	close( out_file );
	out_file = 0;

	dprintf(stdout, "%s:%d\n", WHO, WHERE);
	free_sbufs();

	if (alloc_idx > 0) {
	    fprintf(stdout,
		"alloc_t structures to be freed (%d):", alloc_idx);
	    show_allocs();
	}

	exit( ev );
}


#if 0
/* i is frame index, divide by frame rate to determine the time code */
static
void
show_frame_timecode ( int i )
{
	int m, s, p;

//	WHOAMI;

	p = i * 100;
	p /= frame_rate;
	s = p % 60;
	m = p / 60;
	fprintf( stdout, " %03d:%02d", m, s );
}
#endif


static
void
start_timer ( struct timer_s *t )
{

	WHOAMI;

	if (NULL == t) return;
	memset( t, 0, sizeof(t));
#ifdef USE_LIBRT
	clock_gettime( clock_method, &t->start);
#else
	time( &t->start.tv_sec );
#endif
}


static
void
stop_timer ( struct timer_s *t )
{
	WHOAMI;

	if (NULL == t) return;
#ifdef USE_LIBRT
	clock_gettime( clock_method, &t->stop );
#else
/* minimum ET is 1s */
	time( &t->stop.tv_sec );
	if ( t->start.tv_sec == t->stop.tv_sec ) t->stop.tv_sec++;
#endif
}


static
void
diff_timer ( struct timer_s *t )
{
	WHOAMI;

	memcpy( &t->diff, &t->stop, sizeof(struct timespec) );

	dprintf(stdout, "\n%s\n", WHO );
	dprintf(stdout, "start %d.%09d\n", (int)t->start.tv_sec, (int)t->start.tv_nsec);
	dprintf(stdout, "stop  %d.%09d\n", (int)t->stop.tv_sec, (int)t->stop.tv_nsec);
/* nanosecond borrow from second? */
	if (t->start.tv_nsec > t->stop.tv_nsec) {
		t->diff.tv_sec--; /* borrow */
		t->diff.tv_nsec += 1000000000; /* lend */
	}

	t->diff.tv_sec  -= t->start.tv_sec;
	t->diff.tv_nsec -= t->start.tv_nsec;
	dprintf(stdout, "diff  %d.%09d\n", (int)t->diff.tv_sec, (int)t->diff.tv_nsec);
}



#if 0
/* show the byte offsets and total for cuts, check against free volspace */
static
void
show_scuts ( void )
{
	int i, fce, fco, fca;
	long long cpt, cat, cet, cot, vbf;
	struct statfs fs;
	char anum[32];

// FIXME: disabled for debug, but enable for output redux on cl-usage
//	if (0 != arg_cut) return;

	WHOAMI;

/* preamble byte count is PAT + PMT + optional NULLs */
	calc_null_packets();
	cpt = TSZ * (1 + pmt_pkts);
	cpt += TSZ * null_pkts;

	fce = fco = fca = 0;
	cet = cot = cat = 0;

	build_scut_frames();

/*
	fprintf( stdout, "Cut list from I:P:B thresholds of %d:%d:%d\n",
		arg_ti, arg_tp, arg_tb);
*/

/* if it only shows this one, this trick is ok */
	scuts[scut_idx] = sequence_idx-1;
    
	fprintf( stdout, "Cut #  SEQ #      byte offset        file size  start  runtime frames   %%\n");
	fprintf( stdout, "----- ------ ---------------- ---------------- ------- ------- ------ -----\n");

	if (0 == arg_es) {
	    fprintf( stdout, " none   none %16lld %16lld   PAT+PMT%s pre-amble\n",
			0LL, cpt, (0 != arg_nulls) ?"+NULL":"");
	} else {
	    fprintf( stdout, "Elementary Stream write has no pre-amble\n");
	}
	for (i = 0 ; i < scut_idx-1; i++) {
		long long cs, cz;
		int fcs, fcz;

/* don't need last I frame plus junk */
		if (0 == scuts[i+1]) break;

/* byte start of cut and byte count of cut */
		cs = sequences[ scuts[ i ] ];
		cz = sequences[ scuts[ i+1 ] ] - cs;

/* frame start of cut and frame count of cut */
		fcs = iframes[ scuts[ i ] ];
		fcz = iframes[ scuts[ i+1 ] ] - fcs;

/* cut all total */
		fca += fcz;
		cat += cz;
		cat += cpt;

/* off by one, list starts with cut 1 */
		if (i&1) {
/* cut even total */
			cet += cz;
			cet += cpt;
			fce += fcz;
		} else {
/* cut odd total */
			cot += cz;
			cot += cpt;
			fco += fcz;
		}

		lltoasc( anum, cz + cpt );

/* cut 001 to last cut: cut size includes pre-amble for each cut */
		fprintf( stdout, "%3d @ %6d ", i+1, scuts[i]);
		lltoasc( anum, cs );
		fprintf( stdout, "%16s ", anum );
		lltoasc( anum, cz + cpt);
		fprintf( stdout, "%16s ", anum );

		show_frame_timecode( fcs );
		fprintf( stdout, " ");
		show_frame_timecode( fcz );
		fprintf( stdout, "%7d ", fcz);
		fprintf( stdout, " %3lld%%", (cz * 100) / in_size);
		fprintf( stdout, "\n");
	}

#if 0
/* data after last cut not displayed because it's irrelevant */
	fprintf( stdout, "%3d @ %6d %11lld %11lld  End cruft\n",
		scut_idx,
		sequence_idx - 1, 
		sequences[ sequence_idx - 1 ],
		in_size - sequences[ scuts[ scut_idx - 1 ]]
	);
#endif

	fprintf( stdout, "\nEnd of cut list\n");

	statfs( arg_path, &fs);
	vbf = fs.f_bsize * fs.f_bavail;
	fprintf( stdout, "Free space  %11lld bytes in volume %s:\n",
		 vbf, arg_path);

	fprintf( stdout, " EVEN cut time");
	show_frame_timecode( fce );
	fprintf( stdout, ", bytes %11lld, ", cet);
	fprintf( stdout, "frames %6d (%2d%%), will %sfit.\n",
			fce, (100 * fce) / frame_idx, (cet < vbf) ?"":"NOT " );

	fprintf( stdout, "  ODD cut time");
	show_frame_timecode( fco );
	fprintf( stdout, ", bytes %11lld, ", cot);
	fprintf(stdout, "frames %6d (%2d%%), will %sfit.\n",
			fco, (100 * fco) / frame_idx, (cot < vbf) ?"":"NOT " );

	fprintf( stdout, "  ALL cut time");
	show_frame_timecode( fca );
	fprintf( stdout, ", bytes %11lld, ", cat);
	fprintf(stdout, "frames %6d (%2d%%), will %sfit.\n",
			fca, (100 * fca) / frame_idx, (cat < vbf) ?"":"NOT " );

	fprintf( stdout, "\n" );
}
#endif


/* search forwards and backwards in frames[] and pick nearest I frame */
/* f is the frame number from where to start looking, inclusive */
static
int
find_nearest_iframe ( int f, char *caller )
{
	int i, pi, ni;

	WHOAMI;

	pi = ni = -1;

	iprintf( stdout, "%s:%d %s %d\n\n", WHO, WHERE, caller, f);

	if (0 == f) return 0;


	if (f >= frame_idx) {
	    f = frame_idx - 1;
	    iprintf(stdout, "%s last frame %d, last sequence frame %d \n",
			WHO, f, iframes[iframe_idx - 1 ]);
	}

/* search backwards until an Intra frame found or start of list reached */
	for (i = f; i >= 0; i--) {
		if (1 == frames[i].pct)	{
			pi = i;
			break;
		}
	}

/* search forwards until an Intra frame found or end of list reached */
	for (i = f; i < frame_idx; i++) {
		if (1 == frames[i].pct) {
			ni = i;
			break;
		}
	}

//	fprintf(stdout, "%s f %d p %d n %d\n\n", WHO, f, pi, ni);

/* both negative is invalid, return frame 0 */
	if (( pi < 0) && (ni < 0)) return 0;

/* is previous or next invalid? use other value if so */
	if ( pi < 0 ) return ni;
	if ( ni < 0 ) return pi;

	if ( pi == ni ) return pi;
	i = pi;

/* if distance to previous is more than distance to next, use next */
	if ( (f - pi) > (ni - f) ) i = ni;

//	fprintf( stdout, "%s frames[%d].num %d\n", WHO, i, frames[i].num);

	return i;
}


/* Search forward in frames[] to get frame number of next I frame. */
/* f is the frame number from where to start looking, non-inclusive */
static
int
find_next_iframe ( int f )
{
	int i, j;

	WHOAMI;

	j = f;

/* search forwards until out of frames or Intra found */
	for (i = (f + 1); i < frame_idx; i++)
		if (1 == frames[i].pct)
			return i;
	return j;
}


/* Search backward in frames[] to get frame number of previous I frame. */
/* f is the frame number from where to start looking, non-inclusive */
static
int
find_prev_iframe ( int f )
{
	int i, j;

	WHOAMI;

	j = f;

/* search backwards until out of frames or Intra found */
	for (i = (f  - 1); i >= 0; i--)
		if (1 == frames[i].pct)
			return i;
	return j;
}
/****************************************************************************/


/************************************************************** X functions */

/* compute vertical y offset from fontsize and text line (row) number */
/* xfonts is what font was loaded */
/* FIXME: doesn't check for offscreen */
static
int
x_fontline ( int row )
{
	int ytot;
	if (0 == xinit) return 0;	
//	if (0 != arg_xmsg) WHOAMI;

/* total font height = cell height (ascender) + descender + blank */
	ytot = xfonts->ascent + xfonts->descent + 1; /* 1 or 2 blanks? */

/* however, font baseline does not count descender (leave a blank?) */
/* should already have ytot correct with ascent+descent+blank */
	return (row * ytot) + xfonts->ascent + 1;
/*	return (row * ytot); */
}
static
int
x_fontwide ( int col )
{
	int xtot;

	if (0 == xinit) return 0;

	xtot = xfonts->max_bounds.rbearing - xfonts->min_bounds.lbearing;

	return (col * xtot);
}

/* set font for text display. this doesn't try to revert to "fixed" font */
/* return is 0 if font was not loaded and nz if font was loaded */
static
int
x_setfont ( int w, int h )
{
	char fn[64];

	if (0 == xinit) return 0;

	snprintf(fn, sizeof(fn), "%dx%d", w, h);
	fn[63] = 0;

//	if (0 != arg_xmsg) WHOAMI;

/* set the user requested font, if it fails fall back to tiny fixed */
	xfonts = XLoadQueryFont(xdisplay, fn);
	if ( xfonts != (XFontStruct*)NULL ) {
		XSetFont(xdisplay, xgc, xfonts->fid);
		return 1;
	}

	return 0;
}


/* set font for text display. caller must have already set bg and fg */
static
void
x_set_font ( char *fn )
{
	if (0 == xinit) return;

//	if (0 != arg_xmsg) WHOAMI;

/* set the user requested font, if it fails fall back to tiny fixed */
	xfonts = XLoadQueryFont(xdisplay, fn); /* "fixed" or "10x20" */
	if ( xfonts == (XFontStruct*)NULL ){
		fprintf(stdout,"can't load %s font trying fixed\n", fn);
		xfonts = XLoadQueryFont(xdisplay,"fixed");
		if ( xfonts == (XFontStruct*)NULL) {
			fprintf(stdout,"can't load fixed font\n");

		} else {
			xprintf(stdout,"X font %s loaded\n", fn);
/* should probably abort here */

		}

	} else {
		XSetFont(xdisplay, xgc, xfonts->fid);
//		xprintf(stdout,"X font %s selected\n", fn);
	}
}


static
void
x_title ( void )
{
	if (0 != arg_xmsg) WHOAMI;

	if (0 == xinit) return;

	/* tell window manager about this window: title, icon, hw, xy */
	/* XSetWMProperties is more hassle than is needed here */
	XSetStandardProperties(	xdisplay,
				xwindow,
				xtitle,
				xtitle,
				None,
				NULL,
				0,
				&xhint
			    );
}


/* this draws tc color text without background fill */
static
void
x_draw_text_line ( unsigned int tc, int x, int y, char *t )
{
	if (0 == xinit) return;	
	XSetForeground( xdisplay, xgc, tc );
/* XDrawString draws fg only */
        XDrawString( xdisplay, xwindow, xgc, x, y, t, strlen(t) );
	XSetForeground( xdisplay, xgc, xfg );
}


/* this draws tc color text on current background color fill */
static
void
x_draw_text_line_bg ( unsigned int tc, int x, int y, char *t )
{
	if (0 == xinit) return;	
	XSetForeground( xdisplay, xgc, tc );
/* XDrawImageString draws fg + bg */
        XDrawImageString( xdisplay, xwindow, xgc, x, y, t, strlen(t) );
	XSetForeground( xdisplay, xgc, xfg );
}

/* this draws tc color text with transparent background fill */
static
void
x_draw_text_block ( unsigned int tc, int x, int y, char *s )
{
	char b[132];
	char *t, *e;

	if (0 == *s) return;

	t = b;
	e = t + sizeof(b);
	e--;

	while( 0 != *s ) {
		if ( (t == e) || ('\n' == (*t++ = *s++)) ) {
			t--;
			*t = 0;
			y += x_fontline(0);
			XSetForeground( xdisplay, xgc, tc );
    			XDrawString( xdisplay, xwindow, xgc,
					    x, y, b, strlen(b) );
			XSetForeground( xdisplay, xgc, xfg );
//			x_draw_text_line(fgc, x, y, b);
//			fprintf(stdout, "@%dx%d %s\n", x, y, b);
			t = b;
		}
	}
}

/* find the longest line in p, either NL term or NULL term */
static
int
find_longest_line ( char *p )
{
	int i, j;

	i = j = 0;

	while (0 != *p) {
		if ('\n' == *p++) {
			if (i > j) j = i;
			i = 0;
		} else {
			i++;
		}
	}
	return j;
}

/* count the NL chars */
static
int
find_line_count ( char *p )
{
    int i;
    i = 0;
    while (0 != *p) if ('\n' == *p++) i++;
    return i;
}

/* find a font that will fit inside the x, y pixel box
    with text chars w wide and int h tall

    returns index to xpcf or -1 if can't fit the text
*/
static
int
x_find_quad_font ( int x, int y, int w, int h )
{
    int i, j, w1, h1, x1, y1;
    fnt_t *f;

/* 5x8 font is bare minimum, get out now if that won't fit */
    if (x < (5*w)) return -1;
    if (y < (8*h)) return -1;

/* largest font is index 0, keep going until something fits */
    for (i = 0; i < xpcf_idx; i++) {
	f = &xpcf[i];
	j = x_setfont( f->w, f->h );
	if (0 == j) {
	    dprintf(stdout, "%s could not find font %s\n", WHO, f->name);
	    continue;
	}

	w1 = x_fontwide(1);
	x1 = w * w1;
	h1 = x_fontline(0);
	y1 = h * h1;
	dprintf( stdout, "%s %s i %d x %d y %d w %d h %d x1 %d y1 %d w1 %d h1 %d\n",
		     WHO, f->name, i, x, y, h, w, x1, y1, w1, h1 );
	if (x1 > x) continue; // 1px left/right?
	if (y1 > y) continue; // 1px top/bottom?
	return i;
    }
    return -1;
}


static
void
x_draw_keyhelp ( void )
{
	int i, j, x, y, w, h, m;
	fnt_t *f;
	char *s;

	if (1 != xhelp) return;

//	fprintf(stdout, "%s\n", WHO);

	s = keyhelp;
	build_keyhelp( s, sizeof(keyhelp), 0 );
	w = find_longest_line( s ); // for centering
	j = h = find_line_count( s );

	x = vw.w;
	y = vw.h;
	y -= 50;
	m = 8;

/* which font will fit the keyhelp text in the video area */
	i = x_find_quad_font( x + m, y + m, w, h );

/* no font will fit, nop */
	if (-1 == i) {
		fprintf(stdout,"No font small enough for keyhelp\n");
		return;
	}

	f = &xpcf[i];

	w *= x_fontwide(1);
	x = vw.w - (w + m);
	x >>= 1;

	h *= x_fontline(0);
	y = vw.h - (h + m);
	y >>= 1;

	x_draw_quad_gradientv( COLOR_MAGENTA0, x, y, w + m, h + m, -0.66 );
	m >>= 1;
	x_draw_text_block( COLOR_WHITE, x + m, y + m, s );
}

static
void
x_draw_cfghelp( void )
{
	int i, j, x, y, w, h, m;
	fnt_t *f;
	char *s;

//	fprintf(stdout, "%s\n", WHO);

	s = cfghelp;

	if (2 != xhelp) return;

	build_cfghelp( s, sizeof(cfghelp), 0 );
	w = find_longest_line( s ); // for centering
	j = h = find_line_count( s );

	x = vw.w;
	y = vw.h;
	y -= 50;
	m = 8;

/* which font will fit the keyhelp text in the video area? */
	i = x_find_quad_font( x + m, y + m, w, h );

/* no font will fit, nop */
	if (-1 == i) {
		fprintf(stdout,"No font small enough for cfghelp\n");
		return;
	}

	f = &xpcf[i];

	w *= x_fontwide(1);
	x = vw.w - (w + m);
	x >>= 1;

	h *= x_fontline(0);
	y = vw.h - (h + m);
	y >>= 1;

	x_draw_quad_gradientv( COLOR_GREEN0, x, y, w + m, h + m, -0.66 );
	m >>= 1;
	x_draw_text_block( COLOR_WHITE, x + m, y + m, s );
}

static
void
x_draw_cutlist( void )
{
	int i, j, x, y, w, h, m;
	fnt_t *f;
	char *s;

	WHOAMI;

	s = cutlist;
//	if (0 == *s) return;

	if (3 != xhelp) return;

	build_cutlist( s, sizeof(cutlist) );

//	fprintf(stdout, "%s\n", s);

	w = find_longest_line( s ); // for centering
	j = h = find_line_count( s );

	x = vw.w;
	y = vw.h;
	y -= 50;
	m = 8;

/* which font will fit the keyhelp text in the video area? */
	i = x_find_quad_font( x + m, y + m, w, h );

/* no font will fit, nop */
	if (-1 == i) {
		fprintf(stdout,"No font small enough for cutlist\n");
		return;
	}

	f = &xpcf[i];

	w *= x_fontwide(1);
	x = vw.w - (w + m);
	x >>= 1;

	h *= x_fontline(0);
	y = vw.h - (h + m);
	y >>= 1;

	x_draw_quad_gradientv( COLOR_BLUE, x, y, w + m, h + m, -0.66 );
	m >>= 1;
	x_draw_text_block( COLOR_WHITE, x + m, y + m, s );
}


static
void
x_draw_outlist( void )
{
	int i, j, x, y, w, h, m;
	unsigned int c;
	fnt_t *f;
	char *s;

	WHOAMI;

	s = outlist;
//	if (0 == *s) return;

	if (4 != xhelp) return;

	w = find_longest_line( s ); // for centering
	j = h = find_line_count( s );

	x = vw.w;
	y = vw.h;
	y -= 50;
	m = 8;

/* which font will fit the keyhelp text in the video area? */
	i = x_find_quad_font( x + m, y + m, w, h );

/* no font will fit, nop */
	if (-1 == i) {
		fprintf(stdout,"No font small enough for cut write list\n");
		return;
	}

	f = &xpcf[i];

	w *= x_fontwide(1);
	x = vw.w - (w + m);
	x >>= 1;

	h *= x_fontline(0);
	y = vw.h - (h + m);
	y >>= 1;

	c = COLOR_RED;
	if (0 != scut_done) c = COLOR_GREEN;

	x_draw_quad_gradientv( c, x, y, w + m, h + m, 0.50 );
	m >>= 1;
	x_draw_text_block( COLOR_WHITE, x + m, y + m, s );
}



/* Draw frame timecode at the top of the histogram.
    Even cuts have brighter text than odd cuts.

    x is horizontal offset
    f is frame number
    c is 0 even cut, 1 odd cut
*/
static
void
x_draw_timecode ( int x, int f, int c )
{
	int p, h, m, s;
	unsigned int fg, bg, yoffset;
	char xtext[16];

	if (0 == xinit) return;	
	
//	if (0 != arg_xmsg) WHOAMI;

/* this seems to work well. use it for show_frame_timecode too */
	p = f * 100;
	p /= frame_rate;
	s = p % 60;
	m = p / 60;
	h = p / 3600;

	if (0 == arg_ft) {
	    snprintf( xtext, sizeof(xtext), "%d", f );
	} else {
	    snprintf( xtext, sizeof(xtext), "%02d:%02d:%02d", h, m, s );
	}	

	x_setfont( 10, 20 );

	yoffset = x_fontline(0)-1; /* text origin is font baseline */

	fg = COLOR_WHITE;
	bg = COLOR_BLACK;

	if (0 != c) {
	    fg = COLOR_RED;
//	    bg = COLOR_WHITE;
	}

//	XSetBackground(xdisplay, xgc, bg);

	x_draw_text_line( fg, x+2, yoffset, xtext);

//	XSetBackground(xdisplay, xgc, xbg);
}


/* TESTME: These may need re-ordering to reduce display flicker */
/* Time goes at top of line, cut number goes at bottom of line */
/* These are drawn to left of line so following lines do not overwrite it */
static
void
x_draw_cutnum ( int x, int n)
{
	int y, x1;
	char t[16];

	if (0 == xinit) return;	

//	if (0 != arg_xmsg) WHOAMI;

/* draw cut number n at bottom of red vertical line at x */
/* use spaces to draw black background to left and right of this */
	snprintf( t, sizeof(t)-1, "%d", n);

/* want this to fit within the new eye candy area */
	x_setfont( 10, 20 );

	y = vw.h - 3;
/* 10-12 cuts is most common for 60m, 6-8 for 30m, 16-20 for 2h */

/* FIXME: need char spacing for monospace or lookup proportional */

/* 10x20 font is more readable than fixed */
	x1 = x_fontwide(1);

/* 10-12 cuts is most common for 60 minutes */
	if (n > 9) x1 = x_fontwide(2);

/* won't see 100 cuts unless -i autocut gets it wrong */
	if (n > 99) x1 = x_fontwide(3);

	if (0 == arg_ec) {
		x_draw_text_line_bg( COLOR_WHITE, x-x1, y, t);
	} else {
		x_draw_text_line( COLOR_WHITE, x-x1, y, t);
	}
}


/* draw the IPB legend */
static
void
x_draw_legend ( void )
{
	int x, y, h, c;
	char t[8] = "I P B";

	if (0 == xinit) return;	
	if (0 == arg_lb) return;

//	if (0 != arg_xmsg) WHOAMI;

	x_set_font( "fixed" );

//	fprintf(stdout, "xfw %d xfh %d\n", x_fontwide(1), x_fontline(0));

	h = x_fontline(0);	/* query text height */

/* first the text */

/* FIXME: center needs to have a better algorithm reflecting legend width */
	x = (vw.w/2)-15; /* 10 pixels per cell * 3 cells, center of it */
	y = vw.h;

	y -= 2 * h;
	y -= 4;

/* blank the area used for buttons, bottom center */
//	if (0 != arg_lb)
//		x_draw_quad_fill( 0, x-8, y - ( 4 * h ), 44, 4 * h );
		x_draw_quad_gradientv( COLOR_GREY1,
			     x-8, y - ( 4 * h ), 44, 4 * h, 1.0 );

	byh = 16;
	bys = y - byh;
	tys = y - (3 * byh);
	tys += 3;

	bxi = x;

/* set button area region */
	bax = x - 8;
	bay = y - 4 * h;
	baw = 44;
	bah = 4 * h;

	y -= h;

/* now the blocks with colors, if enabled */
	c = COLOR_CYAN;
	if (0 == bis) c = 0;
	if (0 != arg_lb) x_draw_quad_fill( c, x, y-5, 8, 10);

	x += 10;
	bxp = x;
	c = COLOR_MAGENTA;
	if (0 == bps) c = 0;
	if (0 != arg_lb) x_draw_quad_fill( c, x, y-5, 8, 10);

	x += 10;
	bxb = x;
	c = COLOR_YELLOW;
	if (0 == bbs) c = 0;
	if (0 != arg_lb) x_draw_quad_fill( c, x, y-5, 8, 10);

/* button labels */
	x = bxi;
	y -= h - 3;
	if (0 != arg_lb) x_draw_text_line( COLOR_WHITE, x-1, y, t);

/* time-code or frame number */
	if (0 == arg_ft) {
	    snprintf( t, sizeof(t), "%d", xlfd);
	} else {
	    int p, s, m;
	    p = xlfd * 100;
	    p /= frame_rate;
	    s = p % 60;
	    m = p / 60;
	    snprintf( t, sizeof(t), "%02d:%02d", m, s );
	}
	x = vw.w >> 1;
	x -= (2 * strlen(t));
	x -= 5;
	y -= h;
	y -= 3;

/* needs a background if ipb buttons are not displayed */
	if (0 == arg_lb)
	     x_draw_quad_fill( 0, x-8, y - h, 44, h + 5 );

	x_draw_text_line( COLOR_WHITE, x, y+1, t);
}


#if 0
/* This shows up when histogram scale is set to 100%. */
/* Disabled since it's not helpful for editing. */
static
void
x_draw_reticle( void )
{
	int x, y, x1, y1;
	char t[32];

	x = 0;
	x1 = vw.w;

	x_set_font("fixed"); // 6x12

	y1 = vw.h >> 3;

	for (y = y1; y < vw.h; y += y1) {
		snprintf(t, sizeof(t), "%d bits", 8 * TSZ * y);
		x_draw_line( COLOR_WHITE, x, y, x1, y);
		x_draw_text_line_bg( COLOR_WHITE, x, y+12, t );
	}
}
#endif


#define USE_DRAW_SEGMENTS
/* Visual graph of packet usage for each of I P and B frame types.
   NOTE: This does include the audio packets, but audio only adds
    around 10 to 20 packets per frame depending on audio resolution.

    ->vpn is misleading, should be ->spn for stream packet number.

    XDrawSegments can be optionally used to eliminate some ugly visual
    artifacting when changing histogram scale with B + P lines on.

    Apparently with individual XDrawLine()'s, the CRT refresh catches it.

    Artifact is a black right triangle with 90 degrees angle at top right.
    Theta increases proportional to the height of the histogram until
    it finally covers about top right third of the screen at hist scale 1.0.
    It goes away as soon as you stop changing the scale but it is annoying.

    Also, XDrawSegments draws the P and B lines seemingly a lot faster.
    
*/
static
void
x_draw_histogram ( void )
{
	int i, j, x, y, h, w, c, r, p;
	double a;
	frame_t *f;

#ifdef USE_DRAW_SEGMENTS
#warning using XDrawSegment for histogram
	int k;
	XSegment *xseg;
	XSegment *xseg1;
	xseg = icalloc( vw.w, sizeof(XSegment), "xseg" );
#endif


	if (0 == xinit) return;	
	if (0 == xredraw) return;

	if (0 != arg_xmsg) WHOAMI;


/* build any possible cuts for display */
//	build_scut_frames();

	if (xoffset < 0) xoffset = 0;

	a = 0.0;

	x = y = 0;
	h = vw.h;
	w = vw.w;

/* blank or shade top 16 lines for timecode redraw */
/* TODO: should be font lines size? */
	y = 0;
	if (0 != arg_tc) {
		y = 20;		/* font is 10x20 for timecode */
//		x_draw_quad_fill( 0, 0, 0, w, y);

/* corners need to have color set to black so gradient shows up */
		x_draw_quad_fill( COLOR_BLACK, 0, 0, 8, y);
		x_draw_quad_fill( COLOR_BLACK, w - 8, 0, 8, y);

		x_draw_quad_gradientv( COLOR_GREY1, 0, 0, w, y, -1.0 );
	}

/* draw a shaded bar for the cut numbers to show up against */
	if (0 != arg_ec) {
/* corners need to have color set to black so gradient shows up */
		x_draw_quad_fill( COLOR_BLACK, 0, h-20, 8, 20);
		x_draw_quad_fill( COLOR_BLACK, w - 8, h-20, 8, 20);
		x_draw_quad_gradientv( COLOR_GREY1, 0, h-20, w, 20, 1.0 );
	}

	p = 0;

#ifdef USE_DRAW_SEGMENTS
	if ((0 != arg_hl) && (0 != bps)) {
	    XSetForeground( xdisplay, xgc, COLOR_MAGENTA );
	    if (0 != arg_tc) y = 18;
//	    x_draw_line( c, x, y, x, r + y );
	    k = 0;
	    for (i = 0; i < w; i++) {
		j = xoffset + i;
		x = i;
		f = &frames[j];
		if (2 != f->pct) continue;
		a = hist_scale * (double)f->fpc;
/* frame packet count */
		r = a;
		xseg1 = &xseg[k];
		xseg1->x1 = 0xFFFF & x;
		xseg1->x2 = 0xFFFF & x;
		xseg1->y1 = 0xFFFF & y;
		xseg1->y2 = 0xFFFF & (r + y);
		k++;
	    }

	    if (k > 0)
		XDrawSegments(xdisplay, xwindow, xgc, xseg, k);
	}

	if ((0 != arg_hl) && (0 != bbs)) {
	    XSetForeground( xdisplay, xgc, COLOR_YELLOW );
	    if (0 != arg_tc) y = 20;

	    k = 0;
	    for (i = 0; i < w; i++) {
		j = xoffset + i;
		x = i;
		f = &frames[j];
		if (3 != f->pct) continue;
		a = hist_scale * (double)f->fpc;
/* frame packet count */
		r = a;
		xseg1 = &xseg[k];
		xseg1->x1 = 0xFFFF & x;
		xseg1->x2 = 0xFFFF & x;
		xseg1->y1 = 0xFFFF & y;
		xseg1->y2 = 0xFFFF & (r + y);
		k++;
	    }

	    if (k > 0)
		XDrawSegments(xdisplay, xwindow, xgc, xseg, k);
	}

	ifree( xseg, "xseg" );
#endif

	for (i = 0; i < w; i++) {
	    j = xoffset + i;
	    x = i;

/* boundary limit */
	    if (j >= frame_idx) break;

	    f = &frames[j];

	    a = hist_scale * (double)f->fpc;

/* frame packet count */
	    r = a;
	    
	    c = 0;

#ifdef USE_DRAW_SEGMENT
	    if (1 != f->pct) continue;
#endif
	    switch( f->pct ) {

/* empty entry is a blank line. drawing a blank would erase the render */

/* I frame lines stick above B frame lines by 4 pixels */
/* but want pixels below lines 0-19 to represent scaled packet count */

/* Except for some events on FOX or MNT, I frames aren't drawn enough
   to require the above XDrawSegments speed-up.
*/
	    case 1:

/* most I frame lines start under the time */

		if (0 != arg_tc) y = 16;
/* limit frame time display to every 60 frames at most */
		if ( (0 == i) || (j >= (p+120)) ) {
		    if (0 != arg_tc) {

/* want the line extended upwards to be next to the time code */
			x_draw_line( COLOR_WHITE, x, 0, x, y - 1 );
			x_draw_timecode( i, j, f->tcolor );
		    }
		    p = j;
		}

/* cut is indicated by red line */
		if (0 != f->cut) {
		    c = COLOR_RED;

/* current preview overrides to white line */
		    if (j == xlfd) c = COLOR_WHITE;

/* draw line after drawing cutnum */
		    x_draw_cutnum( x, f->cutnum );
//		    fprintf(stdout, "cut point y %d\n", y);
		    x_draw_line( c, x, y, x, h );
		    break;
		}

/* current preview position is indicated by white line */
		if (j == xlfd) {
		    c = COLOR_WHITE;
//		    fprintf(stdout, "current preview y %d\n", y);
		    x_draw_line( c, x, y, x, h );
		    break;
		}

/* non cut draws and colors the line if I frame histogram selected */
		if ((0 != arg_hl) && (0 != bis)) {
		    c = COLOR_CYAN;
//		    fprintf(stdout, "normal y %d\n", y);
		    x_draw_line( c, x, y, x, r + y );
		    break;
		}

/* whoops missing, caused P to show up as I */
		break;

/* X Draw Segments should be faster when these are enabled */
#ifndef USE_DRAW_SEGMENTS
/* P frame lines are 2 pixels lower than I frame lines */
	    case 2:
		if ((0 != arg_hl) && (0 != bps)) {
		    c = COLOR_MAGENTA;
		    if (0 != arg_tc) y = 18;
		    x_draw_line( c, x, y, x, r + y );
		}
		break;

/* B frame lines are 2 pixels lower than P frame lines */
	    case 3:
		if ((0 != arg_hl) && (0 != bbs)) {
		    c = COLOR_YELLOW;
		    if (0 != arg_tc) y = 20;
		    x_draw_line( c, x, y, x, r + y );
		}
		break;

#endif

	    default:
		break;
	    }

	}

#if USE_RETICLE
	if ( 1.0 == hist_scale ) x_draw_reticle();
#endif

/* This has some odd problems with scaling and colors */
#ifdef USE_IMLIB2_DRAW
	if (NULL != image)
	    imlib_render_image_on_drawable_at_size( 0, 0, vw.w, vw.h );
#endif
}


/* Frame slider bead, visual indication of stream position.
   TODO: "button1 on slider" replaces "button2 anywhere"
*/
static
void
x_draw_slider ( void )
{
	int i, x, y, w, x1, x2;

	if (frame_idx < 1) return;
	if (0 == arg_sb) return;

	x = xoffset * vw.w;
	x /= frame_idx;
	w = vw.w * vw.w;
	w /= frame_idx;

	y = vw.h - 21;

/* if no cuts, use dark grey bead line across screen */
	if (0 == scut_idx) {
	    x_draw_line( COLOR_GREY0, 0, y, vw.w,  y );
	} else {

/* if there are cuts, try to represent them by a long broken bead line */
	    x1 = 0;
/* NOTE:
    The problem with color selection is a visual refraction issue which can
    make red and green and blue lines appear at slightly different heights.
    The refraction issue may be reduced somewhat when using CMY on grey.
*/
	    for (i = 0; i < scut_idx; i++) {

/* what is the x endpoint for this segment? */
		x2 = iframes[scuts[i]];
		x2 *= vw.w;
		x2 /= frame_idx;

/* cut[0] is first cut, is odd numbered cut */
		if (0 == (1 & i)) {
		    x_draw_line( COLOR_CYAN, x1, y, x2, y );
		} else {
		    x_draw_line( COLOR_GREY0,  x1, y, x2, y );
		}
		x1 = x2;
	    }

	    if (x1 < frame_idx) {
		    x_draw_line( COLOR_GREY0, x1, y, vw.w, y );
	    }
	}

/* A visual problem is: viewpoint bead is 3 wasted draws on short streams. */
	if (frame_idx > vw.w)
	{
	    y--;
/* one extra black background line above when eye candy enabled */
	    if (0 != arg_ec)
		x_draw_line( COLOR_BLACK, 0, y, vw.w, y );

/* top line of of viewpoint bead is light grey */
	    x_draw_line( COLOR_GREY1, x, y, x + w, y );
	    y++;
/* middle line of viewpoint bead is white */
	    x_draw_line( COLOR_WHITE, x, y, x + w, y );

/* bottom line of viewpoint bead is light grey */
	    y++;
/* one extra black background line below when eye candy enabled */
	    if (0 != arg_ec)
		x_draw_line( COLOR_BLACK, 0, y, vw.w, y );
	    x_draw_line( COLOR_GREY1, x, y, x + w, y );
	}
}


static
void
x_draw_image ( char *caller )
{
	int o;

	if (0 == xinit) return;

	xprintf(stdout, "%s->%s\n", caller, WHO);

	if (0 != arg_xmsg) WHOAMI;

/* create new image from current middle position */
	if (0 != xumove) {
		o = xoffset + (vw.w>>1);

/* E) short stream starts preview at frame 0 */
		if (frame_idx <= vw.w) {
//		    if (o > frame_idx)
			o = 0;
		}

/* start at first iframe after offset */
		xcfd = find_nearest_iframe( o, WHO );
		if (xcfd != xlfd) {
/* decode the Intra frame to an image buffer */
			parse_intra_frame( sequences[frames[ xcfd ].num], WHO );
			xlfd = xcfd;
		}
		xumove = 0;
	}

#ifdef USE_LIBVO
        if (NULL != xoutput->draw) {
	    if (NULL != m2inf->display_fbuf->id) {
		if (NULL != m2inf->display_fbuf->buf) {
			xoutput->draw( xoutput,
				m2inf->display_fbuf->buf,
				m2inf->display_fbuf->id );
		}
	    }
	}
#endif



/* imlib handles the video resizing */
#ifdef USE_IMLIB2
	if (NULL != image) {
	    imlib_render_image_on_drawable_at_size( 0, 0, vw.w, vw.h );
	}
#endif
#ifdef USE_IMLIB1
	if (NULL != image) {
	    Imlib_apply_image( idata, image, xwindow );
	}
#endif
}


static
void
x_redraw_display ( void )
{
	if (0 == xinit) return;
	if (0 == xredraw) return;

	if (0 != arg_xmsg) WHOAMI;

//#ifdef USE_LIBVO
/* YUV overlay frame can't serve as histogram clear like imlib frame does */
//	x_clear_display();
//#endif
	x_draw_image( WHO );
	x_draw_histogram();
	x_draw_legend();
	x_draw_slider();
	x_draw_keyhelp();
	x_draw_cfghelp();
	x_draw_cutlist();
	x_draw_outlist();

	xumove = 0;
	xredraw = 0;
}


/* shorthand, everyone knows which display window it is */
static
Bool
x_check_twe( int event_type, XEvent *event_return )
{
//	if (0 != arg_xmsg) WHOAMI;
	return
	 XCheckTypedWindowEvent( xdisplay, xwindow, event_type, event_return);
}

/***********************************************************************
 *  start of main block of X window support functions
 *  FIXME: all of these functions need some kind of error handling
 ***********************************************************************/

/* this one is still buggy, eventually crashes the display */
#ifdef USE_MAXIMIZE
static
void
x_resize_display ( void )
{
	XSetWindowAttributes nxswa;
	unsigned long nxswamask;


	if (0 == xinit) return;

	if (0 != arg_xmsg) WHOAMI;

	xredraw = ~0;
	nxswamask = CWOverrideRedirect;
	
	if (0 != xmaxi) {

	    /* unmap the window so we can change attributes */
	    /* loses focus? unmap sent back to previous window? */

	    XUnmapWindow( xdisplay, xwindow );

	    /* wait and eat notify events until we get UnmapNotify */
	    do {} while ( False == x_check_twe( UnmapNotify, &xev ) );

		/* turn off window manager dressing */
		nxswa.override_redirect = True;
		XChangeWindowAttributes( xdisplay,
					xwindow,
					nxswamask,
					&nxswa
					);

		/* map the window back onscreen with new attributes */
		XMapWindow( xdisplay, xwindow );

		/* wait and eat notify events until we get MapNotify */
		do {} while ( False == x_check_twe( MapNotify, &xev ) );

#if 1
/* set focus to the fullscreen window so xinput works.
   NOTE: doesn't seem to work if it's in x_init only
*/
		XSetInputFocus(	xdisplay,
				xwindow,
				RevertToParent,
				CurrentTime
			    );
#endif


		/* maximize the window */
		XMoveResizeWindow(	xdisplay, xwindow,
					xattr.x, xattr.y,
					xattr.width, xattr.height
				);
/* wait for ConfigureNotify event
   have to do this because otherwise xevent() will see
   the resize and try to change the aspect ratio

    still true?
*/
		do {} while ( False == x_check_twe( ConfigureNotify, &xev ) );

/* save current window settings for later restore,
   update current overlay w/ root window attribs, full-screen
*/
		vw0.x = vw.x;
		vw0.y = vw.y;
		vw0.w = vw.w;
		vw0.h = vw.h;

		vw.x = xattr.x;
		vw.y = xattr.y;
		vw.w = xattr.width; /* root window size */
		vw.h = xattr.height; /* root window size */

/* Sequence parse should have been done, so this should work by this point. */
		vw.h = calc_aspect_h( vw.w ); /* aspect height fix */

		if (vw.h > xattr.height) {
		    fprintf( stdout, "\nNeeds calc_aspect attribs fix\n");
		    exit(1);
		}

		xprintf(stdout,"\tMaximized window x%d y%d w%d h%d\n",
			xattr.x, xattr.y, xattr.width, xattr.height);
		xprintf(stdout,"\tOverlay window x%d y%d w%d h%d\n",
			vw.x, vw.y, vw.w, vw.h);

	} else {
		/* restore overlay and window to previous saved.
		   have to adjust for titlebar height again too */
		vw.x = vw0.x;
		vw.y = vw0.y;
		vw.w = vw0.w;
		vw.h = vw0.h;

/* unmap the window so we can change its attributes */
		XUnmapWindow( xdisplay, xwindow);

/* wait and eat notify events until we get UnmapNotify */
		do {} while ( False == x_check_twe( UnmapNotify, &xev ) );

/* turn on window manager dressing */
		nxswa.override_redirect = False;
		XChangeWindowAttributes(    xdisplay,
					    xwindow,
					    nxswamask,
					    &nxswa
					);


/* put cursor back to normal for windowed */
//		XUndefineCursor( xdisplay, xwindow);

/* map the window back onscreen with restored attributes */
		XMapWindow( xdisplay, xwindow );

/* wait and eat notify events until MapNotify */
		do {} while ( False == x_check_twe( MapNotify, &xev ) );

/* restore window size to previously saved size, minus the titlebar height */
		XMoveResizeWindow(	xdisplay,
					xwindow,
					vw.x,
					vw.y, /* +xtbh, */
 					vw.w,
					vw.h
					);

/* wait for and eat the ConfigureNotify event.
   have to do this because otherwise x events will see the resize
   and try to change the aspect ratio [?old cruft?]
*/
		do {} while ( False == x_check_twe( ConfigureNotify, &xev ) );

		xprintf(stdout,"\tRestored x%d y%d w%d h%d\n",
			vw.x, vw.y, vw.w, vw.h);
	}
}
#endif

/* initialize libmpeg2 and load the first Sequence
   also set the default video window size from the first sequence data
*/
static
void
init_mpeg2 ( void )
{
	char *t;

	WHOAMI;

	accel = mpeg2_accel( arg_mx );

#ifdef USE_IMLIB1
	t = votypes[0];
#endif
#ifdef USE_IMLIB2
	t = votypes[1];
#endif
#ifdef USE_LIBVO
	t = votypes[2 + arg_vo];
#endif

	if ((accel >= 0) && (accel < 8))
	    fprintf( stdout, "\n%s using x86 FPU (%d) %s; %s\n",
		    WHO, accel, x86mx[accel], t);

	m2dec = mpeg2_init();
	if ( NULL == m2dec ) {
		fprintf( stdout, "%s could not initialize decoder.\n", WHO );
		exit (1);
	}

	m2inf = mpeg2_info( m2dec );
	if ( NULL == m2inf ) {
		fprintf( stdout, "%s could not initialize info.\n", WHO );
		exit (1);
	}
	m2init = ~0;

/* parse first I frame to load frame size, frame rate and bitrate info */
	parse_intra_frame( sequences[0], WHO );
	fprintf(stdout, "%s parse_intra_frame %09llX\n", WHO, sequences[0] );
	
/* anyone that reports 1080 must be stupid. clamp it to 16 pixel macroblock */
	if (1080 == frameh) frameh = 1088;

/* -w specifies a divisor for the window frame, otherwise uses 960x544 */
/* TODO: better: automatically find which divisor will fit display */
	if (0 != arg_wd) {
		vw.w = framew / arg_wd;
		vw.h = frameh / arg_wd;
	}

	fprintf(stdout,	"Picture %dx%d, aspect %3.2f, div %d, hints %dx%d\n",
		framew, frameh, aspectf, arg_wd, vw.w, vw.h );
}

#ifndef USE_LIBVO
/* This is done after the X display has been initialized */
/* initialize imlib and put the splash on screen. */
static
void
x_init_imlib ( void )
{
	char ft[256], *t0, *t1, *t2, *t3;
	int th, y0, y1, y2, y3, x0, x1, x2, x3;

	if (0 == xinit) return;

	if (0 != arg_xmsg) WHOAMI;

	memset(ft, 0, sizeof(ft));

	t0 = &ft[0];
	t1 = &ft[64];
	t2 = &ft[128];
	t3 = &ft[192];

	snprintf( t0, 63, "%s %s-%s ", NAME, VERSION, LASTEDIT);
	snprintf( t1, 63, "%s by %s", COPYRIGHT, EMAIL );
	snprintf( t2, 63, "This software is licensed to you under the ");
	snprintf( t3, 63, "%s", LICENSE);

	x_set_font( "fixed" );
	th = x_fontline(0) + 2;

/* compute half of text width as offset from centerline */
	x0 = (6 * strlen(t0)) >> 1;
	x1 = (6 * strlen(t1)) >> 1;
	x2 = (6 * strlen(t2)) >> 1;
	x3 = (6 * strlen(t3)) >> 1;

	y0 = 8 + (th * 3);
	y1 = 8 + (th * 2);
	y2 = 8 + (th * 1);
	y3 = 8 + (th * 0);

#ifdef USE_IMLIB2
	xprintf(stdout, "%s using imlib2\n", WHO);

/* low memory footprint: no cache for dynamic mpeg2 frames or banners */
	imlib_set_cache_size( 0 );
	imlib_set_font_cache_size( 0 );

/* don't need it to be smoothly rendered for cutting, only fast */
	imlib_context_set_dither( 0 );
	imlib_context_set_anti_alias( 0 );
	imlib_context_set_blend( 0 );

/* TESTME: will 16 bit visual on 24 bit display render faster? on 32 bit? */
	imlib_context_set_display( xdisplay );
	imlib_context_set_visual( xvisual );
	imlib_context_set_drawable( xwindow );

/* load generic banner */
	image = imlib_load_image( PNG_PATH "xtscut-begin.png" );
	if (NULL != image) {
	    imlib_context_set_image( image );
	    imlib_render_image_on_drawable_at_size(0, 0, vw.w, vw.h);
	}
#endif
#ifdef USE_IMLIB1
	xprintf(stdout, "%s using imlib1\n", WHO);
	idata = Imlib_init( xdisplay );
	if (NULL != idata) {
	    image = Imlib_load_image( idata, PNG_PATH "xtscut-begin.png");
	    if (NULL != image) Imlib_apply_image( idata, image, xwindow );
	}
#endif

/* draw the license on the X window */
	x_draw_text_line( COLOR_WHITE, (vw.w>>1) - x0, vw.h - y0, t0);
	x_draw_text_line( COLOR_WHITE, (vw.w>>1) - x1, vw.h - y1, t1);
	x_draw_text_line( COLOR_WHITE, (vw.w>>1) - x2, vw.h - y2, t2);
	x_draw_text_line( COLOR_WHITE, (vw.w>>1) - x3, vw.h - y3, t3);

	XSync( xdisplay, True );		/* display it NOW */
	nanosleep( &splash_sleep, NULL );
}
#endif /* !USE_LIBVO */

/* connect to server, create and map window */
static
void
x_init_display ( void )
{
	char n[256];
	char o[256];

	if (0 != arg_xmsg) WHOAMI;

/* if the window already exists, return immediately */
	if (0 != xinit) {
		fprintf(stdout, "X display already open!\n");
		return;
	}

/* setup X window hints for initial size and position */
	xhint.x = vw.x;
	xhint.y = vw.y;

	xhint.width = vw.w;
	xhint.height = vw.h;

	xhint.flags = PPosition | PSize;

/* set X window title text */
	filebase( n, in_name, F_TFILE);
	snprintf( o, sizeof(o)-1, "%s.%s", n, fxp);

/* stored in X structure, global xtitle isn't really needed */
	snprintf( xtitle, sizeof(xtitle)-1, NAME" - %s", o);

/* environment variable DISPLAY will override above definition */
	if ( NULL != getenv("DISPLAY")) dispname = getenv("DISPLAY");

#ifdef USE_LIBVO
/******************************************************************** LIBVO */
	switch(arg_vo) {
	    case 0:
		xoutput = vo_x11_open(); /* XShm RGB as fast as imlib2 */
 	    	break;
	    case 1:
		xoutput = vo_xv_open(); /* XVideo YV12 renders the quickest */
		break;
	    case 2:
		xoutput = vo_xv2_open(); /* XVideo YUYV as fast as imlib2 */
		break;
	    default:
		break;
	}

	if ( xoutput->setup( xoutput,
				m2seq->width, m2seq->height,
				m2seq->chroma_width, m2seq->chroma_height,
				&xsetup) )
	{
		fprintf(stdout, "output setup failed\n");
		c_exit(1);
	}


	xprintf(stdout, "LIBVO %s display open\n", votypes[2+arg_vo]);

/* select the event types for normal operation */
	XSelectInput(	xdisplay,
			xwindow,
			StructureNotifyMask
			| KeyPressMask
			| ButtonPressMask
			| FocusChangeMask
#ifdef USE_EXPOSE
#warning using X Expose events
			| ExposureMask
#endif
		    );

	XSetBackground( xdisplay, xgc, xbg );
	XSetForeground( xdisplay, xgc, xfg );

	x_set_font( "fixed" );

	xinit = 1;
	x_title();
	return;

/******************************************************************** LIBVO */
#else

/******************************************************************** Imlib */
/* open the x display for imlib1/2 */
	xdisplay = XOpenDisplay( dispname );

	if (xdisplay == NULL) {
		perror("opening X display");
		exit(1);
	}

/* get default screen number from XOpenDisplay */
	xscreen = DefaultScreen( xdisplay );
	xvisual = DefaultVisual( xdisplay, xscreen );


/* set foreground and background colors */
	xbg = BlackPixel( xdisplay, xscreen );
	xfg = WhitePixel( xdisplay, xscreen );

	XGetWindowAttributes(	xdisplay,
				DefaultRootWindow( xdisplay ),
				&xattr
			    );

	xbpp = xattr.depth;
	xprintf( stdout, "X bpp %d\n", xbpp );
	xcolors = xcolors565;
	if (24 == xbpp) xcolors = xcolors888;
	if (32 == xbpp) xcolors = xcolors888;

/* find a matching visual for the display */
	XMatchVisualInfo( xdisplay,
			  xscreen,
			  xbpp,
			  TrueColor,
			  &vinfo
			);

	xprintf( stdout, "X visual %lx\n",vinfo.visualid );

/* create the color map for the window */
	xcmap = XCreateColormap( xdisplay,
				 RootWindow( xdisplay, xscreen ),
				 vinfo.visual,
				 AllocNone
			      );

	sattr.background_pixmap	= None;
	sattr.background_pixel	= xbg;
	sattr.border_pixel	= xfg;
	sattr.colormap		= xcmap;

/* create window with background pixel, border pixel and colormap */
	sattrmask = 0
		    | CWBackPixel
		    | CWBackPixmap
		    | CWBackingStore
		    | CWBorderPixel
		    | CWColormap;

/* create the window on the display */
	xwindow = XCreateWindow( xdisplay,
				  RootWindow( xdisplay, xscreen ),
				  xhint.x,
				  xhint.y,
				  xhint.width,
				  xhint.height,
				  0,
				  xbpp,
				  InputOutput, /* CopyFromParent, */
				  vinfo.visual,
				  sattrmask,
				  &sattr
				);

/* create a graphic context */
	xgc = XCreateGC(xdisplay, xwindow, 0L, &xgcv);

/* NOTE: Select ConfigureNotify events before mapping window
	    to parse Configure/Map events.
*/

/* select the event types for window setup */
	XSelectInput(	xdisplay,
			xwindow,
			StructureNotifyMask );

/* put the window on the display */
	XMapWindow(xdisplay, xwindow);
	
/* Wait for Map event to indicate the map finished. You can't use the window
    it until it has finished mapping. it will happen after 2 configure events,
    Configure size and Configure position.
*/
	do
	{
		/* wait for window structure events in overlay window */
		XWindowEvent(	xdisplay,
				xwindow,
				StructureNotifyMask,
				&xev);

/* looking for configure notify events to set size/position */
		if (xev.type == ConfigureNotify)
		{

/* The Window Manager may change the actual dimension/position from our hints,
   so get where WM actually put window. That's why they're called 'hints'.
*/
			xprintf(stdout,"X Event:\tConfigureNotify\n");

/* send_event indicates window moved, so udpate x,y */
			if (xev.xconfigure.send_event) {

/* FIXME: This is asking for trouble? Different WM's have other methods? */
				/* get the titlebar height */
				xtbh = xev.xconfigure.y - 1;

				vw.x = xev.xconfigure.x;
				vw.y = xev.xconfigure.y;
				vw.w = xev.xconfigure.width;
				vw.h = xev.xconfigure.height;

				xprintf(stdout,
				    "\tSend Event x%d y%d w%d h%d th%d\n",
				    vw.x, vw.y, vw.w, vw.h, xtbh);
			} else {
/* otherwise it's a window resize, so update w,h */
				vw.w = xev.xconfigure.width;
				vw.h = xev.xconfigure.height;

				xprintf(stdout,"\tUser w%d h%d\n",
				vw.w, vw.h);
			}
		}
/* keep checking events until MapNotify event says window is mapped */
	}  while (xev.type != MapNotify );

	xprintf(stdout,"X Event:\tMapNotify\n");

	XSetBackground( xdisplay, xgc, xbg);
	XSetForeground( xdisplay, xgc, xfg);

	x_set_font( "fixed" );
	
/* show the results of what the overlay and X window were set to */
	xprintf(stdout,"\n%s %0dx%0d @ %0d,%0d\n",
		WHO, vw.w, vw.h, vw.x, vw.y);

/* indicate the window is now initialized */
	xinit = 1;

	x_title();

/* Set the type of events we will allow in this screen:
		ConfigureNotify		window setup
		UnmapNotify		minimize/shutter
		MapNotify		unminimize/unshutter
		KeyPress		stream navigation and control
		ButtonPress1		place a cut
		ButtonPress2		preview a cut point
		ButtonPress3		jump to stream % (invisible slider)
		ButtonPress4		stream navigation backwards
		ButtonPress5		stream navigation forwards

    NOTE: Select ConfigureNotify events before mapping window
	    to parse Configure/Map events.
*/

/* Undefine USE_EXPOSE for remote X session to reduce Expose event redraws.
   2 or 3 expose events with different serial numbers will pop up on move,
   along with a bunch of other expose events with the same serial number.
   FocusIn event will trigger redraw when Expose events are disabled.
*/


/* select the event types for normal operation */
	XSelectInput(	xdisplay,
			xwindow,
			StructureNotifyMask
			| KeyPressMask
			| ButtonPressMask
			| FocusChangeMask
#ifdef USE_EXPOSE
#warning using X Expose events
			| ExposureMask
#endif
		    );

/* set focus so xinput works when in fullscreen mode */
	XSetInputFocus(	xdisplay,
			xwindow,
			RevertToNone,
			CurrentTime
		      );

#ifdef USE_MAXIMIZE
	if (xmaxi) {
		fprintf(stdout,"going full screen\n");
		x_resize_display();
	}
#endif

/******************************************************************** Imlib */
#endif
}

/* Scan 184 chars for GOP start code. Set the broken link if not closed. */
/* This only works right if GOP doesn't straddle p packet boundary. */
//static
inline
int
test_gop_set_broken ( unsigned char *r, unsigned int z, int n )
{
	unsigned int i;
	unsigned char b;

	if (z < 1) return 0;
	if (0 == arg_gop) return ~0; /* pretend it's done if no -g option */


	WHOAMI;

	for (i=0; i < z; i++) {
		sc <<= 8;
		b = *r;
		sc |= b;
		r++;

		if (0x00000100 != (0xFFFFFF00 & sc)) continue;

/* keep looping until first GOP found, or out of bytes */
		if (MPEG_GOP != b) continue;

/* enough bytes left in current packet to find the GOP data? */
		if (i < (z-4)) {

/* check for marker bit at 13th bit in 25 bit time code */
		    if (0 == (8 & r[1])) continue;

/* Set broken_link if not a closed Group of Pictures. See 13818-2 Sect 6.3.8
    GOP broken_link on first GOP at start of cut to dump first B Pictures.
    GOP closed_gop on last GOP at end of cut to not request last B Pictures.
*/ 

/* if closed_gop set, no need to set broken_link on the closed GOP */
		    if (0 == (0x40 & r[3])) {
			    if (0 != arg_gop)
				iprintf(stdout,
				     "GOP broken @ pkt %d\n", n );
/* GOP broken_link set */
			    r[3] |= 0x20;
			    return ~0;
		    } else {
			    if (0 != arg_gop)
				iprintf(stdout, "GOP closed @ pkt %d\n", n );
		    }
		}
/* stop looping after first GOP found */
		return ~0;
	}

/* GOP broken_link was not set */
	return 0;
}

/* in_file and out_file already open, c is count of bytes, s is seek or -1 */
static
void
write_block ( long long s, long long c )
{
	int a, b, g;
	long long i, j, z;
	unsigned char *p;
	unsigned int skip_pkts;
	tsh_t *h;

	if ( (s < 0) || (c < 1) ) return;

	if ((NULL == es_buf) || (NULL == ts_buf)) return;

	WHOAMI;

	z = lseek( in_file, s, SEEK_SET );
	memset( psib, 0, sizeof(psib)); // reset PSI bit count list
	g = 0;				// reset GOP found test
	sc = 0xFFFFFFFF;		// reset GOP start code test
	skip_pkts = 0;
	h = &tsh;

	if (0 == arg_es)
		if (0 != (c % TSZ))
			fprintf( stdout,
				 "write will be short %lld bytes\n", c % TSZ);
	p = ts_buf;

/* clear the payload start indicator flags before doing the cut */
        memset(psib, 0, sizeof(psib));

	j = -1;					/* packet counter */
	i = 0LL;

	while (i < c) {
		z = arg_mbz;
		if ((c - i) < arg_mbz) z = c - i;
		j++;
		if (0 != j) {
/* 50MB w/ 4k arg_mbz */
		    if ( (0 != arg_delay) && (0 == (j % 12894)) ) {
				dprintf(stdout, "%s sleep\n", WHO);
				nanosleep( &write_sleep, NULL );
		    }
		}

		a = read( in_file, ts_buf, z );
		bytes_in += a;
		
/* error EOF */
		if (a < 0) {
			fprintf( stdout,
				 "error %d reading %s\n", a, in_name );
			perror("");
			return;
		}

		i += a;

/* exact EOF */
		if (0 == a) {
			dprintf( stdout, "%s:%d EOB %s\n",
				 WHO, WHERE, in_name);
			break;
		}

/* ES cut writes last partial packet data, TS cut discards it */
		if (a != arg_mbz) {
			if (0 == arg_es) {
			    dprintf( stdout, "%s:%d EOB %s\n",
				     WHO, WHERE, in_name);
			    break;
			}
		}

/* ES cut doesn't use TS sync and header bits, only copies the data */
		if (0 != arg_es) {
			b = write( out_file, ts_buf, a );
			bytes_out += b;
			if (a != b) {
				fprintf( stdout,
					 "error %d writing %s\n",
					 b, out_name);
				perror("");
				return;
			}
			continue;
		}


#ifndef USE_TS_FIX
		b = write( out_file, ts_buf, a );
		bytes_out += b;
		if (a != b) {
			fprintf( stdout, "error %d writing %s\n", b, out_name);
			perror("");
			return;
		}
		continue;
#endif


#ifdef USE_TS_FIX
#warning using extra TS processing (slower)
/* TS processing to remove initial PSI0 cruft, sets first GOP broken_link. */
		if ((0 == arg_nulls) || (0 != arg_gop)) {
		    int n;
		    long long k, x;
		    unsigned char *r;

		    for (k = 0; k < a; k += TSZ) {

			n = ((j * arg_mbz) + k) / TSZ;
			r = p + k;
			x = parse_ts_header( r, h, 0 );
			if (x < 0) {
				fprintf(stdout,"ERROR: Transport Sync Lost\n");
				return;
			}

#ifdef USE_NULL_STRIP
/* skip NULL packets for smaller output files, -n enables NULL packets */
			if (0 == arg_nulls)
				if (MPEG_NULL == h->pid)
					continue;
#endif

/* store count of payload start indicators by pid */
			psib[ h->pid ] += h->psi;

/* NOTE: this should also be a generic fix to wait for start of any payload */
/* Audio fix: if no payload start indicators for audio PID yet, skip packet */
			if (h->pid == arg_apid) {
			    if (0 == psib[ h->pid ]) {
				skip_pkts++;
// skip the packet
//				continue;

/* write the packet as a NULL packet to match show scuts file sizes */
				build_null_packet( r );
			    }
			}

#ifdef USE_GOP_BROKEN_LINK
#warning using GOP broken link test
/* -g, video pid, no gop found yet, and has video payload bytes? */
			if (0 != arg_gop)
			    if ( (h->pid == arg_vpid)
			    && (0 == g)
			    && (x > 0) )
				g = test_gop_set_broken( r + h->pso, x, n );
#endif

#warning using one-packet write (slower)
			b = write( out_file, r, TSZ );
			bytes_out += b;
			if (b != TSZ) {
			    fprintf( stdout,
				    "ERROR: writing %s, out%d != in%d\n",
				    out_name, b, a);
			    perror("");
			    break;
			}
		    }
		}
#endif

	}

//	fprintf(stdout, "skip_pkts %d\n", skip_pkts);

/* TODO:
	 Add an audio packet scavenger to write audio packets until
	first audio PSI1 after the end of the cut. This might cause
	some interesting, and/or bad, audio artifacting if the cut
	isn't done in the middle of the silent part of the fade out.
*/
}

/* Save the list of cuts to input base name + .[esc|tsc] */
static
void
write_cutfile ( void )
{
	FILE *o;
	char n[256], sc_name[512];
	int ok;

	if (0 != arg_cut) return;
#if 0
/* atscap 1.1rc9t fixed to handle the zero byte .tsc index.html status */
	if (0 == scut_idx) return;
#endif

	WHOAMI;

	filebase( n, in_name, F_PFILE ); /* keep path to keep .tsc w/ .ts */
	snprintf( sc_name, sizeof(sc_name)-1, "%s.%sc", n, fxp );
	dprintf( stdout, "Write %d cuts to %s\n", scut_idx-1, sc_name );


/* save cuts for next time */
	o = fopen( sc_name, "w" );
	if ( NULL == o ) {
		fprintf( stdout, "error opening %s\n", sc_name );
		perror("");
		return;
	}

	ok = fwrite( &scuts[1], sizeof(int), scut_idx-1, o );
	if (ok != (scut_idx-1)) {
		fprintf( stdout, "only wrote %d of %d to %s\n",
			 ok, scut_idx, sc_name);
	} else {
		dprintf( stdout, "wrote %d cuts to %s\n", ok, sc_name);
	}

	fclose( o );
}

/*  [c] key removes all cuts */
static
void
clear_scuts ( void )
{
	int i;

	WHOAMI;

/* faster? */
//	for (i = 0; i < iframe_idx; i++) frames[ iframes[ i ] ].cut = 0;
	for (i = 0; i < frame_idx; i++)  frames[i].cut = 0;
	scut_idx = 0;
}

/* write PAT + PMT to filedes f, but only if input scan found anything */
static
void
write_patpmt ( void )
{
	int t;
	WHOAMI;

	if (0x47 != *pat) return;

    	t = write( out_file, pat, TSZ );
	bytes_out += t;

	if (0x47 != *pmt) return;
	if (pmt_pkts < 1) return;

	t = write( out_file, pmt, TSZ * pmt_pkts );
	bytes_out += t;
}


/* Write one second of nulls to start of file at current byte rate,
    after PAT + PMT and before video start to give decoder some slack time.
    1s of NULLs based on bitrate, max will be about 10MB at 80Mbit.

    This is an attempt to deal with xine sync issue on uncached stream start,
    but it may not be enough to correct the problem. Tried this before?
*/
static
void
write_nulls ( void )
{
	char pktnull[ TSZ ];
	int i, j, t;

	if (0 == arg_nulls) return;

	WHOAMI;

/* use last parsed SEQ header bit rate */
	calc_null_packets();

	if (0 == null_pkts) return;

	j = null_pkts;

	if (0 != arg_fmsg) fprintf(stdout, "%s packets %d\n", WHO, j);

	build_null_packet( pktnull );
	for (i = 0; i < j; i++) {

/* FIXME: add error check here */
		t = write( out_file, pktnull, TSZ );
		bytes_out += t;
	}
	return;
}

#if 0
/* create incremental filename (-1 thru -9) in d from basename in s */
static
void
build_next_name ( char *d, char *s, int z )
{
	char t[256];
	struct stat64 fs;
	int i, ok;

	WHOAMI;

	memset( t, 0, sizeof(t) );

	ok = 0;
	for (i = 0; i < 9; i++) {
	    snprintf( d, z, "%s-%d", s, i );
	    snprintf( t, sizeof(t)-1, "%s%s.%s", arg_path, d, fxp);
	    ok = stat64( t, &fs );
	    if (0 != ok) break;
	    fprintf( stdout, "Found %s\n", t );
	}

	if (0 == ok) {
	    snprintf( d, z, "%s-x", s);
	    snprintf( t, sizeof(t)-1, "%s%s.%s", arg_path, d, fxp);
	    fprintf( stdout, "Gave up, using %s.%s\n", t );
	}
}
#endif

/* build output basename, s is source basename, c is cut number */
/* writing to one file truncates the input name to first . */
/* writing to multiple files appends .nn */
/* arg base was set by input filename or -o option */
static
int
build_outname ( char *s, int c )
{
	int ok;
	struct stat64 fs;

	WHOAMI;

	if (0 != arg_one) {
		snprintf(out_base, sizeof(out_base)-1, "%s", arg_base);
	} else {

/* no -o option given uses source basename + ".NN.ts" */
		snprintf(out_base, sizeof(out_base)-1, "%s.%02d", s, c);
	}
	snprintf(out_name, sizeof(out_name)-1, "%s%s.%s",
		 arg_path, out_base, fxp);

	ok = stat64( out_name, &fs );
	if (0 != ok) return 0;

/* FIXME: This isn't enough. It could have an unintended inode collision on
   another filesystem, indicating an error where no error actually exists.
   It first needs to check if input and output are different file systems.
*/
	out_inode = fs.st_ino;
	if (in_inode == out_inode) {
		fprintf(stdout, "ABORT: input %s same as output\n", in_name);
		return -1;
	}
	return 0;
}

/*
    write sequence cuts from scuts array

    if cut type is EVEN or ODD (and option -1 not used):
	
	open file.00.ts and write all cuts to that file

    if cut type is EVEN [or ODD] (and option -1 is used):
	
	open file.00.ts and write all cuts to that file
	generate a new file every time sequences[ scuts[] ] boundary crossed
	open file.00.ts and write PATs and PMTs, then
	     file.02[or 01].ts and write sequences of first EVEN [or ODD] cut
	     file.04[or 03].ts and write sequences of next  EVEN [or ODD] cut
	    .
	    .
	    .
	     file.nn.ts and write sequences of last EVEN [or ODD] cut

	NOTE: The example above shows an even cut write. You may use the
		-r option for sequential numbering starting at 01.

    if cut type is ALL (and option -1 used), cuts write to:
    
	generate a new file every time sequences[ scuts[] ] boundary crossed
	open file.00.ts and write PATs and PMTs, then
	     file.01.ts and write sequence of first cut until cut boundary
	     file.02.ts and write sequence of second cut until cut boundary
	     .
	     .
	     file.nnn.ts and write from last cut to last sequence.

Maybe:
   [write two packets [and null packets?], PAT and PMT, to start of file]
*/

static
void
write_scuts ( void )
{
	char n[256]; /* file based name */
	unsigned int in, out;
	unsigned long long bytes_cut, cpt;
	int ok, i, j, k, cut_num;
	char *t, *p; /* text pointers */
	double et, tp; /* elapsed time, through put */
	char *d;
	int z;

	WHOAMI;

	if ( 0 == scut_idx ) return;
	start_timer( &timer_cut );

	scut_done = 0;

	filebase( n, in_name, F_PFILE );

	write_cutfile();

	d = outlist;
	*d = 0;
	z = sizeof(outlist);

	t = "NONE";
	if (CUT_EVEN == cut_type) t = "EVEN";
	if (CUT_ODD == cut_type) t = "ODD";
	if (CUT_ALL == cut_type) t = "ALL";

	snprintf( in_name, sizeof(in_name)-1, "%s.%s", n, fxp );
	p = strrchr( in_name, '/' );
	if (NULL == p) {
		p = in_name;
	} else {
		p++;
	}

	asnprintf(d, z, "Writing %s cuts From %s:\n", t, p);
	asnprintf(d, z, " To: %s\n", arg_path);

	if (0 != arg_renum)
		asnprintf(d, z, "Cuts will be renumbered sequentially\n");

	in = TSZ;
	out = 0;

/* reset bytes counts and input/output block indices */
	bytes_in = 0;
	bytes_out = 0;
	bytes_total = 0;

/* cut type ALL first cut is file.00.ts, has multiple PATs and PMTs */
/* cut type EVEN/ODD all cuts go to file.00.ts */
	i = 0;
	filebase( n, in_name, F_TFILE ); /* write cuts to current dir */
	memset( out_base, 0, sizeof(out_base));

/* term at first . to keep date and time in filename */
	p = strchr( n, '.' );
	if (NULL !=p) *p = 0;

	strncpy( out_base, n, sizeof(out_base)-1 );

	ok = build_outname( n, i );

	if (0 != ok) {
		fprintf(stdout, "ERROR: Rename the input file\n");
		return;
	}

	asnprintf(d, z, "Cut #  SEQ # byte offset   file size output name\n");
	asnprintf(d, z, "----- ------ ----------- ----------- -----------\n");

	if (0 != arg_one) {

		out_file = open( out_name, FILE_WMODE, FILE_PERMS);
		if (out_file < 3) {
			fprintf( stdout, "\n%s open %s error\n", WHO, out_name);
			perror("");
			return;
		}
	}

	k = 1;
	j = 0;

/* counter-intuitive: cut 000 is not in scuts[]. cut 001 is scuts[0] */
	if (CUT_ALL != cut_type) {
		k = 2;
		if (CUT_EVEN == cut_type) {
			j = 1;
		}
	}

	if (0 != xinit) {
		xhelp = 4;
		x_draw_quad_gradientv(COLOR_RED, 0, 0, vw.w, vw.h, 0.5 );
		x_draw_outlist();
	}

	for ( i = j; i < scut_idx-1; i += k ) {
		cut_num = i;

/* -r option will renumber odd and even cuts to sequential starting at 01,
    but only if writing multiple files and only if not cutting all
 */
		if ( (0 != arg_renum)  && (0 == arg_one)
		  && ( (CUT_ODD == cut_type) || (CUT_EVEN == cut_type)) ) {
			if (CUT_ODD == cut_type) cut_num++;
			cut_num >>= 1;
			dprintf( stdout, "cut %d renumbered to %d\n",
				 i, cut_num+1);
		}

/* -1 option is do not make one file, open a new one for each cut */
		if (0 == arg_one) {

			if (out_file > 2) close( out_file );
			out_file = 0;
			build_outname( n, cut_num + 1 );
			if (in_inode == out_inode) return;

			out_file = open( out_name, FILE_WMODE, FILE_PERMS );
			if (out_file < 3) {
    				fprintf( stdout, "\n%s open %s error\n",
					 WHO, out_name);
				perror("");
    				return;
			}
		}

		bytes_cut = sequences[scuts[ i + 1 ]] - sequences[scuts[ i ]];

		if (0 > bytes_cut)
			bytes_cut = in_size - sequences[ scuts[ i ] ];

		cpt = TSZ * (1 + pmt_pkts);
		cpt += TSZ * null_pkts;

		p = strrchr(out_name, '/');
		if (NULL == p) {
			p = out_name;
		} else {
			p++;
		}
		asnprintf(d, z, "%3d @ %6d %11lld %11lld %s\n",
			i+1, scuts[i], sequences[scuts[i]], bytes_cut+cpt, p);

		dprintf(stdout, "%3d @ %6d %11lld %11lld %s\n",
			i+1, scuts[i], sequences[scuts[i]], bytes_cut+cpt, p);

		if (0 != xinit) {
			xhelp = 4;
			x_draw_outlist();
			XFlush(xdisplay);
		}

/* write any PAT + PMT found by input scan and optional NULL packets */
		write_patpmt();
		write_nulls();
		write_block( sequences[scuts[ i ]], bytes_cut );
	}

	fsync( out_file );
	close( out_file );
	out_file = 0;

	bytes_total = bytes_in + bytes_out;

	stop_timer( &timer_cut );
	diff_timer( &timer_cut );

	et = 0.000000001 * (double)timer_cut.diff.tv_nsec;
	et += 1.0 * (double)timer_cut.diff.tv_sec;

	asnprintf(d, z, "Cut reads/writes %lld MegaBytes in %.2f seconds, ",
			bytes_total >> 20, et);

#if 0
	asnprintf(d, z, "\nInput  %11lld", bytes_in );
	asnprintf(d, z, "\nOutput %11lld", bytes_out );
	asnprintf(d, z, "\nTotal  %11lld, ", bytes_total);
#endif

	tp = (double)bytes_total / et;
	tp /= 1000000.0;

	asnprintf(d, z, "I/O MB/s %.2f\n", tp);

	scut_done = ~0;
	if (0 != xinit) {
		x_draw_quad_gradientv(COLOR_GREEN, 0, 0, vw.w, vw.h, 0.5 );
		x_draw_outlist();
	}
}


/* find I-frame before current frame that has cut set, or iframe 0 */
static
int
find_prev_cut ( void )
{
	int i;

	WHOAMI;

	for (i = (xcfd - 1); i > 0; i-- )
		if (0 != frames[i].cut)
			return i;
	return 0;
}


/* find I-frame after current frame that has cut set */
static
int
find_next_cut ( void )
{
	int i;

	WHOAMI;

	for (i = (xcfd + 1); i < frame_idx; i++ )
		if (0 != frames[i].cut)
			return i;
	return 0;
}


/* return nz if bx,by is within quadrangle defined by x,y,w,h */
static
int
x_button_quad ( int x, int y, int w, int h, int bx, int by )
{
	if (0 != arg_xmsg) WHOAMI;

	if ( (bx >= x) && (bx <= (x+w)) )
		if ( (by >= y) && (by <= y+h) )
			return ~0;
	return 0;
}


/* check for the following events:
	resize/move = update window
	iconify/restore = disable/enable window
	keypress = user navigation and control
	maximize on/off (broken)

    xumove is set if start frame offset changes
    xredraw is set if display needs to be redrawn

    caller handles redrawing display in X scan loop
*/
static
void
x_event ( void )
{
	int i, j, bx, by;
	int save;
	save = 0;
	
//	if (0 != arg_xmsg) WHOAMI;

	xumove = 0;
	uoffset = 0;
	coffset = xoffset;		/* current offset */

/* look for UnmapNotify for minimized/disable overlay */
	if ( x_check_twe( UnmapNotify, &xev) )
		xprintf(stdout,"\nX Event:\tUnmapNotifyEvent");

/* look for ConfigureNotify for resize or move */
	if ( x_check_twe( ConfigureNotify, &xev ))
	{
		xprintf(stdout,"\nX Event:\tConfigureNotify\n");

/* ConfigureNotify event indicates size/position change */
		if (xev.type == ConfigureNotify)
		{

/* send_event indicates window manager move/resize,
   so udpate w,h,x,y with new location */

			if (xev.xconfigure.send_event) {
				vw.x = xev.xconfigure.x;
				vw.y = xev.xconfigure.y;
/* not square
				vw.w = xev.xconfigure.height;
*/
				vw.w = xev.xconfigure.width;
				vw.h = xev.xconfigure.height;

				xprintf(stdout,"\tSend Event x%d y%d w%d h%d\n",
				vw.x, vw.y, vw.w, vw.h);
			} else {
/* otherwise it's a user resize, so update w,h */
				vw.w = xev.xconfigure.width;
/* not square
				vw.w = xev.xconfigure.height;
*/
				vw.h = xev.xconfigure.height;

				xprintf(stdout,"\tUser w%d h%d\n",
					vw.w, vw.h);
			}

			save = ~0;
			xredraw = ~0;
//			x_clear_display();

/* eat the FocusIn after ConfigureNotify */
			x_check_twe( FocusIn, &xev );
		}
	}


/* it's ordered like this so the ConfigureNotify above puts overlay
   back in right location BEFORE enabling it. looks smoother.
   NOTE: from old mzplay.c; leave this comment in place, in case want
   to use overlay instead of rgb colorspace for the preview window
*/

/* look for MapNotify for maximized/enable overlay */
	if ( x_check_twe( MapNotify, &xev ) ) {
		xprintf(stdout,"\nX Event:\tMapNotify\n");
		xredraw = ~0;
	}

/* FocusOut does nothing except empty the queue of the focus out event */
	if ( x_check_twe( FocusOut, &xev ) ) {
		xprintf( stdout, "\nX Event:\tFocusOut\n");
//		xredraw = ~0;
/* but should instead copy the image to backstore for later Expose events */
	}	

/* FocusIn needs to eat the following ButtonPress event so it doesn't pass
   through. Also, this should go before the ButtonPress event test.
*/
	if ( x_check_twe( FocusIn, &xev ) ) {
		xprintf( stdout, "\nX Event:\tFocusIn\n");
//		xredraw = ~0;
		x_check_twe( ButtonPress, &xev);
	}	

/* look for button events */
	while ( x_check_twe( ButtonPress, &xev) ) {
		bx = xev.xbutton.x;
		by = xev.xbutton.y;

		xprintf(stdout,"\nX Event:\tButtonPress button %d\n",
			xev.xbutton.button );

/* check for scrollwheel down, forward in stream vww/2 frames */
		if (5 == xev.xbutton.button) {
			xprintf(stdout,"\nX Event:\tScrollDown ButtonPress\n");
			uoffset = 1 * (vw.w >> 1);
			xumove = ~0;
		}
		
/* check for scrollwheel up, backward in stream vww/2 frames */
		if (4 == xev.xbutton.button) {
			xprintf(stdout,"\nX Event:\tScrollUp ButtonPress\n");
			uoffset = -1 * (vw.w >> 1);
			xumove = ~0;
		}

/* Right Mouse Button is button 3 and performs an I-frame preview */
		if (3 == xev.xbutton.button) {

			i = x_button_quad(bax, bay, baw, bah, bx, by);
			xprintf(stdout,
				"\nX Event:\tRight ButtonPress x %d y %d, i %d\n",
				bx, by, i
			);

/* not in IPB area, is preview select */
			if (0 == i) {

/* if user selection is within valid frame range */
				xlfd = find_nearest_iframe( xoffset + bx, WHO );

				parse_intra_frame(
					sequences[frames[xlfd].num], WHO);


				xredraw = ~0;
			}
		}

/* check middle mouse button for percentage position in sequence list. */
		if (2 == xev.xbutton.button) {

/* no need if all frames fit in display window, use RMB button3 instead */
		    if ((frame_idx > vw.w) && (vw.w > 1)) { 
//			i = bx * iframe_idx;
			i = bx * frame_idx;
			i /= vw.w;
//			j = iframes[i] - (vw.w>>1);
			j = i - (vw.w>>1);
			xoffset = find_next_iframe( j );
			xumove = ~0;
			iprintf( stdout, "Position near I-frame %d\n", i);
		    }
		}


/* Left Mouse Button is button 1, toggles cut select or legend area buttons */
		if (1 == xev.xbutton.button) {

/* test for time-code/frame-number toggle area, same x as I button uses */
			i = x_button_quad( bxi, tys, 30, 12, bx, by);
			if (0 != i) {
				arg_ft = 1 & ~arg_ft;
				save = ~0;
				xredraw = ~0;
				break;
			}

/* test for IPB button area */
			i = x_button_quad(bax, bay, baw, bah, bx, by);
			xprintf(stdout,
				"\nX Event:\tLeft ButtonPress x %d y %d i %d\n",
				bx, by, i
			);

/* no buttons to trigger with legend buttons disabled */
			if (0 == arg_lb) i = 0;

/* not in IPB buttons, or IPB buttons disabled, is cut select */
			if (0 == i) {
			    int ni;
			    ni = find_nearest_iframe( xoffset + bx, WHO );

			    xredraw = ~0;

/* disable frame 0 cut selection, otherwise toggle cut indication */
			    if (ni > 0) {
				frames[ ni ].cut = 1 & ~frames[ ni ].cut;
				build_scut_frames();
				write_cutfile();
			    }
			    break;
			}

/* test for IPB button toggle areas */
			i = x_button_quad( bxi, bys, 9, byh, bx, by);
			if (0 != i) {
				bis = 1 & ~bis;
				save = ~0;
				xredraw = ~0;
			}

			i = x_button_quad( bxp+1, bys, 8, byh, bx, by);
			if (0 != i) {
				bps = 1 & ~bps;
				save = ~0;
				xredraw = ~0;
			}

			i = x_button_quad( bxb+1, bys, 9, byh, bx, by);
			if (0 != i) {
				bbs = 1 & ~bbs;
				save = ~0;
				xredraw = ~0;
			}

			arg_hl = 0;
			if ((0 != bis) || (0 != bps) || (0 != bbs))
			    arg_hl = ~0;

		}
		break; /* only one loop */
	}

#ifdef USE_EXPOSE
/* look for events to indicate screen needs redraw */
	if ( x_check_twe( Expose, &xev )) {
		XExposeEvent *expose = (XExposeEvent *)&xev;
		xprintf( stdout, "X Event:\tExpose type %d %s\n",
			    xev.type, xevtypes[xev.type]);
		xprintf( stdout,
			"\t Serial %lu Send %s x %d y %d w %d h %d c %d\n",
			expose->serial,
			(0 == expose->send_event) ? "FALSE":"TRUE",
			expose->x,
			expose->y,
			expose->width,
			expose->height,
			expose->count );

/*  Wait for serial number to change in case dragging window because
     WindowMaker is set to draw x,y in middle of window and this causes
     the multiple Expose events that pile up in the queue.

    Now you might get two or three re-draws on move with the same serial number,
     but shouldn't be any more. There can be dozens of redraws for certain areas
     of the screen, but they generally have the same serial number and can be
     ignored because partial redraws will not be done any time soon.

    It entails keeping a copy of the last drawn frame + lines, using bits and
     pieces of that to refresh the parts the Expose event wants to refresh.

    For local, the 3 redraws are acceptable. For remote, 1 redraw is enough.

    Redraw on FocusIn may actually be all it needs to get it done.
*/
		if (expose->serial != expose_serial) {
		    if (0 == xredraw) xredraw = ~0;
#if 0
		    xprintf( stdout, "X Event:\tExpose type %d %s\n",
			xev.type, xevtypes[xev.type] );
		    xprintf( stdout,
			"\t Serial %lu Send %s x %d y %d w %d h %d c %d\n",
			expose->serial,
			(0 == expose->send_event) ? "FALSE":"TRUE",
			expose->x,
			expose->y,
			expose->width,
			expose->height,
			expose->count );
#endif
		}
		expose_serial = expose->serial;
	}
#endif

/* look for keypress events */
	if ( x_check_twe( KeyPress, &xev ) )
	{
		int keymax, keylen;
		char keybuf[16];
		XComposeStatus compose;
		KeySym keysym;
		unsigned short keyval;

		keymax = 1;
/* try to convert key event into ASCII */
		keylen = XLookupString(	(XKeyEvent *)&xev,
					keybuf, keymax, &keysym, &compose );
		keybuf[keylen] = 0;
		keyval = keybuf[0];


/* NOTE: ADD MORE KEYS HERE, OR REMOVE UNUSED ONES :> */
		if (0 == keyval) {

/* check for special keys and use negative value for testing below */
//		    xprintf(stdout, "X KeySym %04lX\n", 0xFFFF & keysym );
		    keyval = 0xFFFF & keysym;

		}

		xprintf(stdout,
			"\nX Event:\tKeyPress %04X %d\n", keyval, keyval);

		switch(keyval) {

/*********************************************************** stream control */


/* alpha control, only works for imlib2? */
		    case '-':
			hist_scale -= 0.01;
			xredraw = ~0;
			if (hist_scale <= 0.0) {
				hist_scale = 0.0;
				iprintf(stdout, "Histogram scale at 0%%\n");
				xredraw = 0;
			}
			break;
		    case '+':
			hist_scale += 0.01;
			xredraw = ~0;
			if (hist_scale >= 1.0) {
				hist_scale = 1.0;
				iprintf(stdout, "Histogram scale at 100%%\n");
				xredraw = 0;
			}
			break;

/* toggle single or multi-file write */
		    case 'm':
			arg_one = 1 & ~arg_one;
			fprintf(stdout, "Writing cuts to %s\n",
				    (arg_one)?"one file":"multiple files");
			xredraw = ~0;
			save = ~0;
			break;

/* toggle single or multi-file write */
		    case 'b':
			arg_renum = 1 & ~arg_renum;
			fprintf(stdout, "Cut renumber: %s\n",
				    (arg_renum)?"YES":"NO");
			xredraw = ~0;
			save = ~0;
			break;

/* NULL packet at start of each cut toggle */
		    case 'n':
			arg_nulls = 1 & ~arg_nulls;
			fprintf(stdout, "NULL packets: %s\n",
				    (arg_nulls)?"YES":"NO");
			save = ~0;
			xredraw = ~0;
			break;

/* clear all cuts */
		    case 'c':
			clear_scuts();
//			show_scuts();
			xredraw = ~0;
			break;

/* write all cuts to separate files */
		    case 'w':
			cut_type = CUT_ALL;
//			show_scuts();
			write_scuts();	
			break;

/* write even cuts, [e] or [ENTER] keys (enter is CR not NL in X) */
//		    case 13:
		    case 'e':
			cut_type = CUT_EVEN;
//			show_scuts();
			write_scuts();	
			break;

/* write odd cuts */
		    case 'r':
			cut_type = CUT_ODD;
//			show_scuts();
			write_scuts();	
			break;
			
/* display cuts to stdout */
		    case 'd':
//			show_scuts();
			fprintf(stdout, "xhelp %d\n", xhelp);
			if (3 != xhelp) {
			    xhelp = 3;
			} else {
			    xhelp = 0;
			}
			xredraw = ~0;
			write_cutfile(); // redundant with write at cut toggle
			break;

/* display allocated memory usage */
		    case 'g':
			arg_gop = 1 & ~arg_gop;
			fprintf(stdout, "GOP broken_link %s\n",
				(arg_gop)?"YES":"NO");
			xredraw = ~0;
			save = ~0;
			break;

/* show current config */
		    case '?':
//			show_keys(0); // TODO: move to console scan

			xhelp++;
			xhelp %= 5;
			xredraw = ~0;
			break;

/* List allocations */
		    case 'l':
			show_allocs();
			break;

/*
-66    F1	toggles hide histogram lines
-65    F2       toggles hide IPB legend and buttons
-64    F2	toggles hide timecodes at top
-63    F3	toggles hide visual position slider bead
*/

/* F1 toggles histogram lines */
		    case XK_F1:
			arg_hl = 1 & ~arg_hl;
			xredraw = ~0;
			save = ~0;
			break;

/* F2 toggles IPB legend and buttons */
		    case XK_F2:
			arg_lb = 1 & ~arg_lb;
			xredraw = ~0;
			save = ~0;
			break;

/* F3 toggles timecode at top */
		    case XK_F3:
			arg_tc = 1 & ~arg_tc;
			xredraw = ~0;
			save = ~0;
			break;

/* F4 toggles slider bar */
		    case XK_F4:
			arg_sb = 1 & ~arg_sb;
			xredraw = ~0;
			save = ~0;
			break;

/* F5 toggles eye candy, cornering, shading */
		    case XK_F5:
			arg_ec = 1 & ~arg_ec;
			xredraw = ~0;
			save = ~0;
			break;

/* toggle frame number/time-code display */
		    case 't':
			arg_ft = 1 & ~arg_ft;
			xredraw = ~0;
			save = ~0;
			break;

/* quit. NOTE: does not save cuts in case you hit [c]lear cuts by accident */
		    case 3:
		    case 'q':
			save_config();
			c_exit(0);
			break;

/* ESC forces redraw of display */
		    case 27:
			x_clear_display();
			xredraw = ~0;
			break;

/* jump to previous cut */
		    case 'a':
			i = find_prev_cut();
//			fprintf(stdout, "find prev cut %d\n", i);
			if (0 != i) {
				xoffset = i - (vw.w >> 1);
				xumove = ~0;
			}

			break;
/* jump to next cut */
		    case 's':
			i = find_next_cut();
//			fprintf(stdout, "find next cut %d\n", i);
			if (0 != i) {
				xoffset = i - (vw.w >> 1);
				xumove = ~0;
			}
			break;

#if 0
/* jump to previous i-frame if below last button3 preview packet count */
		    case 'z':
			for (i=xoffset; i >= 0; i--) {
			    if (frames[i].pct != 1) continue;
			    if (frames[i].fpc > frame_threshold) continue;
			    xoffset = i - (vw.w>>1);
			    break;
			}
			break;

/* jump to next i-frame if below last button3 preview packet count */
		    case 'x':
			for (i=xoffset; i < frame_idx; i++) {
			    if (frames[i].pct != 1) continue;
			    if (frames[i].fpc > frame_threshold) continue;
			    xoffset = i - (vw.w>>1);
			    xumove = ~0;
			    break;
			}    
			break;		    
#endif

/******************************************************* stream navigation */
/* jump to start of stream */
		    case '[':
		    case XK_Home:
		    case XK_KP_Home:
			xumove = ~0;
			xoffset = 0;
			break;
			
/* jump to end of stream */
		    case ']':
		    case XK_End:
		    case XK_KP_End:
			xumove = ~0;
			xoffset = frame_idx - vw.w;
			break;
/* scroll keys */
		    case 'y':
		    case XK_Page_Up:
		    case XK_KP_Page_Up:
			uoffset = -2 * vw.w;
			xumove = ~0;
			break;

		    case 'o':
		    case XK_Page_Down:
		    case XK_KP_Page_Down:
			uoffset = 2 * vw.w;
			xumove = ~0;
			break;
		
/* skip greater than or equal to window width */
		    case 'u':
		    case XK_Up:
		    case XK_KP_Up:
			uoffset = -1 * vw.w;
			xumove = ~0;
			break;

		    case 'i':
		    case XK_Down:
		    case XK_KP_Down:
			uoffset = 1 * vw.w;
			xumove = ~0;
			break;

/* Skip to previous or next sequence, I-frame single-step.
   This may be very slow on certain I-frame heavy FOX broadcasts.
*/
		    case 'j':
		    case XK_Left:
		    case XK_KP_Left:
			i = find_prev_iframe( xlfd );
			uoffset = i - xlfd;
			iprintf(stdout, "lfd %d i %d u %d\n", xlfd, i, uoffset);
			if (uoffset < 0) xumove = ~0;
			break;

		    case 'k':
		    case XK_Right:
		    case XK_KP_Right:
			i = find_next_iframe( xlfd );
			uoffset = i - xlfd;
			iprintf(stdout, "lfd %d i %d u %d\n", xlfd, i, uoffset);
			if (uoffset > 0) xumove = ~0;
			break;

#ifdef USE_MAXIMIZE
/* FIXME: full screen toggle, has eventual fatal bug in X events,
   and the aspect ratio is really ugly except on 1920x1200 displays.
*/
		    case 'F':
		    case 'f':
			xmaxi = 1 & ~xmaxi;
			xumove = ~0;
			xredraw = ~0;
			xoffset = 0;
			x_resize_display();
			save = ~0;
			break;
#endif

		    default:
			break;

		} /* switch */
	} /* keypress */

/* viewpoint move request? */
	if (0 != xumove) {
		xredraw = ~0;
		toffset = xoffset + uoffset;	/* target offset */

/* is frame count less than window width? */
		if (frame_idx <= vw.w) {

/* yes, xoffset will stay at 0 */
			xoffset = 0;

/* no, clamp xoffset to last window width frames, plus 1 */
		} else {
			if (toffset < 0) toffset = 0;

			if ((toffset + vw.w) > frame_idx) {
				xoffset = frame_idx - vw.w;
			} else {
				xoffset = toffset;
			}
		}

/* no change in offset disables redraw */
		if (coffset == xoffset) xredraw = 0;
	}


	if (0 != save) save_config();

/* any events left on the queue means a mistake was made above? */
//#define USE_X_DEBUG
#ifdef USE_X_DEBUG
#warning Using X debug code
/* debug. don't leave this in because it empties the X event queue. */
	if ( XCheckWindowEvent( xdisplay, xwindow, 0x01FFFFFF, &xev )) {
	    char *xet = "";
	    if (xev.type < 36) xet = xevtypes[xev.type];
	    xprintf( stdout, "\nX Event type %d not handled: %s\n",
		    xev.type, xet);
	}
#endif
}

#ifdef USE_CONSOLE
/**************************************************** xterm console control */
static
void
console_scan( unsigned char *k )
{

	console_getch_fn( k );
	if (0 == *k) return;

//	WHOAMI;

/* stream control, no navigation keys */
	switch( *k ) {

/* toggle single or multi-file write */
		case 'm':
			arg_one = 1 & ~arg_one;
			fprintf(stdout, "Writing cuts to %s\n",
				(arg_one)?"one file":"multiple files");
		break;

/* toggle NULL blank padding packets at start of each cut */
		case 'b':
		arg_nulls = 1 & ~arg_nulls;
		fprintf(stdout, "Writing NULL blanks to cuts: %s\n",
			(arg_nulls)?"YES":"NO");
			break;

/* clear all cuts */
		case 'c':
			clear_scuts();
//			show_scuts();
			xredraw = ~0;
			break;

/* write all cuts to separate files */
		case 'w':
			arg_one = 0;			
			cut_type = CUT_ALL;
//			show_scuts();
			write_scuts();	
			break;

/* write even cuts, [e] or [ENTER] keys */
		case 10:
		case 'e':
			cut_type = CUT_EVEN;
//			show_scuts();
			write_scuts();	
			break;

/* write odd cuts */
		case 'r':
			cut_type = CUT_ODD;
//			show_scuts();
			write_scuts();	
			break;
			
/* display cuts to stdout */
		case 'd':
//			show_scuts();
			write_cutfile();
			break;


/* toggle frame number/time-code display */
		case 't':
			arg_ft = 1 & ~arg_ft;
			xredraw = ~0;
			break;

/* quit */
		case 'q':

/* goes to c_exit after fixing console */
			console_exit(0);
			break;
		default:
			fprintf(stdout, "Key 0x%02X not handled\n", *k);
			break;
	}
}
/***************************************************** xterm console control */
#endif


/* fill out frames[].num from input data */
static
void
process_frames ( void )
{
	int k, n, i, p, b, s, h;
	double ft, fi, fp, fb;
	frame_t *e, *f;

	WHOAMI;

	if (frame_idx < 2) return;

	i = p = b = h = s = 0;

	for (n = 0; n < frame_idx; n++) {
		k = 0;

		f = &frames[n];
		e = &frames[n + 1];

		f->fpc = e->vpn - f->vpn;

/* last frame in list is always wrong even if it's OK */
		if ( f->fpc < 0 ) f->fpc = 0;

		switch( f->pct ) {
		case 1:
			if (h < f->fpc) h = f->fpc;
			s += f->fpc;
			f->num = i;
			i++;
			break;
		case 2:
			f->num = p;
			p++;
			break;
		case 3:
			f->num = b;
			b++;
			break;
		default:
			break;
		}
	}	

/* -1 to not count the fake I frame placeholder at the end */
	ft = (double) frame_idx - 1;
	fi = (100.0 * (double) (i-1)) / ft;
	fp = (100.0 * (double) p) / ft;
	fb = (100.0 * (double) b) / ft;

	fprintf( stdout, "\nFrame totals %d: "
		"I %d (%4.2f%%), P %d (%4.2f%%), B %d (%4.2f%%)\n\n",
		frame_idx-1, i-1, fi, p, fp, b, fb);

//	fprintf( stdout, "I-frame packets, high %d avg %d\n", h, s / i );
//	hist_hpn = h;
//	hist_hpn = s / (i-1);
}

/* output frame sequence file from previous input file */
static
void
output_frame_sequence ( void )
{
	int i, ok;
	char n[256], fs_name[ 256 ];
	FILE *f;
	struct in_frame_s in_frame;
	long long in_seq;

	WHOAMI;

	filebase( n, in_name, F_PFILE );
	memset( fs_name, 0, sizeof(fs_name) );
	snprintf( fs_name, sizeof(fs_name)-1, "%s.%sx", n, fxp );
	dprintf( stdout, "Writing %s\n", fs_name );
    
	f = fopen( fs_name, "wb" );

	for (i = 0; i < sequence_idx; i++) {
		in_seq = sequences[i];
		ok = fwrite( &in_seq, sizeof(in_seq), 1, f);
	}
	in_seq = -1LL;
	ok = fwrite( &in_seq, sizeof(in_seq), 1, f);

	for (i = 0; i <frame_idx; i++) {
		in_frame.pct = frames[i].pct;
		in_frame.vpn = frames[i].vpn;
		ok = fwrite( &in_frame, sizeof(struct in_frame_s), 1, f);
	}
	fflush( f );
	fclose( f );
}
/* Build .esx file for Elementary Stream
    specified with -y option (should be .es extension specifies it?) */
static
void
build_esx ( void )
{
	int bc, bd;
	unsigned int b, ss;
	unsigned char t, *p, c;
	long long o, r, s, z;
	frame_t *f;
	char n[256];

	WHOAMI;

/* allocate mpeg2 ES buffer */

	if (NULL == es_buf) {
		fprintf(stdout, "%s es_buf not allocated\n", WHO);
		c_exit(1);
	}

	memset(ipbx, 0, sizeof(ipbx));

	ss = 0;
	bc = 0;
	frame_idx = 0;
	sequence_idx = 0;
	s = 0;
	r = -1;
	z = in_size;
	z /= 100;
	if (0 == z) z = 1;
	o = 0;
//	o--;

/* reset at start of stream only */
	b = 0xFFFFFFFF;

/* MPEG ES stream has 00 00 01 for Start Code.
    Next byte is table type
	B3 Sequence
	00 Start of Picture (with picture type)
    Don't need to track it much further than that.
*/
	while (1) {
	    bc = read( in_file, es_buf, arg_mbz );
	    if (0 == bc) {
		    dprintf( stdout, "\n%s EOF %s\n", WHO, in_name);
		    break;
	    }
	    if (arg_mbz != bc) {
		dprintf( stdout, "\n%s EOF %s\n", WHO, in_name);
// no, let it finish the last bit of parsing */
//		break;
	    }

	    p = es_buf;
	    while (p < (es_buf + bc))
	    {
		o++;
	        b <<= 8;
	        b |= *p;
		p++;

/* MPEG Start Code? */
	        if (0x00000100 != (0xFFFFFF00 & b)) continue;
		c = 0xFF & b;

//	fprintf(stdout, "1%02X @ %9llX p %02X\n", c, o -4, (p - es_buf));

#if 0
/* bunch of branch prediction performance hits for no good reason */
/* reset start code test after each start code: NO */
//		b = 0xFFFFFFFF;

/* Skip MPEG 0xE0-0xEF PES header */
		if (MPEG_PES == (0xF0 & c)) continue;

/* Skip ATSC User Data header */
		if (MPEG_UPS == c) continue;

/* Skip MPEG2 Sequence or Picture Extensions */
		if (MPEG_EXT == c) continue;

/* Skip MPEG Slices */
		if ((c > 0x01) && (c < 0xB0)) continue;
#endif

/* MPEG Sequence Start? */
		if (MPEG_SEQ == c) {
			s = o / z;
			if (r != s) {
			    fprintf(stderr, "\r%s %2lld%% ", WHO, s);
			    fprintf(stderr,
				"Frames %06d SEQ %06d I %06d P %06d B %06d",
				frame_idx, sequence_idx,
				ipbx[1], ipbx[2], ipbx[3]);
			    r = s;
			}
//			fprintf( stdout, "1%02X @ %09llX SEQ #%d\n", c, o - 4, ss++ );
			sequences[sequence_idx++] = o - 4;

			snprintf(n, sizeof(n), "sequences[%d]", sequence_idx);
			sequences = irealloc(sequences, sizeof(long long), n);

			f = &frames[frame_idx];
			ipbx[1]++;
			f->pct = 1;
			f->vpn = o / TSZ;
			frame_idx++;

			snprintf(n, sizeof(n), "frames[%d]", frame_idx);
			frames = irealloc(frames, sizeof(frame_t), n);

			continue;
		}

/* MPEG Picture Start? */
		if (MPEG_PIC == c) {

/* Can't get picture_coding_type if start code is at end of packet,
	or at end of packet -1. End of packet -2 is ok.
*/
/* Need 2 bytes after Picture Start code for picture_coding_type */
		    if ( (p+1) >= (es_buf + arg_mbz) ) {

/* Which is why es_buf has + 2 bytes */
//			    fprintf(stdout,"2 byte adjust\n");
			    bd = read( in_file, es_buf + arg_mbz, 2 );
			    if (2 != bd) break;
			    o += 2;
		    }

/* P or B frame? */
		    t = 7 & (p[1]>>3);
		    if ((t > 1) && (t < 4)) {
			    f = &frames[frame_idx];
			    ipbx[t]++;
			    f->pct = t;
			    f->vpn = o / TSZ;
			    frame_idx++;

			    snprintf(n, sizeof(n), "frames[%d]", frame_idx);
			    frames = irealloc(frames, sizeof(frame_t), n);

		    } else {

/* Undefine PIC_DEBUG for B & P parsing errors. It won't affect cutting. */
#ifdef PIC_DEBUG
			    if (1 != t) {
				fprintf( stdout,
					"\nBad PIC type %d @ %9llX "
					"es_buf[0x%02X] sc %08X:\n",
					t, o, (int) (p - es_buf), b );
				dump_packet( o, es_buf, TSZ, p - es_buf);
			    }
#endif
		    }
		    continue;
		}

		/* add more start codes here if needed for private streams */
	    }
	}
	dprintf(stdout, "%s SEQ %d I %d P %d B %d\n", 
		WHO, sequence_idx, ipbx[1], ipbx[2], ipbx[3]);
}


/* Build .tsx file for Packetized Elementary Stream.
   This should be quite a bit faster than calling atscut -s.
   If it's at least as reliable, will enable it.
*/
static
void
build_tsx ( void )
{
	int i, ok, t;
	unsigned char *p, *r, *e, b;
	long long o, c, s, z;
	frame_t *f;
	char n[256];
	tsh_t *h;

	WHOAMI;

	if (NULL == ts_buf) {
		fprintf(stdout, "%s ts_buf not allocated\n", WHO);
		c_exit(1);
	}

	h = &tsh;

	memset( ipbx, 0, sizeof(ipbx));
	ok = 1;

	frame_idx = 0;
	sequence_idx = 0;

	c = o = s = 0;			/* % done status counters */
	z = in_size;			/* input size */
	z /= 100;			/* want % of as int */
	if (0 == z) z = 1;		/* avoid /0 */

	sc = 0xFFFFFFFF;

/* MPEG ES stream has 00 00 01 for Start Code.
    Next byte is table type
	B3 Sequence
	00 Start of Picture (with picture type)
    Don't need to track it much further than that.
*/
	start_timer( &timer_fsx );

/* reset this at start of main loop */
	o = -TSZ;

	while (ok > 0) {

	    ok = read( in_file, ts_buf, arg_mbz );
	    if (ok < 0) {
		fprintf( stdout, "\n%s ERROR %s\n", WHO, in_name);
		perror("");
		break;
	    }

	    if (0 == ok) {
		fprintf( stdout, "\n%s EOF %d %s\n", WHO, ok, in_name);
		break;
	    }


	  for (i = 0; i < ok; i += TSZ) {
/* current byte offset for this packet */
	    o += TSZ;
	    p = ts_buf + i;
	    r = p;

	    if (MPEG_SYN != *r) {

/* Any error in the read_buffer code not handling EOF properly? */
		if (o >= in_size) {
		    fprintf( stdout,
			     "\nread_buffer didn't see EOF @ %9llX!\n",
				in_size );
		    break;
		}
		fprintf( stdout, "\nTS Sync lost @ %9llX:\n", o);
		dump_packet( o, ts_buf, 0, 188 );

		return;
	    }

	    parse_ts_header( p, h, arg_vpid);
	    if (z < 0) return;
	    if (0 == z) continue;

/* FIXME: this shouldn't be needed but it is */
	    if (h->pid != arg_vpid) continue;

	    r = p + h->pso;

	    e = p + TSZ;

	    sc = 0xFFFFFFFF;
	    while (r < e) {
	        sc <<= 8;
		b = *r;
	        sc |= b;
		r++;

/* MPEG Start Code? */
	        if (0x00000100 != (0xFFFFFF00 & sc)) continue;
//		if (sc > 0x1FF) continue;

#if 0
/* Skip MPEG 0xE0-0xEF PES header */
		if (MPEG_PES == (0xF0 & b)) continue;

/* Skip ATSC User Data header */
		if (MPEG_UPS == b) continue;

/* Skip MPEG2 Sequence or Picture Extensions */
		if (MPEG_EXT == b) continue;

/* Skip MPEG Slices */
		if ((b > 1) && (b < 0xB0)) continue;
#endif

/* MPEG Sequence Start Code? */
		if (MPEG_SEQ == b) {

/* TODO: can check Sequence marker bits for extra validation. */
			s = o / z;
			if (c != s) {
			    fprintf(stderr, "\r%s %2lld%% ", WHO, s);
			    fprintf(stderr,
				"Frames %06d SEQ %06d I %06d P %06d B %06d",
				frame_idx, sequence_idx,
				ipbx[1], ipbx[2], ipbx[3]);
			    c = s;
			}
			sequences[sequence_idx++] = o;
			snprintf(n, sizeof(n), "sequences[%d]", sequence_idx);
			sequences = irealloc( sequences, sizeof(long long), n);

/* Can't find Intra frame start in 1st SEQ packet if SEQ data > 1 packet.
    This can happen if all the SEQ quantizer tables are specified.
    Better to assume all Sequences have an implied Intra frame?
*/
			f = &frames[frame_idx];
			ipbx[1]++;
		    	f->pct = 1;
			f->vpn = o / TSZ;
			frame_idx++;

			snprintf(n, sizeof(n), "frames[%d]", frame_idx);
			frames = irealloc( frames, sizeof(frame_t), n);

			continue;
		}

/* Picture Start Code? */
		if (MPEG_PIC == b) {

/* Can't get picture coding type if Picture Start Code at end of buffer.
   This should not happen when using a buffer size that is an even multiple
    of TSZ, but is known to occur with other buffer sizes.
*/
			if ( (r+1) < e ) {
			    t = 7 & (r[1]>>3);
/* P or B frame? */
			    if ((t > 1) && (t < 4)) {
				f = &frames[frame_idx];
				ipbx[t]++;

				f->pct = t;
				f->vpn = o / TSZ;
				frame_idx++;
				snprintf(n, sizeof(n), "frame[%d]", frame_idx);
				frames = irealloc( frames, sizeof(frame_t), n);

			    } else {

/* Undefine PIC_DEBUG for B & P parsing errors. It won't affect cutting. */
#ifdef PIC_DEBUG
				if (1 != t) {

    fprintf( stdout,
	"\n%s TEI%d PRI%d PSI%d PID %04X TSC%d AFC%d afb %3d ts[%4d)\n",
	 WHO, h->tei, h->pri, h->psi, h->pid,
	 h->tsc, h->afc, h->afb, r - p );

    fprintf( stdout, "Bad PIC type %d @ %9llX ts_buf[0x%02X] sc %08X:\n",
	    t, o, (int) (r - p), b );

    dump_packet( o, p, TSZ, r - p - 4);
				}
#endif
			    }
			} else {
			    fprintf(stdout,"\nPIC type not in PSI1 packet!\n");
			}
			continue;
		}
	    }
	  } /* for i loop */
	}
	dprintf(stdout, "%s SEQ %d I %d P %d B %d\n", 
		WHO, sequence_idx, ipbx[1], ipbx[2], ipbx[3]);
}

/* catch all for building .tsx or .esx files */
static
void
build_fsx ( void )
{
	double et, rate;

	WHOAMI;

	start_timer( &timer_fsx );
/* use atscut to create the .tsx file */
	if (0 == arg_es) {
	    build_tsx();

	    if (sequence_idx != ipbx[1]) {
		char s[512] = "";
		fprintf( stdout, "Sequences %d do not match Iframes %d\n",
		    sequence_idx, ipbx[1]);
		fprintf( stdout, "Trying again with \042atscut -s %s\042\n",
			in_name);
		fprintf( stdout, "system( atscut -s %s )\n", in_name);
		snprintf( s, sizeof(s), "atscut -s %s\n", in_name);
		system( s );
		return;
	    }
	} else {
/* create the .esx file with internal function */
		build_esx();
	}
	stop_timer( &timer_fsx );
	diff_timer( &timer_fsx );
	et = 1.0 * (double) timer_fsx.diff.tv_sec;
	et += .000000001 * (double) timer_fsx.diff.tv_nsec;
	rate = (double) in_size;
	rate /= et;
	rate /= 1000000.0;

#if 0
	fprintf( stdout,
		 "\n%s %s SEQ %d Frames %d in %.2f s @ %.2f MB/s\n",
		 WHO, in_name, sequence_idx, frame_idx, et, rate );
#endif

	fprintf( stdout, "\nCreated %sx in %.2f seconds @ %.2f MB/s\n",
		 in_name, et, rate);

	output_frame_sequence();

	fprintf(stdout, "%s:%d ", WHO, WHERE);
	show_allocs();
/* free frames and sequences so input frames can rebuild it? */
}

/* load .tsx file. append one fake intra frame and sequence to the end */
static
void
input_frame_sequence ( void )
{
	struct in_frame_s in_frame;
	long long in_seq;
	char fb[256];
	char m[256];
	char n[256];
	FILE *sf;
	int ok, i, li, fi, lf;
	frame_t *f;
	frame_t *f1;

	WHOAMI;

	filebase( fb, in_name, F_PFILE );

	snprintf( m, sizeof(m), "%s.%sx", fb, fxp );

	sequence_idx = 0;
	frame_idx = 0;
	iframe_idx = 0;

	fprintf( stdout, "Opening frame sequence %s\n", m );
	sf = fopen( m, "rb" );
	if (NULL == sf) {

	    fprintf( stdout,
		     "\nCreating frame sequence file %s, please wait\n", m);
	    build_fsx();

	    dprintf( stdout, "Opening new frame sequence %s\n", m );
	    sf = fopen( m, "rb" );
	    if (NULL == sf) {

		fprintf( stdout, "FATAL: File %s not created\n", m );
		c_exit(1);
	    }
	    dprintf( stdout, "Opened %s\n", m );
	}

/* reset the list made by build_fsx, tests what build_fsx wrote */
	sequence_idx = 0;
	frame_idx = 0;
	iframe_idx = 0;

/* start over again with a new list */
	free_frames();

/* should set all pointers to NULL */
	alloc_frames();

	dprintf( stdout, "Opened %s, seq %d, frame %d, iframe %d\n",
		m, sequence_idx, frame_idx, iframe_idx);

	memset(ipbx, 0, sizeof(ipbx));

/* input sequences */
	while (1) {
		ok = fread( &in_seq, sizeof(in_seq), 1, sf);
		if (1 != ok) {
		    fprintf( stdout, "%s EOF %d @ Sequence %d\n",
			    WHO, ok, sequence_idx);
		    break;
		}
	    	dprintf( stdout, "Sequence %5d @ %llX\n", sequence_idx, in_seq);

		if (-1LL == in_seq) {
			dprintf( stdout, "%s End of Sequences @ %d\n",
				    WHO, sequence_idx);
			break;
		}

		sequences[ sequence_idx ] = in_seq;
	    	dprintf( stdout, "Sequence %5d @ %llX\n", sequence_idx, in_seq);
		sequence_idx++;
		snprintf( n, sizeof(n), "sequences[%d]", sequence_idx );

		sequences = irealloc( sequences, sizeof(long long), n );
		if (NULL == sequences) {
			fprintf( stdout,
				"\n FATAL: can't realloc for sequence %d\n",
				sequence_idx);

			fclose( sf );
			c_exit(1);
		}
	}

/* don't bother with frames if no sequences */
	if (0 == sequence_idx) {
		fprintf( stdout, "\nFATAL: no Sequences in %s\n", m);
		fclose( sf );
		c_exit(1);
	}

/* no found iframe */
	fi = 0;

/* input frames */
	if (sequence_idx > 0)
	    while (1) {
		ok = fread( &in_frame, sizeof(in_frame), 1, sf);
		dprintf( stdout, "fread frame[%d] PIC%d  vpn %5d\n",
			    frame_idx, in_frame.pct, in_frame.vpn );

/* end of file */
		if (0 == ok) {
		    dprintf( stdout, "%s End of Frames    @ %d\n",
			    WHO, frame_idx);
		    break;
		}

/* skip initial partial frames up to first Sequence */
		if ((0 == fi) && (1 != in_frame.pct)) continue;

		fi = ~0;

		if (in_frame.pct > 0) ipbx[in_frame.pct]++;

		f = &frames[frame_idx];

		f->pct = in_frame.pct;
		f->vpn = in_frame.vpn;

/* is it an Intra frame? */
		if (1 == in_frame.pct) {
		    iframes[ iframe_idx ] = frame_idx;
		    dprintf( stdout, "iframes[%d] = %d\n",
		    		 iframe_idx, frame_idx );
		    iframe_idx++;
		    snprintf( n, sizeof(n), "iframes[%d]", iframe_idx );

		    iframes = irealloc( iframes, sizeof(int), n );
		    if (NULL == frames) {
			fprintf( stdout,
				    "FATAL: can't realloc for iframe %d\n",
				    iframe_idx);
			fclose( sf );
			c_exit(1);
		    }
		}

		frame_idx++;

		snprintf( n, sizeof(n), "frames[%d]", frame_idx );

		frames = irealloc( frames, sizeof(frame_t), n );
		if (NULL == frames) {
		    fprintf( stdout,"FATAL: can't realloc for frame %d\n",
			    frame_idx);
		    fclose( sf );
		    c_exit(1);
		}
	}

	fclose( sf );

	if ((0 == fi) || (0 == iframe_idx)) {
		fprintf( stdout, "\nFATAL: no Intra frames in %s\n", m);
		c_exit(1);
	}

/* -k option will append a fake Sequence after the last real Sequence,
    otherwise default to truncating at (i.e. removing) the last Sequence.

	This last Sequence is always a partial Sequence from capture. because
    no capture programs currently in use employ any method to stop the capture
    at the end of the Sequence (or start of next Sequence). Even atscap stops
    the capture 'somewhere' in the last Sequence. This partial Sequence can
    crash the libmpeg2 decoder for a small percentage of captures.

	The situation is different for files made with Xtscut. They should
    all manage to have a complete last Sequence to avoid crashing libmpeg2.

	This may be related to broken_link AND closed GOP flags needing to
    be set on the last Sequence, but the MPEG spec is a bit obtuse about it.

	If broken link isn't set on the last Sequence, the decoder will try
    to parse the two frames sequentially after the Intra frame, but which
    are rendered before the Intra frame by the Picture temporal field.

	If closed GOP is set on the last Sequence, that should tell the
    decoder to not check the NEXT Sequence for the last two frames of the
    CURRENT Sequence. This should be irrelevant becase B and P's are not
    being displayed, but does libmpeg2 decode the P & B frames anyway?

	Also, if the last Sequence + Intra frame is short to the point that
    the Intra frame itself is cut off by EOF, it might crash libmpeg2.
    Right now it says 'EOF reached', but it may yet still crash.

	None of this will address libmpeg2 crashing in the middle of the
    stream, that seems caused by missing data from reception drop-outs.
    Statistically speaking, that is a special cause that can be ignored.
*/

/* See how many frames are after the last Intra frame. If there are 
   at least 2 frames (IBB), it might not crash libmpeg2.
*/

	iprintf( stdout, "Found %d sequences, %d frames\n",
		sequence_idx, frame_idx );

	li = 0;
	if (0 == arg_kls) {
	    fi = frame_idx;
	    li = fi - iframes[ iframe_idx - 1 ];
	    lf = frame_idx - li;

	    iprintf(stdout, "frames %d li %d lf %d\n", fi, li, lf );

/* show last Sequence */
	    for (i = lf; i < frame_idx; i++ ) {
		f = &frames[i];
		f1 = &frames[i + 1];
		iprintf(stdout, "frames[%d] %c, packets %d\n",
			i, pictypes[f->pct], f1->vpn - f->vpn);
	    }

/* default is truncate last partial Sequence if not enough frames */
	    if (li < 4) {
		for (i = lf; i < frame_idx; i++) {
		    f = &frames[i];
		    iprintf( stdout, "Truncating %c frame # %d \n",
				    pictypes[3 & f->pct], i);
		    f->pct = 1;
//		    f->num = iframe_idx - 1;
//		    f->vpn = 0;
		}
		iprintf( stdout, "Truncate: SEQ[%d] = %09llX\n",
			    sequence_idx-1, sequences[sequence_idx-1]);

/* non-reversible change, rest are only counter decrements */
		sequences[ sequence_idx ] = in_size;
//		iframes[ iframe_idx ] = 0;

		frame_idx -= li;
		iframe_idx--;
		sequence_idx--;
	    } else {
		li = 0;
	    }
	}

	f = &frames[frame_idx];

/* non reversible if it truncates then appends */
	f->pct = 1;				/* append a fake frame */

/* possible type conversion issue? */
	f->vpn = in_size / TSZ;

/* TESTME: is this going to write outside the boundary? */
	f = &frames[frame_idx + 1];
	f->vpn = in_size / TSZ;

	iframes[ iframe_idx ] = frame_idx;	/* append a fake iframe */
	sequences[ sequence_idx ] = in_size;	/* append a fake sequence */
	frame_idx++;
	iframe_idx++;
	sequence_idx++;

	iprintf(stdout, "FAKE LAST SEQ[%d] @ EOF %09llX\n",
			sequence_idx - 1, in_size);

	fprintf( stdout, "Found %d Pictures in %d Sequences",
		    frame_idx - 1, sequence_idx - 1);
	if (li > 0)
	    fprintf( stdout, " (%d frames cut)", li);

	fprintf( stdout, "\n");

#if 0
	for (i = frame_idx - 6; i < frame_idx; i++ ) {
	    f = &frames[i];
	    f1 = &frames[i + 1];
	    dprintf(stdout, "frames[%d] %c, packets %d\n",
			i, pictypes[f->pct], f1->vpn - f->vpn);

	}
	dprintf(stdout, "\n");

	for (i = sequence_idx - 4; i < sequence_idx; i++ )
		dprintf(stdout, "SEQNC[%d] %09llX\n", i, sequences[i]);
	dprintf(stdout, "\n");

	for (i = iframe_idx - 4; i < iframe_idx; i++ )
		dprintf(stdout, "INTRA[%d] %d\n", i, iframes[i]);
	dprintf(stdout, "\n\n");
#endif

#if 0
	for (i = 0; i < frame_idx;  i++ ) {
	    f = &frames[i];
	    f1 = &frames[i + 1];
	    fprintf(stdout, "frames[%d] %c, packets %d\n",
			i, pictypes[f->pct], f1->vpn - f->vpn);
	}
#endif

}

/* read cut file into scuts, after input_frame_sequence so iframes[] loaded */
static
void
input_cuts ( void )
{
	FILE *f;
	int ok;
	char n[256], sc_name[256];

	WHOAMI;

	filebase( n, in_name, F_PFILE);
	snprintf( sc_name, sizeof(in_name), "%s.%sc", n, fxp);

	scut_idx = 0;
	clear_scuts();

	f = fopen( sc_name, "r");
	if (NULL == f) {
		fprintf( stdout, "%s %s not found\n", WHO, sc_name);
		return;
	}	    
    
	while(1) {
		ok = fread( &scuts[scut_idx], sizeof(int), 1, f);
		if (0 == ok) break;

// FIXME: put this back, output redux for cl-usage, disabled for debug only
		if (0 == arg_cut) 
			dprintf( stdout,
			    "Cut %02d @ SEQ %5d I-Fframe[%5d] = %6d "
			    "Picture Type %c-Frame (%d)\n",
			    scut_idx + 1,
			    scuts[scut_idx],
			    scuts[scut_idx],
			    iframes[scuts[scut_idx]],
			    pictypes[frames[iframes[scuts[scut_idx]]].pct],
			    frames[iframes[scuts[scut_idx]]].pct
			);

		frames[ iframes[ scuts[ scut_idx ]]].cut = ~0;
		scut_idx++;
	}
    
	fclose(f);

	fprintf( stdout, "%s file %s has %d cuts\n", WHO, sc_name, scut_idx);
}


/* scan input for PIDs, for 5000 packets. PID with highest packet count wins */
/* sets arg_vpid to the PID that had the highest packet count, assumes video */
/* TODO: add "00 00 01 Ex" test for video header at payload start */
static
void
input_scan ( void )
{
	unsigned char buf[ TSZ ], *p, *r;
	unsigned short pids[ 0x2000 ];
	int f, ok, z;
	struct stat64 fs;
	long long i, c;
	tsh_t *h;

	WHOAMI;

	h = &tsh;

/* input scan is not needed when source is Video Elementary Stream */
	if (0 != arg_es) return;

	p = buf;
	pmt_pid = -1;
	pmt_pkts = 1;

	memset( pids, 0, sizeof( pids ));
	memset( pat, 0, sizeof(pat));
	memset( pmt, 0, sizeof(pmt));

	ok = stat64( in_name, &fs );
	if (0 != ok) {
		fprintf( stdout, "%s stat %d input %s\n", WHO, ok, in_name );
		c_exit(1);
	}
	in_size = fs.st_size;
	c = in_size / TSZ;

/* about 1s of stream at 80 megabits/second */
	if (c > 50000LL) c = 50000LL;
	iprintf( stdout, "\n%s will read %lld packets\n", WHO, c);

/* open file */
	f = open( in_name, FILE_RMODE );
	if (f < 2) {
	    fprintf( stdout, "%s failed open %s\n", WHO, in_name);
	    perror("");
	    c_exit(1);
	}

	for (i = 0; i < c; i++) {
	    ok = read( f, &buf, TSZ );
	    if (0 == ok) break;

	    if (TSZ != ok) {
		fprintf( stdout, "%s failed %d read %s\n", WHO, ok, in_name);
/* nothing to do, time to exit */
//		close(f);
//		c_exit(1);
		break;
	    }

/* arg_vpid has not yet been determind so use 0 for pid parameter */
	    z = parse_ts_header( p, h, 0 );

	    if (z < 0) {
		fprintf(stdout, "%s Transport Stream Sync lost.\n", WHO);
		c_exit(1);
	    }
	    pids[ h->pid ]++;
	    if (0 != h->tei) continue;
	    if (0 != h->tsc) continue;
	    if (2 == h->afc) continue;

/* look for table type 0 PAT and table type 2 PMT,
	as well as video and audio stream start codes
*/

/* nulls wont count against the total loops */
	    if (MPEG_NULL == h->pid) {
		    i--;
		    continue;
	    }

	    r = p + h->pso;

	    if ((MPEG_SYN == *pat) && (MPEG_SYN == *pmt)) {
		if (0 != h->psi) {
		    if ( (0 == r[0]) && (0 == r[1] && 1 == r[2]) ) {
			if ( A52_PS1 == r[3] ) arg_apid = h->pid;
			if ( MPEG_PES == (0xF0 & r[3]) ) arg_vpid = h->pid;

/* If PAT + PMT + VID + AUD found, can exit the loop early, but
    it might break the two packet PMT copy. Disabled for now. */
//			if ((0 != arg_apid) && (0 != arg_vpid)) break;
		    }
		}
	    }
/* Only copy PAT if it hasn't yet been copied */
	    if (MPEG_SYN != pat[0]) {
		if (0 == h->pid) { 		/* always PID 0000 */
		    dprintf(stdout, "Copying PAT PID %04X\n", h->pid);
		    memcpy( pat, p, TSZ );
		}
	    }

/* Set PMT PID if payload start and table type at r[1] is 2 for PMT type
   AFC should never be in the PAT or PMT packets.
*/
	    if (0 != h->psi) {
		if (MPEG_PMT == p[5]) { // r[1]
			if (-1 == pmt_pid)
			    dprintf(stdout, "Found PMT PID %04X\n", h->pid);
			pmt_pid = h->pid;
		}
	    }

/* Handle KQED two packet PMTs if atscap didn't define USE_MPEG_REBUILD */
	    if (pmt_pid == h->pid) {
		if (MPEG_SYN != *pmt) {
		    dprintf(stdout, "Copying PMT PID %04X\n", h->pid);
		    if (0 != h->psi) memcpy( pmt, p, TSZ );
		    if (MPEG_SYN == *pmt) {
			if (0 == h->psi) {
			    memcpy( pmt + TSZ, p, TSZ );
			    pmt_pkts = 2;
			}
		    }
		}
	    }
	}

        close(f);

	dprintf( stdout, "%s stopped at %lld iterations\n", WHO, i);

/* debug: list the found packets */
	for (i = 0; i < 0x2000; i++) {
		if (0 == pids[i]) continue;
		dprintf( stdout, "TS PID %04X has packets %lld\n", pids[i], i);
	}

/* Missing PAT + PMT is considered fatal because it's a malformed TS. */
	if (MPEG_SYN != *pat) {
		fprintf( stdout, "FATAL: TS Program Association PID was not found.\n");
		c_exit(1);
	} else {
		fprintf( stdout, "TS PAT PID is %04X\n", 0);
	}
	
	if (MPEG_SYN != *pmt) {
		fprintf( stdout, "FATAL: TS Program Map PID was not found.\n");
		c_exit(1);
	} else {
		fprintf( stdout, "TS PMT PID is %04X %d packet%s\n",
		    pmt_pid, pmt_pkts, (1==pmt_pkts)?"":"s");
	}

/* Missing Video PID is considered fatal. Duh. */
	if (0 == arg_vpid) {
		fprintf( stdout, "FATAL: TS Video PID was not found.\n");
		c_exit(1);
	}
	fprintf( stdout, "TS VID PID is %04X\n", arg_vpid);
	fprintf( stdout, "TS AUD PID is %04X\n\n", arg_apid);

	return;
}

static
void
input_files ( void )
{
	WHOAMI;

	input_scan();
	input_frame_sequence();
	process_frames();
	input_cuts();
}


/* wait for x key events to either zoom/move viewport or quit program */
static
void
x_scan_loop ( void )
{
	if (0 != arg_xmsg) WHOAMI;

/* only redraws if xredraw is not zero */
	while(1) {
		x_event();
		x_redraw_display();
#ifdef USE_CONSOLE
/* is broken, eh wot? same code works fine in atscap. */
/* this checks for sig_kill to try to gracefully exit via c_exit() */
		console_scan( &ckey );
		if (0 != ckey)
			fprintf( stdout, "Console key 0x%02X\n", ckey);
#endif

/* reduce CPU usage during the loop */
		nanosleep( &event_loop_sleep, NULL );
	}
}


/* Put up a video window and step through Intra frames as fast as possible.
    libmpeg2 bug? int math used because -a6 makes all FPU ops return nan.
    It's OK with -a7. MMX disabled when in MMXEXT + 3DNOW only mode?
    It renders a little bit slower with -a7 for some reason.
*/
static
void
benchmark_framerate ( void )
{
	int c, f, g, h, i, j;

	WHOAMI;

	c = sequence_idx - 2;
	if (c >= arg_bfr) c = arg_bfr;

	xumove = 0;
	xoffset = 0;
	xlfd = -1;

	start_timer( &timer_bif );

	for (i = 0; i < c; i++) {
	    parse_intra_frame( sequences[i], WHO );
	    x_draw_image( WHO );
	}

	stop_timer( &timer_bif );
	diff_timer( &timer_bif );

	f = timer_bif.diff.tv_sec;
	g = timer_bif.diff.tv_nsec;

	g /= 10000000;

	h = (100 * f) + g;

	i = (10000 * c) / h;
	j = i % 100;

	i /= 100;

	fprintf(stdout,
		"   %d Intra frames in %d.%02d seconds at %d.%02d FPS\n\n",
		c, f, g, i, j );
    	c_exit(1);
}


static
void
usage ( char ** argv )
{
	WHOAMI;

	fprintf( stdout,
"\n"
"USAGE:\n"
"\t%s [options] file.{ts|tsx}\n"
"\n"
"OPTIONS:\n"
"\n"
"	-h		Help for %s %s %s\n"
"	-v		Show xtscut banner and exit\n"
"\n"
"	-1		Cut to multiple files, -r -cX modifies this\n"
"	-d		Delays the writes to reduce loading on one drive\n"
#ifdef USE_GOP_BROKEN_LINK
"	-g		Sets broken_link flag on 1st GOP in each cut.\n"
#endif
"	-n		Write NULLs at start of cuts (default: strip NULLs)\n"
"	-r		Renumber -1 odd/even cuts to 01 02 03 ... 99.ts\n"
"\n"
"	-f		Function name verbosity (DEBUG)\n"
"	-i		Intra frame processing verbosity (DEBUG)\n"
"	-x		X function verbosity (DEBUG)\n"
"\n"
"	-a FPUbits	Override default MPEG acceleration (see mpeg2.h)\n"
"	-b n		Benchmark frame render rate for n Intra-frames\n"
"	-c e|o|a	If .tsc file exists, cut odd, even or all and exit\n"
"	-k n		Keep Last Sequence Cruft is 1, Test Cruft is 0\n"
#ifdef USE_LIBVO
"	-m libvomode	X video mode, 0 XShm RGB, 1 Xv YV12, 2 Xv YUYV\n"
#endif
"	-o name		Basename. Appends cuts as .00.ts ... .99.ts\n"
"	-p path		Output path for extracted content\n"
"	-s float	Histogram scaling factor, floating point\n"
"	-w [1,2,4,8]	Set video window to 1/divisor of source frame size\n"
"\n", argv[0], NAME, VERSION, LASTEDIT);

    show_keys(1);

    fprintf(stdout,
"NOTES:\n"
"\n"
"	Run this from an xterm if you wish to see the verbose detail.\n"
"	Eventually after you're expert with it, you won't need it.\n"
"	TODO: The console output still needs a -q option to squelch it.\n"
"	WIP: The console output is slowly being replaced with X output.\n"
"\n"
"	The time-codes at the top are rough estimates based on frame number.\n"
"	3:2 pulldown will cause the time-codes to be wrong. This is OK.\n"
"	They aren't used for anything, so correctness doesn't matter.\n"
"\n"
"	Image is Intra frame nearest center at start and stream reposition.\n"
"	Use RMB for preview image to place a cut then LMB to place the cut.\n"
"\n"
"	When you select an Intra frame with LMB, it changes the line color\n"
"	to red and puts a small number at the bottom of the red line. This\n"
"	is the cut number for the cut to the LEFT of the line.\n"
"\n"
"	MMB can be used to jump quickly to any point in the stream.\n"
"	It acts as an invisible slider that is as wide as the video window,\n"
"	i.e. MMB in the middle puts you near 50%% of the stream, etc.\n"
"\n"
"	Quick Example: Select start of cut and end of cut then hit [e]\n"
"	to write the file from the even numbered cut. Use [o] to write\n"
"	odd numbered cuts if stream starts without any lead-in cruft.\n"
"	Write even numbered cuts if there is a bit of lead-in cruft.\n"
"\n"
"	Default is to make one file. Use option -1 to make separate files.\n"
"\n"
"	Deselect the frame types with LMB from IPB legend at the bottom to\n"
"	see full image. If window is 960x544, it renders without artifacts.\n"
"	Use -w1 for SD material to keep aspect ratio correct.\n"
"\n"
"	atscap -m option writes the .tsx sequence/frame index files.\n"
"	atscut -s option also writes a .tsx file if atscap did not.\n"
"	xtscut called without an existing .tsx file calls atscut -s.\n"
"\n"
"	Try the new-style mouse scroll-wheel for navigation!\n"
"\n"
"	If you cut to a different physical volume, it usually writes faster.\n"
"\n"
"SEE ALSO:  xtscut(1)\n"
"\n"
"\n");

	exit(0);
}


static
void
parse_args ( int argc, char ** argv )
{
    int c, i, ok, mbz;
    char *p, n[256], s[256];	/* temps for -o and -p and source */
    struct stat64 fs;

    WHOAMI;

    mbz = 4096;

    if (1 == argc) {
	usage( argv );
	fprintf( stdout, "Need Video .ts or .es file to process\n\n");
	exit( 1 );
    }

    memset( n, 0, sizeof(n));
    memset( arg_base, 0, sizeof(arg_base));

    i = 0;
#ifdef DEBUG
    fprintf( stdout, "args:\n");
    for (i = 0; i < argc; i++)
	fprintf( stdout, "arg[%d] %s\n", i, argv[i]);
#endif

    while ( EOF != ( c = getopt( argc, argv,

		"1dfhinrxv"

#ifdef USE_GOP_BROKEN_LINK
		"g"
#endif
		"a:b:c:k:j:o:p:s:u:w:z:"

#ifdef USE_LIBVO
		"m:"
#endif
		) ) )
    {
	switch(c) {


/* alpha-blending color, may require imlib to use it */
	case 'u':
	    arg_alpha = 0xFF & atoi(optarg);
	    break;

/* jump to this frame number */
	case 'j':
	    if (NULL != optarg) arg_jump = atoi(optarg);
	    break;

/* -z 4096 sets input, output and mpeg es buffers to this size */
	case 'z':
	    if (NULL != optarg) mbz = atoi(optarg);
	    break;

#ifdef USE_LIBVO
	case 'm':
	    if (NULL != optarg) arg_vo = 3 & atoi(optarg);
	    break;
#endif

/* toggle: nz sets GOP broken link flag on the first GOP in each cut */
	case 'g':
#ifndef USE_GOP_BROKEN_LINK
	    fprintf( stdout, "Recompile with USE_GOP_BROKEN_LINK defined.\n");
	    exit(1);
#else
	    arg_gop = 1 & ~arg_gop;
#endif
	    break;

/* benchmark frame render, 999 I frames max */
	case 'b':
	    arg_bfr = 0;
	    if (NULL != optarg)
		arg_bfr = atoi(optarg);
	    if (arg_bfr < 0) arg_bfr = 0;
	    if (0 == arg_bfr) {
		fprintf(stdout,
		     "Please specify how many frames to benchmark.\n\n");
		exit(1);
	    }
	    break;

/* toggle keep last sequence */
	case 'k':
	    arg_kls = 1 & atoi(optarg);
	    break;

/* MPEG2 acceleration model override */
	case 'a':
	    if (NULL != optarg)
		arg_mx = 7 & atoi(optarg);
	    break;

/* toggle 1s NULL packet write to start of every cut */
	case 'n':
	    arg_nulls = 1 & ~arg_nulls;
	    fprintf( stdout, "Writing NULL packets: %s\n",
		    (0 != arg_nulls)?"YES":"No");
	    break;

/* toggle compiled delay */
	case 'd':
	    arg_delay = 1 & ~arg_delay;
	    break;	    

/* toggle compiled -r cut re-numbering default */
	case 'r':
	    arg_renum = 1 & ~arg_renum;
	    break;

/* specifiy cut type, all is default if no parm match */
	case 'c':
	    cut_type = CUT_ALL;
	    arg_cut = ~0;
	    if (NULL != optarg) {
/* only first letter is checked */
		p = optarg;
		if ('e' == *p) cut_type = CUT_EVEN;
		if ('o' == *p) cut_type = CUT_ODD;
		if ('a' == *p) cut_type = CUT_ALL;
	    }
	    break;

/* even/odd cut to one file toggle */
	case '1':
	    arg_one  = 1 & ~arg_one;
	    break;

/* function name debug verbosity */
	case 'f':
	    arg_fmsg = ~0;
	    break;

/* Intra frame debug verbosity */
	case 'i':
	    arg_imsg = ~0;
	    break;

/* X debug verbosity */
	case 'x':
	    arg_xmsg = ~0;
	    break;

/* help on usage */
	case 'h':
	    usage( argv );
	    break;

/* window divisor, 1 2 4 or 8. even divisor handles interlaced w/o banding */
	case 'w':
	    if (NULL != optarg) {
		arg_wd = atoi(optarg);
		if (0 == arg_wd) {
		    vw.w = 960;		/* override config file */
		    vw.h = 544;
		    break;
		}

		if ( (1 != arg_wd) && (2 != arg_wd)
		  && (4 != arg_wd) && (8 != arg_wd) ) {
		    fprintf(stdout,
			    "Invalid window divisor: use 1, 2, 4 or 8\n\n");
		    c_exit(1);
		}
	    }
	    break;

/* show program banner (already done) and exit */
	case 'v':
	    fprintf( stdout, "\n");
	    exit(0);
	    break;

/* histogram scale % of screen height */
	case 's':
	    if (NULL != optarg) hist_scale = atof(optarg);
	    break;

/* output name */
	case 'o':
	    if (NULL != optarg)
		 snprintf( arg_base, sizeof(arg_base),"%s", optarg);
	    break;

/* output path */
	case 'p':
	    if (NULL != optarg)
		 snprintf(arg_path, sizeof(arg_path),"%s", optarg);
	    break;

	default:
	    break;

	}
    }

/* too small or too large values here will not make much sense */

/* normalize the filename to .ts file and set path from it, if -p didn't */
    if (NULL == argv[optind] ) {
	fprintf(stdout, "Specify a source stream file (*.es or *.ts)\n\n");
	exit(0);
    }

    strncpy( s, argv[optind], sizeof(s) );
    s[255] = 0;


/* see if input is .es file */

    p = strrchr(s, '.');
    if (NULL != p) {
	p++;
	if ('e' == *p) arg_es = ~0;
    }

/* FIXME: buffer code is broken under 1880 bytes? */
//#define SMALL_BUFZ (1 * TSZ)
#define SMALL_BUFZ (21 * TSZ)
#define LARGE_BUFZ (1024 * TSZ)
    if (mbz < SMALL_BUFZ) mbz = SMALL_BUFZ;
    if (mbz > LARGE_BUFZ) mbz = LARGE_BUFZ;

/* TS read alignment by shrinking mbz slightly to multiple of packet size.
   ES doesn't need any buffer alignment.
*/
    if (0 == arg_es) { mbz /= TSZ; mbz *= TSZ; }

    arg_mbz = mbz;
    
    if (0 == arg_es) {
	fprintf(stdout, "TS Buffers are using %d bytes %d packets\n",
		mbz, mbz / TSZ);
    } else {
	fprintf(stdout, "ES Buffers are using %d bytes\n", mbz);
    }

    if (CUT_ALL == arg_cut) arg_one = 0;
    fxp = fxt[0];
    if (0 != arg_es) fxp = fxt[1];

    filebase( n, argv[optind], F_PFILE);
    snprintf( in_name, sizeof(in_name), "%s.%s", n, fxp );
    if (0 == *arg_path)
    filebase( arg_path, argv[optind], F_PATH);

/* if arg path non blank, make sure it has / at end of string */
    p = arg_path;
    if (0 != *p) {
	p += strlen(arg_path);
	p--;

/* don't overrun end of string */
	if (strlen(arg_path) < (sizeof(arg_path)-2) ) {
	    if ('/' != *p) {
		p++;
		*p ='/';
		p++;
		*p = 0;
	    }
	} else {
	    fprintf( stdout, "-p option too large, max is %d chars\n",
			sizeof(arg_path)-2);
	    exit(1); 
	}
    }

/* user specified -o basename option overrides auto basename from input */
/* out base is temp file name constructed from source arg base */
    filebase( n, argv[optind], F_TFILE);

/* -o specified, use parameter without changing it */
    p = arg_base;
    if (0 == *p) {
/* automatic will strip everything after first . including the . */
	strncpy(arg_base, n, sizeof(arg_base));
	    p = strchr(arg_base, '.');
	    if (NULL != p) *p = 0;
    }
    snprintf(out_base, sizeof(out_base)-1, "%s", arg_base);

    if (0 != *in_name)
	fprintf( stdout, "Input stream file name: %s\n", in_name);
    if (0 != *out_base)
	fprintf( stdout, "Output path & basename: %s%s\n",arg_path, out_base);

    if (0 != *in_name) {
	ok = stat64( in_name, &fs );
	if (0 != ok) {
		fprintf( stdout, "Could not stat %d input file %s\n",
				ok, in_name );
		perror("");
		c_exit(1);
	}
	in_size = fs.st_size;
	in_inode = fs.st_ino;
	
/* have permission to read the dirent, does it have permission to read file? */

	in_file = open( in_name, FILE_RMODE );
	if (in_file < 3) {
		fprintf( stdout, "Could not open input file %s\n", in_name );
		exit (1);
	}
	fprintf(stdout, "Opened %s, inode %d bufz %d\n",
		    in_name, (int)in_inode, arg_mbz);
    }
#ifdef USE_LIBVO
//    arg_vo = 1; /* 0 and 2 are fixed? [temporarily out of service] */
#endif
}


static
void
show_boilerplate ( void )
{
	WHOAMI;

	fprintf(stdout,
		"\n" NAME" "VERSION "-" LASTEDIT " "COPYRIGHT" "EMAIL"\n"

		"Released under the "LICENSE"\n"

		"This software is supplied AS-IS, with NO WARRANTY.\n"

		"Compiled on " __DATE__ " at " __TIME__

		" for " PLATFORM_NAME "\n\n"
	);
}


/* input files, set up display and wait for interactive user input */
int
main( int argc, char ** argv )
{
	WHOAMI;

	arg_v = argv;
	arg_c = argc;

/* Peter Knaggs found this work-around for Compiz/Beryl on Ubuntu,
   but he followed up later to say Compiz is too buggy to be useful.
*/
	setenv( "XLIB_SKIP_ARGB_VISUALS", "1", 1);

	init_allocs();

	test_clock_res();

	show_boilerplate();

	load_config();

	parse_args( argc, argv );

	save_config();

//	show_config();

#ifdef USE_SIGNALS
	signal_init();
#endif
	alloc_frames();

	alloc_streams();

	clear_arrays();

	input_files();

	build_scut_frames();

/* will read first SEQ + I-Frame */
	init_mpeg2();

/* make it draw the first iframe again, if needed */
	xlfd = -1;

/* move this one around to inspect the allocations as you go */
//	show_allocs();

/* run the gui if no -c option given */
	if (0 == arg_cut) {
		if (0 != arg_bfr) {
		    fprintf( stdout, "Benchmarking %d Intra frames in %s\n",
			    arg_bfr, in_name);
		} else {
		    fprintf( stdout, "Previewing Intra frames in %s\n\n",
			    in_name);
		}
		
		x_init_display();		/* set up X display */

#ifndef USE_LIBVO
		x_init_imlib();			/* load splash image */
#endif
		if (0 == arg_bfr) {
			x_draw_image( WHO );
		} else {
			benchmark_framerate();	/* never returns */
		}
		x_scan_loop();			/* never returns */
	}

/* -c option falls through to here. do the cuts and exit. no gui */

	write_scuts();    
	free_frames();

	return 0;
}


